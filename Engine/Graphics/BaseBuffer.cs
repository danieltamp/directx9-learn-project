﻿#region LGPL License
/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code
contained within this library is a derivative of the open source Object Oriented
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.
Many thanks to the OGRE team for maintaining such a high quality project.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Graphics
{
    public abstract class BaseBuffer : IDisposable, IRestore
    {
        bool isLocked;
        bool managed;
        bool readable;
        bool dynamic;
        protected int m_size; //size in bytes of whole buffer
        protected int m_count; // num of elements(m_type) you are using 
        protected int m_type_size; // size in bytes of one buffer's element
        protected Type m_type; //tipe of buffer's elements
        protected Pool m_memory;
        protected Usage m_usage;
        protected GraphicDevice m_device;
        protected bool disposed;

        IntPtr m_ptr;//Managed Pointer to gpu data stream, if is close is set to zero
        long m_offset_counter = 0;// use to increment offset when write singular value
        unsafe byte* ptr;// pointer of buffer, need in bytes for a correct offset increment
        long bytesInOut = 0;

        /// <summary>
        /// </summary>
        /// <param name="dynamic">if true the buffer is optimized for many update</param>
        /// <param name="readable">default is false, not usefull</param>
        /// <param name="managed">if true the device store a copy in the Ram for device lost and reseting</param>
        public BaseBuffer(GraphicDevice device, bool dynamic, bool readable, bool managed)
        {
            this.dynamic = dynamic;
            this.m_ptr = IntPtr.Zero;
            this.isLocked = false;
            this.disposed = false;
            this.managed = managed;
            this.readable = readable;
            this.m_device = device;
            this.m_usage = Usage.None;
            this.m_memory = managed ? Pool.Managed : Pool.Default;

            if (!managed)
            {
                if (!readable) m_usage |= Usage.WriteOnly;
                if (dynamic) m_usage |= Usage.Dynamic;

                device.resourcesToDispose.Add(this);
                device.resourcesToRestore.Add(this);
            }

        }

        ~BaseBuffer()
        {
            if (!managed)
            {
                m_device.resourcesToDispose.Remove(this);
                m_device.resourcesToRestore.Remove(this);
            }
            Dispose(false);
        }

        /// <summary>
        /// Info about bytes flush in and out Gpu
        /// </summary>
        public long BytesToGpu { get { return bytesInOut; } }
        /// <summary>
        /// Get or Set the buffer position where start writing or reading
        /// </summary>
        public long Position
        {
            get { return m_offset_counter; }
            set { m_offset_counter = value > m_size ? m_size : value; }
        }
        /// <summary>
        /// Get or Set the number of elements to use in the buffer, if exed the size will be set to maximum value
        /// After device reseting and the pool is "default", the Count value was set to zero because all data are
        /// invalidated.
        /// </summary>
        public int Count
        {
            get { return m_count; }
            set { m_count = (value > m_size / m_type_size) ? m_size / m_type_size : value; }
        }
        /// <summary>
        /// Get the maximum numver of elements that buffer can store, if you need increase it the only solution is create a new buffer
        /// </summary>
        public int Capacity { get { return m_size / m_type_size; } }
        /// <summary>
        /// Get the type of elements
        /// </summary>
        public Type Type { get { return m_type; } }
        /// <summary>
        /// Get if buffer can be read.
        /// </summary>
        public bool IsReadable { get { return readable; } }
        /// <summary>
        /// </summary>
        /// <param name="disposing">
        /// If "true", the method has been called directly or indirectly by a user's code. Managed and unmanaged resources can be disposed.
        /// if "false",the method has been called by the runtime from inside the finalizer and you should not reference other objects.
        /// Only unmanaged resources can be disposed.
        /// </param>
        protected abstract void Dispose(bool disposing);
        /// <summary>
        /// Recreate the buffer
        /// </summary>
        public abstract void Restore();
        /// <summary>
        /// Used to destroy the object and release any managed or unmanaged resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Return the gpu buffer managed pointer
        /// </summary>
        protected abstract IntPtr Lock(int offset, int size, LockFlags mode);
        /// <summary>
        /// Release the gpu buffer pointer
        /// </summary>
        protected abstract void UnLock();
        /// <summary>
        /// Open the buffer. The offset and count value are relative to Type used
        /// </summary>
        public bool Open(int offset, int count, bool onlyread)
        {
            bytesInOut = 0;

            if ((offset + count) * m_type_size > m_size)
                throw new ArgumentOutOfRangeException("you try to open a incorrect size");

            if (!readable && onlyread)
                throw new Exception("Can't readonly this buffer, is writeonly");

            // caso solo lettura
            LockFlags mode = onlyread ? LockFlags.ReadOnly : LockFlags.Normal;

            // caso solo scrittura
            if (!managed && !readable)
            {
                if ((m_usage & Usage.Dynamic) != 0)
                {
                    if (count >= m_count)
                        mode |= LockFlags.Discard;
                    else
                        mode |= LockFlags.NoOverwrite;
                }
            }

            m_ptr = Lock(offset * m_type_size, count * m_type_size, mode);

            unsafe
            {
                if (m_ptr != IntPtr.Zero)
                {
                    isLocked = true;
                    ptr = (byte*)m_ptr.ToPointer();
                    //m_datastream = new BufferStream(m_ptr, m_type, count);
                }
                else
                {
                    isLocked = false;
                    ptr = null;
                    //m_datastream = null;
                }
            }
            return isLocked;
        }
        /// <summary>
        /// Open the buffer. The offset and count value are relative to Type used
        /// </summary>
        public bool Open(int offset, int count)
        {
            return Open(offset, count, false);
        }
        /// <summary>
        /// Open entire buffer
        /// </summary>
        public bool Open()
        {
            return Open(0, m_size / m_type_size);
        }
        /// <summary>
        /// Close buffer
        /// </summary>
        public void Close()
        {
            if (isLocked)
                UnLock();
            //m_datastream = null;
            unsafe
            {
                ptr = null;
            }
            m_ptr = IntPtr.Zero;
        }
        /// <summary>
        /// Simple set to device, auto assign vertex declaration
        /// </summary>
        public abstract void SetToDevice();

        #region Read buffer
        /// <summary>
        /// Read the buffer. The startidx and count value are relative to Type used
        /// </summary>
        /// <returns>the array is in type value</returns>
        public Array Read(int startidx, int count)
        {
            if (!isLocked) throw new Exception("Graphic Buffer not open");
            if ((startidx + count) * m_type_size > m_size) throw new ArgumentOutOfRangeException("Try to write a stream too big");
            bytesInOut += m_type_size * (count);

            Array dst = Array.CreateInstance(m_type, count);

            // pin the array so we can get a pointer to it
            GCHandle destHandle = GCHandle.Alloc(dst, GCHandleType.Pinned);
            unsafe
            {
                byte* trgPtr = (byte*)destHandle.AddrOfPinnedObject().ToPointer();
                for (long i = startidx * m_type_size, j = 0; j < count * m_type_size; i++, j++)
                {
                    trgPtr[j] = ptr[i];
                }
            }
            destHandle.Free();
            return dst;
            //return m_datastream.Read(startidx, count);
        }
        public Array Read()
        {
            return Read(0, m_count);
        } 
        #endregion

        #region Write buffer


        /// <summary>
        /// Write to buffer.
        /// ATTENTION : the src can be any type, count value are relative to it, but offset is relative to Type used 
        /// by this buffer
        /// </summary>
        public void Write(Array src, int offset, int count)
        {
            if (!isLocked) throw new Exception("Graphic Buffer not open");
            
            // fix count
            int src_length = src.GetUpperBound(0)+1;
            if (count > src_length) count = src_length;
            
            // get src values
            Type src_type = src.GetType().GetElementType();
            if (!src_type.IsValueType)
                throw new ArgumentException("Source array isn't an array of structs");
            int src_type_size = Marshal.SizeOf(src_type);
            long src_size = count * src_type_size;


            if (offset * m_type_size + src_size > m_size) throw new ArgumentOutOfRangeException("Try to write a stream too big");

            bytesInOut += src_size;

            // pin the array so we can get a pointer to it
            GCHandle handle = GCHandle.Alloc(src, GCHandleType.Pinned);
            unsafe
            {
                // get byte pointers for the source and target
                byte* srcPtr = (byte*)handle.AddrOfPinnedObject().ToPointer();

                // offset is relative to this buffer so we start in "offset * m_type_size"
                // j is the bytes counter of source array
                for (long i = offset * m_type_size, j = 0; j < src_size; i++, j++)
                {
                    ptr[i] = srcPtr[j];
                }

            }
            handle.Free();
        }
        /// <summary>
        /// Write to buffer.
        /// ATTENTION : the src can be any type but offset is relative to type used by this buffer
        /// </summary>
        public void Write(Array src, int offset)
        {
            Write(src, offset, src.GetUpperBound(0) + 1);
        }
        /// <summary>
        /// Write to buffer. The src can be any type.
        /// </summary>
        public void Write(Array src)
        {
            Write(src, 0, src.GetUpperBound(0) + 1);
        }
        #endregion

        #region Write and advance
        public unsafe void Write(float f)
        {
            byte* pdest = (byte*)(ptr + m_offset_counter);
            *(float*)pdest = f;
            m_offset_counter += 4;
        }
        public unsafe void Write(Vector3 v)
        {
            byte* pdest = (byte*)(ptr + m_offset_counter);
            *(Vector3*)pdest = v;
            m_offset_counter += 12;
        }
        public unsafe void Write(int i)
        {
            byte* pdest = (byte*)(ptr + m_offset_counter);
            *(int*)pdest = i;
            m_offset_counter += 4;
        }
        public unsafe void Write(CVERTEX v)
        {
            byte* pdest = (byte*)(ptr + m_offset_counter);
            *(CVERTEX*)pdest = v;
            m_offset_counter += sizeof(CVERTEX);
        }
        public unsafe void Write(NTVERTEX v)
        {
            byte* pdest = (byte*)(ptr + m_offset_counter);
            *(NTVERTEX*)pdest = v;
            m_offset_counter += sizeof(NTVERTEX);
        }


        #endregion
    }

}
