﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using Engine.Tools;

namespace Engine.Graphics
{
    public enum FontName
    {
        Calibri,
        Arial
    }

    public class Font
    {
        GraphicDevice device;
        DxFont font = null;

        /// <summary>
        /// Empty or null
        /// </summary>
        public Font(GraphicDevice device, FontName fontstyle, int fontsize)
        {
            this.device = device;
            font = new DxFont(device.m_device, fontstyle.ToString(), fontsize);

            if (font.MemoryPool != Pool.Managed)
            {
                device.resourcesToDispose.Add(font);
                device.resourcesToRestore.Add(font);
            }
        }

        ~Font()
        {
            if (font.MemoryPool != Pool.Managed)
            {
                device.resourcesToDispose.Remove(font);
                device.resourcesToRestore.Remove(font);
            }
            font = null;
        }
        /// <summary>
        /// Set to device the texture, the method check if current class are initialized.
        /// If texture null or Empty the static int prevHandle will be set to 0;
        /// </summary>
        /// <param name="device"></param>
        public void Draw(string text , int x , int y , Color color)
        {
            font.DrawString(text, x, y, color);
        }
    }
}