﻿// by johnwhile
using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Renderer;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Graphics
{
    public class GraphicDevice : IDisposable
    {
        internal DX9Device m_device;

        Control m_control;
        /// <summary>
        /// Check if device can be used to draw
        /// </summary>
        DeviceState current_state = DeviceState.InvalidDevice;
        /// <summary>
        /// What device can and can't do
        /// </summary>
        public Capabilities capabilities;
        /// <summary>
        /// Render states of device, need to minimize the changes
        /// </summary>
        public RenderStates renderstates;
        /// <summary>
        /// Lights list, this class auto-check the ligth limit
        /// </summary>
        public List<Light> lights = new List<Light>();

        /// <summary>
        /// Resource list what must be rebuilded after device reseting, example resources not managed, if not you can see it on screen
        /// </summary>
        internal List<IRestore> resourcesToRestore = new List<IRestore>();
        /// <summary>
        /// Resource list what must be disposed before device reseting, example resources not managed, if not the device crash
        /// </summary>
        internal List<IDisposable> resourcesToDispose = new List<IDisposable>();
        
        /// <summary>
        /// The size and position of render target
        /// </summary>
        public Viewport viewport
        {
            get { return m_device.viewport; }
            set { m_device.viewport = value; }
        }
        /// <summary>
        /// The GPU class, they use a deph stencil buffer and try to use a puredevice flag.
        /// </summary>
        public GraphicDevice(Control control)
        {
            Trace.WriteLine("Initialize Directx9 Graphic Class...");

            m_control = control;
            m_device = new DX9Device(control, true, true, true);
            capabilities = new Capabilities(m_device);
            renderstates = new RenderStates(m_device);

            renderstates.SetStates();
            m_device.SetRendersStates();

            Trace.Indent();
            Trace.WriteLine("device info      : " + m_device.ToString());
            Trace.Unindent();
            Trace.Flush();
        }

        public void DrawIndexedPrimitives(PrimitiveType primitive, int baseVertex, int minVertex, int numVertices, int startIndex, int numPrimitives)
        {
            Debug.Assert(current_state == DeviceState.OK,"you need checking device.Begin() after all device operations");
            m_device.DrawIndexedPrimitives(primitive, baseVertex, minVertex, numVertices, startIndex, numPrimitives);
        }
        public void DrawPrimitives(PrimitiveType primitive, int startIndex, int numPrimitives)
        {
            Debug.Assert(current_state == DeviceState.OK, "you need checking device.Begin() after all device operations");
            m_device.DrawPrimitives(primitive, startIndex, numPrimitives);
        }

        public void DrawUserPrimitives<T>(PrimitiveType type, int numPrimitives, T[] array) where T : struct
        {
            Debug.Assert(current_state == DeviceState.OK, "you need checking device.Begin() after all device operations");
            m_device.DrawUserPrimitives(type, numPrimitives, array);
        }
        public void Clear(Color background)
        {
            m_device.Clear(background);
        }

        /// <summary>
        /// If you use SwapChain render target, the device use them, to draw with original 
        /// buffers need to re-set them
        /// </summary>
        public void SetRenderTarghet()
        {
            m_device.SetRenderTarghet();
        }   
        /// <summary>
        /// Inform device that it's time to draw, return false if can't. 
        /// </summary>
        public bool BeginDraw()
        {
            current_state = m_device.Begin();
            
            switch(current_state)
            {
                case DeviceState.OK:
                    //Console.WriteLine("DeviceState.OK");
                    // light must be set evary frames
                    for (int i = 0; i < lights.Count; i++)
                        m_device.SetLight(lights[i], i);

                    return true;

                case DeviceState.DeviceLost:
                    Console.WriteLine("DeviceState.DeviceLost");
                    //Can't Reset yet, wait for a bit
                    System.Threading.Thread.Sleep(500);
                    return false;

                case DeviceState.DeviceNotReset:
                    Console.WriteLine("DeviceState.DeviceNotReset");
                    System.Threading.Thread.Sleep(100);
                    this.Reset();
                    return false;

                case DeviceState.InvalidCall:
                    throw new Exception("Device crash");

                default: return false;
            }
        }
        /// <summary>
        /// Inform device to finisch draw
        /// </summary>
        public void EndDraw()
        {
            m_device.End();
        }
        /// <summary>
        /// Draw to control the result
        /// </summary>
        public void PresentDraw()
        {
            m_device.Present();
        }
        /// <summary>
        /// When windows handles linked to device are resized, if you not resize the image
        /// will have a wrong aspect ratio
        /// </summary>
        public void Resize(Size backbuffersize)
        {
            System.Drawing.Rectangle resolution = Screen.PrimaryScreen.Bounds;

            int width = backbuffersize.Width;
            int height = backbuffersize.Height;

            /*
            Viewport[] supported = m_device.SupportedDisplayMode;
            int i=0;
            while (supported[i++].Width < width) ; i--;

            if (height > supported[i].Height)
            {
                Debug.WriteLine(String.Format("fix height from {0} to {1}", height, supported[i].Height));
                height = supported[i].Height;
            }
            */

            // why resize if is the same ?
            if (m_device.BackBufferHeight == height && m_device.BackBufferWidth == width) return;

            Debug.Assert(width <= resolution.Width && height <= resolution.Height, "current control size : <" + height + " x " + width + "> have an invalid size and can generate OutVideoMemory exception");

            if (height > 1024) height = 1024;
            if (width > 1024) width = 1024;

            m_device.BackBufferHeight = height > 1 ? height : 1;
            m_device.BackBufferWidth = width > 1 ? width : 1;

            Console.WriteLine(m_device.BackBufferHeight + " x " + m_device.BackBufferWidth);

            this.Reset();

            m_device.viewport = new Viewport
            {
                Height = height,
                Width = width,
                MaxDepth = 1,
                MinDepth = 0,
                X = 0,
                Y = 0
            };

        }
        /// <summary>
        /// Reset device, dispose and recreate all managed resources
        /// </summary>
        public void Reset()
        {
            foreach (IDisposable res in resourcesToDispose)
                res.Dispose();

            m_device.Reset();

            // restore previus values
            renderstates.SetStates();

            foreach (IRestore res in resourcesToRestore)
                res.Restore();

            // tell to texture class that device texture are lost
            NodeMaterial.RemoveFromDevice(this);
        }

        public void Dispose()
        {
            foreach (IDisposable res in resourcesToDispose)
                res.Dispose();

            m_device.Dispose();
        }

        public override string ToString()
        {
            return DX9Device.graphicVersion.ToString();
        }
    }

    public class Capabilities
    {
        internal DX9Device device;

        internal Capabilities(DX9Device device)
        {
            this.device = device;
        }     
        public int MaxVertexIndex
        {
            get { return device.MaxVertexIndex; }
        }
        public int MaxPrimitiveCount
        {
            get { return device.MaxPrimitiveCount; }
        }
        public bool SupportTextureSize(Size size)
        {
            return device.SupportTextureSize(size);
        }
        public int MaxLights
        {
            get { return device.MaxActiveLights; }
        }
    }

    /// <summary>
    /// This class memorize the render states value what are lost after device reseting
    /// </summary>
    public class RenderStates
    {
        #region memorized
        Cull m_cull;
        FillMode m_fill;
        Matrix4 m_world;
        Matrix4 m_projection;
        Matrix4 m_view;
        float m_depth;
        bool m_lightEnable;
        Color m_ambient;
        #endregion

        internal DX9Device device;

        internal RenderStates(DX9Device device)
        {
            this.device = device;
            DefaultSetting();
        }

        public void SetStates()
        {
            device.CullMode = m_cull;
            device.FillMode = m_fill;
            device.DepthBias = m_depth;
            device.LightEnable = m_lightEnable;
            device.View = m_view;
            device.World = m_world;
            device.Projection = m_projection;
            device.Ambient = m_ambient;
        }

        public void DefaultSetting()
        {
            cull = Cull.CounterClockwise;
            fillMode = FillMode.Solid;
            depth = 0.0f;
            lightEnable = false;
            world = Matrix4.Identity;
            projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45.0f), (float)device.BackBufferWidth / (float)device.BackBufferHeight, 0.1f, 1000.0f);
            view = Matrix4.MakeViewLH(new Vector3(10, 10, 10), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            ambient = Color.Black;
        }

        public Cull cull
        {
            get { return m_cull; }
            set { device.CullMode = value; m_cull = value; }
        }
        public FillMode fillMode
        {
            get { return m_fill; }
            set { device.FillMode = value; m_fill = value; }
        }
        public float depth
        {
            get { return m_depth; }
            set { device.DepthBias = value; m_depth = value; }
        }
        public bool lightEnable
        {
            get { return m_lightEnable; }
            set { device.LightEnable = value; m_lightEnable = value; }
        }
        public Matrix4 view
        {
            get { return m_view; }
            set { device.View = value; m_view = value; }
        }
        public Matrix4 projection
        {
            get { return m_projection; }
            set { device.Projection = value; m_projection = value; }
        }
        public Matrix4 world
        {
            get { return m_world; }
            set { device.World = value; m_world = value; }
        }
        public Color ambient
        {
            get { return m_ambient; }
            set { device.Ambient = value; m_ambient = value; }
        }
    }

}