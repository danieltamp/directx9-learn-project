﻿// by johnwhile
using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Renderer;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Graphics
{
    public class SwapChain : IDisposable
    {
        internal DX9SwapChain m_swapchain;
        GraphicDevice m_device;
        Control m_control;

        public SwapChain(GraphicDevice device,Control control)
        {
            m_device = device;
            m_control = control;
            m_swapchain = new DX9SwapChain(device.m_device, m_control, true, true);
        }
        /// <summary>
        /// Reinitialize the swapchain
        /// </summary>
        public void Resize(Size backbuffersize)
        {
            m_swapchain.Resize(backbuffersize);
        }
        /// <summary>
        /// prepare the device's backbuffer 
        /// </summary>
        public void SetRenderTarget()
        {
            m_swapchain.SetRenderTarget();
        }
        /// <summary>
        /// Clear the current backbuffer
        /// </summary>
        public void Clear(Color background)
        {
            m_swapchain.Clear(background);
        }
        /// <summary>
        /// Draw result to windows handle
        /// </summary>
        public void Present()
        {
            m_swapchain.Present();
        }


        public void Dispose()
        {
            m_swapchain.Dispose();
        }
    }
}