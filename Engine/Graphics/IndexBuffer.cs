﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Maths;

namespace Engine.Graphics
{
    public class IndexBuffer : BaseBuffer
    {
        bool is32bit;
        DX9IndexBuffer m_buffer;

        /// <summary>
        /// </summary>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting</param>
        /// <param name="indexType">Define the type used to rappresent indices, can be a 16 or 32bit number or Face struct</param>
        public IndexBuffer(GraphicDevice device, Type indexType, int size, bool dynamic, bool managed) :
            this(device, indexType, size, dynamic, false, managed) { }

        /// <summary>
        /// </summary>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting</param>
        /// <param name="indexType">Define the type used to rappresent indices, can be a 16 or 32bit number or Face struct</param>
        public IndexBuffer(GraphicDevice device, Type indexType, int size, bool dynamic, bool readable, bool managed)
            : base(device, dynamic,readable, managed)
        {
            m_type = indexType;
            m_type_size = Marshal.SizeOf(m_type);

            if (indexType == typeof(Edge) ||
                indexType == typeof(Face) ||
                indexType == typeof(ushort) ||
                indexType == typeof(short))
            {
                is32bit = false;
            }
            else if (indexType == typeof(uint) ||
                     indexType == typeof(int))
            {
                is32bit = true;
            }
            else
            {
                throw new NotImplementedException(indexType.ToString() + " is unrecognized type");
            }

            m_size = size * m_type_size;
            Restore();
        }

        public override void Restore()
        {
            if (!disposed) Dispose(true);
            m_count = 0;
            m_buffer = new DX9IndexBuffer(m_device.m_device, is32bit, m_size, m_usage, m_memory);
            disposed = false;
        }

        protected override void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (m_buffer!=null)
                        m_buffer.Dispose();
                    m_buffer = null;
                }
                disposed = true;
            }
        }

        public override void SetToDevice()
        {
            m_buffer.SetToDevice();
        }

        /// <summary>
        /// </summary>
        /// <param name="boffset">in bytes</param>
        /// <param name="bsize">in bytes</param>
        protected override IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            return m_buffer.Lock(boffset, bsize, mode);
        }

        protected override void UnLock()
        {
            m_buffer.UnLock();
        }
    }

}
