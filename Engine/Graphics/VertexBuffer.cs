﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Graphics
{
    public class VertexBuffer : BaseBuffer
    {
        VertexDeclaration m_decl;
        DX9VertexBuffer m_buffer;

        public VertexBuffer(GraphicDevice device, IVertexFormat type, int size, bool dynamic, bool managed) :
            this(device, new VertexDeclaration(device, type), size, dynamic, false, managed) { }

        /// <summary>
        /// </summary>
        public VertexBuffer(GraphicDevice device, VertexDeclaration declaration, int size, bool dynamic, bool managed) :
            this(device, declaration, size, dynamic, false, managed) { }

        /// <summary>
        /// </summary>
        /// <param name="declaration">vertex type information</param>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting and all data must be rewritten</param>
        /// <param name="dynamic">improve for many updating</param>
        /// <param name="readable">use buffer as readable, not very usefull</param>
        public VertexBuffer(GraphicDevice device, VertexDeclaration declaration, int size, bool dynamic, bool readable, bool managed)
            : base(device, dynamic,readable, managed)
        {
            m_decl = declaration;
            m_type = m_decl.vertexType;
            m_type_size = m_decl.SizeInByte;
            m_size = size * m_type_size;
            Restore();
        }

        public override void Restore()
        {
            if (!disposed) Dispose(true);
            m_count = 0;
            m_buffer = new DX9VertexBuffer(m_device.m_device, m_decl.vertexFormat, m_size, m_usage, m_memory);
            disposed = false;
        }

        public override void SetToDevice()
        {
            m_buffer.SetStreamSource(0, m_type_size);
        }

        protected override IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            return m_buffer.Lock(boffset, bsize, mode);
        }

        protected override void UnLock()
        {
            m_buffer.UnLock();
        }

        protected override void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (m_buffer != null)
                        m_buffer.Dispose();
                    m_buffer = null;
                }
                disposed = true;
            }
        }
    }
}
