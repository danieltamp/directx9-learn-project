﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Graphics
{
    /// <summary>
    /// TODO : Manager for vertexdeclaration class, simple singletons organization to avoid create same instances
    /// </summary>
    public class VertexDeclarationManager
    {
        /// <summary>
        /// Avoid create the same vertexDeclaration instances
        /// </summary>
        static Dictionary<Type, VertexDeclaration> singletons = new Dictionary<Type, VertexDeclaration>();

        public static void CreateDeclaration(Type vertexType, VertexElement[] elements)
        {

        }
    }

    /// <summary>
    /// Instruct the device about vertex format, contain all informations about vertices
    /// </summary>
    public class VertexDeclaration
    {
        DX9VertexDeclaration m_declaration;
        
        int size = 0;
        Type type;
        VertexFormat flags;
        List<VertexElement> elements;
        Dictionary<VertexFormat, int> elementDictionary;



        /// <summary>
        /// Initialize a new vertex descriptor
        /// </summary>
        /// <param name="device">the device to use</param>
        /// <param name="vertexType">Type of struct, will use to check size</param>
        /// <param name="Elements">Vertex struct descriptor</param>
        public VertexDeclaration(GraphicDevice device, Type vertexType , VertexElement[] Elements)
        {
            elements = new List<VertexElement>(Elements.Length + 1);
            elementDictionary = new Dictionary<VertexFormat, int>(Elements.Length);
            type = vertexType;
            
            int idx = 0;
            foreach (VertexElement e in Elements)
            {
                if (e.Type == DeclarationType.Unused) continue;

                size += e.SizeInByte;

                if (e.Usage == DeclarationUsage.Position)
                {
                    flags |= VertexFormat.Position;
                    elementDictionary.Add(VertexFormat.Position, idx);
                }
                else if (e.Usage == DeclarationUsage.Normal)
                {
                    flags |= VertexFormat.Normal;
                    elementDictionary.Add(VertexFormat.Normal, idx);
                }
                else if (e.Usage == DeclarationUsage.Color)
                {
                    flags |= VertexFormat.Diffuse;
                    elementDictionary.Add(VertexFormat.Diffuse, idx);
                }
                else if (e.Usage == DeclarationUsage.TextureCoordinate && e.UsageIndex == 0)
                {
                    flags |= VertexFormat.Texture1;
                    elementDictionary.Add(VertexFormat.Texture1, idx);
                }
                else throw new NotImplementedException();
                
                elements.Add(e);
                idx++;
            }
            elements.Add(VertexElement.VertexDeclarationEnd);

            int truesize = Marshal.SizeOf(type);

            if (truesize != size)
                throw new Exception("The real size of struct not match with calculated");

            m_declaration = new DX9VertexDeclaration(device.m_device, elements);
        }
        public VertexDeclaration(GraphicDevice device, IVertexFormat vertexType)
            : this(device, vertexType.type, vertexType.elements) { }
        
        public static VertexDeclaration GetDeclaration<T>(GraphicDevice device) where T : struct , IVertexFormat 
        {
            T vertex = new T();
            return new VertexDeclaration(device,vertex);
        }





        /// <summary>
        /// The type of vertex struct
        /// </summary>
        public Type vertexType
        {
            get { return type; }
        }
        /// <summary>
        /// The VertexElements list
        /// </summary>
        public VertexElement[] Elements
        {
            get { return elements.ToArray(); }
        }
        /// <summary>
        /// The VertexFormat flags used by this declaration
        /// </summary>
        public VertexFormat vertexFormat
        {
            get { return flags; }
        }
        /// <summary>
        /// size in byte of vertex
        /// </summary>
        public int SizeInByte
        {
            get { return size; }
        }
        /// <summary>
        /// num of used elements (the VertexDeclarationEnd are omitted)
        /// </summary>
        public int Count
        {
            get { return elements.Count - 1; }
        }

        /// <summary>
        /// Return the index of VertexElement using the targhet VertexFormat
        /// </summary>
        /// <param name="type">ONE vertexFormat flag</param>
        public int FindElement(VertexFormat type)
        {
            // need a singular flag, if isn't power or 2 can't return a value from dictionary
            if (Array.BinarySearch<int>(MathUtils.powersOfTwo, (int)type) == 0)
                throw new ArgumentException("Need a singular flag");

            return Contain(type) ? elementDictionary[type] : -1;
        }
        /// <summary>
        /// Test if this declaration contain one/more format 
        /// </summary>
        /// <param name="type"></param>
        public bool Contain(VertexFormat type)
        {
            return (flags & type) != 0;
        }
        /// <summary>
        /// </summary>
        public void SetToDevice()
        {
            m_declaration.SetToDevice();
        }
    }
}
