﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Basic Cache manager
    /// </summary>
    /// <remarks>
    /// A cache is a basic group of same nodes's type and node's format regrouped in one to call Draw function only once
    /// or to reduce the changes states.
    /// </remarks>
    public abstract class BatchCache : IKey , IComparable
    {
        public uint m_key { get; set; }
        public bool m_removed { get; set; }

        /// <summary>
        /// Set your custom value, define the maximum vertices used for each Draw() call , usually ushort.maxvalue
        /// I noticed that graphic card slow when you open a very big buffer, but a value too small generate more Draw call.
        /// </summary>
        public static int MAX_BATCH_VERTICES = ushort.MaxValue;

        protected EngineCore core;
        protected GraphicDevice device;
        protected VertexDeclaration m_Vdeclaration;
        protected Matrix4 globalCoordSystem;
        protected List<Node> ready2write;
        protected List<Node> ready2remove;
        protected List<Node> ready2overwrite;
        protected PrimitiveType cachePrimitive = 0;
        //protected OrderedDictionary BufferChunks;
        //protected OrderedCollectionManager<EngineChunkInfo> BufferChunks;
        protected List<BatchChunkInfo> BufferChunks;

        protected bool useBatch = true;
        protected bool useLocal = false;
        protected bool useLight = true;
        protected bool indices32bit;
        protected int nRequiredIndis = 0;
        protected int nRequiredVerts = 0;
        protected int MAXVERTEXINDEX = 0;
        protected int MAXPRIMCOUNT = 0;
        protected int MAXVERTS = 0;
        protected int MAXINDIS = 0;
        protected int nodesCount = 0;
        bool isIndexed;
        
        #region Public access  
        public VertexFormat vertexFormat
        {
            get { return m_Vdeclaration.vertexFormat; }
        }
        public Matrix4 cacheWorld
        {
            get { return globalCoordSystem; }
            set { globalCoordSystem = value; }
        }
        public int MaxVerts { get { return MAXVERTS; } }
        public int MaxIndices { get { return MAXINDIS; } }
        public int numNodes { get { return nodesCount; } }
        public int numVertices { get { return nRequiredVerts; } }
        public int numIndices { get { return nRequiredIndis; } }
        public bool UseLocal { get { return useLocal; } }
        public bool UseBatch { get { return useBatch; } }
        #endregion

        public int numChunkVertices(Node node)
        {
            return (BufferChunks[((NodeBatchProperties)node.engineProp).m_ichunk]).nverts;
        }
        public int numChunkIndices(Node node)
        {
            return isIndexed ? ((BatchChunkInfoIndexed)BufferChunks[((NodeBatchProperties)node.engineProp).m_ichunk]).nindis : 0;
        }
            
        /// <summary>
        /// Cache contructor
        /// </summary>
        /// <param name="useBatchMethod">regroup all geometry into one big, but this methods is affected by maxvertexindex value</param>
        /// <param name="useLocalTransform">if not useBatchMethod, set the device.transformstate.world with node's world matrix of each node, improve the movement but slow the render</param>
        /// <param name="indexed">tell now calculate the primitive values</param>
        public BatchCache(
            GraphicDevice device,
            VertexDeclaration verticesDeclaration,
            PrimitiveType primitive,
            bool indexed,
            EngineCore Core,
            bool useBatchMethod,
            bool useLocalTransform)
        {
            this.m_removed = false;

            //this.BufferChunks = new OrderedDictionary();
            //this.BufferChunks = new OrderedCollectionManager<EngineChunkInfo>(false);
            this.BufferChunks = new List<BatchChunkInfo>();

            this.globalCoordSystem = Matrix4.Identity;
            this.ready2write = new List<Node>();
            this.ready2remove = new List<Node>();
            this.ready2overwrite = new List<Node>();

            this.isIndexed = indexed;
            this.core = Core;
            this.device = device;
            this.useBatch = useBatchMethod;
            this.useLocal = !useBatch && useLocalTransform;
            this.cachePrimitive = primitive;
            this.m_Vdeclaration = verticesDeclaration;

            // TODO : the max limit can be greated than calculated if you using "isolated vertices" like in
            // 3dstudio, a vertices don't used for a triangle or line but not rendered. For semplicity i don't
            // implement this case because the renderer must have only necessary data, the optimization will be
            // to in node's GetStreamVertices and Indices

            this.MAXVERTEXINDEX = device.capabilities.MaxVertexIndex; 
            this.MAXPRIMCOUNT = device.capabilities.MaxPrimitiveCount;

            switch (primitive)
            {
                case (PrimitiveType.Instance):
                    throw new NotImplementedException("");

                case (PrimitiveType.PointList):
                    MAXVERTS = indexed ? Math.Min(MAXPRIMCOUNT, MAXVERTEXINDEX) : MAXPRIMCOUNT;
                    break;

                case (PrimitiveType.LineStrip):
                    if (indexed) throw new Exception("LineStrip can't be indexed");
                    if (useBatch) throw new Exception("LineStrip can't be regouped, the -1 vertex index aren't supported in Directx9");
                    MAXVERTS = MAXPRIMCOUNT + 1;
                    break;

                case (PrimitiveType.LineList):
                    MAXVERTS = indexed ? Math.Min(MAXPRIMCOUNT * 2, MAXVERTEXINDEX) : MAXPRIMCOUNT * 2;
                    break;

                case (PrimitiveType.TriangleFan):
                    if (indexed) throw new Exception("TriangleFan can't be indexed");
                    if (useBatch) throw new Exception("TriangleFan can't be regouped, the -1 vertex index aren't supported in Directx9");
                    MAXVERTS = MAXPRIMCOUNT + 2;
                    break;

                case (PrimitiveType.TriangleStrip):
                    if (indexed) throw new Exception("TriangleStrip can't be indexed");
                    if (useBatch) throw new Exception("TriangleStrip can't be regouped, the -1 vertex index aren't supported in Directx9");
                    MAXVERTS = MAXPRIMCOUNT + 2;
                    break;

                case (PrimitiveType.TriangleList):
                    MAXVERTS = indexed ? Math.Min(MAXPRIMCOUNT * 3, MAXVERTEXINDEX) : MAXPRIMCOUNT * 3;
                    break;

                default:
                    throw new NotImplementedException("");
            }

            // if node aren't regrouped, don't use a limit for vertices but check each node
            // with MAXVERTEXINDEX and MAXPRIMCOUNT when added. 3M vertices are a safety limit
            // to avoid memory leack when there are some bugs
            //if (!useBatch || MAXVERTS > 65535) MAXVERTS = 65535;
            if (!useBatch || MAXVERTS > MAX_BATCH_VERTICES) MAXVERTS = MAX_BATCH_VERTICES;

            // set a statistic limit, avoid array increase too many
            MAXINDIS = isIndexed ? MAXVERTS * 6 : 0;

            // if max vertex index is greater than ushort.MaxValue you can use uint, but take care about MAXVERTS value
            this.indices32bit = Math.Min(MAX_BATCH_VERTICES, MAXVERTEXINDEX) > ushort.MaxValue;

            // normal are used when you use light... 
            bool useColor = (this.m_Vdeclaration.vertexFormat & VertexFormat.Diffuse) != 0;
            bool useTexture = (this.m_Vdeclaration.vertexFormat & VertexFormat.Texture1) != 0;
            bool useNormal = (this.m_Vdeclaration.vertexFormat & VertexFormat.Normal) != 0;

            this.useLight = useNormal;
        }

        /// <summary>
        /// Get if cache are empty. It's empty also when there are some node store in buffers but
        /// the remove process will be delete all.
        /// </summary>
        public bool isEmpty
        {
            get { return ready2remove.Count >= BufferChunks.Count; }
        }
        /// <summary>
        /// Safety test if node are stored here, not usefull because "chunkIndex" must be correct
        /// </summary>
        public bool Contain(Node node)
        {
            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
            return (prop.m_ichunk >= 0 && prop.m_ichunk < BufferChunks.Count && BufferChunks[prop.m_ichunk].node == node);
        }   
        /// <summary>
        /// Remove all node's data from here but don't destroy resources (buffers) because can be utilized after
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < BufferChunks.Count; i++)
            {
                BatchChunkInfo chunk = BufferChunks[i];
                Node node = chunk.node;             
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                // reset node's flags
                prop.isInBuffer = false;
                prop.m_ichunk = -1;
                prop.m_cache = null;
            }
            ready2write.Clear();
            ready2remove.Clear();
            ready2overwrite.Clear();
            BufferChunks.Clear();
            nRequiredIndis = 0;
            nRequiredVerts = 0;
            nodesCount = 0;
        }
        /// <summary>
        /// Compare two caches when call sorting method, the compare must be used to minimize the render change states
        /// </summary>
        public virtual int CompareTo(object obj)
        {
            return 0;
        }
        
        public void removeallnodesref()
        {
            BufferChunks.Clear();
        }
        /// <summary>
        /// Ready to delete this class
        /// </summary>
        public virtual void Destroy()
        {
            Clear();
            m_removed = true;
        }
        /// <summary>
        /// When necessary do you clean buffers operation
        /// </summary>
        public abstract void CleanBuffers();
        /// <summary>
        /// "Adding" isn't a temporary job, need to call WriteAll() to complete all jobs and must be called after DeleteAll()
        /// </summary>
        public RESULT Add(Node node, out int vertsAdded, out int indisAdded)
        {
            //Console.WriteLine("*** adding node " + node.handleID + " to cache" + cacheID);
            vertsAdded = node.numVertices;
            indisAdded = isIndexed ? node.numIndices : 0;
            int primitiveDrawed = node.numPrimitives;

            // check if item can be added
            if (!useBatch && primitiveDrawed > MAXPRIMCOUNT)
                return RESULT.OUT_OF_MEMORY;

            if ((vertsAdded + nRequiredVerts) > MAXVERTS || (indisAdded + nRequiredIndis) > MAXINDIS)
                return RESULT.OUT_OF_MEMORY;

            nRequiredVerts += vertsAdded;
            nRequiredIndis += indisAdded;
            nodesCount++;

            ready2write.Add(node);

            return RESULT.OK;
        }
        public RESULT Add(uint handle, out int vertsAdded, out int indisAdded)
        {
            return this.Add( core.m_nodes[handle], out vertsAdded, out indisAdded);
        }
        /// <summary>
        /// "Removing" isn't a temporary job, will be completed with DeleteAll() and this function must be called before WriteAll()
        /// to ensure the "Updating" method
        /// </summary>        
        public RESULT Remove(Node node, out int vertsDeleted, out int indisDeleted)
        {
            uint handle = node.m_key;
            vertsDeleted = 0;
            indisDeleted = 0;

            if (!this.Contain(node)) return RESULT.NOT_FOUND;

            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
            
            if (isIndexed)
            {
                BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[prop.m_ichunk];
                // delete the chunk's data, not the node's data because can be updated and different
                nRequiredVerts -= chunk.nverts;
                nRequiredIndis -= chunk.nindis;
                vertsDeleted += chunk.nverts;
                indisDeleted += chunk.nindis;
            }
            else
            {
                BatchChunkInfo chunk = BufferChunks[prop.m_ichunk];
                nRequiredVerts -= chunk.nverts;
                vertsDeleted += chunk.nverts;
            }
            nodesCount--;
            prop.m_cache = null;

            ready2remove.Add(node);
            return RESULT.OK;
        }
        public RESULT Remove(uint handle, out int vertsDeleted, out int indisDeleted)
        {
            return this.Remove(core.m_nodes[handle], out vertsDeleted, out indisDeleted);
        }
        /// <summary>
        /// "Updating" isn't a temporary job, will be completed with OverwriteAll() and this function must be called after DeleteAll and WriteAll
        /// to ensure the "Brutal Updating" method (remove + add), so updating method overwrite the same number of vertices and indices
        /// </summary>        
        public RESULT OverWrite(Node node)
        {
            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
            if (!prop.isInBuffer) return RESULT.NOT_FOUND;
            ready2overwrite.Add(node);
            return RESULT.OK;
        }
        public RESULT OverWrite(uint handle)
        {
            return this.OverWrite( core.m_nodes[handle]);
        }
        /// <summary>
        /// Add new nodes to buffers with a simple append methods for NOT-INDEXED geometry.
        /// In this case Batching method doesn't involve the calculations
        /// </summary>
        /// <param name="nodes">list of nodes to append</param>
        /// <param name="nvertices">previous nvertices write, will be updated</param>
        /// <param name="vlockoffset">the offset used to Open the vertex buffer</param>
        public void AppendNewNodes(List<Node> nodes, ref int nvertices, int vlockoffset, VertexBuffer VB)
        {
            for (int n = 0; n < nodes.Count; n++)
            {
                Node node = nodes[n];
                Array meshVertices = null;

                if (this.m_Vdeclaration.vertexType != VB.Type)
                    throw new Exception("graphic stream different");

                meshVertices = node.GetVerticesStream(!useLocal, this.m_Vdeclaration);

                VB.Write(meshVertices, nvertices - vlockoffset);
                //GPUvertices.Write(meshVertices, nvertices - vlockoffset);

                BatchChunkInfo chunk = new BatchChunkInfo();
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;    
                chunk.node = node;
                chunk.voffset = nvertices;
                chunk.nverts = meshVertices.Length;

                prop.isInBuffer = true;
                prop.needUpdateIndices = false;
                prop.needUpdateVertices = false;
                prop.m_ichunk = BufferChunks.Count;
                prop.stackoverflowcounter_Add = 0;

                BufferChunks.Add(chunk);

                // update offset
                nvertices += chunk.nverts;
            }
        }
        /// <summary>
        /// Add new nodes to buffers with a simple append methods for INDEXED geomety.
        /// In this case Batching method involve the calculation of nodes
        /// </summary>
        /// <param name="ready2write">list of nodes to append</param>
        /// <param name="nvertices">previous nvertices write, will be updated</param>
        /// <param name="vlockoffset">the offset used to Open the vertex buffer</param>
        /// <param name="nindices">previous nindices write, will be updated</param>
        /// <param name="ilockoffset">the offset used to Open the index buffer</param>
        public void AppendNewNodes(List<Node> ready2write, ref int nvertices, int vlockoffset, VertexBuffer VB, ref int nindices, int ilockoffset,IndexBuffer IB)
        {
            for (int n = 0; n < ready2write.Count; n++)
            {
                Node node = ready2write[n];
                Array meshIndices = null;
                Array meshVertices = null;


                meshIndices = node.GetIndicesStream(useBatch ? nvertices : 0, this.indices32bit);
                IB.Write(meshIndices, nindices - ilockoffset);
                //GPUindices.Write(meshIndices, nindices - ilockoffset);


                if (this.m_Vdeclaration.vertexType != VB.Type)
                    throw new Exception("graphic stream different");

                meshVertices = node.GetVerticesStream(!useLocal, this.m_Vdeclaration);
                VB.Write(meshVertices, nvertices - vlockoffset);
                //GPUvertices.Write(meshVertices, nvertices - vlockoffset);

                BatchChunkInfoIndexed chunk = new BatchChunkInfoIndexed();
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp; 
                chunk.node = node;
                chunk.voffset = nvertices;
                chunk.ioffset = nindices;
                chunk.nverts = meshVertices.Length;
                chunk.nindis = meshIndices.Length;

                prop.isInBuffer = true;
                prop.needUpdateIndices = false;
                prop.needUpdateVertices = false;
                prop.m_ichunk = BufferChunks.Count;
                prop.stackoverflowcounter_Add = 0;

                BufferChunks.Add(chunk);

                // update offset
                nindices += chunk.nindis;
                nvertices += chunk.nverts;           
            }
        }
           
        /// <summary>
        /// Start draw function , set texture and light if need
        /// </summary>
        public void Draw()
        {
            // Before start drawing need some change state, optimized for only nevessary changes
            // If using the batching method, use the default global world matrix.
            if (!useLocal) device.renderstates.world = globalCoordSystem;

            // Set light if need
            device.renderstates.lightEnable = useLight;

            DrawImplementation();
        }
        /// <summary>
        /// Draw implementation, different foreach derived caches
        /// </summary>
        /// <remarks>
        /// TODO : i don't like compute each time the device.transfrom.world * gloablCoordSystem for each node
        /// when "useBatch = true" and "useLocal = false", pre-compute it int the EngineChunkInfo
        /// </remarks>
        protected abstract void DrawImplementation();
        /// <summary>
        /// Do all removing operations
        /// </summary>
        public abstract void DeleteAll();
        /// <summary>
        /// Do all adding operations
        /// </summary>
        public abstract void WriteAll();
        /// <summary>
        /// Do all overwrite operations
        /// </summary>
        public abstract void OverwriteAll();
        /// <summary>
        /// Some internal test (before draw) and after flush all process
        /// </summary>
        public abstract bool DebugFlushAllProcess();
        /// <summary>
        /// Can node be added in this cache ?
        /// </summary>
        public abstract bool isCompatible(Node node);

        public override string ToString()
        {
            StringBuilder str = new StringBuilder("EngineCache >\r\n");
            str.AppendLine("Type : " + this.GetType().ToString());
            str.AppendLine("Empty : " + isEmpty);
            foreach (BatchChunkInfo chunk in BufferChunks)
                str.AppendLine(chunk.ToString());
            return str.ToString();
        }
    }
}
