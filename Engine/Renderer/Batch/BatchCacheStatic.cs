﻿using System;
using System.Drawing;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;

namespace Engine.Renderer
{
    /// <summary>
    /// NOT-INDEXED
    /// The static caches, optimize to fast draw immutable geometries with mininum resources used, buffers use Pool.Default
    /// and the remove process is slower than dynamic version
    /// </summary>
    public abstract class BatchCacheStatic : BatchCache, IRestore
    {
        /// <summary>
        /// First chunks from start rewriting, if == bufferchunks.count not need rewrite.
        /// </summary>
        /// <remarks>
        /// the offset associated to BufferChunks[VminChunkIndex] will be invalidate ( = 0) because don't valid
        /// </remarks>
        protected int vminChunkIndex = 0;
        /// <summary>
        /// Inform write process that need to rewrite some chunks because vertice's chunks are moved , deleted
        /// of invalidate in the device reset case
        /// </summary>
        protected bool vInvalidateChunks = false;
        /// <summary>
        /// The only buffer managed by this cache, when disposed isn't necessary null it, will be done internaly
        /// </summary>
        protected VertexBuffer m_VB;
        /// <summary>
        /// Can change the chunk size to optimize resize function. Can not be &lt; 1 else size will not be resize
        /// </summary>
        /// <remarks>
        /// NewSize = NecessarySize + NecessarySize % ChunkSize
        /// NNewSize = NewSize > MAX ? MAX : NewSize
        /// </remarks>
        protected const int chunksizeV = ushort.MaxValue;
        /// <summary>
        /// Utility function to 
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        int GetVertexSizeUsingChunk(int size)
        {
            int newsize = (size / chunksizeV + 1) * chunksizeV;
            return newsize > MAXVERTS ? MAXVERTS : newsize;
        }

        /// <summary>
        /// For indexed derived class
        /// </summary>
        protected BatchCacheStatic(
            GraphicDevice mdevice,
            VertexDeclaration declaration,
            PrimitiveType primitive,
            EngineCore core,
            bool isIndexed)
            : base(mdevice, declaration, primitive, isIndexed, core, true, false)
        {
            vminChunkIndex = 0;
            vInvalidateChunks = false;

            // static buffer don't use managed resource, so need to link with device events
            device.resourcesToRestore.Add(this);
        }

        public BatchCacheStatic(
            GraphicDevice mdevice,
            VertexDeclaration declaration,
            PrimitiveType primitive,
            EngineCore core)
            : base(mdevice, declaration, primitive, false , core, true, false)
        {
            vminChunkIndex = 0;
            vInvalidateChunks = false;
            // static buffer don't use managed resource, so need to link with device events
            device.resourcesToRestore.Add(this);
        }

        /// <summary></summary>
        ~BatchCacheStatic()
        {
            Destroy();
        }
        
        /// <summary>
        /// Destroing a cache remove all bufferchunks, dispose the buffer, remove from device events and mark as removed
        /// </summary>
        public override void Destroy()
        {
            Clear();
            device.resourcesToRestore.Remove(this);
            m_removed = true;
        }

        public override void CleanBuffers()
        {
            // the cutting of unused space to decrease the buffer memory are already done in WriteAll()
        }

        /// <summary>
        /// Rewrite invalidated vertices chunks and optimize the size of buffer to look
        /// </summary>
        /// <param name="GPUvertices">the vertex buffer stream </param>
        /// <param name="vlockoffset">the offset used when open the buffer</param>
        protected void PrepareVertexBuffer(out int vlockoffset)
        {
            //Console.WriteLine("write list : "); foreach (Node node in ready2write) Console.WriteLine("* " + node.name);

            // nReservedIndis and nReservedVerts are the finally size of
            // buffer. The DxNode must not change so is fondamental what
            // WriteAll are called immediatly after Add process

            // check if need resize buffer : 
            // if nReservedIndis > IndexStream.Capacity mean size too small
            // if nReservedIndis < IndexStream.GetMinCapacity() mean size too big

            //if (m_VB == null) InitializeVB();

            // get new resize capacity
            //int newVertsCapacity = m_VB.GetSizeUsingChunk(nRequiredVerts);

            int newCapacity = nRequiredVerts + nRequiredVerts%chunksizeV;
            if (newCapacity>MAXVERTS) newCapacity = MAXVERTS;

            // resize test : if capacity is too small or is too big
            bool isVerticesResize = m_VB==null || nRequiredVerts > m_VB.Capacity || newCapacity < m_VB.Capacity;


            // if your buffer is reinitialized, you must rewrite previous data
            // the size value are calculated using minimum necessary size
            if (isVerticesResize)
            {
                //m_VB.Initialize(newVertsCapacity);
                vminChunkIndex = 0;
                m_VB = new VertexBuffer(device, base.m_Vdeclaration, newCapacity, false, false);

#if DEBUG_TIMECONSUMING
                Console.WriteLine("...initialize new vertexbuffer with " + newVertsCapacity);
#endif
            }

            // minimize the buffer lock size and avoid to overwrite previous valid data
            if (vminChunkIndex == 0)
            {
                vlockoffset = 0;
            }
            else
            {
                BatchChunkInfo lastchunk = (BatchChunkInfo)BufferChunks[vminChunkIndex - 1];
                vlockoffset = lastchunk.voffset + lastchunk.nverts;
            }

            
            // lock only necessary size
            if (nRequiredVerts - vlockoffset > 0)
            {
                m_VB.Open(vlockoffset, nRequiredVerts - vlockoffset);

                //GPUvertices = m_VB.Open(vlockoffset, nRequiredVerts - vlockoffset);
                //GPUvertices.name = "GPUvertices_offset" + vlockoffset;
            }
            else
            {
                throw new Exception("error");
            }

            //------------------------------
            // Rewrite previous vertices
            //------------------------------
            // num of vertices writted before
            int nvertices = vlockoffset;

#if DEBUG_TIMECONSUMING
            int t0 = Environment.TickCount;
#endif

            for (int i = vminChunkIndex; i < BufferChunks.Count; i++)
            {
                BatchChunkInfo chunk = (BatchChunkInfo)BufferChunks[i];
                Node node = chunk.node;
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                if (prop.m_ichunk != i)
                    throw new Exception("Something wrong with node <-> chunk reference");

#if DEBUG_TIMECONSUMING
                int t00 = Environment.TickCount;
#endif
                Array meshVertices = node.GetVerticesStream(!base.UseLocal, base.m_Vdeclaration);

#if DEBUG_TIMECONSUMING
                int t01 = Environment.TickCount;
#endif             
                
                if (meshVertices.Length != chunk.nverts)
                    throw new Exception("vertices stream length of current node is different from previous added, wrong way to update");

                m_VB.Write(meshVertices, nvertices - vlockoffset);
                //GPUvertices.Write(meshVertices, nvertices - vlockoffset);

#if DEBUG_TIMECONSUMING
                //int t02 = Environment.TickCount;
                if ((t01-t00)>0)
                Console.WriteLine(string.Format("..... get mesh vertices for node {0} in {1}", node.name, t01 - t00));
                //Console.WriteLine(string.Format("..... set mesh vertices in buffer in {0}", t02 - t01));
#endif
                // update old values
                prop.isInBuffer = true;
                // now this node will not be overwritten in the Overwrite process because its already done
                prop.needUpdateVertices = false;
                prop.m_ichunk = i;
                chunk.voffset = nvertices;
                nvertices += chunk.nverts;
            }
            // Now "nvertices" match with all vertices written so we can do a check
            m_VB.Count = nvertices;

#if DEBUG_TIMECONSUMING
            int t1 = Environment.TickCount;
            Console.WriteLine("...rewrite prev data in " + (t1 - t0));
#endif

            // check if num indices and num vertices previous added match with recalculated
            if (BufferChunks.Count > 0)
            {
                BatchChunkInfo lastchunk = BufferChunks[BufferChunks.Count - 1];
                if (nvertices != lastchunk.voffset + lastchunk.nverts)
                    throw new Exception("previous num of vertices don't match");
            }
        }  
        /// <summary>
        /// Open the minimum necessary size of buffer and overwrite vertices. The static buffer discard
        /// all data in this size, so need rewrite also all previos vertices
        /// </summary>
        protected void OverwriteVertices()
        {
            //Console.WriteLine("update list : "); foreach (Node node in ready2overwrite) Console.WriteLine("* " + node.name);

            //----------------
            // Only Vertices
            //----------------
            // find first and last chunk index to use for all nodes what need vertex rewriting
            // the Write process has already rewrite some data and marked needUpdateVertices to false
            int vmin = 0;
            int vmax = ready2overwrite.Count - 1;
            while (vmin < ready2overwrite.Count && !((NodeBatchProperties)ready2overwrite[vmin].engineProp).needUpdateVertices) vmin++;

            while (vmax > vmin && !((NodeBatchProperties)ready2overwrite[vmax].engineProp).needUpdateVertices) vmax--;

            if (vmin < ready2overwrite.Count)
            {
                int firstChunkIdx = ((NodeBatchProperties)ready2overwrite[vmin].engineProp).m_ichunk;
                int lastChunkIdx = ((NodeBatchProperties)ready2overwrite[vmax].engineProp).m_ichunk;

                BatchChunkInfo first = (BatchChunkInfo)BufferChunks[firstChunkIdx];
                BatchChunkInfo last = (BatchChunkInfo)BufferChunks[lastChunkIdx];

                if (first.voffset > last.voffset)
                    throw new Exception("ready2overwrite.sort() bug");


                int vlockoffset = first.voffset;
                int nverts = last.voffset + last.nverts - vlockoffset;

                m_VB.Open(vlockoffset, nverts);
                //BufferStream gpuVertices = m_VB.Open(vlockoffset, nverts);
                //gpuVertices.name = "GPUvertices_" + vlockoffset;

                nverts = 0; // now use it as offset

                // need to rewrite also all node inside this range and that not need update
                // because static buffer lost all data that are locked
                for (int i = firstChunkIdx; i <= lastChunkIdx; i++)
                {
                    BatchChunkInfo chunk = (BatchChunkInfo)BufferChunks[i];
                    Node node = chunk.node;

                    Array meshVertices = node.GetVerticesStream(!base.UseLocal, base.m_Vdeclaration);
                    m_VB.Write(meshVertices, nverts);
                    //gpuVertices.Write(meshVertices, nverts);
                    nverts += chunk.nverts;

                }
                for (int i = vmin; i <= vmax; i++)
                {
                    Node node = ready2overwrite[i];
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    prop.needUpdateVertices = false;
                    prop.stackoverflowcounter_Update = 0;
                    //EngineChunkInfo chunk = (EngineChunkInfo)BufferChunks[node.chunkIndex];

                    //Array meshVertices = node.GetVerticesStream(!base.UseLocal, base.vertexDeclaration);
                    //gpuVertices.Write(meshVertices, nverts);
                    //nverts += chunk.nverts;
                }
            }
            m_VB.Close();
        }
        /// <summary>
        /// Tell cache that device are reseted and need restore not managed resources, need rewrite all nodes when 
        /// run WriteAll function. Can't use Restore to repopulate disposed buffers, this because the geometry data are stored
        /// in the Node's classes and they can change the number of vertices or indices in the update process.
        /// </summary>
        public virtual void Restore()
        {
            vminChunkIndex = 0;
            vInvalidateChunks = true;
        }
        /// <summary>
        /// Removing all chunks data marked as removed but not push all value at the beginning of buffers because is a write only cache
        /// and the best way is rewrite them (and new data) in the WriteAll() function
        /// </summary>
        public override void DeleteAll()
        {
            if (ready2remove.Count == 0) return;

            //Console.WriteLine("remove list : "); foreach (Node node in ready2remove) Console.WriteLine("* " + node.name);

            // if all data are removed do a quick erase
            if (ready2remove.Count == BufferChunks.Count)
            {
                foreach (Node node in ready2remove)
                {
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    // reset node's flags
                    prop.isInBuffer = false;
                    prop.m_ichunk = -1;
                    prop.m_cache = null;
                    prop.stackoverflowcounter_Remove = 0;
                }

                BufferChunks.Clear();
                ready2remove.Clear();
                vInvalidateChunks = false;
                vminChunkIndex = 0;
                m_VB.Count = 0;

                // if buffer will not utilized int next process is a good idea destory buffers
                if (ready2write.Count == 0)
                {

                }

                return;
            }

            foreach (Node node in ready2remove)
            {
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                // memorize the first chunk index deleted
                int i = prop.m_ichunk;
                if (i < vminChunkIndex) vminChunkIndex = i;

                BatchChunkInfo chunk = BufferChunks[i];

                if (chunk.node != node)
                    throw new Exception("something wrong... buffer's chunk don't match");

                // mark as deleted
                chunk.node = null;
                // reset node's flags
                prop.isInBuffer = false;
                prop.m_ichunk = -1;
                prop.m_cache = null;
                prop.stackoverflowcounter_Remove = 0;
            }

            // remove all chunks marked, start from end to use correcly the index
            for (int i = BufferChunks.Count - 1; i >= vminChunkIndex; i--)
                if (BufferChunks[i].node == null)
                    BufferChunks.RemoveAt(i);

            // update the new node chunk index
            for (int i = vminChunkIndex; i < BufferChunks.Count; i++)
            {
                BatchChunkInfo chunk = BufferChunks[i];
                Node node = chunk.node;
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                prop.m_ichunk = i;
                // invalidate these data, they must be recalculated
                chunk.voffset = 0;
                // nverts and nindis not change, if are different from current node properties something are wrong
            }

            // calculate if where are some chunks that need moved down, else simply cut off buffers
            // VertexBuffer.Count and IndexBuffer.Count now are the number of values what not need to move
            vInvalidateChunks = BufferChunks.Count > vminChunkIndex;

            ready2remove.Clear();

            // update buffer count, will be recalculated if "vInvalidateChunks" in the Write process          
            if (vminChunkIndex == 0)
            {
                m_VB.Count = 0;
            }
            else
            {
                BatchChunkInfo lastchunk = (BatchChunkInfo)BufferChunks[vminChunkIndex - 1];
                m_VB.Count = lastchunk.voffset + lastchunk.nverts;
            }
        }
        /// <summary>
        /// All new node are added at end of buffer (append method)
        /// A trimBuffer implementation ensure the minimum size of buffer
        /// The VinvalidateChunks flag tell you what you need rewrite some data because they move at the beginning of buffer
        /// </summary>
        public override void WriteAll()
        {
            if (ready2write.Count == 0 && !vInvalidateChunks) return;
            
            int vlockoffset;
            PrepareVertexBuffer(out vlockoffset);
            
            // num of vertices written before append new nodes
            int nvertices = m_VB.Count;

            //------------------------------
            // Write new data
            //------------------------------
            if (ready2write.Count > 0)
            {
                base.AppendNewNodes(ready2write, ref nvertices, vlockoffset, m_VB);
            }

            if (nRequiredVerts != nvertices)
            {
                for (int i = 0; i < BufferChunks.Count; i++)
                {
                    BatchChunkInfo chunk = (BatchChunkInfo)BufferChunks[i];
                    Node node = chunk.node;

                    if (node.numVertices != chunk.nverts)
                        Console.WriteLine("node " + node.ToString() + " changed");
                }

                throw new Exception("some Node change their data during Flush process... ");
            }

            //------------------------------
            // Set new valid data counters
            //------------------------------
            m_VB.Count = nRequiredVerts;
            m_VB.Close();

            // set minchunkindex over last index to tell cache what all chunks are written
            vminChunkIndex = BufferChunks.Count;
            vInvalidateChunks = false;

            ready2write.Clear();
        }
        /// <summary>
        /// Overwriting method try to lock a smaller buffer size, the wrost case is when you update two nodes
        /// one in first and second in last position. A static cache can't be used to update many times
        /// </summary>
        public override void OverwriteAll()
        {
            if (ready2overwrite.Count == 0) return;
            // sort node using bufferchunk order
            ready2overwrite.Sort();
            OverwriteVertices();
            ready2overwrite.Clear();
        }

        public override string ToString()
        {
            return base.ToString() + "\r\nVBUFFER : " + m_VB.ToString() + "\r\n";
        }
    }

    /// <summary>
    /// INDEXED
    /// <see cref="EngineStaticCache"/>
    /// </summary>
    public abstract class BatchCacheStaticIndexed : BatchCacheStatic
    {
        /// <summary><see cref="chunksizeV"/></summary>
        protected const int chunksizeI = ushort.MaxValue * 6;
        
        /// <summary><see cref="BatchCacheStatic.VminChunkIndex"/> </summary>
        int iminChunkIndex = 0;
        
        /// <summary><see cref="BatchCacheStatic.VinvalidateChunks"/></summary>
        bool iInvalidateChunks = false;
        
        /// <summary></summary>
        protected IndexBuffer m_IB;
        
        /// <summary></summary>
        public BatchCacheStaticIndexed(
            GraphicDevice mdevice,
            VertexDeclaration declaration,
            PrimitiveType primitive,
            EngineCore Core)
            : base(mdevice, declaration, primitive, Core,true)
        {
            iminChunkIndex = 0;
            iInvalidateChunks = false;
        }
        

        /// <summary></summary>
        protected void PrepareIndexBuffer(out int ilockoffset)
        {
            // get new size
            int newCapacity = nRequiredIndis + nRequiredIndis % chunksizeI;
            if (newCapacity > MAXINDIS) newCapacity = MAXINDIS;
            
            
            // resize test : if capacity is too small or is too big
            bool isIndicesResize = m_IB==null || nRequiredIndis > m_IB.Capacity || newCapacity < m_IB.Capacity;
            
            // num of vertices before lock position, necessary to calculate index offset
            int nvertices;

            // if your buffer is reinitialized, you must rewrite previous data
            // the size value are calculated using minimum necessary size
            if (isIndicesResize)
            {
                Type indextype = base.indices32bit ? typeof(uint) : typeof(ushort);
                m_IB = new IndexBuffer(device, indextype, newCapacity, false, false);
                iminChunkIndex = 0;
            }
            // minimize the buffer lock size and avoid to overwrite previous valid data
            if (iminChunkIndex ==0)
            {
                ilockoffset = 0;
                nvertices = 0;
            }
            else
            {
                // the BufferChunks[iminChunkInde] is first invalidate chunk so need to get values from previos valid chunk
                BatchChunkInfoIndexed lastchunk = (BatchChunkInfoIndexed)BufferChunks[iminChunkIndex - 1];
                ilockoffset = lastchunk.ioffset + lastchunk.nindis;
                nvertices = lastchunk.voffset + lastchunk.nverts;
            }

            // lock only necessary size
            if (nRequiredIndis - ilockoffset > 0)
            {
                m_IB.Open(ilockoffset, nRequiredIndis - ilockoffset);
                //GPUindices = m_IB.Open(ilockoffset, nRequiredIndis - ilockoffset);
                //GPUindices.name = "GPUindices_offset" + ilockoffset;
            }
            else
            {
                throw new Exception("error");
            }

            //------------------------------
            // Rewrite previous indices
            //------------------------------
            // num of indices writted before
            int nindices = ilockoffset;
            for (int i = iminChunkIndex; i < BufferChunks.Count; i++)
            {
                BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                Node node = chunk.node;
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;


                if (prop.m_ichunk != i)
                    throw new Exception("Something wrong with node <-> chunk reference");

                int indexOffset = base.UseBatch ? nvertices : 0;

                Array meshIndices = node.GetIndicesStream(indexOffset, base.indices32bit);

                if (meshIndices.Length != chunk.nindis)
                    throw new Exception("indices stream of current node is different from previous added, wrong way to update");

                m_IB.Write(meshIndices, nindices - ilockoffset);
                //GPUindices.Write(meshIndices, nindices - ilockoffset);

                // update old values
                prop.isInBuffer = true;
                prop.needUpdateIndices = false;
                prop.m_ichunk = i;
                chunk.ioffset = nindices;
                nindices += chunk.nindis;
                nvertices += chunk.nverts;
            }

            // Now "nvertices" and "nindices" match with all data written so we can do a check
            m_IB.Count = nindices;
            if (m_VB.Count != nvertices)
                throw new Exception("previous num of vertices don't match");

            // check if num indices and num vertices previous added match with recalculated
            if (BufferChunks.Count > 0)
            {
                BatchChunkInfoIndexed lastchunk = (BatchChunkInfoIndexed)BufferChunks[BufferChunks.Count - 1];
                if (nvertices != lastchunk.voffset + lastchunk.nverts)
                    throw new Exception("previous num of vertices don't match");

                if (nindices != lastchunk.ioffset + lastchunk.nindis)
                    throw new Exception("previous num of indices don't match");
            }
        }
        /// <summary></summary>
        protected void OverwriteIndices()
        {
            //Console.WriteLine("update list : "); foreach (Node node in ready2overwrite) Console.WriteLine("* " + node.name);
            //----------------
            // Only Indices
            //----------------
            // find first and last chunk index to use for all nodes what need index rewriting
            int imin = 0;
            int imax = ready2overwrite.Count - 1;
            while (imin < ready2overwrite.Count && !((NodeBatchProperties)ready2overwrite[imin].engineProp).needUpdateIndices) imin++;
            while (imax > imin && !((NodeBatchProperties)ready2overwrite[imax].engineProp).needUpdateIndices) imax--;

            if (imin < ready2overwrite.Count)
            {
                int firstChunkIdx = ((NodeBatchProperties)ready2overwrite[imin].engineProp).m_ichunk;
                int lastChunkIdx = ((NodeBatchProperties)ready2overwrite[imax].engineProp).m_ichunk;

                BatchChunkInfoIndexed first = (BatchChunkInfoIndexed)BufferChunks[firstChunkIdx];
                BatchChunkInfoIndexed last = (BatchChunkInfoIndexed)BufferChunks[lastChunkIdx];

                if (first.ioffset > last.ioffset)
                    throw new Exception("ready2overwrite.sort() bug");

                
                int ilockoffset = first.ioffset;
                int nindis = last.ioffset + last.nindis - ilockoffset;

                m_IB.Open(ilockoffset, nindis);
                //BufferStream gpuIndices = m_IB.Open(ilockoffset, nindis);
                //gpuIndices.name = "GPUindices_" + ilockoffset;

                nindis = 0; // now use it as offset


                // need to rewrite also all node inside this range and that not need update
                // because static buffer lost all data that are locked
                for (int i = firstChunkIdx; i <= lastChunkIdx; i++)
                {
                    BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                    Node node = chunk.node;

                    Array meshIndices = node.GetIndicesStream(base.UseBatch ? chunk.voffset : 0, base.indices32bit);
                    //gpuIndices.Write(meshIndices, nindis);
                    m_IB.Write(meshIndices, nindis);
                    nindis += chunk.nindis;

                }

                for (int i = imin; i <= imax; i++)
                {
                    Node node = ready2overwrite[i];
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    prop.needUpdateIndices = false;
                    prop.stackoverflowcounter_Update = 0;
                    //EngineChunkInfo chunk = (EngineChunkInfo)BufferChunks[node.chunkIndex];

                    //Array meshIndices = node.GetIndicesStream(base.UseBatch ? chunk.voffset : 0, base.indices32bit);
                    //gpuIndices.Write(meshIndices, nindis);
                    //nindis += chunk.nindis;
                }
            }
            m_IB.Close();
        }     
        /// <summary> <see cref="BatchCacheStatic.Restore"/></summary>
        public override void Restore()
        {
            iInvalidateChunks = true;
            vInvalidateChunks = true;
            iminChunkIndex = 0;
            vminChunkIndex = 0;
        }
        /// <summary></summary>
        public override void DeleteAll()
        {
            if (ready2remove.Count == 0) return;

            //Console.WriteLine("remove list : "); foreach (Node node in ready2remove) Console.WriteLine("* " + node.name);
 
            // if all data are removed do a quick erase
            if (ready2remove.Count == BufferChunks.Count)
            {
                foreach (Node node in ready2remove)
                {
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    // reset node's flags

                    prop.isInBuffer = false;
                    prop.m_ichunk = -1;
                    prop.m_cache = null;
                    prop.stackoverflowcounter_Remove = 0;
                }
                BufferChunks.Clear();
                ready2remove.Clear();
                vInvalidateChunks = false;
                iInvalidateChunks = false;
                vminChunkIndex = 0;
                iminChunkIndex = 0;


                m_VB.Count = 0;
                m_IB.Count = 0;

                // if buffer will not utilized int next process is a good idea destory buffers
                if (ready2write.Count == 0)
                {

                }

                return;
            }


            foreach (Node node in ready2remove)
            {
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                // memorize the first chunk index to delete, the indexed data are the same
                if (prop.m_ichunk < vminChunkIndex)
                {
                    vminChunkIndex = prop.m_ichunk;
                    iminChunkIndex = prop.m_ichunk;
                }

                BatchChunkInfo chunk = BufferChunks[prop.m_ichunk];

                if (chunk.node != node)
                    throw new Exception("something wrong... buffer's chunk don't match");

                // mark as deleted
                chunk.node = null;
                // reset node's flags
                prop.isInBuffer = false;
                prop.m_ichunk = -1;
                prop.m_cache = null;
                prop.stackoverflowcounter_Remove = 0;
            }



            // remove all chunks marked, start from end to use correcly the index
            for (int i = BufferChunks.Count - 1; i >= vminChunkIndex; i--)
                if (BufferChunks[i].node == null)
                    BufferChunks.RemoveAt(i);

            // update the new node chunk index
            for (int i = vminChunkIndex; i < BufferChunks.Count; i++)
            {
                BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                Node node = chunk.node;
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                prop.m_ichunk = i;
                // invalidate these data, they must be recalculated
                chunk.voffset = 0;
                chunk.ioffset = 0;
                // nverts and nindis not change, if are different from current node properties something are wrong
            }

            // calculate if where are some chunks that need moved down, else simply cut off buffers
            // VertexBuffer.Count and IndexBuffer.Count now are the number of values what not need to move
            vInvalidateChunks = iInvalidateChunks = BufferChunks.Count > vminChunkIndex;

            ready2remove.Clear();

            // set the new buffer count, will be update int the write process if "vInvalidateChunks"
            if (vminChunkIndex == 0)
            {
                m_VB.Count = m_IB.Count = 0;
            }
            else
            {
                BatchChunkInfoIndexed lastchunk = (BatchChunkInfoIndexed)BufferChunks[vminChunkIndex - 1];
                m_VB.Count = lastchunk.voffset + lastchunk.nverts;
                m_IB.Count = lastchunk.ioffset + lastchunk.nindis;
            }

            
        }
        /// <summary></summary>
        public override void WriteAll()
        {
            if (ready2write.Count == 0 && !vInvalidateChunks && !iInvalidateChunks) return;

            //BufferStream GPUvertices, GPUindices;
            int vlockoffset, ilockoffset;
            

#if DEBUG_TIMECONSUMING
            int t0 = Environment.TickCount;
#endif
            PrepareVertexBuffer(out vlockoffset);

#if DEBUG_TIMECONSUMING
            int t1 = Environment.TickCount;
#endif
            PrepareIndexBuffer(out ilockoffset);

#if DEBUG_TIMECONSUMING
            int t2 = Environment.TickCount;
#endif

            // num of vertices written before append new nodes
            int nvertices = m_VB.Count;
            int nindices = m_IB.Count;


            //------------------------------
            // Write new data
            //------------------------------
            if (ready2write.Count > 0)
            {
                base.AppendNewNodes(ready2write, ref nvertices, vlockoffset, m_VB, ref nindices, ilockoffset, m_IB);
            }

            if (nRequiredVerts != nvertices || nRequiredIndis != nindices)
            {
                for (int i = 0; i < BufferChunks.Count; i++)
                {
                    BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                    Node node = chunk.node;

                    if (node.numVertices != chunk.nverts || node.numIndices != chunk.nindis)
                        Console.WriteLine("node " + node.ToString() + " changed");
                }

                throw new Exception("some Node change their data during Flush process... ");
            }

            //------------------------------
            // Set new valid data counters
            //------------------------------
            m_IB.Count = nRequiredIndis;
            m_IB.Close();

            m_VB.Count = nRequiredVerts;
            m_VB.Close();

            // set minchunkindex over last index to tell cache what all chunks are written
            iminChunkIndex = vminChunkIndex = BufferChunks.Count;
            iInvalidateChunks = vInvalidateChunks = false;
            
            ready2write.Clear();

#if DEBUG_TIMECONSUMING
            int t3 = Environment.TickCount;
            Console.WriteLine("..writeall prepareV in " + (t1 - t0));
            Console.WriteLine("..writeall prepareI in " + (t2 - t1));
            Console.WriteLine("..writeall  addnew  in " + (t3 - t2));
#endif

        }
        /// <summary></summary>
        public override void OverwriteAll()
        {
            if (ready2overwrite.Count == 0) return;
            // sort node using bufferchunk order
            ready2overwrite.Sort();

            OverwriteVertices();
            OverwriteIndices();
            
            ready2overwrite.Clear();
        }
    
        public override string ToString()
        {
            return base.ToString() + "\r\nIBUFFER : " + m_IB.ToString() + "\r\n";
        }
    }
}
