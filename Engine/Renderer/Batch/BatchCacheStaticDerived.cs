﻿using System;
using System.Drawing;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;

namespace Engine.Renderer
{
    /// <summary>
    /// Cache used for indexed triangles, triangle.strip and triangle.fan will be converted into indexed triangles for semplicity
    /// </summary>
    public class BatchCacheStatic_TriMesh : BatchCacheStaticIndexed , INodeMaterial
    {
        public NodeMaterial m_material { get; set; }
        public uint m_material_key { get { return m_material != null ? m_material.m_key : 0; } }

        /// <summary>
        /// </summary>
        public BatchCacheStatic_TriMesh(GraphicDevice mdevice, VertexDeclaration declaration, NodeMaterial material, EngineCore Core)
            : base(mdevice, declaration, PrimitiveType.TriangleList, Core)
        {
            if (material != null)
            {
                // not in the main collector
                if (material.m_key == 0)
                {
                    core.Add(material);
                }
                m_material = material;
                m_material.numassignment++;
            }
            else
            {
                m_material = null;
            }
        }
        /// <summary>
        /// Decrease also the material counter
        /// </summary>
        public override void Destroy()
        {
            base.Destroy();
            if (m_material != null) m_material.numassignment--;
        }
        /// <summary>
        /// Sorting using material is a good idea
        /// </summary>
        public override int CompareTo(object obj)
        {
            return NodeMaterial.CompareByTexture(this, (INodeMaterial)obj);
        }     
        /// <summary>
        /// test is this node can be stored here
        /// </summary>
        public override bool isCompatible(Node node)
        {
            //check if is a triangle primitive
            if (!(node is PrimitiveTriangle)) return false;
            
            PrimitiveTriangle mesh = (PrimitiveTriangle)node;

            //check if it using the same vertex format, same texture and same rendermode
            return (mesh.vertexFormat == this.vertexFormat) && (mesh.m_material_key == m_material_key) && !mesh.engineProp.isAnimable;
        }
        /// <summary>
        /// Test if my functions have worked correctly
        /// </summary>
        public override bool DebugFlushAllProcess()
        {
            if (m_IB.Count != nRequiredIndis || m_IB.Count % 3 != 0)
                throw new Exception("incoerent value : " + this.m_IB.Count + " != " + nRequiredIndis);
            if (m_VB.Count != nRequiredVerts)
                throw new Exception("incoerent value : " + this.m_VB.Count + " != " + nRequiredVerts);
            return true;
        }
        /// <summary>
        /// Draw methods for triangle mesh geometry
        /// </summary>
        protected override void DrawImplementation()
        {
            if (m_VB == null || m_VB.Count == 0) return;

            // Set material
            if (NodeMaterial.SetToDevice(device, m_material, m_Vdeclaration.Contain(VertexFormat.Texture1)))
                core.nTotTextureSet++;

            m_IB.SetToDevice();
            m_VB.SetToDevice();

            device.m_device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_VB.Count, 0, m_IB.Count / 3);
            core.nTotDrawCall++;
        }
    }

    /// <summary>
    /// Cache used for indexed lines, line.strip will be converted into indexed line for semplicity
    /// </summary>
    public class BatchCacheStatic_Spline : BatchCacheStaticIndexed
    {
        public override void DeleteAll()
        {
            int count = this.ready2remove.Count;
            base.DeleteAll();
            count = this.ready2remove.Count;
        }
        /// <summary>
        /// </summary>
        public BatchCacheStatic_Spline(GraphicDevice mdevice, VertexDeclaration declaration, EngineCore Core)
            : base(mdevice, declaration, PrimitiveType.LineList, Core)
        {
            // for lines the textures are incompatible
        }

        public override bool isCompatible(Node node)
        {
            //check if is a triangle primitive
            if (!(node is PrimitiveLine)) return false;
            PrimitiveLine spline = (PrimitiveLine)node;

            //check if it using the same vertex format

            return (spline.vertexFormat == this.vertexFormat) && !spline.engineProp.isAnimable;
        }

        /// <summary>
        /// Test if my functions have worked correctly
        /// </summary>
        public override bool DebugFlushAllProcess()
        {
            if (m_IB.Count != nRequiredIndis || m_IB.Count % 2 != 0)
                throw new Exception("incoerent value : " + this.m_IB.Count + " != " + nRequiredIndis);
            if (m_VB.Count != nRequiredVerts)
                throw new Exception("incoerent value : " + this.m_VB.Count + " != " + nRequiredVerts);
            return true;
        }

        protected override void DrawImplementation()
        {
            if (m_VB == null || m_VB.Count == 0) return;

            NodeMaterial.RemoveFromDevice(device);

            m_IB.SetToDevice();
            m_VB.SetToDevice();

            device.m_device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, m_VB.Count, 0, m_IB.Count / 2);
            core.nTotDrawCall++;
        }
    }
}
