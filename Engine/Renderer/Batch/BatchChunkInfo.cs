﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;

namespace Engine.Renderer
{
    /// <summary>
    /// Address informations of chunks data
    /// </summary>
    public class BatchChunkInfo : IComparable<BatchChunkInfo> 
    {
        public bool show { get; set; }
        /// <summary>
        /// num of vertices in the buffer
        /// </summary>
        public int nverts { get; set; }
        /// <summary>
        /// vertices offset in the buffer 
        /// </summary>
        public int voffset { get; set; }
        /// <summary>
        /// node handle
        /// </summary>
        public Node node = null;
        /// <summary>
        /// for NOT indexed geometry in buffer
        /// </summary>
        public BatchChunkInfo()
        {
            node = null;
            show = true;
            nverts = 0;
            voffset = 0;
        }
        /// <summary>
        /// Sort the chunk list using vertex offset order
        /// </summary>
        public int CompareTo(BatchChunkInfo other)
        {
            return this.voffset.CompareTo(other.voffset);
        }

        /// <summary>
        /// Converting a not-indexed to indexed chunk set the indexed values to zero
        /// </summary>
        public BatchChunkInfoIndexed AsIndexed(BatchChunkInfo info)
        {
            return new BatchChunkInfoIndexed
            {
                show = info.show,
                nverts = info.nverts,
                voffset = info.voffset,
                node = info.node,
                nindis = 0,
                minverts = 0,
                ioffset = 0
            };
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(String.Format("H:{0}", node != null ? node.m_key.ToString("X4") : "NULL"));
            str.Append(String.Format("v:{0,-4} vs:{1,-4} ", nverts, voffset));
            return str.ToString();
        }
    }

    /// <summary>
    /// Address informations of indexed chunks data, the sorting was made always only by vertices order
    /// </summary>
    public class BatchChunkInfoIndexed : BatchChunkInfo
    {
        /// <summary>
        /// minimum vertex index in the indices list, not used
        /// </summary>
        public int minverts { get; set; }
        /// <summary>
        /// num of indices int the buffer
        /// </summary>
        public int nindis { get; set; }
        /// <summary>
        /// indices offset in the buffer
        /// </summary>
        public int ioffset { get; set; }
        
        /// <summary>
        /// for indexed geometry in buffer
        /// </summary>
        public BatchChunkInfoIndexed() : base()
        {
            minverts = 0;
            nindis = 0;
            ioffset = 0;
        }

        public override string ToString()
        {
            return base.ToString() + String.Format("vmin:{0,-4} i:{1,-4} is:{2,-4} ", minverts, nindis, ioffset);
        }
    }
}