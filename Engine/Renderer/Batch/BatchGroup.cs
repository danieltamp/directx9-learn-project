﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// A Group class manage the relavite caches list. Contain Add, Remove and Update node functions to manage the
    /// changes of node in the core.
    /// </summary>
    /// <remarks>
    /// The "temporary" functions are the first step of nodes's job, optimize all input/output changes BEFORE drawing
    /// The "completing" functions are the second step of nodes's job, are called AFTER temporary function and at the beginning of draw function.
    /// So the temporary functions optimize the write remove functions. With is very usefull when you need to add a lot of nodes in the same time.
    /// </remarks>
    public abstract class BatchGroup
    {
        /// <summary>
        /// The Maximum number of batching groups or the "Draw" call, the goal is to reduce it to minimum.
        /// </summary>
        const int MAXCACHE = 60;
        Matrix4 globalCoordSys = Matrix4.Identity;

        protected EngineCore core;
        protected GraphicDevice m_device;
        protected List<Node> nodesToWrite =  new List<Node>();     // containing test in DxNode.isInWrite value
        protected List<Node> nodesToRemove = new List<Node>();     // containing test in DxNode.isInRemove value
        protected List<Node> nodesToOverwrite = new List<Node>();  // containing test in DxNode.isInOverwrite value

        public OrderedCollectionManager<BatchCache> m_cache = new OrderedCollectionManager<BatchCache>(true);

        /// <summary>
        /// Set the coordinate system used for all caches
        /// </summary>
        public Matrix4 GroupWorld
        {
            get { return globalCoordSys; }
            set { globalCoordSys = value; foreach (BatchCache cache in m_cache) cache.cacheWorld = value; }
        }

        /// <summary>
        /// Generic group class
        /// </summary>
        public BatchGroup(GraphicDevice device, EngineCore Core)
        {
            core = Core; 
            this.m_device = device;
        }

        public int nTotCache { get { return m_cache.Count; } }
        public int nTotIndices
        {
            get
            {
                int n = 0;
                foreach (BatchCache cache in m_cache) n += cache.numIndices;
                return n;
            }
        }
        public int nTotVerts
        {
            get
            {
                int n = 0;
                foreach (BatchCache cache in m_cache) n += cache.numVertices;
                return n;
            }
        }
        public int nTotNodes
        {
            get
            {
                int n = 0;
                foreach (BatchCache cache in m_cache) n += cache.numNodes;
                return n;
            }
        }

        /// <summary>
        /// Get a new cache using DxNode setting
        /// </summary>
        public abstract BatchCache AddNewCache(Node fornode);

        /// <summary>
        /// when work with buffer need to check if there are some bugs, if it generate some exception
        /// you need to debug code
        /// </summary>
        /// <exception cref="System.Exception">node.cacheKey == 0</exception>
        /// <exception cref="System.Exception">m_cache[node.cacheKey] not exist</exception>
        /// <exception cref="System.Exception">m_cache[node.cacheKey] don't contain node</exception>
        void CheckNode(Node node)
        {
            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

            if (prop.m_cache==null)
            {
                throw new NullReferenceException("node cache is null");
            }
            if (!prop.m_cache.Contain(node))
            {
                throw new Exception("cache " + prop.m_cache.GetType().ToString() + " don't contain node");
            }
        }
        /// <summary>
        /// One Draw call for each caches
        /// </summary>
        public void Draw()
        {
            foreach (BatchCache cache in m_cache) cache.Draw();
        }

        public void removeallnodesref()
        {
            foreach (BatchCache cache in m_cache)
            {
                cache.removeallnodesref();
            }
        }
        /// <summary>
        /// Remove all nodes from all caches, delete all caches
        /// </summary>
        public void Destroy()
        {
            foreach (BatchCache cache in m_cache)
            {
                cache.Destroy();
            }
            m_cache.Clear();
            nodesToWrite.Clear();
            nodesToRemove.Clear();
            nodesToOverwrite.Clear();
            //nodesRemoved.Clear();
        }
        /// <summary>
        /// Remove unused caches and clean remain
        /// </summary>
        protected void Clean()
        {
            foreach (BatchCache cache in m_cache)
            {
                if (cache.isEmpty)
                {
                    cache.Destroy();
                    cache.m_removed = true;
                }
            }
            m_cache.Clean();

            // clean remaining caches
            foreach (BatchCache cache in m_cache)
                cache.CleanBuffers();
        }
        /// <summary>
        /// Safety test if node are stored here
        /// </summary>
        public bool Contain(Node node)
        {
            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

            if (prop.isInBuffer)
            {
                if (prop.m_cache == null || !prop.m_cache.Contain(node))
                {
                    Console.WriteLine("node cache " + prop.m_cache.GetType().ToString() + " not exist");
                    return false;
                }
            }
            else
            {
                if ((prop.isInRemove && nodesToRemove.Contains(node)) ||
                    (prop.isInWrite && nodesToWrite.Contains(node)) ||
                    (prop.isInOverwrite && nodesToOverwrite.Contains(node)))
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("node " + node.m_key + " not exist in the temp lists");
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Safety test if some cache contain this material
        /// </summary>
        public virtual bool Contain(NodeMaterial material)
        {
            return false;
        }

        ////////////////////////////////////////////////////
        // the DxNode can still change, so add to temporary
        // list, when list are flushed i assume that DxNode
        // is fixed (not indices and vertices number variations)
        ////////////////////////////////////////////////////
        
        #region temporary functions
    
        public bool AddNode(Node node)
        {
            bool result = false;
            //Console.WriteLine("add node " + node.handle.ToString("X4"));
            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
            
            // basic case : Not In Buffer, remove from all and add do write list
            if (!prop.isInBuffer)
            {
                if (prop.isInRemove)
                {
                    nodesToRemove.Remove(node);
                    prop.isInRemove = false;
                    result = true;
                }
                if (prop.isInOverwrite)
                {
                    nodesToOverwrite.Remove(node);
                    prop.isInOverwrite = false;
                    result = true;
                }
                if (!prop.isInWrite)
                {
                    nodesToWrite.Add(node);
                    prop.isInWrite = true;
                    result = true;
                }
            }
            // updating case : In Buffer
            else
            {
                result = UpdateNode(node);
            }
            return result;
        }
        public bool Remove(Node node)
        {
            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
            bool result = false;
            
            // can't remove a node that not exist in this cache
            if (prop.chunkflags == BatchChunkBufferFlags.None) return false;

            // basic case : Not In Buffer , remove from all
            if (!prop.isInBuffer)
            {
                if (prop.isInWrite)
                {
                    nodesToWrite.Remove(node);
                    prop.isInWrite = false;
                    result = true;
                }
                if (prop.isInOverwrite)
                {
                    nodesToOverwrite.Remove(node);
                    prop.isInOverwrite = false;
                    result = true;
                }
                if (prop.isInRemove)
                {
                    nodesToRemove.Remove(node);
                    prop.isInRemove = false;
                    result = true;
                }
            }
            // removing case : In Buffer , remove from all and add to Remove list
            else
            {
                CheckNode(node);

                if (prop.isInWrite)
                {
                    nodesToWrite.Remove(node);
                    prop.isInWrite = false;
                    result = true;
                }
                if (prop.isInOverwrite)
                {
                    nodesToOverwrite.Remove(node);
                    prop.isInOverwrite = false;
                    result = true;
                }
                if (!prop.isInRemove)
                {
                    nodesToRemove.Add(node);
                    prop.isInRemove = true;
                    result = true;
                }
            }
            return result;
        }     
        public bool UpdateNode(Node node)
        {
            //Console.WriteLine("update node " + node.ToString());

            NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

            // basic case : Not In Buffer , do nothing and not touch write and remove list
            if (!prop.isInBuffer)
            {
                Debug.WriteLine("can't update a node not in buffer");
                return false;

                //AddNode(node); need to test this way
            }
            else
            {
                CheckNode(node);

                BatchCache cache = prop.m_cache;
                
                // only Transform require 
                if (prop.updateflags == UpdateFlags.Transform)
                {
                    // if you don't use batch and you use local isn't necessary any operation
                    if (!cache.UseBatch && cache.UseLocal)
                    {
                        prop.updateflags = UpdateFlags.None;
                        return true;
                    }
                }
                // in all other case change transform require to recalculate vertices
                if (prop.needUpdateTransform) prop.needUpdateVertices = true;
                
                
                // fix the flags value
                if (prop.updateflags == UpdateFlags.RenderState ||
                    cache.vertexFormat != node.vertexFormat ||
                    cache.numChunkVertices(node) != node.numVertices ||
                    cache.numChunkIndices(node) != node.numIndices ||
                    ((cache is INodeMaterial) && ((INodeMaterial)cache).m_material_key != ((INodeMaterial)node).m_material_key))
                {
                    prop.updateflags = UpdateFlags.RenderState;
                }


                // case: the render mode was changed, the size is different or the user set a force updating flag : force re-add
                // ATTENTION EngineGroup.AddNode() function call this function if check an updating case, so don't call AddNode() in this
                // function to avoid infinite loop
                if (prop.updateflags == UpdateFlags.RenderState)
                {
                    if (prop.isInOverwrite)
                    {
                        nodesToOverwrite.Remove(node);
                        prop.isInOverwrite = false;
                    }
                    if (!prop.isInRemove)
                    {
                        nodesToRemove.Add(node);
                        prop.isInRemove = true;
                    }
                    if (!prop.isInWrite)
                    {
                        nodesToWrite.Add(node);
                        prop.isInWrite = true;
                    }
                    return true;
                }
                // case: same size, the chunk's data will be overwrite
                else
                {
                    if (!prop.isInOverwrite)
                    {
                        nodesToOverwrite.Add(node);
                        prop.isInOverwrite = true;
                    }
                    if (prop.isInRemove)
                    {
                        nodesToRemove.Remove(node);
                        prop.isInRemove = false;
                    }
                    if (prop.isInWrite)
                    {
                        nodesToWrite.Remove(node);
                        prop.isInWrite = false;
                    }
                    return true;
                }
            }
        }
        #endregion

        ////////////////////////////////////////////////////
        // the DxNode can't change so need to call DeleteAll
        // and WriteAll in the same time. To enable the Updating
        // the removing operation run before adding.
        ////////////////////////////////////////////////////

        #region completing functions
        /// <summary>
        /// Complete all temp job : Remove + Add + Overwrite and return a list with a reference of all node that will
        /// be deleted from core
        /// </summary>
        /// <param name="nodeCompletlyRemoved">DEPRECATED : a list what contain all node completly removed from all caches</param>
        public void FlushAll(ref List<Node> nodeCompletlyRemoved)
        {
            // add all node what will be removed now but they aren't added in a second time in the WriteAll process
            //foreach (Node node in nodesToRemove) if (!node.isInWrite) nodeCompletlyRemoved.Add(node);
 #if DEBUG_TIMECONSUMING           
            int t0 = Environment.TickCount;
#endif
            DeleteAll(); // do the jobs for all caches
            foreach (BatchCache cache in m_cache) cache.DeleteAll();

 #if DEBUG_TIMECONSUMING           
            int t1 = Environment.TickCount;
#endif
            WriteAll();
            foreach (BatchCache cache in m_cache) cache.WriteAll();

#if DEBUG_TIMECONSUMING
            int t2 = Environment.TickCount;
#endif
            OverwriteAll();
            foreach (BatchCache cache in m_cache) cache.OverwriteAll();

#if DEBUG_TIMECONSUMING
            int t3 = Environment.TickCount;
#endif
            foreach (BatchCache cache in m_cache) cache.DebugFlushAllProcess();


            // update flags
            foreach (Node node in nodesToRemove) ((NodeBatchProperties)node.engineProp).isInRemove = false;
            nodesToRemove.Clear();
            foreach (Node node in nodesToWrite) ((NodeBatchProperties)node.engineProp).isInWrite = false;
            nodesToWrite.Clear();
            foreach (Node node in nodesToOverwrite) ((NodeBatchProperties)node.engineProp).isInOverwrite = false;
            nodesToOverwrite.Clear();

            // special operation to clean caches, not very usefull
            Clean();

            // sort caches using a function to reduce the change states
            // For first test i simply sort cache priority by texture key
            m_cache.Sort();

#if DEBUG_TIMECONSUMING
            int t4 = Environment.TickCount;

            Console.WriteLine(".deleteall in " + (t1 - t0));
            Console.WriteLine(".writeall  in " + (t2 - t1));
            Console.WriteLine(".overwrite in " + (t3 - t2));
            Console.WriteLine(".complete  in " + (t4 - t3));
#endif

        }  
        protected void DeleteAll()
        {
            if (nodesToRemove.Count == 0) return;

            //Console.WriteLine("Deleting");
            int vertsDeleted = 0;
            int indisDeleted = 0;

            for (int n = 0; n < nodesToRemove.Count; n++)
            {
                int vertices, indices;
                Node node = nodesToRemove[n];

                BatchCache cache = ((NodeBatchProperties)node.engineProp).m_cache;

                if (!cache.Contain(node))
                    throw new Exception("Lost node, the cache are wrong");

                RESULT result = cache.Remove(node, out vertices, out indices);

                if (result == RESULT.OK)
                {
                    vertsDeleted += vertices;
                    indisDeleted += indices;
                }
                else
                    throw new Exception("Error deleting node " + result.ToString());
            }

            //nTotVerts -= vertsDeleted;
            //nTotIndices -= indisDeleted;
            //nTotNodes -= nodesToRemove.Count;
        }     
        protected void WriteAll()
        {
            if (nodesToWrite.Count == 0) return;
            //Console.WriteLine("Adding");

            // handlesToWrite will be cleared alfer complete this
            for (int n = 0; n < nodesToWrite.Count; n++)
            {
                Node node = nodesToWrite[n];
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                //Console.WriteLine("Node " + handle.ToString("X4"));

                if (prop.isInBuffer)
                {
                    throw new Exception("Error , node in buffer, isn't the correct way to overwrite an existing node, must be remove");
                }
                // set to false
                RESULT result = RESULT.ERR;
                // set the first cache to test
                int nCache = 0;
                int nVerts = 0;
                int nIndis = 0;
                prop.m_cache = null;
                prop.m_ichunk = -1;

                // find a valid cache where node can be stored. The cache.add method return INCOMPATIBLE
                // if the class of node isn't compatible with DxTriMesh or DxSplineShape cache types
                for (nCache = 0; nCache < nTotCache; nCache++)
                {
                    BatchCache cache = m_cache[nCache];
                    if (cache.isCompatible(node))
                    {
                        // if cache are full the result aren't OK
                        result = cache.Add(node, out nVerts, out nIndis);
                        prop.m_cache = cache;

                        if (result == RESULT.OK)
                        {
                            break;
                        }
                        else
                        {
                            //Console.WriteLine(node.handle.ToString("X4") + " can't be added : " + result.ToString());
                        }
                    }
                    else
                    {
                        //Console.WriteLine(node.handle.ToString("X4") + " is incompatible for all caches");
                    }
                }
                // if result == RESULT.OK jump to next node, else try to add a new cache
                
                if (result != RESULT.OK)
                {
                    // all cache incompatible or full, add new one
                    if (nTotCache < MAXCACHE)
                    {
                        m_cache.Add(AddNewCache(node));
                    }
                    else
                    {
                        throw new OutOfMemoryException("Error , can't add new cache , safety limit reach");
                    }
                    BatchCache cache = m_cache[nCache];

                    // some errors that can not be happen
                    result = m_cache[nCache].Add(node, out nVerts, out nIndis);               
                    if (result != RESULT.OK)
                    {
                        //handlesUnManaged.Add(node.handle);
                        throw new Exception("Error , can't add " + node.m_key + " to new cache" + cache.ToString() + " : " + result.ToString());
                    }
                    prop.m_cache = cache;
                }

                //nTotIndices += nIndis;
                //nTotVerts += nVerts;
                //nTotNodes++;
            }
        }
        protected void OverwriteAll()
        {
            if (nodesToOverwrite.Count == 0) return;

            for (int n = 0; n < nodesToOverwrite.Count; n++)
            {
                Node node = nodesToOverwrite[n];
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                RESULT result = prop.m_cache.OverWrite(node);
                
                if (result == RESULT.OK)
                {
                }
                else
                {
                    throw new Exception("Error updating node " + result.ToString());
                }
            }
        }
        #endregion

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("@@@ BatchGroup : " + this.GetType().ToString() + " @@@");
            str.AppendLine(m_cache.ToString());
            return str.ToString();
        }
    }
}
