﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using Engine.Tools;
using Engine.Graphics;

namespace Engine.Renderer
{
#pragma warning disable
    /// <summary>
    /// Manage the Indexed Triangles Groups. All pure triangle list, triangle fan and triangle strip primitives are converted into 
    /// indexed triangle list for semplicity
    /// </summary>
    public class BatchGroupTriangle : BatchGroup
    {
        public BatchGroupTriangle(GraphicDevice device, EngineCore core)
            : base(device, core)
        {
        }
        public override BatchCache AddNewCache(Node fornode)
        {
            PrimitiveTriangle node = (PrimitiveTriangle)fornode;

            VertexDeclaration declaration;

            switch (node.vertexFormat)
            {
                case (VertexFormat.Position | VertexFormat.Diffuse):
                    declaration = VertexDeclaration.GetDeclaration<CVERTEX>(m_device);
                    break;
                //case (VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1): declaration = NTVERTEX.Declaration; break;
                case (VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1):
                    declaration = VertexDeclaration.GetDeclaration<NTVERTEX16>(m_device);
                    break;
                //case (VertexFormat.Position | VertexFormat.Normal | VertexFormat.Diffuse): declaration = NCVERTEX.Declaration; break;
                case (VertexFormat.Position | VertexFormat.Normal | VertexFormat.Diffuse):
                    declaration = VertexDeclaration.GetDeclaration<NCVERTEX16>(m_device);
                    break;
                default:
                    declaration = VertexDeclaration.GetDeclaration<VERTEX>(m_device);
                    break;
            }
            // TO DO : need to implement a optimization because isn't true that all caches need the same algorithm...
            bool UseBatch = false;
            bool UseLocal = true;

            BatchCache cache = node.engineProp.isAnimable ?
                //(BatchCache)new BatchCacheDynamic_TriMesh(device, declaration, node.m_material, core, UseBatch, UseLocal) :
                (BatchCache)new BatchCacheStatic_TriMesh(m_device, declaration, node.m_material, core) :
                (BatchCache)new BatchCacheStatic_TriMesh(m_device, declaration, node.m_material, core);

            if (!cache.isCompatible(node))
                throw new Exception("Cache generator return a not compatible cache");

            return cache;
        }


        public override bool Contain(NodeMaterial material)
        {
            foreach (INodeMaterial cache in m_cache)
                if (cache.m_material == material) return true;
            return false;
        }


    }
    /// <summary>
    /// Manage the Indexed Lines Groups. All pure line list and line stip primitives are converted into 
    /// indexed line list for semplicity
    /// </summary>
    public class BatchGroupLine : BatchGroup
    {
        public BatchGroupLine(GraphicDevice device, EngineCore core)
            : base(device, core)
        {
        }
        public override BatchCache AddNewCache(Node fornode)
        {
            PrimitiveLine node = (PrimitiveLine)fornode;
            VertexFormat vFormat = node.vertexFormat;
            VertexDeclaration declaration;
            switch (node.vertexFormat)
            {
                case (VertexFormat.Position | VertexFormat.Diffuse):
                    declaration = VertexDeclaration.GetDeclaration<CVERTEX>(m_device);
                    break;
                default:
                    declaration = VertexDeclaration.GetDeclaration<VERTEX>(m_device);
                    break;
            }

            // TO DO : need to implement a optimization because isn't true that all caches need the same algorithm...
            bool UseBatch = false;
            bool UseLocal = true;

            BatchCache cache = node.engineProp.isAnimable ?
                //(BatchCache)new BatchCacheDynamic_Spline(device, declaration, core, UseBatch, UseLocal) :
                (BatchCache)new BatchCacheStatic_Spline(m_device, declaration, core) :
                (BatchCache)new BatchCacheStatic_Spline(m_device, declaration, core);

            if (!cache.isCompatible(node))
                throw new Exception("Cache generator return a not compatible cache");

            return cache;
        }
    }

    /// <summary>
    /// Manage the Not-Indexed Points Groups.
    /// </summary>  
    public class BatchGroupPoint : BatchGroup
    {
        public BatchGroupPoint(GraphicDevice device, EngineCore core)
            : base(device, core)
        {
        }
        public override BatchCache AddNewCache(Node fornode)
        {
            PrimitivePoint node = (PrimitivePoint)fornode;
            VertexDeclaration declaration;
            switch (node.vertexFormat)
            {
                case (VertexFormat.Position | VertexFormat.Diffuse):
                    declaration = VertexDeclaration.GetDeclaration<CVERTEX>(m_device);
                    break;
                default:
                    declaration = VertexDeclaration.GetDeclaration<VERTEX>(m_device);
                    break;
            }

            throw new NotImplementedException("");
        }
    }
#pragma warning restore

}
