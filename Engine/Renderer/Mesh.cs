﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Indipendent mesh with a vertexbuffer and indexbuffer
    /// </summary>
    public class Mesh
    {
        PrimitiveType m_type;
        VertexBuffer m_vb;
        IndexBuffer m_ib;
        VertexDeclaration m_decl;
        GraphicDevice m_device;
        int numprimitives;

        /// <summary>
        /// Indexed mesh
        /// </summary>
        public Mesh(GraphicDevice device, PrimitiveType type, Array vertices, Array indices)
            : this(device,type,vertices)
        {
            m_type = type;
            int numindices = indices.GetUpperBound(0) + 1;
            Type itype = indices.GetType().GetElementType();
           
            m_ib = new IndexBuffer(device, itype, numindices, false, true);
            m_ib.Open();
            m_ib.Write(indices);
            m_ib.Close();
            m_ib.Count = numindices;


            switch (type)
            {
                case PrimitiveType.PointList: 
                    if (itype != typeof(uint) && itype != typeof(ushort) && itype != typeof(int) && itype != typeof(short))
                        throw new ArgumentException("invalid index type");
                    else
                        numprimitives = numindices;
                    break;

                case PrimitiveType.TriangleList:
                    if (itype == typeof(Edge))
                    {
                        throw new ArgumentException("invalid index type");
                    }
                    else if (itype == typeof(Face))
                    {
                        numprimitives = numindices;
                    }
                    else if (itype != typeof(uint) && itype != typeof(ushort) && itype != typeof(int) && itype != typeof(short))
                    {
                        throw new ArgumentException("invalid index type");
                    }
                    else
                    {
                        numprimitives = numindices / 3;
                    }
                    break;

                case PrimitiveType.LineList:
                    
                    if (itype == typeof(Face))
                    {
                        throw new ArgumentException("invalid index type");
                    }
                    else if (itype == typeof(Edge))
                    {
                        numprimitives = numindices;
                    }
                    else if (itype != typeof(uint) && itype != typeof(ushort) && itype != typeof(int) && itype != typeof(short))
                    {
                        throw new ArgumentException("invalid index type");
                    }
                    else
                    {
                        numprimitives = numindices / 2;
                    }
                    break;

                default : throw new ArgumentException("invalid primitive type");
            }

        }
        /// <summary>
        /// Not indexed mesh
        /// </summary>
        public Mesh(GraphicDevice device, PrimitiveType type, Array vertices)
        {
            m_device = device;
            int numvertices = vertices.GetUpperBound(0) + 1;
            Type vtype = vertices.GetType().GetElementType();

            if (vtype is IVertexFormat)
            {
                IVertexFormat vertex = vertices.GetValue(0) as IVertexFormat;
                m_decl = new VertexDeclaration(device, vertex.type, vertex.elements);
            }
            else
            {
                throw new Exception("unknow vertices array type");
            }
            m_vb = new VertexBuffer(device, m_decl, numvertices, false, true);
            m_vb.Open();
            m_vb.Write(vertices);
            m_vb.Close();
            m_vb.Count = numvertices;
        }


        public void Draw()
        {
            m_decl.SetToDevice();
            m_vb.SetToDevice();
            
            if (m_ib == null)
            {
                m_ib.SetToDevice();
                m_device.DrawPrimitives(m_type, 0, numprimitives);
            }
            else
            {
                m_device.DrawIndexedPrimitives(m_type, 0, 0, m_vb.Count, 0, numprimitives);
            }
        }
    }
}
