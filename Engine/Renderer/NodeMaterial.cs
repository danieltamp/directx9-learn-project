﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

using Engine.Tools;
using Engine.Graphics;

namespace Engine.Renderer
{
    public interface INodeMaterial
    {
        NodeMaterial m_material { get; set; }
        uint m_material_key { get; }
    }

    /// <summary>
    /// Where texture are
    /// </summary>
    internal enum TextureSave
    {
        /// <summary>null texture </summary>
        Empty,
        /// <summary>saved in your hard disk, when restored the engine read from file </summary>
        FromFile,
        /// <summary>saved as bitmap class, usefull for little texture</summary>
        FromBitmap,
    }

    /// <summary>
    /// A material used only for Node class in the EngineCore manager
    /// </summary>
    [DebuggerDisplay("{m_location.ToString() + name.ToString()}")]
    public class NodeMaterial : IKey, IDisposable, IRestore
    {
        public uint m_key { get; set; }
        public bool m_removed { get; set; }
        public string name { get; set; }
        /// <summary>
        /// number of node that have this texture, if 0 is ready to be removed
        /// </summary>
        public int numassignment = 0;
        
        static NodeMaterial prevMaterial = null;
        Material m_material = Material.Default;
        DxTexture m_texture = null;
        GraphicDevice m_device;
        Pool m_memory;
        TextureSave m_location;
        Bitmap m_bitmap = null;
        string m_filename = string.Empty;
        bool isInitialized = false;

        private NodeMaterial(GraphicDevice device , bool managed)
        {
            this.m_device = device;
            m_key = 0;
            m_removed = false;
            m_memory = managed ? Pool.Managed : Pool.Default;
            
            if (!managed)
            {
                device.resourcesToDispose.Add(this);
                device.resourcesToRestore.Add(this);
            }
        }
        ~NodeMaterial()
        {
            Destroy();
            Dispose();
        }

        /// <summary>
        /// Empty texture, default material
        /// </summary>
        public static NodeMaterial Default(GraphicDevice device)
        {
            NodeMaterial mat = new NodeMaterial(device, false);
            mat.m_location = TextureSave.Empty;
            mat.name = "Default";
            return mat;
        }
        public static NodeMaterial TextureFromFile(GraphicDevice device, string filename)
        {
            NodeMaterial mat = new NodeMaterial(device, false);
            mat.m_location = TextureSave.FromFile;
            mat.m_bitmap = null;
            mat.m_filename = filename;
            mat.name = System.IO.Path.GetFileName(filename);
            return mat;
        }
        public static NodeMaterial TextureFromImage(GraphicDevice device,Bitmap bitmap)
        {
            NodeMaterial mat = new NodeMaterial(device,false);
            mat.m_location = TextureSave.FromBitmap;
            mat.m_bitmap = bitmap;
            mat.m_filename = string.Empty;
            mat.name = bitmap.Tag.ToString();
            return mat;
        } 
        /// <summary>
        /// Before apply texture need to be load it in the device's events, this create the texture buffers resources
        /// </summary>
        /// <remarks>
        /// If missing filename or bitmap i set my default image, the isInitialized flags will be set to true to avoid
        /// a continuos initialization each time you call SetToDevice()
        /// </remarks>
        bool InitializeMaterial()
        {
            bool result = true;
            Bitmap bmp = m_bitmap;
            string tmp = m_filename;

            switch (m_location)
            {
                case TextureSave.FromBitmap:
                    bmp = m_bitmap;
                    if (bmp == null) 
                    {
                        bmp = Engine.Properties.Resources.empytexture0;
                        result = false;
                    }
                    m_texture = new DxTexture(m_device.m_device, bmp);
                    
                    break;

                case TextureSave.FromFile:
                    tmp = m_filename;
                    if (string.IsNullOrEmpty(tmp) || !System.IO.File.Exists(tmp))
                    {
                        bmp = Engine.Properties.Resources.empytexture0;
                        m_texture = new DxTexture(m_device.m_device, bmp);
                        result = false;
                    }
                    else
                    {
                        m_texture = new DxTexture(m_device.m_device, tmp);
                    }
                    break;
                default: break;
            }

            isInitialized = true;
            return result;
        }
        /// <summary>
        /// Release the DxTexture resources
        /// </summary>
        public void Dispose()
        {
            if (m_texture != null) m_texture.Dispose();
            m_texture = null;
            isInitialized = false;
        }
        /// <summary>
        /// DxTexture resource reinitialized if use a not managed memory
        /// </summary>
        public void Restore()
        {
            InitializeMaterial();
        }
        /// <summary>
        /// DxTexture ready to be deleted, remove from device events
        /// </summary>
        public void Destroy()
        {
            if (m_texture!=null && m_memory != Pool.Managed && isInitialized)
            {
                m_device.resourcesToDispose.Remove(m_texture);
                m_device.resourcesToRestore.Remove(m_texture);
            }
        }
        /// <summary>
        /// Return true if material is applied to device. To avoid multiple set, the function return false also when material is already set
        /// </summary>
        /// <param name="applytexture">tell to methods if need to apply only material or also texture, example when vertexdeclaration don't contain Texture1 format</param>
        /// <param name="material">if null, remove the device texture (material must exist anyway)</param>
        public static bool SetToDevice(GraphicDevice device , NodeMaterial material, bool applytexture)
        {
            // Set a texture
            if (material != null)
            {
                material.m_device = device;
                // only once
                if (applytexture && NodeMaterial.prevMaterial != material)
                {
                    if (!material.isInitialized)
                    {
                        // error when create texture
                        if (!material.InitializeMaterial())
                        {
                            Console.WriteLine("Can't initialize to device the nodematerial " + material.m_key + ", i will use my default image");
                            RemoveFromDevice(material.m_device);
                            return false;
                        }
                    }
                    device.m_device.Texture = material.m_texture;
                    device.m_device.Material = material.m_material;
                    NodeMaterial.prevMaterial = material;
                    return true;
                }
                return false;
            }
            // Set a null texture
            else
            {
                return RemoveFromDevice(device);
            }
        }
        /// <summary>
        /// Remove the material applied to device, this is necessary when render lines, the texture create a wrong vertex colors
        /// </summary>
        /// <returns>return true if need remove a previous texture</returns>
        public static bool RemoveFromDevice(GraphicDevice device)
        {
            if (NodeMaterial.prevMaterial != null)
            {
                device.m_device.Texture = null;
                device.m_device.Material = Material.Default;
                NodeMaterial.prevMaterial = null;
                return true;
            }
            return false;
        }
        /// <summary>
        /// The comparing function for all cache, they are sorted by texture index to minimize the textures change
        /// </summary>
        public static int CompareByTexture(INodeMaterial node1, INodeMaterial node2)
        {
            return node1.m_material_key.CompareTo(node2.m_material_key);
        } 
    }
}
