﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Resources
{
    public static class LineBoxInstance
    {
        static int numVertices;
        static int numIndices;

        static VertexBuffer vbuffer = null;
        static IndexBuffer ibuffer = null;

        public static bool initialized = false;
        
        public static void Init(GraphicDevice device)
        {
            initialized = true;

            CVERTEX[] vertices = new CVERTEX[]{
                // Default 8 vertices
                new CVERTEX(0,0,0,Color.Blue),
                new CVERTEX(1,0,0,Color.Blue),
                new CVERTEX(0,1,0,Color.Blue),
                new CVERTEX(1,1,0,Color.Blue),
                new CVERTEX(0,0,1,Color.Blue),
                new CVERTEX(1,0,1,Color.Blue),
                new CVERTEX(0,1,1,Color.Blue),
                new CVERTEX(1,1,1,Color.Blue),
                // Selected 8 vertices
                new CVERTEX(0,0,0,Color.White),
                new CVERTEX(1,0,0,Color.White),
                new CVERTEX(0,1,0,Color.White),
                new CVERTEX(1,1,0,Color.White),
                new CVERTEX(0,0,1,Color.White),
                new CVERTEX(1,0,1,Color.White),
                new CVERTEX(0,1,1,Color.White),
                new CVERTEX(1,1,1,Color.White)
            
            };

            numVertices = vertices.Length;

            VertexDeclaration vdecl = VertexDeclaration.GetDeclaration<CVERTEX>(device);

            //vbuffer = new VertexBuffer(device, vdecl,numVertices, BufferUsage.StaticWriteOnly, true);
            //vbuffer.Initialize(numVertices);
            //vbuffer.SetData(vertices, 0);

            // indices for Default vertices , for selected need to set BaseVertex = 8
            ushort[] indices = new ushort[] { 0, 1, 1, 3, 3, 2, 2, 0, 4, 5, 5, 7, 7, 6, 6, 4, 0, 4, 1, 5, 2, 6, 3, 7 };
            numIndices = indices.Length;
            //ibuffer = new DxIndexBuffer(device,false, indices.Length, BufferUsage.StaticWriteOnly, true);
            //ibuffer.Initialize(indices.Length);
            //ibuffer.SetData(indices, 0);

        }
        
        public static void SetBuffer()
        {
            ibuffer.SetToDevice();
            vbuffer.SetToDevice();
        }
        public static void DrawIstance(GraphicDevice device, Matrix4 transform, bool select)
        {
            int BaseVertex = select ? 8 : 0; // where vertices start
            int numVertex = 8; //max_i - min_i + 1
            int minVertex = 0; // 0 because start at the begin of BaseVertex
            int numPrimitives = numIndices / 2;
            int startIndex = 0; // i use the same indexBuffer so no-offset

            device.renderstates.world = transform;
            device.DrawIndexedPrimitives(PrimitiveType.LineList, BaseVertex, minVertex, numVertex, startIndex, numPrimitives);
        }
    }

    public static class LineAxisInstance
    {
        static int numVertices;
        static VertexBuffer vbuffer = null;

        public static bool initialized = false;
        public static void Init(GraphicDevice device)
        {
            initialized = true;

            CVERTEX[] vertices = new CVERTEX[]{
                new CVERTEX(0,0,0,Color.Red),
                new CVERTEX(10,0,0,Color.Red),
                new CVERTEX(0,0,0,Color.Green),
                new CVERTEX(0,10,0,Color.Green),
                new CVERTEX(0,0,0,Color.Blue),
                new CVERTEX(0,0,10,Color.Blue)
            };
            VertexDeclaration vdecl = VertexDeclaration.GetDeclaration<CVERTEX>(device);
            numVertices = vertices.Length;
            //vbuffer = new DxVertexBuffer(device, vdecl,numVertices, BufferUsage.StaticWriteOnly, true);
            //vbuffer.Initialize(vertices.Length);
            //vbuffer.SetData(vertices, 0);
        }
        public static void SetBuffer()
        {
            if (!initialized) return;
            vbuffer.SetToDevice();
        }
        public static void DrawIstance(DX9Device device, Matrix4 transform)
        {
            if (!initialized) return;
            device.World = transform;
            device.DrawPrimitives(PrimitiveType.LineList, 0, 3);
        }
    }

    public static class LineRayInstance
    {
        static CVERTEX[] vertices;
        static int numVertices;
        //static VertexBuffer vbuffer = null;

        public static bool updated = false;
        public static bool initialized = false;

        public static void Init(GraphicDevice device)
        {
            initialized = true;
            vertices = new CVERTEX[]{
                new CVERTEX(0,0,0,Color.Red),
                new CVERTEX(100,100,100,Color.CornflowerBlue)};

            numVertices = vertices.Length;

            VertexDeclaration vdecl = VertexDeclaration.GetDeclaration<CVERTEX>(device);
            //vbuffer = new DxVertexBuffer(device,vdecl, numVertices, BufferUsage.StaticWriteOnly, true);
            //vbuffer.Initialize(numVertices);
            //vbuffer.SetData(vertices, 0);
        }
        public static void Update(Ray mray)
        {
            if (!initialized) return;
            updated = true;

            vertices[0].position = mray.orig;
            vertices[1].position = mray.orig + mray.dir * 10f;
            //vbuffer.SetData(vertices, 0);
        }
        public static void SetBuffer(DX9Device device)
        {
            if (!initialized) return;
            if (!updated) return;
            //vbuffer.SetToDevice(device);
        }
        public static void DrawIstance(DX9Device device)
        {
            if (!initialized) return;
            if (!updated) return;
            updated = false;
            device.World = Matrix4.Identity;
            device.DrawPrimitives(PrimitiveType.LineList, 0, 1);
        }
    }
}
