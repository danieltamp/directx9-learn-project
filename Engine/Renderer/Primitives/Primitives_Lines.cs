﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Abstract class, used to derive all similar Lines Primitives
    /// </summary>
    public abstract class PrimitiveLine : Node
    {
        public new PrimitiveType primitive
        {
            get { return m_primitive; }
            set
            {
                Debug.Assert(value == PrimitiveType.LineList || value == PrimitiveType.LineStrip, "Invalid primitive type : " + value.ToString());
                m_primitive = value;
            }
        }
        public override int numIndices { get { return numSegments * 2; } }
        public override int numVertices { get { return (vertices != null) ? vertices.Length : 0; } }
        public override int numPrimitives { get { return numSegments; } }
        public abstract int numSegments { get; }
        
        public Vector3[] vertices;
        public Color[] colors;

        /// <summary>
        /// Get the vertex indices of this primitive
        /// </summary>
        public abstract Edge GetSegment(int i);


        public PrimitiveLine() : base()
        {
            m_primitive = PrimitiveType.LineList;
            vertexFormat = VertexFormat.Diffuse | VertexFormat.Position;
        }
        /// <summary>
        /// start selection of primitive with this values
        /// </summary>
        int startAt = 0;
        /// <summary>
        /// intersection of ray from each segments
        /// </summary>
        public override int PrimitiveIntersection(Ray ray, out float t ,out Vector3 intersection)
        {
            intersection = Vector3.Zero;
            t = 0;
            int numSeg = this.numSegments;

            Ray localray = Ray.TransformCoordinate(ray, base.transform_inv);

            startAt = Math.Abs(startAt % numSeg);
            int s = startAt;
            do
            {
                //Console.WriteLine("  test segment " + s);

                Edge seg = GetSegment(s);
                Vector3 P0 = vertices[seg.I];
                Vector3 P1 = vertices[seg.J];

                if (localray.IntersectSegment(P0, P1, 0.05f, out t))
                {
                    startAt = Math.Abs((s + 1) % numSeg);
                    return s;
                }
                s = Math.Abs((s + 1) % numSeg); //loop of primitives
            } while (s != startAt);

            return -1;
        }


        public override Array GetVerticesStream(bool globalCoorSys, VertexDeclaration declaration)
        {
            int ipos = declaration.FindElement(VertexFormat.Position);
            int idif = declaration.FindElement(VertexFormat.Diffuse);

            bool setPosition = vertices != null && ipos>-1;
            bool setColor = colors != null && idif > -1;

            //-------------------------------------------------------------
            //              VERTEX
            //-------------------------------------------------------------
            if (declaration.vertexType == typeof(VERTEX))
            {
                VERTEX[] stream = new VERTEX[numVertices];

                if (setPosition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], this.transform);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                return stream;
            }
            //-------------------------------------------------------------
            //              CVERTEX
            //-------------------------------------------------------------
            else if (declaration.vertexType == typeof(CVERTEX))
            {
                CVERTEX[] stream = new CVERTEX[numVertices];
 
                if (setPosition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], this.transform);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                if (setColor)
                {
                    for (int i = 0; i < numVertices; i++)
                        stream[i].color = colors[i].ToArgb();
                }
                return stream;
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        public override Array GetIndicesStream(int index_offset, bool use32bit)
        {
            if (use32bit)
            {
                uint[] stream = new uint[numSegments * 2];

                for (int e = 0; e < numSegments; e++)
                {
                    Edge edge = GetSegment(e);
                    for (int j = 0; j < 2; j++)
                        stream[e * 2 + j] = (uint)edge[j] + (uint)index_offset;
                }
                return stream;
            }
            else
            {
                if (index_offset + numVertices > ushort.MaxValue)
                    throw new ArgumentOutOfRangeException(String.Format("index_offset{0} + maxindex {1} > of ushort.maxvalue {2}. These operation generate a wrong index in cast conversion", index_offset, numVertices, ushort.MaxValue));

                ushort[] stream = new ushort[numSegments * 2];

                for (int e = 0; e < numSegments; e++)
                {
                    Edge edge = GetSegment(e);
                    for (int j = 0; j < 2; j++)
                        stream[e * 2 + j] = (ushort)(edge[j] + index_offset);
                }
                return stream;
            }
        }

        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);

            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];

            transform = newtransform;
        }
    
    
    }
    
    /// <summary>
    /// Lines geometries not-indexed
    /// </summary>
    public class Line : PrimitiveLine
    {
        //    LINE-LIST        LINE-STRIP
        //     0   2          0   2   4
        //      \   \          \ / \ /
        //       1   3          1   3

        public override int numSegments
        {
            get { return (m_primitive == PrimitiveType.LineList) ? numVertices / 2 : numVertices - 1; }
        }
        
        public override Edge GetSegment(int i)
        {
            if (m_primitive == PrimitiveType.LineList)
                return new Edge((ushort)(i * 2), (ushort)(i * 2 + 1));
            else
                return new Edge((ushort)i, (ushort)(i + 1));
        }

        public Line() : base()
        {
            vertices = null;
            colors = null;
        }

        public static explicit operator SplineShape(Line line)
        {
            SplineShape shape = new SplineShape();
            shape.vertexFormat = line.vertexFormat;

            // clonable because struct was passed as value and not by reference
            if (line.vertices != null) shape.vertices = (Vector3[])line.vertices.Clone();
            if (line.colors != null) shape.colors = (Color[])line.colors.Clone();

            // build indices , the getTriangle methods return indices used for TriangleList
            if (line.numSegments > 0)
            {
                shape.edges = new Edge[line.numSegments];
                for (int s = 0; s < shape.edges.Length; s++)
                    shape.edges[s] = line.GetSegment(s);
            }
            return shape;
        }


        public static Line Axis3D()
        {
            Line axis = new Line();

            axis.name = "axis3d";
            axis.vertices = new Vector3[]
            {
                new Vector3(0,0,0),
                new Vector3(1,0,0),
                new Vector3(0,0,0),
                new Vector3(0,1,0),
                new Vector3(0,0,0),
                new Vector3(0,0,1)
            };
            axis.colors = new Color[]
            {
                Color.Red,
                Color.Red,
                Color.Green,
                Color.Green,
                Color.Blue , 
                Color.Blue        
            };
            axis.primitive = PrimitiveType.LineList;

            return axis;
        }
        public static Line Grid2D()
        {
            Line grid = new Line();
            grid.name = "grid2d";
            grid.vertices = new Vector3[40];
            int i=0;
            for (int x = -5; x < 6; x++)
            {
                if (x == 0) continue;
                grid.vertices[i++] = new Vector3(x, 0, -5);
                grid.vertices[i++] = new Vector3(x, 0, 5);
            }
            for (int z = -5; z < 6; z++)
            {
                if (z == 0) continue;
                grid.vertices[i++] = new Vector3(-5, 0, z);
                grid.vertices[i++] = new Vector3(5, 0, z);
            }

            grid.colors = new Color[grid.vertices.Length];
            for (i = 0; i < grid.vertices.Length; i++) grid.colors[i] = Color.Gray;

            grid.primitive = PrimitiveType.LineList;

            return grid;
        }
        public static Line Circle(int numpoints)
        {
            float dang = (float)Math.PI * 2.0f / numpoints;
            int numverts = numpoints + 1;

            Line obj = new Line();

            obj.vertices = new Vector3[numverts];
            obj.colors = new Color[numverts];
            for (int i = 0; i < numverts; i++)
            {
                obj.vertices[i] = new Vector3((float)Math.Sin(dang * i), (float)Math.Cos(dang * i), 0);
                obj.colors[i] = Color.Yellow;
            }
            obj.primitive = PrimitiveType.LineStrip;
            return obj;
        }

    }

    /// <summary>
    /// Lines geometries with indices
    /// </summary>
    public class SplineShape : PrimitiveLine
    {
        // can't be different
        public new PrimitiveType primitive
        { 
            get { return PrimitiveType.LineList; } 
            set { throw new NotSupportedException("Can't be different"); }
        }
        public Edge[] edges;

        public override int numSegments { get { return (edges != null) ? edges.Length : 0; } }

        public override Edge GetSegment(int i) { return edges[i]; }
        public SplineShape()
            : base()
        {
            base.m_primitive = PrimitiveType.LineList;
        }

        /// <summary>
        /// A sphere made with three yellow circle, minimum 3 points
        /// </summary>
        public static SplineShape SphereGizmo(float radius,int numpoints)
        {
            Debug.Assert(numpoints > 2, "minimum 3");

            float dang = (float)Math.PI * 2.0f / numpoints;

            SplineShape shape = new SplineShape();

            shape.vertices = new Vector3[numpoints * 3 ];
            shape.edges = new Edge[numpoints * 3];
            shape.colors = new Color[shape.vertices.Length];

            /////////////////////circle A
            for (int i = 0; i < numpoints; i++)
                shape.vertices[i] = new Vector3((float)Math.Sin(dang * i), (float)Math.Cos(dang * i), 0);

            for (int i = 0; i < numpoints; i++)
                shape.edges[i] = new Edge(i, i + 1);
            shape.edges[numpoints - 1].J = 0;


            /////////////////////circle B
            for (int i = numpoints; i < numpoints*2; i++)
                shape.vertices[i] = new Vector3(0, (float)Math.Cos(dang * i), (float)Math.Sin(dang * i));

            for (int i = numpoints; i < numpoints*2; i++)
                shape.edges[i] = new Edge(i, i + 1);
            shape.edges[numpoints * 2 - 1].J = (ushort)numpoints;



            /////////////////////circle C
            for (int i = numpoints*2; i < numpoints * 3; i++)
                shape.vertices[i] = new Vector3((float)Math.Cos(dang * i), 0, (float)Math.Sin(dang * i));

            for (int i = numpoints*2; i < numpoints * 3; i++)
                shape.edges[i] = new Edge(i, i + 1);
            shape.edges[numpoints * 3 - 1].J = (ushort)(numpoints*2);


            for (int i = 0; i < shape.vertices.Length; i++)
            {
                shape.vertices[i] *= radius;
                shape.colors[i] = Color.Yellow;
            }
            return shape;
        }

        /// <summary>
        /// A box with semi-size
        /// </summary>
        public static SplineShape BoxGizmo(float dx, float dy, float dz)
        {

            //           5______4
            //           /     /|         Y
            //         1/_____/0|         |
            //          | 7   | /6        *---> Z
            //          |_____|/         /
            //         3      2         X
            SplineShape shape = new SplineShape();

            shape.vertices = new Vector3[]{
                new Vector3( dx, dy, dz),
                new Vector3( dx, dy,-dz),
                new Vector3( dx,-dy, dz),
                new Vector3( dx,-dy,-dz),
                new Vector3(-dx, dy, dz),
                new Vector3(-dx, dy,-dz),
                new Vector3(-dx,-dy, dz),
                new Vector3(-dx,-dy,-dz)};

            shape.colors = new Color[8];
            for (int i = 0; i < 8; i++)
                shape.colors[i] = Color.Yellow;

            shape.edges = new Edge[]{
                new Edge(0,1),
                new Edge(1,3),
                new Edge(3,2),
                new Edge(2,0),
                new Edge(4,5),
                new Edge(5,7),
                new Edge(7,6),
                new Edge(6,4),
                new Edge(0,4),
                new Edge(2,6),
                new Edge(3,7),
                new Edge(1,5)};

            return shape;
        }

        /// <summary>
        /// A box with only corner, min and max are in global coordinate system
        /// </summary>
        public static SplineShape SelectBoxGizmo(Vector3 min, Vector3 max)
        {
            //            3           7
            //          0/___1   5__4/     
            //          |           |
            //          |           |
            //         2            6

            SplineShape shape = new SplineShape();

            Vector3 d = max - min;

            float x = Math.Abs(d.x/3.0f);
            float y = Math.Abs(d.y/3.0f);
            float z = Math.Abs(d.z/3.0f);


            shape.vertices = new Vector3[32];

            // corner +X+Y+Z
            shape.vertices[0] = new Vector3(max.x, max.y, max.z);
            shape.vertices[1] = new Vector3(max.x - x, max.y, max.z);
            shape.vertices[2] = new Vector3(max.x, max.y - y, max.z);
            shape.vertices[3] = new Vector3(max.x, max.y, max.z - z);

            // corner +X+Y-Z
            shape.vertices[4] = new Vector3(max.x, max.y, min.z);
            shape.vertices[5] = new Vector3(max.x - x, max.y, min.z);
            shape.vertices[6] = new Vector3(max.x, max.y - y, min.z);
            shape.vertices[7] = new Vector3(max.x, max.y, min.z+z);

            // corner +X-Y+Z
            shape.vertices[8] = new Vector3(max.x, min.y, max.z);
            shape.vertices[9] = new Vector3(max.x - x, min.y, max.z);
            shape.vertices[10] = new Vector3(max.x, min.y+y, max.z);
            shape.vertices[11] = new Vector3(max.x, min.y, max.z - z);

            // corner +X-Y-Z
            shape.vertices[12] = new Vector3(max.x, min.y, min.z);
            shape.vertices[13] = new Vector3(max.x - x, min.y, min.z);
            shape.vertices[14] = new Vector3(max.x, min.y+y, min.z);
            shape.vertices[15] = new Vector3(max.x, min.y, min.z+z);


            // corner -X+Y+Z
            shape.vertices[16] = new Vector3(min.x, max.y, max.z);
            shape.vertices[17] = new Vector3(min.x + x, max.y, max.z);
            shape.vertices[18] = new Vector3(min.x, max.y - y, max.z);
            shape.vertices[19] = new Vector3(min.x, max.y, max.z - z);

            // corner -X+Y-Z
            shape.vertices[20] = new Vector3(min.x, max.y, min.z);
            shape.vertices[21] = new Vector3(min.x + x, max.y, min.z);
            shape.vertices[22] = new Vector3(min.x, max.y - y, min.z);
            shape.vertices[23] = new Vector3(min.x, max.y, min.z + z);

            // corner -X-Y+Z
            shape.vertices[24] = new Vector3(min.x, min.y, max.z);
            shape.vertices[25] = new Vector3(min.x + x, min.y, max.z);
            shape.vertices[26] = new Vector3(min.x, min.y+y, max.z);
            shape.vertices[27] = new Vector3(min.x, min.y, max.z - z);

            // corner -X-Y-Z
            shape.vertices[28] = new Vector3(min.x, min.y, min.z);
            shape.vertices[29] = new Vector3(min.x + x, min.y, min.z);
            shape.vertices[30] = new Vector3(min.x, min.y+y, min.z);
            shape.vertices[31] = new Vector3(min.x, min.y, min.z+z);

            shape.edges = new Edge[24];

            shape.edges[0] = new Edge(0, 1);
            shape.edges[1] = new Edge(0, 2);
            shape.edges[2] = new Edge(0, 3);

            int j = 3;
            int k = 4;
            for (int i = 1; i < 8; i++)
            {
                shape.edges[j++] = shape.edges[0] + k;
                shape.edges[j++] = shape.edges[1] + k;
                shape.edges[j++] = shape.edges[2] + k;
                k += 4;
            }

            shape.colors = new Color[32];
            for (int i = 0; i < 32; i++)
            {
                shape.colors[i] = Color.White;
            }

            return shape;
        }
    
    }
}
