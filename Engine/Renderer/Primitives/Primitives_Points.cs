﻿// by johnwhile
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Manage the points geometries
    /// </summary>
    public class PrimitivePoint : Node
    {
        public PrimitiveType primitiveType { get { return m_primitive; } }

        /// <summary>
        /// don't exist a indexed point's geometry, so not exist indices and primitives math with num of vertices
        /// </summary>
        public override int numIndices { get { return numVertices; } }
        public override int numPrimitives { get { return numVertices; } }       
        public override int numVertices { get { return (vertices != null) ? vertices.Length : 0; } }

        //primitivetype is only points
        public Vector3[] vertices;
        public Color[] colors;

        public PrimitivePoint() : base()
        {
            m_primitive = PrimitiveType.PointList;
        }

        /// <summary>
        /// start selection of primitive with this values
        /// </summary>
        int startAt = 0;

        /// <summary>
        /// in this case intersection is the vertices == the primitive returned
        /// </summary>
        public override int PrimitiveIntersection(Ray ray, out float t,out Vector3 intersection)
        {
            // vertices are in local coord system, the faster way is convert ray and not all vertices
            t = 0;
            intersection = Vector3.Zero;

            Ray localray = Ray.TransformCoordinate(ray, base.transform_inv);

            int numPoints = this.numVertices;
            float EPSILON = 0.001f;

            startAt = Math.Abs(startAt % numPoints);
            int i = startAt;
            do
            {
                //Console.WriteLine("  test triangle " + f);
                Vector3 V = vertices[i] - localray.orig;
                if (Vector3.GetLengthSquared(Vector3.Cross(localray.dir, V)) < EPSILON * EPSILON) 
                {
                    t = Vector3.Dot(localray.dir, V);
                    return i;
                }
                i = Math.Abs((i + 1) % numPoints); //loop of primitives

            } while (i != startAt);

            return -1;

        }

        /// <summary>
        /// Get the vertex stream ready to write into buffer
        /// </summary>
        /// <param name="globalCoorSys">if true get in world coorinate system</param>
        public override Array GetVerticesStream(bool globalCoorSys, VertexDeclaration declaration)
        {
            int ipos = declaration.FindElement(VertexFormat.Position);
            int idif = declaration.FindElement(VertexFormat.Diffuse);

            bool setPosition = vertices != null && ipos>-1;
            bool setColor = colors != null && idif > -1;

            //-------------------------------------------------------------
            //              VERTEX
            //-------------------------------------------------------------
            if (declaration.vertexType == typeof(VERTEX))
            {
                VERTEX[] stream = new VERTEX[numVertices];

                if (setPosition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], this.transform);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                return stream;
            }
            //-------------------------------------------------------------
            //              CVERTEX
            //-------------------------------------------------------------
            else if (declaration.vertexType == typeof(CVERTEX))
            {
                CVERTEX[] stream = new CVERTEX[numVertices];

                if (setPosition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], this.transform);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                if (setColor)
                {
                    for (int i = 0; i < numVertices; i++)
                        stream[i].color = colors[i].ToArgb();
                }
                return stream;
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        /// <summary>
        /// Indices stream don't exist in the points class
        /// </summary>
        public override Array GetIndicesStream(int index_offset, bool use32bit)
        {
            throw new NotSupportedException("Indices aren't used for points");
        }

        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);
            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];
            transform = newtransform;
        }
    }
}
