﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Abstract class, used to derive all similar Triangles Primitives
    /// </summary>
    public abstract class PrimitiveTriangle : Node , INodeMaterial
    {
        public virtual PrimitiveType primitiveType
        {
            get { return m_primitive; }
            set
            {
                Debug.Assert(value == PrimitiveType.TriangleFan || value == PrimitiveType.TriangleList || value == PrimitiveType.TriangleStrip, "Invalid primitive type : " + value.ToString());
                m_primitive = value;
            }
        }

        /// <summary>
        /// A triangles node can has ONE texture, multy texture aren't supported, 0 for no texture usage, only primitivetriangle can change this value
        /// </summary>  
        public NodeMaterial m_material { get; set; }
        /// <summary>
        /// Get the material Key used currently by this node, if material is null or not initialized return 0
        /// </summary>
        public uint m_material_key { get { return m_material != null ? m_material.m_key : 0; } }


        public override int numIndices { get { return numTriangles * 3; } }
        public override int numVertices { get { return (vertices != null) ? vertices.Length : 0; } }
        public override int numPrimitives { get { return numTriangles; } }  
        public abstract int numTriangles { get; }

        public PrimitiveTriangle() : base()
        {
            m_primitive = PrimitiveType.TriangleList;
        }

        /// <summary>
        /// Get the vertex indices of this primitive
        /// </summary>
        public abstract Face GetTriangle(int i);
        /// <summary>
        /// Get the num of primitives of this Triangle's Type 
        /// </summary>
        

        public ushort[] attribute; //face material id
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector2[] textures;
        public Color[] colors;

        /// <summary>
        /// Virtual method, recalculate all normal by face normal
        /// </summary>
        public virtual void SetDefaultNormal()
        {
            Face[] faces = new Face[numTriangles];
            for (int t = 0; t < numTriangles; t++)
                faces[t] = GetTriangle(t);
            normals = GeometryTools.GetDefaultNormal(vertices, faces);
        }
        /// <summary>
        /// Virtual method, recalculate all texture by face using a Matrix4.Identity projection plane
        /// </summary>
        public virtual void SetDefaultTexture()
        {
            textures = GeometryTools.GetPlanarProjection(vertices, new Vector2(0, 0), new Vector2(1, 1), Matrix4.Identity);
        }

        /// <summary>
        /// start selection of primitive with this values
        /// </summary>
        int startAt = 0;

        /// <summary>
        /// get the primitive hit by ray , if not return -1
        /// </summary>
        /// <param name="ray">input ray</param>
        /// <param name="t">parametric value of ray at the intersection point</param>
        /// <param name="intersection">intersection point</param>
        public override int PrimitiveIntersection(Ray ray, out float t, out Vector3 intersection)
        {
            intersection = Vector3.Zero;
            t = 0;

            // vertices are in local coord system, the faster way is convert ray and not all vertices
            Ray localray = Ray.TransformCoordinate(ray, base.transform_inv);

            int numFaces = this.numTriangles;

            startAt = Math.Abs(startAt % numFaces);
            int f = startAt;
            do
            {
                //Console.WriteLine("  test triangle " + f);
                Face tri = GetTriangle(f);
                Vector3 P0 = vertices[tri.I];
                Vector3 P1 = vertices[tri.J];
                Vector3 P2 = vertices[tri.K];

                if (localray.IntersectTriangle(P0, P1, P2, false, out t, out intersection))
                    return f;

                f = Math.Abs((f + 1) % numFaces); //loop of primitives

            } while (f != startAt);

            return -1;
        }


        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);
            // remove traslation componenent because normal are a direction (position = 0,0,0)
            Matrix4 matrix_normal = matrix;
            matrix_normal.TranslationComponent = new Vector3(0, 0, 0);

            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];

            if (normals != null)
                for (int i = 0; i < numVertices; i++)
                    normals[i] = matrix_normal * normals[i];

            transform = newtransform;
        }
        /// <summary>
        /// Get the vertex stream ready to write into buffer
        /// </summary>
        /// <param name="globalCoorSys">if true get in world coorinate system</param>
        public override Array GetVerticesStream(bool globalCoorSys, VertexDeclaration declaration)
        {
            int ipos = declaration.FindElement(VertexFormat.Position);
            int idif = declaration.FindElement(VertexFormat.Diffuse);
            int inorm = declaration.FindElement(VertexFormat.Normal);
            int itext = declaration.FindElement(VertexFormat.Texture1);

            bool useposition = vertices != null && ipos > -1;
            bool usecolor = colors != null && idif > -1;
            bool usenormal = normals != null && inorm > -1;
            bool useuvw = textures != null && itext > -1;

            int numColors = numVertices;
            int numNormals = numVertices;
            int numUvw = numVertices;

            if (usecolor && colors.Length < numVertices) numColors = colors.Length;
            if (usenormal && normals.Length < numNormals) numNormals = normals.Length;
            if (useuvw && textures.Length < numUvw) numUvw = textures.Length;



            // local coordinate
            Matrix4 localcoord = base.transform;
            // remove traslation componenent
            //Matrix4 localcoord_normal = base.transform;
            //localcoord_normal.TranslationComponent = new Vector3(0, 0, 0);
            
            #region buffer type : V
            //--------------------------------------------------------------------
            //                          VERTEX
            //--------------------------------------------------------------------
            if (declaration.vertexType == typeof(VERTEX))
            {
                VERTEX[] stream = new VERTEX[numVertices];

                if (useposition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], localcoord);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                return stream;
            }
            #endregion

            #region buffer type : C + V
            //--------------------------------------------------------------------
            //                          CVERTEX
            //--------------------------------------------------------------------
            if (declaration.vertexType == typeof(CVERTEX))
            {
                CVERTEX[] stream = new CVERTEX[numVertices];

                if (useposition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], localcoord);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                if (usecolor)
                {
                    for (int i = 0; i < numColors; i++)
                        stream[i].color = colors[i].ToArgb();
                }
                return stream;
            }
            #endregion

            #region buffer type : N + C + V
            //--------------------------------------------------------------------
            //                          NCVERTEX
            //--------------------------------------------------------------------
            if (declaration.vertexType == typeof(NCVERTEX))
            {
                NCVERTEX[] stream = new NCVERTEX[numVertices];

                if (useposition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], localcoord);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                if (usecolor)
                {
                    for (int i = 0; i < numColors; i++)
                        stream[i].color = colors[i].ToArgb();
                }

                if (usenormal)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numNormals; i++)
                            //stream[i].normal = Vector3.GetNormal(Vector3.TransformCoordinate(normals[i], localcoord_normal));
                            stream[i].normal = Vector3.GetNormal(Vector3.TransformNormal(normals[i], localcoord));
                    else
                        for (int i = 0; i < numNormals; i++)
                            stream[i].normal = Vector3.GetNormal(normals[i]);
                }
                return stream;
            }
            #endregion

            #region buffer type : Nx16 + C + V
            //--------------------------------------------------------------------
            //                          N16CVERTEX
            //--------------------------------------------------------------------
            if (declaration.vertexType == typeof(NCVERTEX16))
            {
                NCVERTEX16[] stream = new NCVERTEX16[numVertices];

                if (useposition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], localcoord);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }
                if (usecolor)
                {
                    for (int i = 0; i < numColors; i++)
                        stream[i].color = colors[i].ToArgb();
                }

                if (usenormal)
                {
                    if (globalCoorSys)
                    {
                        for (int i = 0; i < numNormals; i++)
                        {
                             stream[i].normal = (Vector4x16)Vector3.GetNormal(Vector3.TransformNormal(normals[i], localcoord));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < numNormals; i++)
                        {
                            stream[i].normal = (Vector4x16)Vector3.GetNormal(normals[i]);
                        }
                    }
                }
                return stream;
            }
            #endregion

            #region buffer type : N + T + V
            //--------------------------------------------------------------------
            //                          NTVERTEX
            //--------------------------------------------------------------------
            if (declaration.vertexType == typeof(NTVERTEX))
            {
                NTVERTEX[] stream = new NTVERTEX[numVertices];

                if (useposition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].m_pos = Vector3.TransformCoordinate(vertices[i], localcoord);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].m_pos = vertices[i];
                }

                if (usenormal)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numNormals; i++)
                            //stream[i].normal = Vector3.GetNormal(Vector3.TransformCoordinate(normals[i], localcoord_normal));
                            stream[i].m_norm = Vector3.GetNormal(Vector3.TransformNormal(normals[i], localcoord));
                    else
                        for (int i = 0; i < numNormals; i++)
                            stream[i].m_norm = Vector3.GetNormal(normals[i]);
                }

                if (useuvw)
                {
                    for (int i = 0; i < numUvw; i++)
                        stream[i].m_uv = textures[i];
                }
                return stream;
            }
            #endregion

            #region buffer type : Nx16 + Tx16 + V
            //--------------------------------------------------------------------
            //                          N16T16VERTEX
            //--------------------------------------------------------------------
            if (declaration.vertexType == typeof(NTVERTEX16))
            {
                NTVERTEX16[] stream = new NTVERTEX16[numVertices];

                if (useposition)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = Vector3.TransformCoordinate(vertices[i], localcoord);
                    else
                        for (int i = 0; i < numVertices; i++)
                            stream[i].position = vertices[i];
                }

                if (usenormal)
                {
                    if (globalCoorSys)
                        for (int i = 0; i < numNormals; i++)
                            //stream[i].normal = Vector3.GetNormal(Vector3.TransformCoordinate(normals[i], localcoord_normal));
                            stream[i].normal = Vector3.GetNormal(Vector3.TransformNormal(normals[i], localcoord));
                    else
                        for (int i = 0; i < numNormals; i++)
                            stream[i].normal = Vector3.GetNormal(normals[i]);
                }

                if (useuvw)
                {
                    for (int i = 0; i < numUvw; i++)
                        stream[i].m_uv = textures[i];
                }
                return stream;
            }
            #endregion

            throw new NotImplementedException();
        }
        /// <summary>
        /// Get the indices stream ready to write into buffer. A ArgumentOutOfRangeException are generated if index_offset
        /// is too big. The function return the faces array as uint/ushort also for not-indexed geometry
        /// </summary>
        /// <param name="index_offset">precalculate a offset, if index + offset > IT.maxvalue genereate a exception</param>
        public override Array GetIndicesStream(int index_offset, bool use32bit)
        {
            if (use32bit)
            {
                uint[] stream = new uint[numTriangles * 3];
                for (int f = 0; f < numTriangles; f++)
                {
                    Face face = GetTriangle(f);
                    for (int j = 0; j < 3; j++)
                        stream[f * 3 + j] = (uint)(face[j] + index_offset);
                }
                return stream;
            }
            else
            {
                if (index_offset + numVertices > ushort.MaxValue)
                    throw new ArgumentOutOfRangeException(String.Format("index_offset{0} + maxindex {1} > of ushort.maxvalue {2}. These operation generate a wrong index in cast conversion", index_offset, numVertices, ushort.MaxValue));


                // carefull because if edge + offset > ushort.maxvalue the cast set a wrong index
                ushort[] stream = new ushort[numTriangles * 3];

                for (int f = 0; f < numTriangles; f++)
                {
                    Face face = GetTriangle(f);
                    for (int j = 0; j < 3; j++)
                        stream[f * 3 + j] = (ushort)(face[j] + index_offset);
                }
                return stream;
            }
        }
    }

    /// <summary>
    /// Triangles geometries not-indices
    /// </summary>
    public class Triangle : PrimitiveTriangle
    {    
        //    TRIANGLE-FAN        TRIANGLE-LIST         TRIANGLE-STRIP
        //     [0]______1         0______1   3          0_____1____ 3
        //      /|\    /           \    /   /\          \    /\    /
        //     / | \  /             \  /   /  \          \  /  \  /
        //    /__|__\/               \/   /____\          \/____\/
        //   4   3   2               2   5      4         2      4
        // 0-1-2-3-4 in clockwire

        /// <summary>
        /// a not-indexed geometry have always a primitives count
        /// </summary>
        public override int numTriangles
        {
            get
            {
                switch (primitiveType)
                {
                    case PrimitiveType.TriangleFan: return (numVertices - 2);
                    case PrimitiveType.TriangleList: return (numVertices / 3);
                    case PrimitiveType.TriangleStrip: return (numVertices - 2);
                    default: return 0;
                }
            }
        }
        
        public override Face GetTriangle(int i)
        {
            switch (primitiveType)
            {
                case PrimitiveType.TriangleFan: return new Face((ushort)0, (ushort)(i + 1), (ushort)(i + 2));
                case PrimitiveType.TriangleList: return new Face((ushort)(i * 3), (ushort)(i * 3 + 1), (ushort)(i * 3 + 2));
                case PrimitiveType.TriangleStrip: return new Face((ushort)(i), (ushort)(i + 1), (ushort)(i + 2));
            }
            return new Face();
        }

        public Triangle() : base()
        {
            vertices = null;
            normals = null;
            textures = null;
            colors = null;
        }
        
        public static explicit operator TriMesh(Triangle tri)
        {
            TriMesh mesh = new TriMesh();
            mesh.transform = tri.transform;
            mesh.vertexFormat = tri.vertexFormat;
            mesh.vertices = tri.vertices;
            mesh.normals =  tri.normals;
            mesh.textures = tri.textures;
            mesh.colors =   tri.colors;

            // build indices , the getTriangle methods return indices used for TriangleList
            if (tri.numTriangles > 0)
            {
                mesh.faces = new Face[tri.numTriangles];
                for (int f = 0; f < mesh.faces.Length; f++)
                    mesh.faces[f] = tri.GetTriangle(f);
            }
            return mesh;
        }


        public static Triangle SingleTriangle()
        {
            //       Y
            //       |______ Z
            //       / _/
            //      /_/
            //     X
            Triangle mesh = new Triangle();

            mesh.vertices = new Vector3[] {
                new Vector3(0, 0, 0),
                new Vector3(1, 0, 0),
                new Vector3(0, 0, 1)};

            mesh.colors = new Color[] {
                Color.Red,
                Color.Green,
                Color.Blue};

            mesh.normals = new Vector3[]{
                new Vector3(0,1,0),
                new Vector3(0,1,0),
                new Vector3(0,1,0)};

            mesh.textures = new Vector2[]{
                new Vector2(0,0),
                new Vector2(0,1),
                new Vector2(1,0)};

            mesh.primitiveType = PrimitiveType.TriangleList;
            mesh.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;


            mesh.boundSphere = BoundarySphere.GetBoundingSphereFast(mesh.vertices);

            return mesh;
        }
        public static Triangle Piramid()
        {
            Vector3 p0 = new Vector3(0, 1, 0);
            Vector3 p1 = new Vector3(1, 0, 0);
            Vector3 p2 = new Vector3(0, 0, -1);
            Vector3 p3 = new Vector3(-1, 0, 0);
            Vector3 p4 = new Vector3(0, 0, 1);

            Triangle mesh = new Triangle();

            mesh.vertices = new Vector3[]{
                p0,p1,p2,
                p0,p2,p3,
                p0,p3,p4,
                p0,p4,p1,
                p1,p3,p2,
                p1,p4,p3
            };
            mesh.colors = new Color[mesh.vertices.Length];
            mesh.colors[0] = mesh.colors[1] = mesh.colors[2] = Color.Red;
            mesh.colors[3] = mesh.colors[4] = mesh.colors[5] = Color.Green;
            mesh.colors[6] = mesh.colors[7] = mesh.colors[8] = Color.Blue;
            mesh.colors[9] = mesh.colors[10] = mesh.colors[11] = Color.Cyan;
            mesh.colors[12] = mesh.colors[13] = mesh.colors[14] = Color.Yellow;
            mesh.colors[15] = mesh.colors[16] = mesh.colors[17] = Color.Magenta;

            mesh.textures = new Vector2[mesh.vertices.Length];
            for (int i = 0; i < mesh.textures.Length; i+=3)
            {
                mesh.textures[i + 0] = new Vector2(0.5f, 1);
                mesh.textures[i + 1] = new Vector2(1, 0);
                mesh.textures[i + 2] = new Vector2(0, 0);
            }

            mesh.SetDefaultNormal();

            mesh.primitiveType = PrimitiveType.TriangleList;
            
            // Set the default render mode 
            mesh.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;

            mesh.boundSphere = BoundarySphere.GetBoundingSphereFast(mesh.vertices);

            return mesh;
        }
        public static Triangle Quad()
        {
            Triangle mesh = new Triangle();

            mesh.vertices = new Vector3[]{
                new Vector3(1,1,0),
                new Vector3(0,1,0),
                new Vector3(0,0,0),
                new Vector3(1,0,0)};

            mesh.textures = new Vector2[]{
                new Vector2(0,1),
                new Vector2(1,1),
                new Vector2(1,0),
                new Vector2(0,0)};

            mesh.normals = new Vector3[]{
                new Vector3(0,0,1),
                new Vector3(0,0,1),
                new Vector3(0,0,1),
                new Vector3(0,0,1)};

            mesh.colors = new Color[]{
                Color.Red,
                Color.Green,
                Color.Blue,
                Color.White};

            mesh.primitiveType = PrimitiveType.TriangleFan;
            mesh.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;
            mesh.boundSphere = new BoundarySphere(new Vector3(0.5f, 0.5f, 0), 0.5f);

            return mesh;
        }

    }

    /// <summary>
    /// Triangles geometries indixed
    /// </summary>
    public class TriMesh : PrimitiveTriangle
    {
        // can't be changed
        public override PrimitiveType primitiveType
        {
            get { return PrimitiveType.TriangleList; }
            set { }
        }
        public Face[] faces;

        public override int numTriangles { get { return (faces != null) ? faces.Length : 0; } }
        
        public override Face GetTriangle(int i) { return faces[i]; }

        public override void SetDefaultNormal()
        {
            normals = GeometryTools.GetDefaultNormal(vertices, faces);
        }

        public TriMesh() : base()
        {
            m_primitive = PrimitiveType.TriangleList;
        }

        /// <summary>
        /// A simple rectangle oriented to normal, used only for debug planes
        /// </summary>
        public static TriMesh Plane(Plane plane)
        {
            TriMesh quad = new TriMesh();

            quad.faces = new Face[]{
                new Face(0,1,2),
                new Face(0,2,3)};

            // oriented to Z
            quad.vertices = new Vector3[]{
                new Vector3(-1,-1,0),
                new Vector3(-1, 1,0),
                new Vector3( 1, 1,0),
                new Vector3( 1,-1,0)};

            quad.textures = new Vector2[]{
                new Vector2(0,1),
                new Vector2(0,0),
                new Vector2(1,0),
                new Vector2(1,1)};

            quad.normals = new Vector3[]{
                new Vector3(0,0,-1),
                new Vector3(0,0,-1),
                new Vector3(0,0,-1),
                new Vector3(0,0,-1)};

            quad.colors = new Color[]{
                Color.Red,
                Color.Green,
                Color.Blue,
                Color.White};

            quad.vertexFormat = VertexFormat.Position | VertexFormat.Diffuse;


            Matrix4 transform = Matrix4.Identity;
            Vector3 vz = plane.norm;
            Vector3 vx = Vector3.Cross(vz, Vector3.UnitY);
            Vector3 vy = Vector3.Cross(vz, vx);

            vx.Normalize();
            vy.Normalize();
            vz.Normalize();

            transform.setRow(0, vx);
            transform.setRow(1, vy);
            transform.setRow(2, vz);

            transform = Matrix4.Inverse(transform);

            transform.TranslationComponent = plane.Origin;    
            quad.transform = transform;

            quad.boundSphere.radius = 1;
            return quad;
        }
        /// <summary>
        /// A cube with 6 quads detached, usefull when use texture for each quad.
        /// </summary>
        public static TriMesh CubeOpen()
        {
            TriMesh mesh = new TriMesh { name = "CubeOpen"};

            mesh.faces = new Face[12];
            mesh.vertices = new Vector3[24];
            mesh.colors = new Color[24];
            mesh.normals = new Vector3[24];
            mesh.textures = new Vector2[24];

            //front
            mesh.faces[0] = new Face(3, 1, 0);
            mesh.faces[1] = new Face(3, 0, 2);
            mesh.vertices[0] = new Vector3(1, 1, 1);
            mesh.vertices[1] = new Vector3(1, 1, -1);
            mesh.vertices[2] = new Vector3(1, -1, 1);
            mesh.vertices[3] = new Vector3(1, -1, -1);
            mesh.normals[0] = mesh.normals[1] = mesh.normals[2] = mesh.normals[3] = new Vector3(1, 0, 0);
            mesh.colors[0] = mesh.colors[1] = mesh.colors[2] = mesh.colors[3] = Color.Red;
            //right
            mesh.faces[2] = new Face(3, 1, 0) + 4;
            mesh.faces[3] = new Face(3, 0, 2) + 4;
            mesh.vertices[4] = new Vector3(1, 1, 1);
            mesh.vertices[5] = new Vector3(1, -1, 1);
            mesh.vertices[6] = new Vector3(-1, 1, 1);
            mesh.vertices[7] = new Vector3(-1, -1, 1);
            mesh.normals[4] = mesh.normals[5] = mesh.normals[6] = mesh.normals[7] = new Vector3(0, 0, 1);
            mesh.colors[4] = mesh.colors[5] = mesh.colors[6] = mesh.colors[7] = Color.Green;
            //back
            mesh.faces[4] = new Face(3, 0, 1) + 8;
            mesh.faces[5] = new Face(3, 2, 0) + 8;
            mesh.vertices[8] = new Vector3(-1, 1, 1);
            mesh.vertices[9] = new Vector3(-1, 1, -1);
            mesh.vertices[10] = new Vector3(-1, -1, 1);
            mesh.vertices[11] = new Vector3(-1, -1, -1);
            mesh.normals[8] = mesh.normals[9] = mesh.normals[10] = mesh.normals[11] = new Vector3(-1, 0, 0);
            mesh.colors[8] = mesh.colors[9] = mesh.colors[10] = mesh.colors[11] = Color.Blue;
            //left
            mesh.faces[6] = new Face(3, 0, 1) + 12;
            mesh.faces[7] = new Face(3, 2, 0) + 12;
            mesh.vertices[12] = new Vector3(1, 1, -1);
            mesh.vertices[13] = new Vector3(1, -1, -1);
            mesh.vertices[14] = new Vector3(-1, 1, -1);
            mesh.vertices[15] = new Vector3(-1, -1, -1);
            mesh.normals[12] = mesh.normals[13] = mesh.normals[14] = mesh.normals[15] = new Vector3(0, 0, -1);
            mesh.colors[12] = mesh.colors[13] = mesh.colors[14] = mesh.colors[15] = Color.Yellow;
            //top
            mesh.faces[8] = new Face(3, 0, 1) + 16;
            mesh.faces[9] = new Face(3, 2, 0) + 16;
            mesh.vertices[16] = new Vector3(1, 1, 1);
            mesh.vertices[17] = new Vector3(1, 1, -1);
            mesh.vertices[18] = new Vector3(-1, 1, 1);
            mesh.vertices[19] = new Vector3(-1, 1, -1);
            mesh.normals[16] = mesh.normals[17] = mesh.normals[18] = mesh.normals[19] = new Vector3(0, 1, 0);
            mesh.colors[16] = mesh.colors[17] = mesh.colors[18] = mesh.colors[19] = Color.Magenta;
            //bottom
            mesh.faces[10] = new Face(3, 1, 0) + 20;
            mesh.faces[11] = new Face(3, 0, 2) + 20;
            mesh.vertices[20] = new Vector3(1, -1, 1);
            mesh.vertices[21] = new Vector3(1, -1, -1);
            mesh.vertices[22] = new Vector3(-1, -1, 1);
            mesh.vertices[23] = new Vector3(-1, -1, -1);
            mesh.normals[20] = mesh.normals[21] = mesh.normals[22] = mesh.normals[23] = new Vector3(0, -1, 0);
            mesh.colors[20] = mesh.colors[21] = mesh.colors[22] = mesh.colors[23] = Color.Cyan;

            for (int i = 0; i < 6; i++)
            {
                mesh.textures[i * 4 + 0] = new Vector2(0, 0);
                mesh.textures[i * 4 + 1] = new Vector2(0, 1);
                mesh.textures[i * 4 + 2] = new Vector2(1, 0);
                mesh.textures[i * 4 + 3] = new Vector2(1, 1);
            }
            mesh.boundSphere.radius = (float)Math.Sqrt(3);
            mesh.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;

            return mesh;
        }
        /// <summary>
        /// A default cube with 8 vertices and 12 faces.
        /// </summary>
        public static TriMesh CubeClose()
        {
            //           5______4
            //           /     /|         Y
            //         1/_____/0|         |
            //          | 7   | /6        *---> Z
            //          |_____|/         /
            //         3      2         X

            TriMesh cube = new TriMesh { name = "CubeClose" };

            cube.vertices = new Vector3[]{
                new Vector3( 1, 1, 1),
                new Vector3( 1, 1,-1),
                new Vector3( 1,-1, 1),
                new Vector3( 1,-1,-1),
                new Vector3(-1, 1, 1),
                new Vector3(-1, 1,-1),
                new Vector3(-1,-1, 1),
                new Vector3(-1,-1,-1)};

            cube.faces = new Face[]{
                //front
                new Face(3,1,0),
                new Face(3,0,2),
                //top
                new Face(1,5,4),
                new Face(1,4,0),
                //back
                new Face(4,7,6),
                new Face(4,5,7),
                //bottom
                new Face(7,2,6),
                new Face(7,3,2),
                //right
                new Face(0,4,6),
                new Face(0,6,2),
                //left
                new Face(3,5,1),
                new Face(3,7,5)};

            cube.colors = new Color[]{
                Color.Red,
                Color.Blue,
                Color.Green,
                Color.Magenta,
                Color.Cyan,
                Color.Yellow,
                Color.White,
                Color.Black};

            cube.normals = new Vector3[cube.vertices.Length];
            for (int i = 0; i < cube.vertices.Length; i++)
                cube.normals[i] = cube.vertices[i].Normal;

            cube.boundSphere.radius = (float)Math.Sqrt(3);
            cube.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Diffuse;

            return cube;
        }
        /// <summary>
        /// A classic sphere
        /// </summary>
        /// <param name="slices">verticals lines</param>
        /// <param name="stacks">orizontals lines</param>
        public static TriMesh Sphere(int slices, int stacks, float radius)
        {
            TriMesh mesh = new TriMesh { name = "Sphere" };

            int numVerticesPerRow = slices + 1;
            int numVerticesPerColumn = stacks + 1;

            int numverts = numVerticesPerRow * numVerticesPerColumn;
            mesh.vertices = new Vector3[numverts];
            mesh.colors = new Color[numverts];
            mesh.normals = new Vector3[numverts];

            float theta = 0.0f;
            float phi = 0.0f;
            float verticalAngularStride = (float)Math.PI / (float)stacks;
            float horizontalAngularStride = ((float)Math.PI * 2) / (float)slices;
            int i = 0;

            for (int ivertical = 0; ivertical < numVerticesPerColumn; ivertical++)
            {
                // beginning on top of the sphere:
                theta = ((float)Math.PI / 2.0f) - verticalAngularStride * ivertical;

                for (int ihorizontal = 0; ihorizontal < numVerticesPerRow; ihorizontal++)
                {
                    phi = horizontalAngularStride * ihorizontal;
                    float x = radius * (float)Math.Cos(theta) * (float)Math.Cos(phi);
                    float y = radius * (float)Math.Cos(theta) * (float)Math.Sin(phi);
                    float z = radius * (float)Math.Sin(theta);
                    mesh.vertices[i++] = new Vector3(x, z, y);
                }
            }

            for (i = 0; i < numverts; i++)
            {
                mesh.normals[i] = mesh.vertices[i].Normal;
                mesh.colors[i] = Color.Blue;
            }   


            int numfaces = slices * stacks * 2;
            i = 0;
            mesh.faces = new Face[numfaces];

            for (int verticalIt = 0; verticalIt < stacks; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    int lt = horizontalIt + verticalIt * (numVerticesPerRow);
                    int rt = (horizontalIt + 1) + verticalIt * (numVerticesPerRow);

                    int lb = horizontalIt + (verticalIt + 1) * (numVerticesPerRow);
                    int rb = (horizontalIt + 1) + (verticalIt + 1) * (numVerticesPerRow);

                    mesh.faces[i++] = new Face(lt, rt, lb);
                    mesh.faces[i++] = new Face(rt, rb, lb);
                }
            }
            mesh.textures = GeometryTools.GetCilindralProjection(mesh.vertices, Matrix4.Identity);
            mesh.boundSphere.radius = radius;

            return mesh;
        }
        /// <summary>
        /// Cone with Y peak , heigth = 1.0 , base circle in XZ plane with radius = 1.0  
        /// </summary>
        public static TriMesh Cone(int basePoints)
        {
            float dang = (float)Math.PI * 2.0f / basePoints;
            int numverts = basePoints + 1;

            TriMesh obj = new TriMesh { name = "Cone" };

            obj.vertices = new Vector3[numverts];
            obj.colors = new Color[numverts];
            obj.faces = new Face[basePoints * 2 - 2];

            // base vertices 
            for (int i = 0; i < numverts; i++)
            {
                obj.vertices[i] = new Vector3((float)Math.Sin(dang * i), 0, (float)Math.Cos(dang * i));
                obj.colors[i] = Color.Blue;
            }
            // peak vertex
            obj.vertices[numverts - 1] = new Vector3(0, 1, 0);

            // cone faces
            int f = 0;
            for (int i = 0; i < basePoints - 1; i++)
                obj.faces[f++] = new Face(i, i + 1, numverts - 1);
            obj.faces[f++] = new Face(basePoints - 1, 0, basePoints);

            // base faces
            for (int i = 0; i < basePoints - 2; i++)
                obj.faces[f++] = new Face(0, i + 2, i + 1);

            obj.boundSphere = BoundarySphere.GetBoundingSphereFast(obj.vertices);

            return obj;
        }

        /// <summary>
        /// Cylinder with base center [0,0,0]
        /// </summary>
        public static TriMesh Cylinder(float radius, float height, int slices)
        {
            TriMesh mesh = new TriMesh { name = "Cylinder" };

            int numverts = (slices + 1) * 2 + 2;
            float theta = 0.0f;
            float horizontalAngularStride = ((float)Math.PI * 2) / (float)slices;
            int i = 0;

            mesh.vertices = new Vector3[numverts];
            mesh.colors = new Color[numverts];

            for (i = 0; i < numverts; i++) mesh.colors[i] = Color.Blue;

            i = 0;
            for (int verticalIt = 0; verticalIt < 2; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices + 1; horizontalIt++)
                {
                    float x;
                    float y;
                    float z;

                    theta = (horizontalAngularStride * horizontalIt);

                    if (verticalIt == 0)
                    {
                        // upper circle
                        x = radius * (float)Math.Cos(theta);
                        z = radius * (float)Math.Sin(theta);
                        y = height;
                        
                    }
                    else
                    {
                        // lower circle
                        x = radius * (float)Math.Cos(theta);
                        z = radius * (float)Math.Sin(theta);
                        y = 0;
                    }
                    mesh.vertices[i++] = new Vector3(x, y, z);
                }
            }
            mesh.vertices[i++] = new Vector3(0, height, 0);
            mesh.vertices[i++] = new Vector3(0, 0, 0);

            int nfaces = slices * 4;
            mesh.faces = new Face[nfaces];
            i = 0;
            for (int verticalIt = 0; verticalIt < 1; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    ushort lt = (ushort)(horizontalIt + verticalIt * (slices + 1));
                    ushort rt = (ushort)((horizontalIt + 1) + verticalIt * (slices + 1));
                    ushort lb = (ushort)(horizontalIt + (verticalIt + 1) * (slices + 1));
                    ushort rb = (ushort)((horizontalIt + 1) + (verticalIt + 1) * (slices + 1));

                    mesh.faces[i++] = new Face(lt, rt, lb);
                    mesh.faces[i++] = new Face(rt, rb, lb);
                }
            }

            for (int verticalIt = 0; verticalIt < 1; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    ushort lt = (ushort)(horizontalIt + verticalIt * (slices + 1));
                    ushort rt = (ushort)((horizontalIt + 1) + verticalIt * (slices + 1));

                    ushort patchIndexTop = (ushort)((slices + 1) * 2);
                    mesh.faces[i++] = new Face(lt, patchIndexTop, rt);
                }
            }

            for (int verticalIt = 0; verticalIt < 1; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    ushort lb = (ushort)(horizontalIt + (verticalIt + 1) * (slices + 1));
                    ushort rb = (ushort)((horizontalIt + 1) + (verticalIt + 1) * (slices + 1));
                    ushort patchIndexBottom = (ushort)((slices + 1) * 2 + 1);
                    mesh.faces[i++] = new Face(lb, rb, patchIndexBottom);
                }
            }

            mesh.normals = GeometryTools.GetDefaultNormal(mesh.vertices, mesh.faces);


            mesh.textures = GeometryTools.GetCilindralProjection(mesh.vertices, Matrix4.Identity);
            mesh.boundSphere.center = new Vector3(0, height * 0.5f, 0);
            mesh.boundSphere.radius = (float)Math.Sqrt(radius * radius + height * height);
            return mesh;
        }
    
    }
}
