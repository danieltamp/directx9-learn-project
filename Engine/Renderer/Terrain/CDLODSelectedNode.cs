﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer.Terrain
{
    public struct CDLODSelectedNode
    {
        public int X;
        public int Y;
        public int Size;
        public float MinHeight;
        public float MaxHeight;
        public int Level;

        public void GetBoundingBox(ref Vector3 terrainOffset, float mapScale, float heightScale, out BoundaryAABB boundingBox)
        {
            Vector3 min = new Vector3
            {
                x = X * mapScale + terrainOffset.x,
                y = MinHeight * heightScale + terrainOffset.y,
                z = Y * mapScale + terrainOffset.z
            };
            Vector3 max = new Vector3
            {
                x = (X + Size) * mapScale + terrainOffset.x,
                y = MaxHeight * heightScale + terrainOffset.y,
                z = (Y + Size) * mapScale + terrainOffset.z
            };
            boundingBox = new BoundaryAABB(max,min);

        }
    }
}
