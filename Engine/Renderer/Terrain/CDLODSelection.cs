﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer.Terrain
{
    public sealed class CDLODSelection
    {
        public const int MaxSelectedNodeCount = 500;
        
        public Vector3 TerrainOffset;
        public Matrix4 View;
        public Matrix4 Projection;
        public Vector3 EyePosition;

        public Texture2D HeightMapTexture;
        public Texture2D NormalMapTexture;
        public CDLODSettings Settings;

        CDLODSelectedNode[] selectedNodes;

        public ICDLODVisibleRanges VisibleRanges { get; private set; }
        public Frustum Frustum { get; private set; }
        public int SelectedNodeCount { get; private set; }

        public CDLODSelection(CDLODSettings settings, ICDLODVisibleRanges visibleRanges)
        {
            Settings = settings;
            VisibleRanges = visibleRanges;

            Frustum = new Frustum(Matrix4.Identity, Matrix4.Identity, Matrix4.Identity);

            selectedNodes = new CDLODSelectedNode[MaxSelectedNodeCount];
        }

        public void Prepare()
        {
            // Calculate the eye position from a view matrix.
            Matrix4 inverseView = Matrix4.Inverse(View);

            EyePosition = inverseView.TranslationComponent;
            // Update the view frustum.
            /////Frustum.Matrix = View * Projection;
            Frustum = new Frustum(Projection, View, Matrix4.Identity);
        }

        public void ClearSelectedNodes()
        {
            // Reset the counter.
            SelectedNodeCount = 0;
        }

        public void GetSelectedNode(int index, out CDLODSelectedNode selectedNode)
        {
            selectedNode = selectedNodes[index];
        }

        public void GetVisibilitySphere(int level, out BoundarySphere sphere)
        {
            sphere = new BoundarySphere(EyePosition, VisibleRanges[level]);
        }

        internal void AddSelectedNode(Node node)
        {
            if (MaxSelectedNodeCount <= SelectedNodeCount) return;

            selectedNodes[SelectedNodeCount++] = new CDLODSelectedNode
            {
                X = node.X,
                Y = node.Y,
                MinHeight = node.MinHeight,
                MaxHeight = node.MaxHeight,
                Size = node.Size,
                Level = node.Level
            };
        }

        internal void GetPatchInstanceVertex(int index, out PatchInstanceVertex instance)
        {
            instance = new PatchInstanceVertex();
            instance.Offset.x = selectedNodes[index].X * Settings.MapScale;
            instance.Offset.y = selectedNodes[index].Y * Settings.MapScale;
            instance.Scale = selectedNodes[index].Size * Settings.MapScale;
            instance.Level = selectedNodes[index].Level;
        }
    }
}
