﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer.Terrain
{
    public sealed class CDLODTerrain
    {
        CDLODSettings settings;
        QuadTree quadTree;

        public IMap<float> HeightMap { get; set; }

        public CDLODTerrain(CDLODSettings settings)
        {
            this.settings = settings;
            quadTree = new QuadTree(settings);
        }

        public void Build()
        {
            if (HeightMap == null)
                throw new InvalidOperationException("HeightMap is null.");

            // Build the quadtree.
            quadTree.Build(HeightMap);
        }

        public void Select(CDLODSelection selection)
        {
            // Prepare selection's state per a terrain.
            selection.ClearSelectedNodes();

            // Select visible nodes.
            quadTree.Select(selection);
        }
    }
}
