﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer.Terrain
{
    public interface ICDLODVisibleRanges
    {
        int Count { get; }

        float this[int level] { get; }

        // Muse invoke this method after constructing or setting properties.
        void Initialize();
    }
}
