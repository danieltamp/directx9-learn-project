﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Renderer;
using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Scene
{
    /// <summary>
    /// Sort node in the scene using binary splitting
    /// </summary>
    public class SceneManagerBinary : SceneManagerBasic
    {
        /// <summary>
        /// Need the range of world and level of suddivisions
        /// </summary>
        public SceneManagerBinary(Vector3 Max, Vector3 Min, int deph)
            : base()
        {
        }

        public override void AddSceneNode(ISceneNode node)
        {
            base.AddSceneNode(node);
        }
        public override ISceneNode PickSceneNode(Ray ray, bool firstHit)
        {
            return null;
        }
        /// <summary>
        /// Draw to panel the scene using your custom options 
        /// </summary>
        public override void Render(GraphicPanel panel, ViewportOption options)
        {
            if (renderer == null) return;
            renderer.Draw();

            foreach (ISceneNode node in Nodes)
            {
                Point pos = panel.WorldToPoint(node.transform.TranslationComponent);
                renderer.fontDx.Draw(node.name.ToString(), pos.Y,pos.Y, Color.Black);
            }
        }
        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
