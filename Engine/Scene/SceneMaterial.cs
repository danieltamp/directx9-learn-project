﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;
using Engine.Resources;
using Engine.Graphics;


namespace Engine.Scene
{
    public class SceneMaterial : IKey
    {
        public uint m_key { get; set; }
        public bool m_removed { get; set; }

        internal NodeMaterial m_material = null;

        protected SceneManager manager = null;
        
        /// <summary>
        /// Custom name, can be the same than other scene material because isn't used to search from a list
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        
        /// <summary>
        /// Generate a new SceneMaterial, the constructor insert this class in the SceneManager
        /// </summary>
        public SceneMaterial(SceneManager manager)
        {
            this.m_key = 0;
            this.m_removed = false;
            this.manager = manager;
            this.Name = "Material";

            if (manager == null)
                throw new ArgumentNullException("Need a manager class");

            bool state = this.manager.Add(this);
            if (!state || m_key == 0)
                throw new Exception("material can't added to manager");
        }
        /// <summary>
        /// don't forghet to remove renderer resources when you lost this class. 
        /// </summary>
        ~SceneMaterial()
        {
            this.Dispose();
        }
        /// <summary>
        /// Remove this class from SceneManager, need a overload to remove also from renderer.
        /// </summary>
        public virtual void Dispose()
        {
            // handle == 0 mean already removed
            if (m_key > 0)
                manager.Remove(this);
        }

        public static SceneMaterial Default(SceneManager manager)
        {
            SceneMaterial mat = new SceneMaterial(manager);
            mat.m_material = NodeMaterial.Default(manager.renderer.m_device);
            mat.Name = "DefaultMaterial";
            return mat;
        }
    }
}
