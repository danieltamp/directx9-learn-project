﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;
using Engine.Resources;
using Engine.Graphics;


namespace Engine.Scene
{

    public interface IUpdatable
    {
        void Update(TrackballCamera camera);
    }
    /// <summary>
    /// All graphics, effect, lights ecc... inerits to 3d world must use this methods
    /// </summary>
    public interface ISceneNode : IHandle, ITrasformable, IUpdatable
    {
        /// <summary>
        /// Not used by renderer engine , can be equal to another scene nodes
        /// </summary>
        string name { get; set; }

        Vector3 position { get; set; }
        /// <summary>
        /// A scene node can contain more than one DXNode instances, example when contain different primitive types
        /// </summary>
        Node[] geometries { get; }
        /// <summary>
        /// Bounding sphere
        /// </summary>
        BoundarySphere boundSphere { get; }
        /// <summary>
        /// Used example for scene octree manager
        /// </summary>
        BoundaryAABB boundAABB { get; }
        /// <summary>
        /// Foreach DxNode use their intersection implementations
        /// </summary>
        bool IntersectGeometries(Ray ray, out float t);

        bool Selected { get; set; }
        bool Hide { get; set; }
    }


    /// <summary>
    /// SceneNode
    /// </summary>
    public class SceneNode2 : ISceneNode
    {
        /// <summary>
        /// unique key definition, was used by engine to managed its, can't be modifier by user
        /// </summary>
        internal uint handle { get; set; }
        /// <summary>
        /// Gets unique key definition
        /// </summary>
        public uint Handle { get { return handle; } }

        /// <summary>
        /// Scene Node Coordinate System
        /// </summary>
        public Matrix4 TransformMatrix
        {
            get { return localCoord; }
            set { }
        }





        protected Matrix4 localCoord = Matrix4.Identity;
        protected Node[] dxnodes = new Node[0];
        protected Matrix4[] dxtransform = new Matrix4[0]; // relative transform of dxnodes[i] from "localCoord"

        protected BoundarySphere bSphere = BoundarySphere.Empty; // used for collision
        protected BoundaryAABB bAABB = BoundaryAABB.Empty; // used for spatial partitioning
        protected BoundaryOBB bOBB = BoundaryOBB.Empty;


        public string name
        {
            get;
            set;
        }

        public bool Selected { get; set; }
        public bool Hide { get; set; }

        /// <summary>
        /// Scene Node Coordinate system
        /// </summary>
        public virtual Matrix4 transform
        {
            //If change, also all relative DxNode coordinate are changed
            get { return localCoord; }
            set
            {
                Vector3 traslation = value.TranslationComponent - localCoord.TranslationComponent;

                //ATTENTION : is a simple traslation only when there aren't a scaling transformation (the scene node rotate inside it's sphere)  
                bSphere.center += traslation;

                //ATTENTION : isn't a simple traslation, need to update the Max Min value for the new transformation
                bAABB.center += traslation;

                localCoord = value;
                for (int i = 0; i < dxnodes.Length; i++)
                    dxnodes[i].transform = dxtransform[i] * localCoord;
            }
        }
        public Vector3 position
        {
            get { return localCoord.TranslationComponent; }
            set { Traslate(value - localCoord.TranslationComponent); }
        }
        /// <summary>
        /// Traslate the entire scene node (position change have less operations than other transformations)
        /// </summary>
        public void Traslate(Vector3 vector)
        {
            localCoord.Translate(vector);

            foreach (Node node in dxnodes)
            {
                Matrix4 mat = node.transform;
                mat.Translate(vector);
                node.transform = mat;
            }

            bSphere.center += vector;
            bAABB.center += vector;
        }

        public Node[] geometries { get { return dxnodes; } }

        public BoundarySphere boundSphere { get { return bSphere; } }
        public BoundaryAABB boundAABB { get { return bAABB; } }

        /// <summary>
        /// </summary>
        public SceneNode2()
        {
            name = "<SceneNodeName>";
            Selected = false;
            Hide = false;
        }

        /// <summary>
        /// Faster intersection test
        /// </summary>
        public bool IntersectBoundingSphere(Ray ray, out float t_enter, out float t_exit)
        {
            return bSphere.IntersectRay(ray, out t_enter, out t_exit);
        }
        /// <summary>
        /// Used for a spatial partitioning
        /// </summary>
        public bool IntersectBoundingAABB(Ray ray, out float t)
        {
            return bAABB.IntersectRay(ray, out t);
        }
        /// <summary>
        /// Accurate test for all primitives (triangle , line or points) in the dxnode list
        /// </summary>
        public bool IntersectGeometries(Ray ray, out float t)
        {
            t = 0;
            Vector3 inter;
            foreach (Node node in dxnodes)
                if (node.PrimitiveIntersection(ray, out t, out inter) > -1)
                    return true;
            return false;
        }

        public static SceneNode2 Box()
        {
            SceneNode2 node = new SceneNode2();
            node.name = "SceneBox";
            node.dxnodes = new Node[] { new ObjCube() };
            node.dxtransform = new Matrix4[] { Matrix4.Identity };
            node.bSphere = new BoundarySphere(new Vector3(0, 0, 0), (float)Math.Sqrt(3));
            node.bAABB = new BoundaryAABB(new Vector3(0, 0, 0), 2, 2, 2);
            return node;
        }

        public override string ToString()
        {
            return name;
        }

        public void Update(TrackballCamera camera)
        {

        }
    }


    public class SceneNode : ILink<SceneNode>, IHandle
    {
        bool isSelected = false;
        bool isHidden = false;

        /// <summary>
        /// unique key definition, was used by engine to managed its, can't be modifier by user
        /// </summary>
        public uint handle { get; set; }
        /// <summary>
        /// Gets unique key definition
        /// </summary>
        public uint Handle { get { return handle; } }

        public SceneNode Next { get; set; }
        public SceneNode Prev { get; set; }
        public bool marked2remove { get { return false; } }

        public bool Select
        {
            get { return isSelected; }
            set { if (value != isSelected) { isSelected = value; NeedRendererUpdate(UpdateModeEnum.Force); } }
        }
        public bool Hide
        {
            get { return isHidden; }
            set { if (value != isHidden) { isHidden = value; NeedRendererUpdate(UpdateModeEnum.Force); } }
        }

        void NeedRendererUpdate(UpdateModeEnum mode)
        {

        }

        public SceneNode(SceneManagerBasic manager)
        {
            //manager.AddSceneNode(this);

            if (handle == 0)
                throw new Exception("node can't added to manager");
        }

    }



    /// <summary>
    /// Basic Triangle mesh node, internal limit of 65535 vertices
    /// </summary>
    public class SceneMesh : SceneNode
    {
        internal TriMesh mesh = new TriMesh(); // store the geometry
        internal BoundarySphere bSphere = BoundarySphere.Empty; // used for collision
        internal BoundaryAABB bAABB = BoundaryAABB.Empty; // used for spatial partitioning
        internal BoundaryOBB bOBB = BoundaryOBB.Empty; // used to update bounding sphere

        /// <summary>
        /// Scene Node Coordinate System
        /// </summary>
        public Matrix4 TransformMatrix
        {
            get { return mesh.transform; }
            set { mesh.transform = value; }
        }

        public SceneMesh(SceneManagerBasic manager)
            : base(manager)
        {

        }

    }




    /// <summary>
    /// SceneNode optimized for a pure triangle mesh
    /// </summary>
    public class SceneTriMesh : SceneNode2
    {


        /// <summary>
        /// Scene Node Coordinate system, to improve a little i override previous version, dxnodes have the same coord of 
        /// </summary>
        public override Matrix4 transform
        {
            //If change, also all relative DxNode coordinate are changed
            get { return localCoord; }
            set
            {
                localCoord = value;
                dxnodes[0].transform = localCoord;
            }
        }
        /// <summary>
        /// The Vertices are in local coord system
        /// </summary>
        public SceneTriMesh(Vector3[] vertices, Face[] faces, Vector2[] textures, Vector3[] normals, int textureID)
        {
            name = "<SceneTriMesh>";
            TriMesh trimesh = new TriMesh();
            trimesh.vertices = vertices;
            trimesh.textures = textures;
            trimesh.normals = normals;
            trimesh.faces = faces;

            bSphere = BoundarySphere.GetBoundingSphereFast(vertices);
            bAABB = BoundaryAABB.GetBoundaryAABB(bSphere);

            dxnodes = new Node[] { trimesh };
            // dxnodes[0] transformation from this scene node are coincident with its globalcoord
            dxtransform = new Matrix4[] { Matrix4.Identity };

            Selected = false;
            Hide = false;
        }
    }
}