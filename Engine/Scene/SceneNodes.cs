﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;
using Engine.Resources;
using Engine.Graphics;


namespace Engine.Scene
{
    public abstract class SceneNode : IKey
    {
        public uint m_key { get; set; }
        public bool m_removed { get; set; }

        SceneMaterial m_material = null;
        bool selected = false;
        bool show = true;

        /// <summary>
        /// Necessary to automatize Add , Remove and Update operations
        /// </summary>
        protected SceneManager manager = null;
        /// <summary>
        /// TODO : implement the possibilty to use more than one node
        /// </summary>
        internal Node geometry = null;
        /// <summary>
        /// used for collision with ray, coordinate system is local : need use center = Vector3.TransformCoordinate(bSphere.center)
        /// </summary>
        internal BoundarySphere bSphere = BoundarySphere.NaN;
        /// <summary>
        /// used for spatial partitioning , coordinate system is local
        /// </summary>
        internal BoundaryAABB bAABB = BoundaryAABB.NaN;
        /// <summary>
        /// used to update bounding sphere or store the minimum enclosing box because it orientation match with this.Transform
        /// </summary>
        internal BoundaryOBB bOBB = BoundaryOBB.NaN;
        /// <summary>
        /// Custom name, can be the same than other scene node because isn't used to search from a list
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        /// <summary>
        /// Generate a new SceneNode, the constructor insert this class in the SceneManager
        /// </summary>
        protected SceneNode(SceneManager manager)
        {
            this.m_key = 0;
            this.m_removed = false;
            this.manager = manager;
            this.Name = "SceneNode";

            if (manager == null)
                throw new ArgumentNullException("Need a manager class");

            bool state = this.manager.Add(this);
            if (!state || m_key == 0)
                throw new Exception("node can't added to manager");
        }
        /// <summary>
        /// don't forghet to remove renderer resources when you lost this class. 
        /// </summary>
        ~SceneNode()
        {
            //this.Dispose();
        }
        /// <summary>
        /// Remove this class from SceneManager, need a overload to remove also from renderer.
        /// </summary>
        public virtual void Remove()
        {
            // handle == 0 mean already removed
            if (m_key > 0)
                manager.Remove(this);
        }
        /// <summary>
        /// Scene Node Coordinate System
        /// </summary>
        public Matrix4 TransformMatrix
        {
            get { return geometry.transform; }
            set { geometry.transform = value; }
        }
        /// <summary>
        /// After applied changes, you need update renderer geometry. The user can decide when is time to update
        /// </summary>
        /*
        public void Update()
        {
            // is hidded but are in the renderer
            if (!show && geometry.Key > 0)
            {
                manager.renderer.RemoveNode(geometry);
            }
            // is visible but aren't in the renderer
            if (show && geometry.Key == 0)
            {
                manager.renderer.AddNode(geometry);
            }

            // is selected but aren't in the selection list
            if (selected && !manager.m_selection.Contain(this))
            {
                geometry.isAnimable = true;
                geometry.UpdateFlag = UpdateFlags.RenderState;
                manager.AddSelected(this);
            }

            // isn't selected but are in the selection list
            if (!selected && manager.m_selection.Contain(this))
            {
                geometry.isAnimable = false;
                geometry.UpdateFlag = UpdateFlags.RenderState;
                manager.RemoveSelected(this);
            }

            if (geometry.UpdateFlag != UpdateFlags.None)
            {
                manager.renderer.UpdateNode(geometry);
            }
        }
        */
        /// <summary>
        /// Get or Set the selection mode of this node, if a node is selected the renderer engine set it's buffer as dynamic to improve the 
        /// changes performance
        /// </summary>
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                // is selected but aren't in the selection list
                if (selected && !manager.m_selection.Contain(this))
                {
                    geometry.engineProp.isAnimable = true;
                    geometry.engineProp.updateflags = UpdateFlags.RenderState;
                    if (geometry.m_key > 0) manager.renderer.Update(geometry);

                    manager.add_selected(this);
                }
                // isn't selected but are in the selection list
                if (!selected && manager.m_selection.Contain(this))
                {
                    geometry.engineProp.isAnimable = false;
                    geometry.engineProp.updateflags = UpdateFlags.RenderState;
                    if (geometry.m_key > 0) manager.renderer.Update(geometry);

                    manager.remove_selected(this);
                }
            }
        }
        /// <summary>
        /// Get or Set the visibility of this node, if node is hidden the renderer engine remove it, can be a slow operation
        /// </summary>
        public bool Visible
        {
            get
            {
                return show;
            }
            set
            {
                show = value;
                // is hidded but are in the renderer
                if (!show && geometry.m_key > 0)
                {
                    manager.renderer.Remove(geometry);
                }
                // is visible but aren't in the renderer
                if (show && geometry.m_key == 0)
                {
                    manager.renderer.Add(geometry);
                }
            }
        }

        /// <summary>
        /// Get or Set an existing material to this node, if material will be destoyed the scenemanager set a default material to this node
        /// </summary>
        public SceneMaterial material
        {
            get { return m_material; }
            set
            {
                if (geometry is PrimitiveTriangle)
                {
                    PrimitiveTriangle mesh = (PrimitiveTriangle)geometry;
                    if (value != m_material)
                    {
                        m_material = value;
                        if (value != null)
                        {
                            if (mesh.m_material == null || mesh.m_material != value.m_material)
                            {
                                mesh.m_material = value.m_material;
                                mesh.engineProp.updateflags = UpdateFlags.Material;
                            }
                        }
                        else
                        {
                            mesh.m_material = null;
                            mesh.engineProp.updateflags = UpdateFlags.Material;
                        }
                    }
                }
            }
        }


        public override string ToString()
        {
            return Name + ",id:0x" + m_key.ToString();
        }
    }



    /// <summary>
    /// Basic Triangle mesh node, internal limit of 65535 vertices
    /// </summary>
    public class SceneMesh : SceneNode
    {
        int numVertices = 0;
        int numFaces = 0;

        /// <summary>
        /// Get or Set the geometry, if set the node must be Updated
        /// </summary>
        public TriMesh trimesh
        {
            get { return (TriMesh)base.geometry; }
            set
            {
                value.engineProp.updateflags = UpdateFlags.None;
                numVertices = value.numVertices;
                numFaces = value.numTriangles;
                value.m_material = ((TriMesh)geometry).m_material;
                manager.renderer.Remove(geometry);
                manager.renderer.Add(value);
                geometry = value;         
            }
        }
        
        #region MeshOps



        
        #endregion


        public SceneMesh(SceneManager manager)
            : base(manager)
        {

        }

        /// <summary>
        /// Remove this class from SceneManager and from Renderer
        /// </summary>
        public override void Remove()
        {
            base.Remove();
            if (base.geometry != null && base.geometry.m_key > 0)
                base.manager.renderer.Remove(base.geometry);
        }

        /// <summary>
        /// build a box with semi-lenght
        /// </summary>
        public static SceneMesh Box(SceneManager manager, float dx, float dy, float dz)
        {
            SceneMesh box = new SceneMesh(manager);
            box.Name = "SceneMesh_Box";

            TriMesh mesh = TriMesh.CubeOpen();
            mesh.vertexFormat = VertexFormat.Diffuse | VertexFormat.Position | VertexFormat.Normal;
            mesh.transform = Matrix4.Scaling(dx, dy, dz);
            mesh.changeTransform(Matrix4.Identity);
            mesh.engineProp.isAnimable = false;
            box.geometry = mesh;

            box.bSphere.center = box.geometry.boundSphere.center;
            box.bSphere.radius = (float)Math.Sqrt(dx * dx + dy * dy + dz * dz);
            box.bAABB.max = new Vector3(dx, dy, dz);
            box.bAABB.min = new Vector3(-dx, -dy, -dz);
            
            box.bOBB.center = new Vector3(0, 0, 0);
            box.bOBB.length = 1;
            box.bOBB.height = 1;
            box.bOBB.width = 1;
            box.bOBB.world = Matrix4.Scaling(dx, dy, dz);


            SplineShape gizmo = SplineShape.BoxGizmo(1, 1, 1);
            gizmo.transform = Matrix4.Scaling(dx, dy, dz);
            //box.manager.renderer.AddNode(gizmo);

            gizmo = SplineShape.SphereGizmo(box.bSphere.radius,15);
            gizmo.transform = Matrix4.Identity;
            //box.manager.renderer.AddNode(gizmo);


            box.manager.renderer.Add(box.geometry);

            return box;
        }
        /// <summary>
        /// build a sphere
        /// </summary>
        public static SceneMesh Sphere(SceneManager manager, int slices,int stacks, float radius)
        {
            SceneMesh sphere = new SceneMesh(manager);
            sphere.Name = "SceneMesh_Sphere";

            TriMesh mesh = TriMesh.Sphere(slices, stacks, radius);
            //mesh.vertexFormat = VertexFormat.Diffuse | VertexFormat.Position | VertexFormat.Normal;
            mesh.transform = Matrix4.Identity;
            mesh.engineProp.isAnimable = false;
            mesh.name += sphere.m_key;

            //mesh.m_material = manager.renderer.DefaultTextureNode;

            mesh.vertexFormat = VertexFormat.Texture1 | VertexFormat.Position | VertexFormat.Normal;


            sphere.geometry = mesh;
            sphere.bSphere.center = new Vector3(0, 0, 0);
            sphere.bSphere.radius = radius;
            sphere.bAABB.max = new Vector3(radius, radius, radius);
            sphere.bAABB.min = new Vector3(-radius, -radius, -radius);

            sphere.bOBB.center = new Vector3(0, 0, 0);
            sphere.bOBB.length = radius;
            sphere.bOBB.height = radius;
            sphere.bOBB.width = radius;

            SplineShape gizmo = SplineShape.BoxGizmo(radius, radius, radius);
            //box.manager.renderer.AddNode(gizmo);

            gizmo = SplineShape.SphereGizmo(radius, 15);
            gizmo.transform = Matrix4.Identity;
            //box.manager.renderer.AddNode(gizmo);

            sphere.manager.renderer.Add(sphere.geometry);

            return sphere;
        }

    }
}