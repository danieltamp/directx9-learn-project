﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Graphics;

namespace Engine.Renderer
{
    /// <summary>
    /// A Trackball camera is like 3dstudio max camera , in OnMouseMove i use the quaternion
    /// because is exactly the math interpretation of movement. UpdateView and UpdateProjection
    /// can be called only when necessay
    /// </summary>
    public class TrackballCamera
    {

        public CameraValues values;
        
        GraphicPanel panel;
        Point mouseStart = new Point(0, 0);
        bool mousing = false;
        bool orthogonal = false;
        float floatXpixel = 1.0f;
        float zoom = 1.0f;

        /// <summary>
        /// used for othogonal problem, is the view range of X axis, the other axis are scaled with aspect ratio.
        /// </summary>
        float planeWidth = 40.0f;


        public void setDefualt(ViewsList viewlist)
        {
            values.world = Matrix4.Identity;
            values.viewport = new Viewport
            {
                Width = panel.ClientSize.Width,
                Height = panel.ClientSize.Height
            };
            switch (viewlist)
            {
                default:
                case ViewsList.Prospective:
                    eye = new Vector3(20, 20, 20);
                    look = new Vector3(0, 0, 0);
                    up = new Vector3(0, 1, 0);
                    orthogonal = false;
                    break;

                case ViewsList.Front:
                    eye = new Vector3(0, 0, 20);
                    look = new Vector3(0, 0, 0);
                    up = new Vector3(0, 1, 0);
                    orthogonal = true;
                    break;
                case ViewsList.Top:
                    eye = new Vector3(0, 20, 0);
                    look = new Vector3(0, 0, 0);
                    up = new Vector3(0, 0, -1);
                    orthogonal = true;
                    break;
            }
            UpdateView();
            UpdateProjection(values.viewport);
        }


        // view matrix component
        public float nearZ = 0.1f;
        public float farZ = 3000f;
        public Vector3 eye = new Vector3(50, 50, 50);
        public Vector3 look = new Vector3(0, 0, 0);
        Vector3 up = new Vector3(0, 1, 0);
        
        // view matrix vector
        Vector3 vRight { get { return new Vector3(values.view.m11, values.view.m21, values.view.m31); } } // vettore x
        Vector3 vUp { get { return new Vector3(values.view.m12, values.view.m22, values.view.m32); } } // vettore y
        Vector3 vLook { get { return new Vector3(values.view.m13, values.view.m23, values.view.m33); } } // vettore z

        public TrackballCamera(GraphicPanel panelControl)
        {
            panel = panelControl;
            setDefualt(ViewsList.Prospective);
        }

        /// <summary>
        /// Update output values only when need
        /// </summary>
        public void UpdateView()
        {
            values.view = Matrix4.MakeViewLH(eye, look, up);
            values.inview = values.view.Inverse();
            zoom = Vector3.GetLength(eye - look);
        }

        /// <summary>
        /// Update output values only for projection when need,
        /// check if orthogonal or prospective
        /// </summary>
        public void UpdateProjection(Viewport viewport)
        {
            //float ratio = (float)mdevice.Viewport.Width / (float)mdevice.Viewport.Height;
            values.projection = orthogonal ?
                Matrix4.MakeOrthoLH(MathUtils.DegreeToRadian(45.0f), viewport, nearZ, farZ) :
                Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45.0f), viewport, nearZ, farZ);
                
                //Matrix.OrthoLH(planeWidth, planeWidth / ratio, nearZ, farZ) :
                //Matrix.PerspectiveFovLH(MathUtils.DegreeToRadian(45.0f), ratio, nearZ, farZ);
        }

        /// <summary>
        /// Store floatXpixel factor, usefull to calculate movement, not rotation
        /// </summary>
        public void OnMouseDown(object sender, MouseEventArgs e)
        {
            GraphicPanel panel = (GraphicPanel)sender;
            mouseStart.X = e.X;
            mouseStart.Y = e.Y;

            if (e.Button.Equals(MouseButtons.Left))
            {
                mousing = true;
                Vector3 vect = look - eye;
                float Height = (float)panel.Height;
                float Width = (float)panel.Width;

                floatXpixel = !orthogonal ?
                    // get the length in units / lenght of screen for stardard projection
                    vect.Length * 1.5f * (float)Math.Tan(45.0 * 0.5f) / (Height > Width ? Height : Width) :
                    // get the length in units / lenght of screen for ortogonal projection
                    planeWidth / Width;

            }
            else if (e.Button.Equals(MouseButtons.Right))
            {
                mousing = true;
            }
            panel.Invalidate();
        } 

        /// <summary>
        /// The more problematic method, need to change some math
        /// </summary>
        public void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (mousing)
            {
                GraphicPanel panel = (GraphicPanel)sender;

                // MOVEMENT
                if (e.Button.Equals(MouseButtons.Left))
                {
                    float dx = this.mouseStart.X - e.X;
                    float dy = this.mouseStart.Y - e.Y;
                    this.mouseStart.X = e.X;
                    this.mouseStart.Y = e.Y;
                    Vector3 move = vRight * dx * floatXpixel - vUp * dy * floatXpixel;
                    eye += move;
                    look += move;
                    UpdateView();
                }
                // TRACKBALL ROTATION
                else if (e.Button.Equals(MouseButtons.Right))
                {
                    float dx = (e.X - this.mouseStart.X);
                    float dy = (this.mouseStart.Y - e.Y);

                    Vector3 mouse0 = panel.PointToWorld(e.X, e.Y);

                    this.mouseStart.X = e.X;
                    this.mouseStart.Y = e.Y;
                    float distance = eye.Length;

                    Vector3 spin = vRight * -dx + vUp * -dy;
                    spin.Normalize();
                    spin *= 0.01f * distance; //set the velocity

                    Vector3 mouse1 = mouse0 + spin;
                    mouse0.Normalize();
                    mouse1.Normalize();

                    // get the axle where eye rotate
                    Vector3 axis = Vector3.Cross(mouse0, mouse1);
                    float ang = 0.9f;// Vector3.Dot(mouse0, mouse1);

                    // get quaternion rotation between two vectors
                    Quaternion q = new Quaternion(axis.x, axis.y, axis.z, ang);
                    q.Normalize();

                    Matrix4 rot = (Matrix4)q;

                    Vector3 neweye = rot * (eye - look);

                    neweye += look;
                    eye = neweye;
                    up = rot * up;

                }
                UpdateView();
                UpdateProjection(values.viewport);

                panel.Invalidate();
            }
        }
        
        /// <summary>
        /// Invalidating viewport is always a good idea to refresh last operations
        /// </summary>
        public void OnMouseUp(object sender, MouseEventArgs e)
        {
            mousing = false;
            GraphicPanel panel = (GraphicPanel)sender;
            if (e.Button.Equals(MouseButtons.Right))
            {

            }
            panel.Invalidate();
        }
        
        /// <summary>
        /// Very simple method, the zoom are proportional to distance from targhet point
        /// </summary>
        public void OnMouseWheel(object sender, MouseEventArgs e)
        {
            mousing = false;
            GraphicPanel panel = (GraphicPanel)sender;
            
            if (!orthogonal)
            {
                Vector3 rayView = look - eye;
                Vector3 move = vLook * (rayView.Length * e.Delta / 600.0f);
                eye += move;
                UpdateView();
            }
            else
            {
                planeWidth *= e.Delta > 0 ? 1.1f : 0.9f;
                UpdateProjection(values.viewport);
            }
            panel.Invalidate();
        }

    }
}