﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace Engine.Renderer
{
    public class ViewportMenu : Form
    {
        public PropertyGrid propertyGrid;

        public ViewportMenu()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.SuspendLayout();
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.Size = new System.Drawing.Size(219, 371);
            this.propertyGrid.TabIndex = 0;
            this.propertyGrid.ToolbarVisible = false;
            // 
            // ViewportMenu
            // 
            this.ClientSize = new System.Drawing.Size(219, 371);
            this.Controls.Add(this.propertyGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Text = "Viewport Options";
            this.ResumeLayout(false);

        }
    }
}
