﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;

namespace Engine.Renderer
{
    public enum ViewsList
    {
        Prospective,
        Front,
        Top
    }
    public class ViewportOption
    {
        GraphicPanel panel;
        FillMode fill = 0;
        Cull culo = 0;
        Color background = Color.CornflowerBlue;
        ViewsList view = ViewsList.Prospective;
        bool debugname = false;
        //float scale = 1.0f;


        public ViewportOption(GraphicPanel panelControl)
        {
            panel = panelControl;
        }

        public ViewsList viewMode
        {
            get { return view; }
            set
            {
                view = value;
                panel.camera.setDefualt(value);
                panel.Invalidate();
            }
        }
        public String labelName
        {
            get { return panel.viewportlabel.Text; }
            set { panel.viewportlabel.Text = value; }
        }
        public FillMode fillMode
        {
            get { return fill; }
            set { fill = value; panel.Invalidate(); }
        }
        public Cull cullMode
        {
            get { return culo; }
            set { culo = value; panel.Invalidate(); }

        }
        public Color backGroundColor
        {
            get { return background; }
            set { background = value; panel.Invalidate(); }
        }

        public bool ShowDebugDxNodeName
        {
            get { return debugname; }
            set { debugname = value; }
        }
    }

}
