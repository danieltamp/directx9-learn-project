﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

using Font = Engine.Graphics.Font;

namespace Engine.Renderer
{
    /// <summary>
    /// Windows Control to automatic manage the swapchain function
    /// </summary>
    public partial class GraphicPanel : UserControl
    {
        static int GraphicPanelCount = 0;

        /// <summary>
        /// </summary>
        public delegate void RenderFunctionDelegate(GraphicPanel panel, ViewportOption options , CameraValues matrices);
        /// <summary>
        /// </summary>
        public RenderFunctionDelegate RenderFunction;
        
        GraphicDevice graphicDevice;
        Viewport viewportSize;
        DX9SwapChain swapchain;
        bool useDevice = false;
        bool useSwapChain = false;
        bool CRASH = false;

        public Label viewportlabel;
        public TrackballCamera camera;
        public ViewportOption options;

        public override Color BackColor
        {
            get { return options.backGroundColor; }
            set { base.BackColor = options.backGroundColor = value; }
        }

        
        /// <summary>
        /// </summary>
        public GraphicPanel()
        {
            this.SetStyle(ControlStyles.Opaque | ControlStyles.AllPaintingInWmPaint, true);            
            options = new ViewportOption(this);
            camera = new TrackballCamera(this);

            camera.values.viewport = viewportSize;

            InitializeComponent();

            viewportSize = new Viewport();
            viewportSize.MaxDepth = 1;
            viewportSize.MinDepth = 0;
            viewportSize.Width = this.ClientSize.Width;
            viewportSize.Height = this.ClientSize.Height;
            viewportSize.X = viewportSize.Y = 0;

            this.Tag = GraphicPanelCount;

            options.labelName = "dxswapchain" + this.Tag.ToString();

            GraphicPanelCount++;
        }

        /// <summary>
        /// Initialize the SwapChain in this panel, device are initialized in other side
        /// </summary>
        /// <param name="device"></param>
        public void InitializeSwapChain(GraphicDevice device)
        {
            this.useDevice = false;
            this.useSwapChain = true;
            this.swapchain = new DX9SwapChain(device.m_device, (Control)this, (int)this.Tag > 1, (int)this.Tag > 1);
            this.graphicDevice = device;
        }
        /// <summary>
        /// Initialize the Device in this panel, device are returned
        /// </summary>
        /// <returns></returns>
        public GraphicDevice InitializeDevice()
        {
            this.useDevice = true;
            this.useSwapChain = false;
            this.graphicDevice = new GraphicDevice(this);
            this.swapchain = null;
            return this.graphicDevice;
        }

        ~GraphicPanel()
        {
            GraphicPanelCount--;
        }

        public Vector3 PointToWorld(int mouseX, int mouseY)
        {
            return PointToWorld(mouseX, mouseY, 0);
        }
        public Vector3 PointToWorld(int mouseX, int mouseY , float depthZ)
        {
            //Viewport dxv = m_viewport;
            //return Vector3.Unproject(new Vector3(mouseX, mouseY, dxv.MinZ), m_viewport, camera.projection, camera.view, camera.world);
            return Vector3.Unproject(mouseX, mouseY, depthZ, viewportSize, camera.values.projection, camera.values.view, camera.values.world);
            
        }
        public Point WorldToPoint(Vector3 position)
        {
            //Viewport dxv = m_viewport;
            //Vector3 project = Vector3.Project(position, dxv, camera.projection, camera.view, camera.world);
            //return new Point((int)project.X, (int)project.Y);

            Vector3 point = Vector3.Project(position, camera.values.worldViewProj);
            return new Point((int)point.x, (int)point.y);
        }

        #region CAMERA
        bool cameraEventsState = false;
        public bool cameraEvents
        {
            [Browsable(false)]
            get
            {
                return cameraEventsState;
            }
            set
            {
                //Console.WriteLine("set camera events to " + value);
                // enable events if they didn't are enabled
                if (value && !cameraEventsState)
                {
                    this.MouseDown += new MouseEventHandler(this.camera.OnMouseDown);
                    this.MouseUp += new MouseEventHandler(this.camera.OnMouseUp);
                    this.MouseMove += new MouseEventHandler(this.camera.OnMouseMove);
                    this.MouseWheel += new MouseEventHandler(this.camera.OnMouseWheel);
                    cameraEventsState = true;
                }
                //disable events if they are added
                else if (!value && cameraEventsState)
                {
                    this.MouseDown -= this.camera.OnMouseDown;
                    this.MouseUp -= this.camera.OnMouseUp;
                    this.MouseMove -= this.camera.OnMouseMove;
                    this.MouseWheel -= this.camera.OnMouseWheel;
                    cameraEventsState = false;
                }
            }
        }
        #endregion

        #region PICKING
        //public delegate ISceneNode PickFuncDelegate();
        //public PickFuncDelegate PickFunc;
        
        public Ray GetRay(int mouseX, int mouseY)
        {
            Vector3 raynear = PointToWorld(mouseX, mouseY, 0);
            Vector3 rayfar = PointToWorld(mouseX, mouseY, 1);
            //Vector3 raynear = Vector3.Unproject(new Vector3(mouseX, mouseY, dxv.MinZ), dxv ,camera.projection, camera.view, camera.world);
            //Vector3 rayfar = Vector3.Unproject(new Vector3(mouseX, mouseY, dxv.MaxZ), dxv, camera.projection, camera.view, camera.world);
            return new Ray(raynear, rayfar - raynear);
        }
        #endregion

        /// <summary>
        /// The render function set current renderstates
        /// </summary>
        /// <param name="m_device"></param>
        void Render(GraphicDevice m_device)
        {
            if (RenderFunction != null) RenderFunction(this, options, camera.values);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if ((int)this.Tag > 1)
            {
                camera.eye = Vector3.TransformCoordinate(camera.eye, Matrix4.RotationYawPitchRoll(0.002f, 0.001f, 0));
                camera.look = Vector3.Zero;
                camera.UpdateView();
            }

            if (useDevice)
            {
                throw new NotSupportedException("i have a problem with graphicDevice.preset");

                /*
                if (graphicDevice == null) throw new NullReferenceException("Device not set");
                
                graphicDevice.viewport = viewportSize;
                graphicDevice.SetRenderTarghet();
                
                bool state = graphicDevice.BeginDraw();
                Debug.Assert(state, "Render Fail");
                if (state)
                {    
                    graphicDevice.Clear(Color.CornflowerBlue);
                    Render(graphicDevice);
                    graphicDevice.EndDraw();
                    graphicDevice.PresentDraw();
                }
                return;
                 */
            }

            if (useSwapChain)
            {
                if (swapchain == null)
                    throw new NullReferenceException("Device not set");

                // se the current size of panel
                graphicDevice.viewport = viewportSize;
                // assign to device the backbuffer of chain
                swapchain.SetRenderTarget();

                // Draw
                bool state = graphicDevice.BeginDraw();
                Debug.Assert(state, "Render Fail");          
                if (state)
                {
                    swapchain.Clear((int)this.Tag > 1 ? Color.Blue : Color.CornflowerBlue);
                    Render(graphicDevice);
                    graphicDevice.EndDraw();
                    swapchain.Present();
                }
                graphicDevice.SetRenderTarghet();

                return;
            }

            //Debug.WriteLine("paint event : swapchain or device not found");

            System.Drawing.Font fontForDesignMode = new System.Drawing.Font("Arial", 20, FontStyle.Bold);
            e.Graphics.Clear(System.Drawing.Color.WhiteSmoke);
            
            string text = CRASH ? "SwapChain Error" : "DirectX9 RenderControl";

            var sizeText = e.Graphics.MeasureString(text, fontForDesignMode);
            e.Graphics.DrawString(text, fontForDesignMode, new SolidBrush(System.Drawing.Color.Black), (ClientSize.Width - sizeText.Width) / 2, (ClientSize.Height - sizeText.Height) / 2);
        }

        protected override void OnResize(EventArgs e)
        {
            viewportSize.Width = this.ClientSize.Width;
            viewportSize.Height = this.ClientSize.Height;
            camera.values.viewport = viewportSize;
            camera.UpdateProjection(viewportSize);

            if (useSwapChain)
            {
                try
                {
                    System.Threading.Thread.Sleep(100);
                    swapchain.Resize(this.ClientSize);  
                }
                catch(Exception error)
                {
                    Console.WriteLine(error.ToString());
                    useSwapChain = false;
                    useDevice = false;
                    CRASH = true;
                }
            }
            else if (useDevice)
            {
                try
                {
                    graphicDevice.Resize(this.ClientSize);
                }
                catch (Exception error)
                {
                    Console.WriteLine(error.ToString());
                    useSwapChain = false;
                    useDevice = false;
                    CRASH = true;
                }
            }
            else
            {
                Debug.WriteLine("resize event : swapchain or device not set");
            }

            this.Invalidate();
        }

        private void viewportlabel_Click(object sender, EventArgs e)
        {
            ViewportMenu menu = new ViewportMenu();
            menu.propertyGrid.SelectedObject = options;
            menu.ShowDialog(this);
        }
    }

}
