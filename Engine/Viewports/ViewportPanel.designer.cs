﻿namespace Engine.Renderer
{
    partial class GraphicPanel
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (swapchain != null)
            {
                swapchain.Dispose();
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewportlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // viewportlabel
            // 
            this.viewportlabel.AutoSize = true;
            this.viewportlabel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.viewportlabel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.viewportlabel.ForeColor = System.Drawing.Color.Black;
            this.viewportlabel.Location = new System.Drawing.Point(0, 0);
            this.viewportlabel.Name = "viewportlabel";
            this.viewportlabel.Size = new System.Drawing.Size(45, 13);
            this.viewportlabel.TabIndex = 0;
            this.viewportlabel.Text = "<name>";
            this.viewportlabel.Click += new System.EventHandler(this.viewportlabel_Click);
            // 
            // ViewportDx
            // 
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.viewportlabel);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.MinimumSize = new System.Drawing.Size(1, 1);
            this.MaximumSize = new System.Drawing.Size(2000, 2000);
            this.Name = "ViewportDx";
            //this.Size = new System.Drawing.Size(148, 148);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        
        public override string ToString()
        {
            return this.Name;
        }

    }
}
