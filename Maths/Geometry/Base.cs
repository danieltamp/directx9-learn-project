﻿// by johnwhile
using System;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// Transformable properties
    /// </summary>
    public interface ITrasformable
    {
        Matrix4 transform { get; set; }
    }

    /// <summary>
    /// Define base informations what a generic geometry must have
    /// </summary>
    public abstract class BaseGeometry : ITrasformable
    {
        protected Matrix4 globalcoord;
        protected Matrix4 globalcoord_inv;
        protected PrimitiveType m_primitive;
        /// <summary>
        /// need a basic volume information example to understand che center of mesh, the value are in LOCAL space.
        /// The only remark if for instance nodes, the bounding sphere is stored in the main node.
        /// Need to be updated when you want.
        /// </summary>
        public BoundarySphere boundSphere;

        /// <summary>
        /// </summary>
        public BaseGeometry()
        {
            globalcoord = globalcoord_inv = Matrix4.Identity;
            m_primitive = PrimitiveType.Instance;
            boundSphere = BoundarySphere.NaN;
        }        

        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseGeometry(BaseGeometry src)
        {
            m_primitive = src.m_primitive;
            boundSphere = src.boundSphere;
            globalcoord = src.globalcoord;
            globalcoord_inv = src.globalcoord_inv;
            name = src.name;
        }

        /// <summary>
        /// The number of vertices
        /// </summary>
        public abstract int numVertices { get; }
        /// <summary>
        /// The number of primitives, depend by primitive you are using
        /// </summary>
        public abstract int numPrimitives { get; }
        /// <summary>
        /// changing the transfrom matrix without affect vertices position in the world space. The vertices
        /// and normals are trasformed in world space and re-trasformed in the new local space
        /// </summary>
        public abstract void changeTransform(Matrix4 newtransform);
        /// <summary>
        /// you can set a string to debug
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Get the type of primitive used for this node, if is instance the primitive must be search in the parent node
        /// </summary>
        public PrimitiveType primitive
        {
            get { return m_primitive; }
        }
        /// <summary>
        /// Is the transformation of this node from 3d world root
        /// </summary>
        public Matrix4 transform
        {
            get { return globalcoord; }
            set { globalcoord = value; globalcoord_inv = Matrix4.Inverse(value); }
        }
        /// <summary>
        /// When necessary is usefull to have a inverse matrix calculated only when necessary
        /// </summary>
        public Matrix4 transform_inv
        {
            get { return globalcoord_inv; }
        }
    }
}
