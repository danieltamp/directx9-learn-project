﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Tools
{
    public static class GeometryTools
    {
        /// <summary>
        /// Calculate default normal using faces
        /// </summary>
        public static Vector3[] GetDefaultNormal(Vector3[] vertices, Face[] faces)
        {
            int numVertices = vertices.GetUpperBound(0)+1;
            int numTriangles = faces.GetUpperBound(0) + 1;

            Vector3[] normals = new Vector3[numVertices];
            normals.Initialize();

            for (int t = 0; t < numTriangles; t++)
            {
                Face face = faces[t];
                Vector3 v0 = vertices[face.I];
                Vector3 v1 = vertices[face.J];
                Vector3 v2 = vertices[face.K];

                Vector3 e0 = v1 - v0;
                Vector3 e1 = v2 - v0;
                Vector3 e2 = v2 - v1;

                Vector3 n = Vector3.Cross(e0, e1);
                float dot0 = e0.LengthSq;
                float dot1 = e1.LengthSq;
                float dot2 = e2.LengthSq;

                if (dot0 < 0.0001) dot0 = 1.0f;
                if (dot1 < 0.0001) dot1 = 1.0f;
                if (dot2 < 0.0001) dot2 = 1.0f;

                normals[face.I] += n * (1.0f / (dot0 * dot1));
                normals[face.J] += n * (1.0f / (dot2 * dot0));
                normals[face.K] += n * (1.0f / (dot1 * dot2));
            }
            for (int i = 0; i < numVertices; i++)
                normals[i].Normalize();

            return normals;
        }

        /// <summary>
        /// Calculate default texture using and an alligned plane
        /// </summary>
        /// <param name="minBound">minimum corner</param>
        /// <param name="maxBound">maximum corner</param>
        /// <param name="plane">plane to project the texture coordinates</param>
        public static Vector2[] GetPlanarProjection(Vector3[] vertices, Vector2 minBound, Vector2 maxBound, Matrix4 plane)
        {
            int numVertices = vertices.GetUpperBound(0) + 1;

            Vector2[] texture = new Vector2[numVertices];

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);

            for (int i = 0; i < numVertices; i++)
            {
                Vector3 proj = Vector3.Project(vertices[i],plane);
                texture[i] = new Vector2(proj.x, proj.y);

                if (proj.x > max.x) max.x = proj.x;
                if (proj.y > max.y) max.y = proj.y;
                if (proj.x < min.x) min.x = proj.x;
                if (proj.y < min.y) min.y = proj.y;
            }
            // interpolate the bound condition y = (x-x0)(y1-y0)/(x1-x0) + y0
            // where x are the local coordinate , y the final bounding rectangle
            // y = (x-x0) * m + y0
            float mU = (maxBound.x - minBound.x) / (max.x - min.x);
            float mV = (maxBound.y - minBound.y) / (max.y - min.y);

            for (int i = 0; i < numVertices; i++)
            {
                texture[i].x = (texture[i].x - min.x) * mU + minBound.x;
                texture[i].y = (texture[i].y - min.y) * mV + minBound.y;
            }
            return texture;
        }

        /// <summary>
        /// Calculate default texture using 
        /// </summary>
        /// <param name="cilindral">cilindral transformation</param>
        /// <returns></returns>
        public static Vector2[] GetCilindralProjection(Vector3[] vertices, Matrix4 cilindral)
        {
            int numvertices = vertices.Length;
            Vector2[] textures = new Vector2[numvertices];

            float minh = float.MaxValue;
            float maxh = float.MinValue;

            for (int i = 0; i < numvertices; i++)
            {
                Vector3 v = Vector3.TransformCoordinate(vertices[i], cilindral);
                float r = (float)Math.Sqrt(v.x * v.x + v.z * v.z);
                float t = (float)Math.Asin(v.z / r);
                float h = v.y;

                if (v.x < 0) t = (float)Math.PI - t;
                if (v.y < minh) minh = v.y;
                if (v.y > maxh) maxh = v.y;

                textures[i] = new Vector2((float)(t / Math.PI / 2.0), h);
            }

            for (int i = 0; i < numvertices; i++)
            {
                textures[i].y = (textures[i].y - minh) / (maxh - minh);
            }

            return textures;
        }
    }


    /// <summary>
    /// Generate a Convex Hull in 2D 
    /// </summary> 
    public static class ConvexHull2D
    {
        private static sbyte Compare(Vector3 a, Vector3 b)
        {
            if (a.x > b.x) return 1;
            else if (a.x < b.x) return -1;
            else if (a.z > b.z) return 1;
            else if (a.z < b.z) return -1;
            return 0;
        }
        private static void quicksort(Vector3[] array, ushort[] index, int left, int right)
        {
            int l = left;
            int r = right;
            int p = (left + right) / 2;

            // 1. Pick a pivot value somewhere in the middle.
            Vector3 pivot = array[index[p]];

            // 2. Loop until pointers meet on the pivot.
            while (l <= r)
            {
                // 3. Find a larger value to the right of the pivot.
                //    If there is non we end up at the pivot.
                while (Compare(array[index[l]], pivot) < 0) l++;

                // 4. Find a smaller value to the left of the pivot.
                //    If there is non we end up at the pivot.
                while (Compare(array[index[r]], pivot) > 0) r--;

                // 5. Check if both pointers are not on the pivot.
                if (l <= r)
                {
                    // 6. Swap both values to the right side.
                    ushort swap = index[l];
                    index[l] = index[r];
                    index[r] = swap;

                    l++;
                    r--;
                }
            }
            // Here's where the pivot value is in the right spot

            // 7. Recursively call the algorithm on the unsorted array 
            //    to the left of the pivot (if exists).
            if (left < r) quicksort(array, index, left, r);

            // 8. Recursively call the algorithm on the unsorted array 
            //    to the right of the pivot (if exists).
            if (l < right) quicksort(array, index, l, right);

            // 9. The algorithm returns when all sub arrays are sorted.
        }


        // Copyright 2001, softSurfer (www.softsurfer.com)
        // This code may be freely used and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        // Assume that a class is already given for the object:
        //    Point with coordinates {float x, y;}
        //===================================================================

        // isLeft(): tests if a point is Left|On|Right of an infinite line.
        //    Input:  three points P0, P1, and P2
        //    Return: >0 for P2 left of the line through P0 and P1
        //            =0 for P2 on the line
        //            <0 for P2 right of the line
        //    See: the January 2001 Algorithm on Area of Triangles

        private static float isLeft(Vector3 P0, Vector3 P1, Vector3 P2)
        {
            return (P1.x - P0.x) * (P2.z - P0.z) - (P2.x - P0.x) * (P1.z - P0.z);
        }
        //===================================================================
        // chainHull_2D(): Andrew's monotone chain 2D convex hull algorithm
        //     Input:  P[] = an array of 2D points 
        //                   presorted by increasing x- and y-coordinates
        //             n = the number of points in P[]
        //     Output: H[] = an array of the convex hull vertices (max is n)
        //     Return: the number of points in H[]
        public static int chainHull_2D(Vector3[] P, ref Vector3[] H, bool needresort)
        {
            // the output array H[] will be used as the stack
            int bot = 0, top = (-1);  // indices for bottom and top of the stack
            int i;                // array scan index
            // Get the indices of points with min x-coord and min|max y-coord
            int minmin = 0, minmax;

            int N = P.Length;
            ushort[] index = new ushort[N];
            for (i = 0; i < N; i++) index[i] = (ushort)i;


            if (needresort)
            {
                quicksort(P, index, 0, N - 1);
            }


            float xmin = P[0].x;
            for (i = 1; i < N; i++)
                if (P[i].x != xmin) break;
            minmax = i - 1;
            if (minmax == N - 1)
            {       // degenerate case: all x-coords == xmin
                H[++top] = P[minmin];
                if (P[minmax].z != P[minmin].z) // a nontrivial segment
                    H[++top] = P[minmax];
                H[++top] = P[minmin];           // add polygon endpoint
                return top + 1;
            }

            // Get the indices of points with max x-coord and min|max y-coord
            int maxmin, maxmax = N - 1;
            float xmax = P[N - 1].x;
            for (i = N - 2; i >= 0; i--)
                if (P[i].x != xmax) break;
            maxmin = i + 1;

            // Compute the lower hull on the stack H
            H[++top] = P[minmin];      // push minmin point onto stack
            i = minmax;
            while (++i <= maxmin)
            {
                // the lower line joins P[minmin] with P[maxmin]
                if (isLeft(P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin)
                    continue;          // ignore P[i] above or on the lower line

                while (top > 0)        // there are at least 2 points on the stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break;         // P[i] is a new hull vertex
                    else
                        top--;         // pop top point off stack
                }
                H[++top] = P[i];       // push P[i] onto stack
            }

            // Next, compute the upper hull on the stack H above the bottom hull
            if (maxmax != maxmin)      // if distinct xmax points
                H[++top] = P[maxmax];  // push maxmax point onto stack
            bot = top;                 // the bottom point of the upper hull stack
            i = maxmin;
            while (--i >= minmax)
            {
                // the upper line joins P[maxmax] with P[minmax]
                if (isLeft(P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
                    continue;          // ignore P[i] below or on the upper line

                while (top > bot)    // at least 2 points on the upper stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break;         // P[i] is a new hull vertex
                    else
                        top--;         // pop top point off stack
                }
                H[++top] = P[i];       // push P[i] onto stack
            }
            if (minmax != minmin)
                H[++top] = P[minmin];  // push joining endpoint onto stack

            return top + 1;
        }
    }

    /// <summary>
    /// Find minimum distance rotation with "rotation caliper algoritm"
    /// </summary> 
    public static class RotationCaliper
    {
        public static Matrix4 GetMinRotation(Vector3[] hull)
        {
            throw new NotImplementedException();
        }
    }
}
