﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// Tessellated sphere calculated from tetrahedron
    /// A not primitive mesh, contain internal functions example for tessellation.
    /// </summary>
    /// <remarks>
    /// TODO : decrease tessellation
    /// </remarks>
    public class Tetrahedron : HMesh
    {
        float m_radius = 0;
        static float _C23 = -(float)Math.Sqrt(2.0) / 3.0f;
        static float _C13 = -1f / 3f;
        static float _C63 = (float)Math.Sqrt(6.0) / 3.0f;

        /// <summary>
        /// Generate a base tetrahedron to start tessellation
        /// </summary>
        public Tetrahedron(float radius)
        {
            m_radius = radius;

            int vcount = 0;
            int fcount = 0;

            // build the default data of a tetrahedron
            m_verts = new List<HVertex>(4);
            m_verts.Add(new HVertex(new CVERTEX(0, 0, 1, Color.Red), vcount++));
            m_verts.Add(new HVertex(new CVERTEX(0, (2.0f * (float)Math.Sqrt(2.0)) / 3.0f, _C13, Color.Green), vcount++));
            m_verts.Add(new HVertex(new CVERTEX(-_C63, _C23, _C13, Color.Blue), vcount++));
            m_verts.Add(new HVertex(new CVERTEX(_C63, _C23, _C13, Color.White), vcount++));

            // update vertex position in a sphere surface
            for (int i = 0; i < m_verts.Count; i++)
            {
                Vector3 pos = m_verts[i].data.position;
                pos.Normalize();
                m_verts[i].data.position = pos * m_radius;
            }

            m_faces = new List<HFace>(4);
            m_faces.Add(new HFace(m_verts[2], m_verts[0], m_verts[1], fcount++));
            m_faces.Add(new HFace(m_verts[3], m_verts[0], m_verts[2], fcount++));
            m_faces.Add(new HFace(m_verts[1], m_verts[0], m_verts[3], fcount++));
            m_faces.Add(new HFace(m_verts[1], m_verts[3], m_verts[2], fcount++));

            calculateEdgeTable();
        }
        /// <summary>
        /// Split all faces in 4 faces, using the default tessellation
        /// </summary>
        public void IncreaseTesselation()
        {
            TriangleTessellate1<CVERTEX>();

            // update vertex position in a sphere surface
            for (int i = 4; i < m_verts.Count; i++)
            {
                Vector3 pos = m_verts[i].data.position;
                //pos.Normalize();
                //m_verts[i].data.position = pos * m_radius;
            }
        }

    }


    public class Tetrahedron_DEPRECATED : BaseGeometry
    {
        int vcount = 0;
        float m_radius = 0;
        static float _C23 = -(float)Math.Sqrt(2.0) / 3.0f;
        static float _C13 = -1f / 3f;
        static float _C63 = (float)Math.Sqrt(6.0) / 3.0f;

        Color tricolor;
        public List<Vector3> vertices;
        public List<Vector2> textures;
        public List<Color> colors;
        public List<Face> faces;

        public override int numVertices { get { return vertices.Count; } }
        public override int numPrimitives { get { return faces.Count; } }
        public override void changeTransform(Matrix4 newtransform)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Generate a base tetrahedron to start tessellation
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="iterations">with ushort indices this value must be &lt; 8 </param>
        public Tetrahedron_DEPRECATED(float radius, int iterations)
        {
            this.m_radius = radius;
            boundSphere = new BoundarySphere(Vector3.Zero, radius);

            int numtriangles = 4 << (2 * iterations); // = 4^(N+1)
            int numvertices = iterations > 0 ? (4 << (iterations*2))/4 * 3 - 2 : 4; // 4^n * 3 -2

            if (numvertices > ushort.MaxValue - 1)
                throw new OverflowException("uint faces can't contain >65534 vertices");

            vertices = new List<Vector3>(numvertices);
            faces = new List<Face>(numtriangles);
            textures = new List<Vector2>(numvertices);
            colors = new List<Color>(numvertices);

            vcount = 0;

            Vector3 p0 = new Vector3(0, 0, 1);
            Vector3 p1 = new Vector3(0, (2.0 * Math.Sqrt(2.0)) / 3.0, _C13);
            Vector3 p2 = new Vector3(-_C63, _C23, _C13);
            Vector3 p3 = new Vector3(_C63, _C23, _C13);

            //indices of corner
            tricolor = Color.Red;
            int i0 = AddVertex(p0);
            tricolor = Color.Green;
            int i1 = AddVertex(p1);
            tricolor = Color.Blue;
            int i2 = AddVertex(p2);
            tricolor = Color.White;
            int i3 = AddVertex(p3);

            // indices of middle points
            int i01, i02, i03, i12, i13, i23;
            i01 = i02 = i03 = i12 = i13 = i23 = -1;

            tricolor = Color.Red;
            RecurseTesselate(i2, i0, i1, ref i02, ref i01, ref i12, iterations);
            tricolor = Color.Green;
            RecurseTesselate(i3, i0, i2, ref i03, ref i02, ref i23, iterations);
            tricolor = Color.Blue;
            RecurseTesselate(i1, i0, i3, ref i01, ref i03, ref i13, iterations);
            tricolor = Color.White;
            RecurseTesselate(i1, i3, i2, ref i13, ref i23, ref i12, iterations);
        }


        /// <summary>
        /// </summary>
        /// <param name="r">right point</param>
        /// <param name="ir">index of right point</param>
        /// <param name="i_ra">index of middle point of edge right-apex</param>
        /// <param name="level">level of tessellation, 0 = leaf triangle</param>
        void RecurseTesselate(int i_r, int i_a, int i_l, ref int i_ra, ref int i_al, ref int i_lr, int level)
        {
            if (level > 0)
            {
                // calculate middle points
                Vector3 ra = (vertices[i_r] + vertices[i_a]) * 0.5f;
                Vector3 al = (vertices[i_a] + vertices[i_l]) * 0.5f;
                Vector3 lr = (vertices[i_l] + vertices[i_r]) * 0.5f;

                // insert if need these points
                if (i_ra < 0) i_ra = AddVertex(ra);
                if (i_al < 0) i_al = AddVertex(al);
                if (i_lr < 0) i_lr = AddVertex(lr);

                // indices of middle-middle neighbour points
                int i_r_ra = -1;
                int i_ra_a = -1;
                int i_a_al = -1;
                int i_al_l = -1;
                int i_l_lr = -1;
                int i_lr_r = -1;

                // indices of middle-middle interior points
                int i_ra_al = -1;
                int i_al_lr = -1;
                int i_lr_ra = -1;

                --level;
                // Subdivide triangle into four triangles
                RecurseTesselate(i_ra, i_a, i_al, ref i_ra_a, ref i_a_al, ref i_ra_al, level);
                RecurseTesselate(i_lr, i_al, i_l, ref i_al_lr, ref i_al_l, ref i_l_lr, level);
                RecurseTesselate(i_al, i_lr, i_ra, ref i_al_lr, ref i_lr_ra, ref i_ra_al, level);
                RecurseTesselate(i_r, i_ra, i_lr, ref i_r_ra, ref i_lr_ra, ref i_lr_r, level);

            }
            else
            {
                faces.Add(new Face(i_r, i_a, i_l));
            }
        }

        /// <summary>
        /// return the index of vertex added to list
        /// </summary>
        int AddVertex(Vector3 v)
        {
            v.Normalize();

            Vector2 uv = new Vector2();
            uv.x = (float)(Math.Atan2(v.y, v.x) / (Math.PI * 2) + 0.5);
            uv.y = (float)(Math.Asin(v.z) / Math.PI  + 0.5);
            
            Color color = tricolor;
            //color = Color.FromArgb((int)(uv.x * 255), (int)(uv.y * 255), 128);

            v *= m_radius;
            vertices.Add(v);
            textures.Add(uv);
            colors.Add(color);//nice rainbow gradient
            return vcount++;
        }


        /// <summary>
        /// Down casting from iterative to primitive geometry, all tessellation information will be lost.
        /// Return a static geometry.
        /// </summary>
        public static explicit operator MeshGeometry(Tetrahedron_DEPRECATED sphere)
        {
            MeshGeometry mesh = new MeshGeometry((BaseGeometry)sphere);
            mesh.name = sphere.name;
            mesh.vertices = sphere.vertices.ToArray();
            mesh.faces = sphere.faces.ToArray();
            mesh.textures = sphere.textures.ToArray();
            mesh.colors = sphere.colors.ToArray();

            mesh.boundSphere = sphere.boundSphere;
            return mesh;
        }
    }

    /// <summary>
    /// TODO : avoid duplicated vertices
    /// Tessellated sphere calculated from icosahedron
    /// </summary>
    public class Icosahedron_DEPRECATED : BaseGeometry
    {
        // rapporto aureo
        static float t = (1 + (float)Math.Sqrt(5.0)) / 2f;

        float m_radius;
        int vcount = 0;
        List<Vector3> vertices;
        List<Vector2> textures;
        List<Color> colors;
        List<Face> faces;

        public Icosahedron_DEPRECATED(float radius , int iterations)
        {
            vcount = 0;
            m_radius = radius;
            vertices = new List<Vector3>();
            textures = new List<Vector2>();
            colors = new List<Color>();
            faces = new List<Face>();

            List<Face> tmp_f = new List<Face>();
            List<Vector3> tmp_v = new List<Vector3>();

            // create 12 vertices of a icosahedron
            tmp_v.Add(new Vector3(-1, t, 0));
            tmp_v.Add(new Vector3(1, t, 0));
            tmp_v.Add(new Vector3(-1, -t, 0));
            tmp_v.Add(new Vector3(1, -t, 0));
            tmp_v.Add(new Vector3(0, -1, t));
            tmp_v.Add(new Vector3(0, 1, t));
            tmp_v.Add(new Vector3(0, -1, -t));
            tmp_v.Add(new Vector3(0, 1, -t));
            tmp_v.Add(new Vector3(t, 0, -1));
            tmp_v.Add(new Vector3(t, 0, 1));
            tmp_v.Add(new Vector3(-t, 0, -1));
            tmp_v.Add(new Vector3(-t, 0, 1));

            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i] = vertices[i].Normal * m_radius;
                colors.Add(Color.Blue);
                textures.Add(new Vector2(0, 0));          
            }

            // 5 faces around point 0
            tmp_f.Add(new Face(0, 11, 5));
            tmp_f.Add(new Face(0, 5, 1));
            tmp_f.Add(new Face(0, 1, 7));
            tmp_f.Add(new Face(0, 7, 10));
            tmp_f.Add(new Face(0, 10, 11));

            // 5 adjacent faces
            tmp_f.Add(new Face(1, 5, 9));
            tmp_f.Add(new Face(5, 11, 4));
            tmp_f.Add(new Face(11, 10, 2));
            tmp_f.Add(new Face(10, 7, 6));
            tmp_f.Add(new Face(7, 1, 8));

            // 5 faces around point 3
            tmp_f.Add(new Face(3, 9, 4));
            tmp_f.Add(new Face(3, 4, 2));
            tmp_f.Add(new Face(3, 2, 6));
            tmp_f.Add(new Face(3, 6, 8));
            tmp_f.Add(new Face(3, 8, 9));

            // 5 adjacent faces
            tmp_f.Add(new Face(4, 9, 5));
            tmp_f.Add(new Face(2, 4, 11));
            tmp_f.Add(new Face(6, 2, 10));
            tmp_f.Add(new Face(8, 6, 7));
            tmp_f.Add(new Face(9, 8, 1));


            for (int i = 0; i < tmp_f.Count; i++)
            {
                RecurseTessellate(
                    tmp_v[tmp_f[i].I],
                    tmp_v[tmp_f[i].J],
                    tmp_v[tmp_f[i].K],
                    iterations);

                List<Face> subdiv = new List<Face>();

                for (int n = 0; n < iterations; n++)
                {

                }
            }
        }
        int AddVertex(Vector3 v)
        {
            vertices.Add(v.Normal * m_radius);
            colors.Add(Color.Blue);
            textures.Add(Vector2.Zero);
            return vcount++;
        }

        void RecurseTessellate(Vector3 v1, Vector3 v2, Vector3 v3, int depth)
        {
            if (depth == 0)
            {
                Face f = new Face();
                f.I = (ushort)AddVertex(v1);
                f.J = (ushort)AddVertex(v2);
                f.K = (ushort)AddVertex(v3);
                faces.Add(f);
            }
            else
            {
                Vector3 v12 = (v1 + v2) * 0.5f;
                Vector3 v23 = (v2 + v3) * 0.5f;
                Vector3 v31 = (v3 + v1) * 0.5f;
                RecurseTessellate(v1, v12, v31, depth - 1);
                RecurseTessellate(v2, v23, v12, depth - 1);
                RecurseTessellate(v3, v31, v23, depth - 1);
                RecurseTessellate(v12, v23, v31, depth - 1);
            }
        }



        public override int numVertices { get { return vertices.Count; } }
        public override int numPrimitives { get { return faces.Count; } }

        public override void changeTransform(Matrix4 newtransform)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Down casting from iterative to primitive geometry, all tessellation information will be lost.
        /// Return a static geometry.
        /// </summary>
        public static explicit operator MeshGeometry(Icosahedron_DEPRECATED sphere)
        {
            MeshGeometry mesh = new MeshGeometry((BaseGeometry)sphere);
            mesh.name = sphere.name;
            mesh.vertices = sphere.vertices.ToArray();
            mesh.faces = sphere.faces.ToArray();
            mesh.textures = sphere.textures.ToArray();
            mesh.colors = sphere.colors.ToArray();

            mesh.boundSphere = sphere.boundSphere;
            return mesh;
        }
    }
}
