﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Maths;
using Engine.Graphics;

namespace Engine.Tools
{
    public class HVertex
    {
        /// <summary>
        /// unique id of vertices, is the position in the list
        /// </summary>
        public int ID;
        /// <summary>
        /// TODO : The vertex can be any struct
        /// </summary>
        public IVertexPosition data;
        public bool marked { get; set; }

        public HVertex(IVertexPosition v, int id)
        {
            this.marked = false;
            this.data = v;
            this.ID = id;
        }
        public HVertex(IVertexPosition v) : this(v, -1) { }

        public override string ToString()
        {
            return "v" + ID.ToString();
        }
    }

    public class HEdge
    {
        /// <summary>
        /// unique id of edges, is the position in the list
        /// </summary>
        public int ID;

        public HVertex[] v = new HVertex[2];
        public HFace[] f = new HFace[2];
        public bool marked { get; set; }

        public HEdge() : this(-1) { }      
        public HEdge(int id)
        {
            this.marked = false;
            this.ID = id;
        }
        public HEdge(HVertex v0, HVertex v1) : this(v0, v1, -1) { }
        public HEdge(HVertex v0, HVertex v1, int id) : this(id)
        {
            this.v[0] = v0;
            this.v[1] = v1;
        }

        public override string ToString()
        {
            return string.Format("e{0}({1},{2}) f{3} f{4}", ID, v[0].ID, v[1].ID, (f[0] != null) ? f[0].ID.ToString() : "-", (f[1] != null) ? f[1].ID.ToString() : "-");
        }
    }

    /// <summary>
    /// I use this scheme:
    /// f.e[0] = e(v0,v1)
    /// f.e[1] = e(v1,v2)
    /// f.e[2] = e(v0,v2)
    /// </summary>
    public class HFace
    {
        public int ID;
        /// <summary>
        /// The position in the array match with GetEdgeIdxContainVertsID() algorithm to mantain a sort or "chain" of edges
        /// </summary>
        public HEdge[] e = new HEdge[3];
        /// <summary>
        /// Set vertices in counterclockwire
        /// </summary>
        public HVertex[] v = new HVertex[3];
        /// <summary>
        /// a flag for special use
        /// </summary>
        public bool marked { get; set; }
        /// <summary>
        /// Create a face and assign an id
        /// </summary>
        public HFace(int id) 
        {
            this.marked = false;
            this.ID = id;
        } 
        /// <summary>
        /// Create empty face
        /// </summary>
        public HFace() : this(-1) { }
        /// <summary>
        /// Create a face using 3 vertices and an id
        /// </summary>
        public HFace(HVertex v0, HVertex v1, HVertex v2, int id)
            : this(id)
        {
            v[0] = v0; v[1] = v1; v[2] = v2;
        }
        /// <summary>
        /// Create face using 3 vertices
        /// </summary>
        public HFace(HVertex v0, HVertex v1, HVertex v2) : this(v0, v1, v2, -1) { }
        /// <summary>
        /// Return the edge index in this face what contain id1 and id2 vertices.
        /// </summary>
        /// <returns>
        /// -1 not fount
        /// -2 incoerent id, the face contain two identical vertices
        /// </returns>
        public int GetEdgeIdxContainVertsID(int ID1, int ID2)
        {
            // using flag to un-mark the vertices that mach with eij
            // 110 ; 6 return edge[0] because find first and second index
            // 011 ; 3 return edge[1] because find second and third index
            // 101 ; 5 return edge[2] because find first and third index
            // 111 ; 7 face contain two vertice with same index, error
            // 000 001 010 100; 0 1 2 4 face don't contain this edge(id1,id2), return -1
            byte pos = 0;
            byte mark = 4;
            for (int i = 0; i < 3; i++, mark>>=1)
            {
                if (v[i].ID == ID1 || v[i].ID == ID2)
                    pos |= mark;
            }
            if (pos == 6) return 0;
            else if (pos == 3) return 1;
            else if (pos == 5) return 2;
            else if (pos == 7) return -2;
            else return -1;
        }
        /// <summary>
        /// Create a new edge using vertices stored in this face and using edge position
        /// </summary>
        public HEdge CreateEdge(int index)
        {
            // the table of vertices match with GetEdgeIdxContainVertsID() algorithm
            switch (index)
            {
                case 0: return new HEdge(v[0], v[1]);
                case 1: return new HEdge(v[1], v[2]);
                case 2: return new HEdge(v[0], v[2]);
                default: return null;
            }
        }
        /// <summary>
        /// return if face don't contain duplicated vertices
        /// </summary>
        public bool IsCoerent()
        {
            if (v[0] == null || v[1] == null || v[2] == null) return false;
            if (v[0].ID == v[1].ID || v[0].ID == v[2].ID || v[1].ID == v[2].ID) return false;
            return true;
        }
        public override string ToString()
        {
            return string.Format("f{0} [{1} {2} {3}] -> {4} {5} {6}", ID, v[0].ID, v[1].ID, v[2].ID, e[0], e[1], e[2]);
        }
    }
    
  
    /// <summary>
    /// Half-Edge mesh structure, used to work with geometry but bad to store data
    /// </summary>
    public class HMesh
    {
        /// <summary>
        /// the type of vertices stored in m_verts[].data
        /// </summary>
        public Type m_vertstype { get; private set; }
        // vertes edge and faces table
        public List<HVertex> m_verts;
        public List<HEdge> m_edges;
        public List<HFace> m_faces;

        /// <summary>
        /// Estract or Insert the base geometry format (for rendering) 
        /// </summary>
        public MeshGeometry TriMesh
        {
            get { return getTriMesh(); }
            set { setTriMesh(value.faces, value.vertices, value.colors); }
        }


        public int NumVerts { get { return m_verts.Count; } }
        public int NumFaces { get { return m_faces.Count; } }


        public HMesh()
        {
            m_vertstype = null;
            m_verts = new List<HVertex>();
            m_edges = new List<HEdge>();
            m_faces = new List<HFace>();
        }
        /// <summary>
        /// Initialize a Half-Edge structure using a base static mesh, edge will be calculated
        /// </summary>
        public HMesh(IList<Face> faces, IList<Vector3> vertices, IList<Color> colors)
            : this()
        {
            setTriMesh(faces, vertices, colors);
            calculateEdgeTable();
        }

        /// <summary>
        /// return true is HalfEdge structure is correct
        /// </summary>
        /// <returns></returns>
        public bool CoerenceTest(out string message)
        {
            bool fail = false;
            string message_f = "Face : OK";
            string message_v = "Vertex : OK";
            string message_e = "Edge : OK";

            for (int i = 0; i < m_faces.Count; i++)
            {
                if (!m_faces[i].IsCoerent())
                {
                    message_f = "Face : incoerent vertex id";
                    fail = true;
                    break;
                }
            }
            for (int i = 0; i < m_verts.Count; i++)
                if (m_verts[i].ID != i)
                {
                    message_v = "Vertex : incoerent id";
                    fail = true;
                    break;
                }


            //find T junction, can't calculate it with incoerent id
            if (!fail)
            foreach (HEdge e in m_edges)
            {
                HFace fa = e.f[0];
                HFace fb = e.f[1];
                
                if (fa == null)
                {
                    message_e = "Edge : is not link to face";
                    fail = true;
                    break;
                }

                if (fa.e[0] != e && fa.e[1] != e && fa.e[2] != e)
                {
                    message_e = "Edge : is not link to face";
                    fail = true;
                    break;
                }

                if (fb != null && fb.e[0] != e && fb.e[1] != e && fb.e[2] != e)
                {
                    message_e = "Edge : is not link to face";
                    fail = true;
                    break;
                }
                if (fail) break;
            }
            message = message_v + "\n" + message_f + "\n" + message_e;


            return !fail;
        }

        /// <summary>
        /// Build the edge table using vertices and faces tab, carefull to assign previosly the ID 
        /// </summary>
        protected void calculateEdgeTable()
        {
            m_edges.Clear();         
            int ecount = 0;

            for (int i = 0; i < m_faces.Count; i++)
            {
                HFace face = m_faces[i];
                if (face.marked) continue;

                //foreach edges
                for (int j = 0; j < 3; j++)
                {
                    HEdge ej = face.e[j];
                    //edge(x,y) not created
                    if (ej == null)
                    {
                        // create the edge(x,y) where x and y are the vertex ID of j-th edge
                        // using a fix order
                        ej = face.e[j] = face.CreateEdge(j);
                        
                        // foreach all other faces, find a faces that contain same edge(x,y) using vertex id x and y
                        for (int ii = i + 1; ii < m_faces.Count; ii++)
                        {
                            // completly assigned, not need test
                            if (m_faces[ii].marked) continue;
                            // if idx == -1, the current face don't contain the x and y vertex ID
                            int idx = m_faces[ii].GetEdgeIdxContainVertsID(ej.v[0].ID, ej.v[1].ID);
                            // find a face that containt the vertices indices x and y
                            if (idx > -1)
                            {
                                m_faces[ii].e[idx] = ej;
                                // link in second position
                                ej.f[1] = m_faces[ii];
                                // a edge contain only tw0 neighbour faces, else is a not defualt HalfEdge structure
                                break;
                            }
                        }
                        
                        // if edge is created at first time, assign it in first position
                        ej.f[0] = face;
                        ej.ID = ecount++;
                        m_edges.Add(ej);
                    }
                }
                // mark this face as completly assigned
                face.marked = true;
            }
            m_edges.TrimExcess();
        }

        /// <summary>
        /// Increase the tessellation splitting each faces in 4 sub-faces.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void TriangleTessellate1<T>() where T : struct , IVertexPosition
        {
            /* pseudocode:
             * foreach old faces 
             *    split in 4 faces
             *    foreach 3 border of face
             *      if not splitted, split in 2 edge and generate middle vertex
             *         add these 2 new edge to list
             *      else get the 2 splitted edges and middle vertex from e_crack list associated to this border
             *    add this 4 new faces to list
             * delete old faces and edges
             *
             *                 v0
             *                /  \
             *           eLT /    \ e RT
             *              /  T   \
             *            vL________vR
             *            / \  C   / \
             *       eLD /  eCL   eCR \ eRD
             *          /  L  \  /  R  \
             *         /_______\/_______\
             *         v1      vD       v2
             *            eDL      eDR
             */

            // fix verts id
            for (int i = 0; i < m_verts.Count; i++)
                m_verts[i].ID = i;

            // the two crack edge for each edges. Need an array to custom access to all position 
            HEdge[] e_crack = new HEdge[m_edges.Count * 2];
            // the new center edges.
            HEdge[] tmp_edges = new HEdge[m_faces.Count * 3];

            // not need array, this temporary list will substitute the main list
            List<HFace> tmp_faces = new List<HFace>(m_faces.Count * 4);

            int vcount = m_verts.Count;
            int ecount = 0;

            for (int f = 0; f < m_faces.Count; f++)
            {
                HFace froot = m_faces[f];
                HEdge eL = froot.e[0];
                HEdge eD = froot.e[1];
                HEdge eR = froot.e[2];

                HVertex vL, vD, vR;
                HEdge eLT, eLD, eDR, eDL, eRD, eRT;

                // build the reference of new child faces
                HFace fT = new HFace(tmp_faces.Count);
                tmp_faces.Add(fT);
                HFace fL = new HFace(tmp_faces.Count);
                tmp_faces.Add(fL);
                HFace fC = new HFace(tmp_faces.Count);
                tmp_faces.Add(fC);
                HFace fR = new HFace(tmp_faces.Count);
                tmp_faces.Add(fR);

                // link the neigthbour faces using the id of border edge splitted or not in two crack edges
                linkborder<T>(eL.ID, e_crack, out vL, froot.v[0], froot.v[1], out eLT, out eLD, fT, fL);
                linkborder<T>(eD.ID, e_crack, out vD, froot.v[1], froot.v[2], out eDL, out eDR, fL, fR);
                linkborder<T>(eR.ID, e_crack, out vR, froot.v[2], froot.v[0], out eRD, out eRT, fR, fT);

                // centers faces and edges 
                HEdge eCL = new HEdge(vL, vD);
                HEdge eCR = new HEdge(vD, vR);
                HEdge eCT = new HEdge(vR, vL);

                tmp_edges[ecount++] = eCL;
                tmp_edges[ecount++] = eCR;
                tmp_edges[ecount++] = eCT;

                //use always this scheme
                //f.e[0] = e(v0,v1)
                //f.e[1] = e(v1,v2)
                //f.e[2] = e(v0,v2)

                fT.v[0] = froot.v[0]; fT.v[1] = vL; fT.v[2] = vR;
                fL.v[0] = vL; fL.v[1] = froot.v[1]; fL.v[2] = vD;
                fC.v[0] = vL; fC.v[1] = vD; fC.v[2] = vR;
                fR.v[0] = vR; fR.v[1] = vD; fR.v[2] = froot.v[2];


                fT.e[0] = eLT;
                fT.e[1] = eCT;
                fT.e[2] = eRT;

                fL.e[0] = eLD;
                fL.e[1] = eDL;
                fL.e[2] = eCL;

                fC.e[0] = eCR;
                fC.e[1] = eCT;
                fC.e[2] = eCL;

                fR.e[0] = eCR;
                fR.e[1] = eDR;
                fR.e[2] = eRD;

                // link central edges, they are always new so f[] is empty
                eCR.f[0] = fC;
                eCT.f[0] = fC;
                eCL.f[0] = fC;
                eCR.f[1] = fR;
                eCT.f[1] = fT;
                eCL.f[1] = fL;
            }

            m_faces = tmp_faces;

            // rewrite main edges list
            ecount = 0;
            m_edges.Clear();
            
            for (int i = 0; i < e_crack.Length; i++)
            {
                e_crack[i].ID = ecount++;
                m_edges.Add(e_crack[i]);
            }
            for (int i = 0; i < tmp_edges.Length; i++)
            {
                tmp_edges[i].ID = ecount++;
                m_edges.Add(tmp_edges[i]);
            }

            m_verts.TrimExcess();
            m_edges.TrimExcess();
            m_faces.TrimExcess();
        }

        /// <summary>
        /// </summary>
        /// <param name="eID">id of edged to split</param>
        /// <param name="e_crack">list of splitted edges</param>
        /// <param name="vM">middle vertex</param>
        /// <param name="e0m">first crack edge</param>
        /// <param name="em1">second crack edge</param>
        void linkborder<T>(int eID, HEdge[] e_crack, out HVertex vM, HVertex v0, HVertex v1, out HEdge e0m, out HEdge em1, HFace f0, HFace f1) where T : struct , IVertexPosition
        {
            HEdge ecrack0 = e_crack[eID * 2];
            HEdge ecrack1 = e_crack[eID * 2 + 1];

            // if edges was splitted previosly
            if (ecrack0 != null)
            {
                // vM is the common vertices of two cracked edges
                vM = ecrack0.v[0] == ecrack1.v[0] ? ecrack0.v[0] : ecrack0.v[1];

                // get the correct order
                if (ecrack0.v[0] == v0 || ecrack0.v[1] == v0)
                {
                    e0m = ecrack0;
                    em1 = ecrack1;
                }
                else
                {
                    e0m = ecrack1;
                    em1 = ecrack0;
                }
                //link faces to border edges, if was from splitted edge, the first face is assigned previosly
                e0m.f[1] = f0;
                em1.f[1] = f1;
            }
            // not splitted, create the middle vertex and two split edges
            else
            {
                // copy vm data from v0 data, is a struct so create a shallow copy, it copy colors, normals ecc...
                T v = new T();
                v.position = (v0.data.position + v1.data.position) * 0.5f;
                
                vM = new HVertex(v, m_verts.Count);

                m_verts.Add(vM);

                e0m = new HEdge(v0, vM);
                em1 = new HEdge(vM, v1);

                ecrack0 = e0m;
                ecrack1 = em1;

                // assign the currect face to first position, if the main edge have a second faces,
                // the new faces will be link in the second position
                e0m.f[0] = f0;
                em1.f[0] = f1;
            }
            e_crack[eID * 2] = ecrack0;
            e_crack[eID * 2 + 1] = ecrack1;
        }


        /// <summary>
        /// Convert the half-edge structure in the base structure
        /// </summary>
        MeshGeometry getTriMesh()
        {
            MeshGeometry mesh = new MeshGeometry();

            if (m_verts.Count > ushort.MaxValue - 1)
                throw new OverflowException("too many vertices for a 16 bit indices");

            if (m_vertstype == typeof(CVERTEX))
            {
                mesh.colors = new Color[m_verts.Count];
                mesh.vertices = new Vector3[m_verts.Count];

                for (int i = 0; i < m_verts.Count; i++)
                {
                    CVERTEX v = (CVERTEX)m_verts[i].data;
                    mesh.vertices[i] = v.position;
                    mesh.colors[i] = Color.FromArgb(v.color);
                }
            }
            else if (m_vertstype == typeof(VERTEX))
            {
                mesh.colors = null;
                mesh.vertices = new Vector3[m_verts.Count];
                for (int i = 0; i < m_verts.Count; i++)
                {
                    VERTEX v = (VERTEX)m_verts[i].data;
                    mesh.vertices[i] = v.position;
                }
            }
            else throw new NotImplementedException();

            //fix the id
            for (int i = 0; i < m_verts.Count; i++) m_verts[i].ID = i;

            mesh.faces = new Face[m_faces.Count];
            for (int i = 0; i < m_faces.Count; i++)
            {
                mesh.faces[i] = new Face();
                mesh.faces[i].I = (ushort)m_faces[i].v[0].ID;
                mesh.faces[i].J = (ushort)m_faces[i].v[1].ID;
                mesh.faces[i].K = (ushort)m_faces[i].v[2].ID;
            }
            return mesh;
        }
        /// <summary>
        /// Convert the base structure to half-edge structure
        /// </summary>
        void setTriMesh(IList<Face> faces, IList<Vector3> vertices, IList<Color> colors)
        {
            m_verts.Clear();
            m_faces.Clear();
            if (vertices != null)
            {
                if (colors != null)
                {
                    m_vertstype = typeof(CVERTEX);
                    for (int i = 0; i < vertices.Count; i++)
                        m_verts.Add(new HVertex(new CVERTEX(vertices[i], colors[i]), i));
                }
                else
                {
                    m_vertstype = typeof(VERTEX);
                    for (int i = 0; i < vertices.Count; i++)
                        m_verts.Add(new HVertex(new VERTEX(vertices[i]), i));
                }
            }
            else
            {
                throw new ArgumentNullException();
            }

            foreach (Face f in faces)
                m_faces.Add(new HFace(m_verts[f.I], m_verts[f.J], m_verts[f.K]));

            m_verts.TrimExcess();
            m_faces.TrimExcess();
        }

    }

}
