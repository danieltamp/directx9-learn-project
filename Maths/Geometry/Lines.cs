﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// Abstract class, used to derive all similar Primitives Lines geometries
    /// </summary>
    public abstract class BaseLineGeometry : BaseGeometry
    {
        public override int numVertices { get { return (vertices != null) ? vertices.Length : 0; } }

        public Vector3[] vertices;
        public Color[] colors;

        /// <summary>
        /// Get the vertex indices of this primitive
        /// </summary>
        public abstract Edge GetSegment(int i);


        public BaseLineGeometry()
            : base()
        {
            m_primitive = PrimitiveType.LineList;
            vertices = null;
            colors = null;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseLineGeometry(BaseLineGeometry src)
            : base(src)
        {
            vertices = src.vertices;
            colors = src.colors;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseLineGeometry(BaseGeometry src)
            : base(src) { }

        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);

            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];

            transform = newtransform;
        }
    
    
    }
    
    /// <summary>
    /// Primitives Lines geometries NOT-INDEXED
    /// </summary>
    public class LineGeometry : BaseLineGeometry
    {
        //    LINE-LIST        LINE-STRIP
        //     0   2          0   2   4
        //      \   \          \ / \ /
        //       1   3          1   3

        public override int numPrimitives
        {
            get { return (m_primitive == PrimitiveType.LineList) ? numVertices / 2 : numVertices - 1; }
        }
        
        public override Edge GetSegment(int i)
        {
            if (m_primitive == PrimitiveType.LineList)
                return new Edge((ushort)(i * 2), (ushort)(i * 2 + 1));
            else
                return new Edge((ushort)i, (ushort)(i + 1));
        }

        public LineGeometry()
            : base() { }
        /// <summary>
        /// Copy from other souuce instance
        /// </summary>
        public LineGeometry(BaseLineGeometry src)
            : base(src) { }
        /// <summary>
        /// Copy from other souuce instance
        /// </summary>
        public LineGeometry(BaseGeometry src)
            : base(src) { }

        public static explicit operator SplineGeometry(LineGeometry line)
        {
            SplineGeometry shape = new SplineGeometry();

            // clonable because struct was passed as value and not by reference
            if (line.vertices != null) shape.vertices = (Vector3[])line.vertices.Clone();
            if (line.colors != null) shape.colors = (Color[])line.colors.Clone();

            // build indices , the getTriangle methods return indices used for TriangleList
            if (line.numPrimitives > 0)
            {
                shape.edges = new Edge[line.numPrimitives];
                for (int s = 0; s < shape.edges.Length; s++)
                    shape.edges[s] = line.GetSegment(s);
            }
            return shape;
        }

        public static LineGeometry Axis3D()
        {
            LineGeometry axis = new LineGeometry();

            axis.name = "axis3d";
            axis.vertices = new Vector3[]
            {
                new Vector3(0,0,0),
                new Vector3(1,0,0),
                new Vector3(0,0,0),
                new Vector3(0,1,0),
                new Vector3(0,0,0),
                new Vector3(0,0,1)
            };
            axis.colors = new Color[]
            {
                Color.Red,
                Color.Red,
                Color.Green,
                Color.Green,
                Color.Blue , 
                Color.Blue        
            };
            axis.m_primitive = PrimitiveType.LineList;

            return axis;
        } 
        public static LineGeometry Grid2D()
        {
            LineGeometry grid = new LineGeometry();
            grid.name = "grid2d";
            grid.vertices = new Vector3[40];
            int i=0;
            for (int x = -5; x < 6; x++)
            {
                if (x == 0) continue;
                grid.vertices[i++] = new Vector3(x, 0, -5);
                grid.vertices[i++] = new Vector3(x, 0, 5);
            }
            for (int z = -5; z < 6; z++)
            {
                if (z == 0) continue;
                grid.vertices[i++] = new Vector3(-5, 0, z);
                grid.vertices[i++] = new Vector3(5, 0, z);
            }

            grid.colors = new Color[grid.vertices.Length];
            for (i = 0; i < grid.vertices.Length; i++) grid.colors[i] = Color.Gray;

            grid.m_primitive = PrimitiveType.LineList;

            return grid;
        }
        public static LineGeometry Circle(int numpoints)
        {
            float dang = (float)Math.PI * 2.0f / numpoints;
            int numverts = numpoints + 1;

            LineGeometry obj = new LineGeometry();

            obj.vertices = new Vector3[numverts];
            obj.colors = new Color[numverts];
            for (int i = 0; i < numverts; i++)
            {
                obj.vertices[i] = new Vector3((float)Math.Sin(dang * i), (float)Math.Cos(dang * i), 0);
                obj.colors[i] = Color.Yellow;
            }
            obj.m_primitive = PrimitiveType.LineStrip;
            return obj;
        }

    }

    /// <summary>
    /// Primitives Lines geometries INDEXED
    /// </summary>
    public class SplineGeometry : BaseLineGeometry
    {
        public Edge[] edges;

        // TODO : use the 0xFFFF reset index implementation for line strip
        //ushort[] indices;

        public override int numPrimitives
        {
            get { return (edges != null) ? edges.Length : 0; }
        }

        public override Edge GetSegment(int i)
        {
            return edges[i];
        }

        public SplineGeometry()
            : base()
        {
            m_primitive = PrimitiveType.LineList;
        }

        /// <summary>
        /// Copy from source instance
        /// </summary>
        public SplineGeometry(SplineGeometry src)
            : base(src) { edges = src.edges; }
        /// <summary>
        /// Copy from other souuce instance
        /// </summary>
        public SplineGeometry(BaseLineGeometry src)
            : base(src) { }
        /// <summary>
        /// Copy from other souuce instance
        /// </summary>
        public SplineGeometry(BaseGeometry src)
            : base(src) { }

        /// <summary>
        /// A sphere made with three yellow circle, minimum 3 points
        /// </summary>
        public static SplineGeometry SphereGizmo(float radius, int numpoints)
        {
            Debug.Assert(numpoints > 2, "minimum 3");

            float dang = (float)Math.PI * 2.0f / numpoints;

            SplineGeometry shape = new SplineGeometry();

            shape.vertices = new Vector3[numpoints * 3];
            shape.edges = new Edge[numpoints * 3];
            shape.colors = new Color[shape.vertices.Length];

            /////////////////////circle A
            for (int i = 0; i < numpoints; i++)
                shape.vertices[i] = new Vector3((float)Math.Sin(dang * i), (float)Math.Cos(dang * i), 0);

            for (int i = 0; i < numpoints; i++)
                shape.edges[i] = new Edge(i, i + 1);
            shape.edges[numpoints - 1].J = 0;


            /////////////////////circle B
            for (int i = numpoints; i < numpoints * 2; i++)
                shape.vertices[i] = new Vector3(0, (float)Math.Cos(dang * i), (float)Math.Sin(dang * i));

            for (int i = numpoints; i < numpoints * 2; i++)
                shape.edges[i] = new Edge(i, i + 1);
            shape.edges[numpoints * 2 - 1].J = (ushort)numpoints;



            /////////////////////circle C
            for (int i = numpoints * 2; i < numpoints * 3; i++)
                shape.vertices[i] = new Vector3((float)Math.Cos(dang * i), 0, (float)Math.Sin(dang * i));

            for (int i = numpoints * 2; i < numpoints * 3; i++)
                shape.edges[i] = new Edge(i, i + 1);
            shape.edges[numpoints * 3 - 1].J = (ushort)(numpoints * 2);


            for (int i = 0; i < shape.vertices.Length; i++)
            {
                shape.vertices[i] *= radius;
                shape.colors[i] = Color.Yellow;
            }
            return shape;
        }

        /// <summary>
        /// A box with semi-size
        /// </summary>
        public static SplineGeometry BoxGizmo(float dx, float dy, float dz)
        {

            //           5______4
            //           /     /|         Y
            //         1/_____/0|         |
            //          | 7   | /6        *---> Z
            //          |_____|/         /
            //         3      2         X
            SplineGeometry shape = new SplineGeometry();

            shape.vertices = new Vector3[]{
                new Vector3( dx, dy, dz),
                new Vector3( dx, dy,-dz),
                new Vector3( dx,-dy, dz),
                new Vector3( dx,-dy,-dz),
                new Vector3(-dx, dy, dz),
                new Vector3(-dx, dy,-dz),
                new Vector3(-dx,-dy, dz),
                new Vector3(-dx,-dy,-dz)};

            shape.colors = new Color[8];
            for (int i = 0; i < 8; i++)
                shape.colors[i] = Color.Yellow;

            shape.edges = new Edge[]{
                new Edge(0,1),
                new Edge(1,3),
                new Edge(3,2),
                new Edge(2,0),
                new Edge(4,5),
                new Edge(5,7),
                new Edge(7,6),
                new Edge(6,4),
                new Edge(0,4),
                new Edge(2,6),
                new Edge(3,7),
                new Edge(1,5)};

            return shape;
        }

        /// <summary>
        /// A box with only corner, min and max are in global coordinate system
        /// </summary>
        public static SplineGeometry SelectBoxGizmo(Vector3 min, Vector3 max)
        {
            //            3           7
            //          0/___1   5__4/     
            //          |           |
            //          |           |
            //         2            6

            SplineGeometry shape = new SplineGeometry();

            Vector3 d = max - min;

            float x = Math.Abs(d.x / 3.0f);
            float y = Math.Abs(d.y / 3.0f);
            float z = Math.Abs(d.z / 3.0f);


            shape.vertices = new Vector3[32];

            // corner +X+Y+Z
            shape.vertices[0] = new Vector3(max.x, max.y, max.z);
            shape.vertices[1] = new Vector3(max.x - x, max.y, max.z);
            shape.vertices[2] = new Vector3(max.x, max.y - y, max.z);
            shape.vertices[3] = new Vector3(max.x, max.y, max.z - z);

            // corner +X+Y-Z
            shape.vertices[4] = new Vector3(max.x, max.y, min.z);
            shape.vertices[5] = new Vector3(max.x - x, max.y, min.z);
            shape.vertices[6] = new Vector3(max.x, max.y - y, min.z);
            shape.vertices[7] = new Vector3(max.x, max.y, min.z + z);

            // corner +X-Y+Z
            shape.vertices[8] = new Vector3(max.x, min.y, max.z);
            shape.vertices[9] = new Vector3(max.x - x, min.y, max.z);
            shape.vertices[10] = new Vector3(max.x, min.y + y, max.z);
            shape.vertices[11] = new Vector3(max.x, min.y, max.z - z);

            // corner +X-Y-Z
            shape.vertices[12] = new Vector3(max.x, min.y, min.z);
            shape.vertices[13] = new Vector3(max.x - x, min.y, min.z);
            shape.vertices[14] = new Vector3(max.x, min.y + y, min.z);
            shape.vertices[15] = new Vector3(max.x, min.y, min.z + z);


            // corner -X+Y+Z
            shape.vertices[16] = new Vector3(min.x, max.y, max.z);
            shape.vertices[17] = new Vector3(min.x + x, max.y, max.z);
            shape.vertices[18] = new Vector3(min.x, max.y - y, max.z);
            shape.vertices[19] = new Vector3(min.x, max.y, max.z - z);

            // corner -X+Y-Z
            shape.vertices[20] = new Vector3(min.x, max.y, min.z);
            shape.vertices[21] = new Vector3(min.x + x, max.y, min.z);
            shape.vertices[22] = new Vector3(min.x, max.y - y, min.z);
            shape.vertices[23] = new Vector3(min.x, max.y, min.z + z);

            // corner -X-Y+Z
            shape.vertices[24] = new Vector3(min.x, min.y, max.z);
            shape.vertices[25] = new Vector3(min.x + x, min.y, max.z);
            shape.vertices[26] = new Vector3(min.x, min.y + y, max.z);
            shape.vertices[27] = new Vector3(min.x, min.y, max.z - z);

            // corner -X-Y-Z
            shape.vertices[28] = new Vector3(min.x, min.y, min.z);
            shape.vertices[29] = new Vector3(min.x + x, min.y, min.z);
            shape.vertices[30] = new Vector3(min.x, min.y + y, min.z);
            shape.vertices[31] = new Vector3(min.x, min.y, min.z + z);

            shape.edges = new Edge[24];

            shape.edges[0] = new Edge(0, 1);
            shape.edges[1] = new Edge(0, 2);
            shape.edges[2] = new Edge(0, 3);

            int j = 3;
            int k = 4;
            for (int i = 1; i < 8; i++)
            {
                shape.edges[j++] = shape.edges[0] + k;
                shape.edges[j++] = shape.edges[1] + k;
                shape.edges[j++] = shape.edges[2] + k;
                k += 4;
            }

            shape.colors = new Color[32];
            for (int i = 0; i < 32; i++)
            {
                shape.colors[i] = Color.White;
            }

            return shape;
        }
    }
}
