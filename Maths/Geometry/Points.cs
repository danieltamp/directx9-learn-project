﻿// by johnwhile
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// Manage the Primitives Points geometries NOT-INDEXED
    /// </summary>
    public class PointGeometry : BaseGeometry
    {
        public override int numVertices 
        { 
            get { return (vertices != null) ? vertices.Length : 0; } 
        }

        public override int numPrimitives 
        {
            get { return numVertices; }
        } 
        
        //primitivetype is only points
        public Vector3[] vertices;
        public Color[] colors;

        public PointGeometry() : base()
        {
            m_primitive = PrimitiveType.PointList;
            vertices = null;
            colors = null;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public PointGeometry(PointGeometry src)
            : base(src)
        {
            vertices = src.vertices;
            colors = src.colors;
        }
        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);
            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];
            transform = newtransform;
        }
    }
}
