﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// Abstract class, used to derive all similar Primitives Triangles
    /// </summary>
    public abstract class BaseTriGeometry : BaseGeometry
    {
        public override int numVertices { get { return (vertices != null) ? vertices.Length : 0; } }

        public BaseTriGeometry()
            : base()
        {
            m_primitive = PrimitiveType.TriangleList;
            vertices = null;
            normals = null;
            textures = null;
            colors = null;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseTriGeometry(BaseTriGeometry src)
            : base(src)
        {
            vertices = src.vertices;
            normals = src.normals;
            textures = src.textures;
            colors = src.colors;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseTriGeometry(BaseGeometry src)
            : base(src) { }

        /// <summary>
        /// Get the vertex indices of this primitive
        /// </summary>
        public abstract Face GetTriangle(int i);
        /// <summary>
        /// Get the num of primitives of this Triangle's Type 
        /// </summary>
        
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector2[] textures;
        public Color[] colors;

        /// <summary>
        /// Virtual method, recalculate all normal by face normal
        /// </summary>
        public virtual void SetDefaultNormal()
        {
            Face[] faces = new Face[numPrimitives];
            for (int t = 0; t < numPrimitives; t++)
                faces[t] = GetTriangle(t);
            normals = GeometryTools.GetDefaultNormal(vertices, faces);
        }
        /// <summary>
        /// Virtual method, recalculate all texture by face using a Matrix4.Identity projection plane
        /// </summary>
        public virtual void SetDefaultTexture()
        {
            textures = GeometryTools.GetPlanarProjection(vertices, new Vector2(0, 0), new Vector2(1, 1), Matrix4.Identity);
        }

        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);
            // remove traslation componenent because normal are a direction (position = 0,0,0)
            Matrix4 matrix_normal = matrix;
            matrix_normal.TranslationComponent = new Vector3(0, 0, 0);

            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];

            if (normals != null)
                for (int i = 0; i < numVertices; i++)
                    normals[i] = matrix_normal * normals[i];

            transform = newtransform;
        }
    }

    /// <summary>
    /// Primitives Triangles geometries NOT-INDEXED
    /// </summary>
    public class TriangleGeometry : BaseTriGeometry
    {    
        //    TRIANGLE-FAN        TRIANGLE-LIST         TRIANGLE-STRIP
        //     [0]______1         0______1   3          0_____1____ 3
        //      /|\    /           \    /   /\          \    /\    /
        //     / | \  /             \  /   /  \          \  /  \  /
        //    /__|__\/               \/   /____\          \/____\/
        //   4   3   2               2   5      4         2      4
        // 0-1-2-3-4 in clockwire

        /// <summary>
        /// a not-indexed geometry have always a primitives count
        /// </summary>
        public override int numPrimitives
        {
            get
            {
                switch (m_primitive)
                {
                    case PrimitiveType.TriangleFan: return (numVertices - 2);
                    case PrimitiveType.TriangleList: return (numVertices / 3);
                    case PrimitiveType.TriangleStrip: return (numVertices - 2);
                    default: return 0;
                }
            }
        }
        
        /// <summary>
        /// Return the i-th triangle value, the geometry is considered as trianglelist
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public override Face GetTriangle(int i)
        {
            switch (m_primitive)
            {
                case PrimitiveType.TriangleFan: return new Face((ushort)0, (ushort)(i + 1), (ushort)(i + 2));
                case PrimitiveType.TriangleList: return new Face((ushort)(i * 3), (ushort)(i * 3 + 1), (ushort)(i * 3 + 2));
                case PrimitiveType.TriangleStrip: return new Face((ushort)(i), (ushort)(i + 1), (ushort)(i + 2));
            }
            return new Face();
        }

        public TriangleGeometry()
            : base() { }
        public TriangleGeometry(BaseTriGeometry src)
            : base(src) { }
        public TriangleGeometry(BaseGeometry src)
            : base(src) { }

        public static explicit operator MeshGeometry(TriangleGeometry tri)
        {
            MeshGeometry mesh = new MeshGeometry();
            mesh.transform = tri.transform;
            mesh.vertices = tri.vertices;
            mesh.normals =  tri.normals;
            mesh.textures = tri.textures;
            mesh.colors =   tri.colors;

            // build indices , the getTriangle methods return indices used for TriangleList
            if (tri.numPrimitives > 0)
            {
                mesh.faces = new Face[tri.numPrimitives];
                for (int f = 0; f < mesh.faces.Length; f++)
                    mesh.faces[f] = tri.GetTriangle(f);
            }
            return mesh;
        }


        public static TriangleGeometry Triangle()
        {
            //       Y
            //       |______ Z
            //       / _/
            //      /_/
            //     X
            TriangleGeometry mesh = new TriangleGeometry();

            mesh.vertices = new Vector3[] {
                new Vector3(0, 0, 0),
                new Vector3(1, 0, 0),
                new Vector3(0, 0, 1)};

            mesh.colors = new Color[] {
                Color.Red,
                Color.Green,
                Color.Blue};

            mesh.normals = new Vector3[]{
                new Vector3(0,1,0),
                new Vector3(0,1,0),
                new Vector3(0,1,0)};

            mesh.textures = new Vector2[]{
                new Vector2(0,0),
                new Vector2(0,1),
                new Vector2(1,0)};

            mesh.m_primitive = PrimitiveType.TriangleList;
            mesh.boundSphere = BoundarySphere.GetBoundingSphereFast(mesh.vertices);

            return mesh;
        }
        public static TriangleGeometry Piramid()
        {
            Vector3 p0 = new Vector3(0, 1, 0);
            Vector3 p1 = new Vector3(1, 0, 0);
            Vector3 p2 = new Vector3(0, 0, -1);
            Vector3 p3 = new Vector3(-1, 0, 0);
            Vector3 p4 = new Vector3(0, 0, 1);

            TriangleGeometry mesh = new TriangleGeometry();

            mesh.vertices = new Vector3[]{
                p0,p1,p2,
                p0,p2,p3,
                p0,p3,p4,
                p0,p4,p1,
                p1,p3,p2,
                p1,p4,p3
            };
            mesh.colors = new Color[mesh.vertices.Length];
            mesh.colors[0] = mesh.colors[1] = mesh.colors[2] = Color.Red;
            mesh.colors[3] = mesh.colors[4] = mesh.colors[5] = Color.Green;
            mesh.colors[6] = mesh.colors[7] = mesh.colors[8] = Color.Blue;
            mesh.colors[9] = mesh.colors[10] = mesh.colors[11] = Color.Cyan;
            mesh.colors[12] = mesh.colors[13] = mesh.colors[14] = Color.Yellow;
            mesh.colors[15] = mesh.colors[16] = mesh.colors[17] = Color.Magenta;

            mesh.textures = new Vector2[mesh.vertices.Length];
            for (int i = 0; i < mesh.textures.Length; i+=3)
            {
                mesh.textures[i + 0] = new Vector2(0.5f, 1);
                mesh.textures[i + 1] = new Vector2(1, 0);
                mesh.textures[i + 2] = new Vector2(0, 0);
            }

            mesh.SetDefaultNormal();

            mesh.m_primitive = PrimitiveType.TriangleList;
            mesh.boundSphere = BoundarySphere.GetBoundingSphereFast(mesh.vertices);

            return mesh;
        }
        public static TriangleGeometry Quad()
        {
            TriangleGeometry mesh = new TriangleGeometry();

            mesh.vertices = new Vector3[]{
                new Vector3(1,1,0),
                new Vector3(0,1,0),
                new Vector3(0,0,0),
                new Vector3(1,0,0)};

            mesh.textures = new Vector2[]{
                new Vector2(0,1),
                new Vector2(1,1),
                new Vector2(1,0),
                new Vector2(0,0)};

            mesh.normals = new Vector3[]{
                new Vector3(0,0,1),
                new Vector3(0,0,1),
                new Vector3(0,0,1),
                new Vector3(0,0,1)};

            mesh.colors = new Color[]{
                Color.Red,
                Color.Green,
                Color.Blue,
                Color.White};

            mesh.m_primitive = PrimitiveType.TriangleFan;
            mesh.boundSphere = new BoundarySphere(new Vector3(0.5f, 0.5f, 0), 0.5f);

            return mesh;
        }

    }

    /// <summary>
    /// Primitives Triangles geometries INDEXED
    /// </summary>
    public class MeshGeometry : BaseTriGeometry
    {
        public Face[] faces;
        // TODO : use the 0xFFFF reset index implementation for triangle strip
        //ushort[] indices;

        public override int numPrimitives { get { return (faces != null) ? faces.Length : 0; } }

        public override Face GetTriangle(int i) { return faces[i]; }

        public override void SetDefaultNormal()
        {
            normals = GeometryTools.GetDefaultNormal(vertices, faces);
        }

        public MeshGeometry()
            : base()
        {
            m_primitive = PrimitiveType.TriangleList;
        }

        /// <summary>
        /// Copy from source instance
        /// </summary>
        public MeshGeometry(MeshGeometry src)
            : base(src) { faces = src.faces; }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public MeshGeometry(BaseTriGeometry src)
            : base(src) { }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public MeshGeometry(BaseGeometry src)
            : base(src) { }

        /// <summary>
        /// A simple rectangle oriented to normal, used only for debug planes
        /// </summary>
        public static MeshGeometry Plane(Plane plane)
        {
            MeshGeometry quad = new MeshGeometry();

            quad.faces = new Face[]{
                new Face(0,1,2),
                new Face(0,2,3)};

            // oriented to Z
            quad.vertices = new Vector3[]{
                new Vector3(-1,-1,0),
                new Vector3(-1, 1,0),
                new Vector3( 1, 1,0),
                new Vector3( 1,-1,0)};

            quad.textures = new Vector2[]{
                new Vector2(0,1),
                new Vector2(0,0),
                new Vector2(1,0),
                new Vector2(1,1)};

            quad.normals = new Vector3[]{
                new Vector3(0,0,-1),
                new Vector3(0,0,-1),
                new Vector3(0,0,-1),
                new Vector3(0,0,-1)};

            quad.colors = new Color[]{
                Color.Red,
                Color.Green,
                Color.Blue,
                Color.White};

            Matrix4 transform = Matrix4.Identity;
            Vector3 vz = plane.norm;
            Vector3 vx = Vector3.Cross(vz, Vector3.UnitY);
            Vector3 vy = Vector3.Cross(vz, vx);

            vx.Normalize();
            vy.Normalize();
            vz.Normalize();

            transform.setRow(0, vx);
            transform.setRow(1, vy);
            transform.setRow(2, vz);

            transform = Matrix4.Inverse(transform);

            transform.TranslationComponent = plane.Origin;
            quad.transform = transform;

            quad.boundSphere.radius = 1;
            return quad;
        }
        /// <summary>
        /// A cube with 6 quads detached, usefull when use texture for each quad.
        /// </summary>
        public static MeshGeometry CubeOpen()
        {
            MeshGeometry mesh = new MeshGeometry { name = "CubeOpen" };

            mesh.faces = new Face[12];
            mesh.vertices = new Vector3[24];
            mesh.colors = new Color[24];
            mesh.normals = new Vector3[24];
            mesh.textures = new Vector2[24];

            //front
            mesh.faces[0] = new Face(3, 1, 0);
            mesh.faces[1] = new Face(3, 0, 2);
            mesh.vertices[0] = new Vector3(1, 1, 1);
            mesh.vertices[1] = new Vector3(1, 1, -1);
            mesh.vertices[2] = new Vector3(1, -1, 1);
            mesh.vertices[3] = new Vector3(1, -1, -1);
            mesh.normals[0] = mesh.normals[1] = mesh.normals[2] = mesh.normals[3] = new Vector3(1, 0, 0);
            mesh.colors[0] = mesh.colors[1] = mesh.colors[2] = mesh.colors[3] = Color.Red;
            //right
            mesh.faces[2] = new Face(3, 1, 0) + 4;
            mesh.faces[3] = new Face(3, 0, 2) + 4;
            mesh.vertices[4] = new Vector3(1, 1, 1);
            mesh.vertices[5] = new Vector3(1, -1, 1);
            mesh.vertices[6] = new Vector3(-1, 1, 1);
            mesh.vertices[7] = new Vector3(-1, -1, 1);
            mesh.normals[4] = mesh.normals[5] = mesh.normals[6] = mesh.normals[7] = new Vector3(0, 0, 1);
            mesh.colors[4] = mesh.colors[5] = mesh.colors[6] = mesh.colors[7] = Color.Green;
            //back
            mesh.faces[4] = new Face(3, 0, 1) + 8;
            mesh.faces[5] = new Face(3, 2, 0) + 8;
            mesh.vertices[8] = new Vector3(-1, 1, 1);
            mesh.vertices[9] = new Vector3(-1, 1, -1);
            mesh.vertices[10] = new Vector3(-1, -1, 1);
            mesh.vertices[11] = new Vector3(-1, -1, -1);
            mesh.normals[8] = mesh.normals[9] = mesh.normals[10] = mesh.normals[11] = new Vector3(-1, 0, 0);
            mesh.colors[8] = mesh.colors[9] = mesh.colors[10] = mesh.colors[11] = Color.Blue;
            //left
            mesh.faces[6] = new Face(3, 0, 1) + 12;
            mesh.faces[7] = new Face(3, 2, 0) + 12;
            mesh.vertices[12] = new Vector3(1, 1, -1);
            mesh.vertices[13] = new Vector3(1, -1, -1);
            mesh.vertices[14] = new Vector3(-1, 1, -1);
            mesh.vertices[15] = new Vector3(-1, -1, -1);
            mesh.normals[12] = mesh.normals[13] = mesh.normals[14] = mesh.normals[15] = new Vector3(0, 0, -1);
            mesh.colors[12] = mesh.colors[13] = mesh.colors[14] = mesh.colors[15] = Color.Yellow;
            //top
            mesh.faces[8] = new Face(3, 0, 1) + 16;
            mesh.faces[9] = new Face(3, 2, 0) + 16;
            mesh.vertices[16] = new Vector3(1, 1, 1);
            mesh.vertices[17] = new Vector3(1, 1, -1);
            mesh.vertices[18] = new Vector3(-1, 1, 1);
            mesh.vertices[19] = new Vector3(-1, 1, -1);
            mesh.normals[16] = mesh.normals[17] = mesh.normals[18] = mesh.normals[19] = new Vector3(0, 1, 0);
            mesh.colors[16] = mesh.colors[17] = mesh.colors[18] = mesh.colors[19] = Color.Magenta;
            //bottom
            mesh.faces[10] = new Face(3, 1, 0) + 20;
            mesh.faces[11] = new Face(3, 0, 2) + 20;
            mesh.vertices[20] = new Vector3(1, -1, 1);
            mesh.vertices[21] = new Vector3(1, -1, -1);
            mesh.vertices[22] = new Vector3(-1, -1, 1);
            mesh.vertices[23] = new Vector3(-1, -1, -1);
            mesh.normals[20] = mesh.normals[21] = mesh.normals[22] = mesh.normals[23] = new Vector3(0, -1, 0);
            mesh.colors[20] = mesh.colors[21] = mesh.colors[22] = mesh.colors[23] = Color.Cyan;

            for (int i = 0; i < 6; i++)
            {
                mesh.textures[i * 4 + 0] = new Vector2(0, 0);
                mesh.textures[i * 4 + 1] = new Vector2(0, 1);
                mesh.textures[i * 4 + 2] = new Vector2(1, 0);
                mesh.textures[i * 4 + 3] = new Vector2(1, 1);
            }
            mesh.boundSphere.radius = (float)Math.Sqrt(3);
            return mesh;
        }
        /// <summary>
        /// A default cube with 8 vertices and 12 faces.
        /// </summary>
        public static MeshGeometry CubeClose()
        {
            //           5______4
            //           /     /|         Y
            //         1/_____/0|         |
            //          | 7   | /6        *---> Z
            //          |_____|/         /
            //         3      2         X

            MeshGeometry cube = new MeshGeometry { name = "CubeClose" };

            cube.vertices = new Vector3[]{
                new Vector3( 1, 1, 1),
                new Vector3( 1, 1,-1),
                new Vector3( 1,-1, 1),
                new Vector3( 1,-1,-1),
                new Vector3(-1, 1, 1),
                new Vector3(-1, 1,-1),
                new Vector3(-1,-1, 1),
                new Vector3(-1,-1,-1)};

            cube.faces = new Face[]{
                //front
                new Face(3,1,0),
                new Face(3,0,2),
                //top
                new Face(1,5,4),
                new Face(1,4,0),
                //back
                new Face(4,7,6),
                new Face(4,5,7),
                //bottom
                new Face(7,2,6),
                new Face(7,3,2),
                //right
                new Face(0,4,6),
                new Face(0,6,2),
                //left
                new Face(3,5,1),
                new Face(3,7,5)};

            cube.colors = new Color[]{
                Color.Red,
                Color.Blue,
                Color.Green,
                Color.Magenta,
                Color.Cyan,
                Color.Yellow,
                Color.White,
                Color.Black};

            cube.normals = new Vector3[cube.vertices.Length];
            for (int i = 0; i < cube.vertices.Length; i++)
                cube.normals[i] = cube.vertices[i].Normal;

            cube.boundSphere = new BoundarySphere(Vector3.Zero, (float)Math.Sqrt(3));

            return cube;
        }
        /// <summary>
        /// A classic sphere
        /// </summary>
        /// <param name="slices">verticals lines</param>
        /// <param name="stacks">orizontals lines</param>
        public static MeshGeometry Sphere(int slices, int stacks, float radius)
        {
            MeshGeometry mesh = new MeshGeometry { name = "Sphere" };

            int numVerticesPerRow = slices + 1;
            int numVerticesPerColumn = stacks + 1;

            int numverts = numVerticesPerRow * numVerticesPerColumn;
            mesh.vertices = new Vector3[numverts];
            mesh.colors = new Color[numverts];
            mesh.normals = new Vector3[numverts];

            float theta = 0.0f;
            float phi = 0.0f;
            float verticalAngularStride = (float)Math.PI / (float)stacks;
            float horizontalAngularStride = ((float)Math.PI * 2) / (float)slices;
            int i = 0;

            for (int ivertical = 0; ivertical < numVerticesPerColumn; ivertical++)
            {
                // beginning on top of the sphere:
                theta = ((float)Math.PI / 2.0f) - verticalAngularStride * ivertical;

                for (int ihorizontal = 0; ihorizontal < numVerticesPerRow; ihorizontal++)
                {
                    phi = horizontalAngularStride * ihorizontal;
                    float x = radius * (float)Math.Cos(theta) * (float)Math.Cos(phi);
                    float y = radius * (float)Math.Cos(theta) * (float)Math.Sin(phi);
                    float z = radius * (float)Math.Sin(theta);
                    mesh.vertices[i++] = new Vector3(x, z, y);
                }
            }

            for (i = 0; i < numverts; i++)
            {
                mesh.normals[i] = mesh.vertices[i].Normal;
                mesh.colors[i] = Color.Blue;
            }


            int numfaces = slices * stacks * 2;
            i = 0;
            mesh.faces = new Face[numfaces];

            for (int verticalIt = 0; verticalIt < stacks; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    int lt = horizontalIt + verticalIt * (numVerticesPerRow);
                    int rt = (horizontalIt + 1) + verticalIt * (numVerticesPerRow);

                    int lb = horizontalIt + (verticalIt + 1) * (numVerticesPerRow);
                    int rb = (horizontalIt + 1) + (verticalIt + 1) * (numVerticesPerRow);

                    mesh.faces[i++] = new Face(lt, rt, lb);
                    mesh.faces[i++] = new Face(rt, rb, lb);
                }
            }
            mesh.textures = GeometryTools.GetCilindralProjection(mesh.vertices, Matrix4.Identity);
            mesh.boundSphere.radius = radius;

            return mesh;
        }
        /// <summary>
        /// Cone with Y peak , heigth = 1.0 , base circle in XZ plane with radius = 1.0  
        /// </summary>
        public static MeshGeometry Cone(int basePoints)
        {
            float dang = (float)Math.PI * 2.0f / basePoints;
            int numverts = basePoints + 1;

            MeshGeometry obj = new MeshGeometry { name = "Cone" };

            obj.vertices = new Vector3[numverts];
            obj.colors = new Color[numverts];
            obj.faces = new Face[basePoints * 2 - 2];

            // base vertices 
            for (int i = 0; i < numverts; i++)
            {
                obj.vertices[i] = new Vector3((float)Math.Sin(dang * i), 0, (float)Math.Cos(dang * i));
                obj.colors[i] = Color.Blue;
            }
            // peak vertex
            obj.vertices[numverts - 1] = new Vector3(0, 1, 0);

            // cone faces
            int f = 0;
            for (int i = 0; i < basePoints - 1; i++)
                obj.faces[f++] = new Face(i, i + 1, numverts - 1);
            obj.faces[f++] = new Face(basePoints - 1, 0, basePoints);

            // base faces
            for (int i = 0; i < basePoints - 2; i++)
                obj.faces[f++] = new Face(0, i + 2, i + 1);

            obj.boundSphere = BoundarySphere.GetBoundingSphereFast(obj.vertices);

            return obj;
        }

        /// <summary>
        /// Cylinder with base center [0,0,0]
        /// </summary>
        public static MeshGeometry Cylinder(float radius, float height, int slices)
        {
            MeshGeometry mesh = new MeshGeometry { name = "Cylinder" };

            int numverts = (slices + 1) * 2 + 2;
            float theta = 0.0f;
            float horizontalAngularStride = ((float)Math.PI * 2) / (float)slices;
            int i = 0;

            mesh.vertices = new Vector3[numverts];
            mesh.colors = new Color[numverts];

            for (i = 0; i < numverts; i++) mesh.colors[i] = Color.Blue;

            i = 0;
            for (int verticalIt = 0; verticalIt < 2; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices + 1; horizontalIt++)
                {
                    float x;
                    float y;
                    float z;

                    theta = (horizontalAngularStride * horizontalIt);

                    if (verticalIt == 0)
                    {
                        // upper circle
                        x = radius * (float)Math.Cos(theta);
                        z = radius * (float)Math.Sin(theta);
                        y = height;

                    }
                    else
                    {
                        // lower circle
                        x = radius * (float)Math.Cos(theta);
                        z = radius * (float)Math.Sin(theta);
                        y = 0;
                    }
                    mesh.vertices[i++] = new Vector3(x, y, z);
                }
            }
            mesh.vertices[i++] = new Vector3(0, height, 0);
            mesh.vertices[i++] = new Vector3(0, 0, 0);

            int nfaces = slices * 4;
            mesh.faces = new Face[nfaces];
            i = 0;
            for (int verticalIt = 0; verticalIt < 1; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    ushort lt = (ushort)(horizontalIt + verticalIt * (slices + 1));
                    ushort rt = (ushort)((horizontalIt + 1) + verticalIt * (slices + 1));
                    ushort lb = (ushort)(horizontalIt + (verticalIt + 1) * (slices + 1));
                    ushort rb = (ushort)((horizontalIt + 1) + (verticalIt + 1) * (slices + 1));

                    mesh.faces[i++] = new Face(lt, rt, lb);
                    mesh.faces[i++] = new Face(rt, rb, lb);
                }
            }

            for (int verticalIt = 0; verticalIt < 1; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    ushort lt = (ushort)(horizontalIt + verticalIt * (slices + 1));
                    ushort rt = (ushort)((horizontalIt + 1) + verticalIt * (slices + 1));

                    ushort patchIndexTop = (ushort)((slices + 1) * 2);
                    mesh.faces[i++] = new Face(lt, patchIndexTop, rt);
                }
            }

            for (int verticalIt = 0; verticalIt < 1; verticalIt++)
            {
                for (int horizontalIt = 0; horizontalIt < slices; horizontalIt++)
                {
                    ushort lb = (ushort)(horizontalIt + (verticalIt + 1) * (slices + 1));
                    ushort rb = (ushort)((horizontalIt + 1) + (verticalIt + 1) * (slices + 1));
                    ushort patchIndexBottom = (ushort)((slices + 1) * 2 + 1);
                    mesh.faces[i++] = new Face(lb, rb, patchIndexBottom);
                }
            }

            mesh.normals = GeometryTools.GetDefaultNormal(mesh.vertices, mesh.faces);


            mesh.textures = GeometryTools.GetCilindralProjection(mesh.vertices, Matrix4.Identity);
            mesh.boundSphere.center = new Vector3(0, height * 0.5f, 0);
            mesh.boundSphere.radius = (float)Math.Sqrt(radius * radius + height * height);
            return mesh;
        }
    }
}
