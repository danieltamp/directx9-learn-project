﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// A camara class what contain all usefull information, not memory optimized
    /// </summary>
    public class Camera
    {
        public Frustum m_frustum; // the view area
        public Viewport screen = new Viewport(1200, 700, 0, 0);
        public Matrix4 m_view;
        public Matrix4 m_proj;
        public Matrix4 m_world;
        public float fovy = MathUtils.DegreeToRadian(45);
        public float aspect = 1200f / 700f;
        public float znear = 1000f;
        public float zfar = 0.1f;
        public Vector3 m_eye = Vector3.UnitZ;
        public Vector3 m_targhet = Vector3.Zero;
        public Vector3 m_up = Vector3.UnitY;

        public Camera()
        {
            this.m_world = Matrix4.Identity;
            this.m_view = Matrix4.MakeViewLH(m_eye, m_targhet, m_up);
            this.m_proj = Matrix4.MakeProjectionLH(fovy, screen, znear, zfar);
            this.m_frustum = new Frustum(this.m_proj, this.m_view,this.m_world);
        }

        public void Update()
        {
            m_world = Matrix4.Identity;
            m_view = Matrix4.MakeViewLH(m_eye, m_targhet, m_up);
            m_proj = Matrix4.MakeProjectionLH(fovy, screen, znear, zfar);
            m_frustum = new Frustum(m_proj, m_view, m_world);
        }


        public override string ToString()
        {
            string str = "";
            return str;
        }
    }
}
