﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Graphics
{
    #region Directx9 enum <-> numeric conversion

    /*
     * int32 range : −2147483648 to 2147483647
     * 
    /// <summary>
    /// Int32 enum from Microsoft SDK
    /// </summary>
    public enum DeviceState
    {
        OutOfVideoMemory = -2005532292
        DeviceLost = -2005530520,
        DeviceRemoved = -2005530512,
        DeviceHung = -2005530508,
        Ok = 0,
        PresentModeChanged = 141953143,
        PresentOccluded = 141953144,
    }
    /// <summary>
    /// Int32 enum from SharpDx
    /// </summary>
    public enum ResultCode
    {
        WrongTextureFormat = -2005530600,
        UnsupportedColorOperation = -2005530599,
        UnsupportedColorArgument = -2005530598,
        UnsupportedAlphaOperation = -2005530597,
        UnsupportedAlphaArgument = -2005530596,
        TooManyOperations = -2005530595,
        ConflictingTextureFilter = -2005530594,
        UnsupportedFactorValue = -2005530593,
        UnsupportedTextureFilter = -2005530590,
        DeviceLost = -2005530520,
        DeviceNotReset = -2005530519,
        NotAvailable = -2005530518,
        InvalidDevice = -2005530517,
        InvalidCall = -2005530516,
        Success = 0,
    }
    // from sharpdx
    [Flags]
    public enum DeviceCaps
    {
        ExecuteSystemMemory = 16,
        ExecuteVideoMemory = 32,
        TLVertexSystemMemory = 64,
        TLVertexVideoMemory = 128,
        TextureSystemMemory = 256,
        TextureVideoMemory = 512,
        DrawPrimTLVertex = 1024,
        CanRenderAfterFlip = 2048,
        TextureNonLocalVideoMemory = 4096,
        DrawPrimitives2 = 8192,
        SeparateTextureMemory = 16384,
        DrawPrimitives2Extended = 32768,
        HWTransformAndLight = 65536,
        CanBlitSysToNonLocal = 131072,
        HWRasterization = 524288,
        PureDevice = 1048576,
        QuinticRTPatches = 2097152,
        RTPatches = 4194304,
        RTPatchHandleZero = 8388608,
        NPatches = 16777216,
    }
    */


    /// <summary>
    /// Device state
    /// </summary>
    public enum DeviceState
    {
        OK = 0,
        DeviceNotReset = -2005530519,
        DeviceLost= -2005530520,
        InvalidDevice = -2005530517,
        InvalidCall = -2005530516,
    }

    /// <summary>
    /// Defines the primitives supported by Microsoft Direct3D. (from microsoft sdk)
    /// I added new Instance primitive 
    /// </summary>
    public enum PrimitiveType
    {
        Instance = 0,

        PointList = 1,

        LineList = 2,
        LineStrip = 3,

        TriangleList = 4,
        TriangleStrip = 5,
        TriangleFan = 6,
    }
    /// <summary>
    /// Defines the type of lock to perform. (from microsoft sdk)
    /// </summary>  
    [Flags]
    public enum LockFlags
    {
        Normal = 0,
        ReadOnly = 16,
        //NoSystemLock = 2048,
        NoOverwrite = 4096,
        Discard = 8192,
        //DoNotWait = 16384,
        //NoDirtyUpdate = 32768,
    }
    /// <summary>
    /// Enum : Defines the supported culling modes, which specify how back faces are culled during geometry rendering.
    /// </summary>  
    public enum Cull
    {
        None = 1,
        Clockwise = 2,
        CounterClockwise = 3,
    }
    /// <summary>
    /// Enum : Defines constants that describe the fill mode.
    /// </summary>
    public enum FillMode
    {
        Point = 1,
        WireFrame = 2,
        Solid = 3,
    }
    /// <summary>
    /// Flag : Describes values that define a vertex format used to describe the contents
    /// of vertices that are stored interleaved in a single data stream.
    /// </summary>
    [Flags]
    public enum VertexFormat
    {
        //Texture0 = 0,
        None = 0,
        Position = 2,
        //Transformed = 4,
        //PositionBlend1 = 6,
        //PositionBlend2 = 8,
        //TextureCountShift = 8,
        //PositionBlend3 = 10,
        //PositionBlend4 = 12,
        //PositionBlend5 = 14,
        Normal = 16,
        //PositionNormal = 18,
        //PointSize = 32,
        Diffuse = 64,
        //Specular = 128,
        Texture1 = 256,
        //Texture2 = 512,
        //Texture3 = 768,
        //Texture4 = 1024,
        //Texture5 = 1280,
        //Texture6 = 1536,
        //Texture7 = 1792,
        //Texture8 = 2048,
        //TextureCountMask = 3840,
        //LastBetaUByte4 = 4096,
        //PositionW = 16386,
        //PositionMask = 16398,
        //LastBetaD3DColor = 32768,
    }
    
    public enum DeclarationMethod
    {
        Default = 0,
        PartialU = 1,
        PartialV = 2,
        CrossUV = 3,
        UV = 4,
        Lookup = 5,
        LookupPresampled = 6,
    }
    public enum DeclarationType
    {
        Float1 = 0,
        Float2 = 1,
        Float3 = 2,
        Float4 = 3,
        Color = 4,
        Ubyte4 = 5,
        Short2 = 6,
        Short4 = 7,
        UByte4N = 8,
        Short2N = 9,
        Short4N = 10,
        UShort2N = 11,
        UShort4N = 12,
        UDec3 = 13,
        Dec3N = 14,
        HalfTwo = 15,
        HalfFour = 16,
        Unused = 17,
    }
    public enum DeclarationUsage
    {
        Position = 0,
        BlendWeight = 1,
        BlendIndices = 2,
        Normal = 3,
        PointSize = 4,
        TextureCoordinate = 5,
        Tangent = 6,
        Binormal = 7,
        TessellateFactor = 8,
        PositionTransformed = 9,
        Color = 10,
        Fog = 11,
        Depth = 12,
        Sample = 13,
    }


    /// <summary>
    /// Flag : Defines supported usage types for the current resource.
    /// </summary>
    [Flags]
    public enum Usage
    {
        None = 0,
        //RenderTarget = 1,
        //DepthStencil = 2,
        WriteOnly = 8,
        SoftwareProcessing = 16,
        //DoNotClip = 32,
        //Points = 64,
        //RTPatches = 128,
        //NPatches = 256,
        Dynamic = 512,
        //AutoGenerateMipMap = 1024,
        //QueryDisplacementMap = 16384,
        //QueryLegacyBumpMap = 32768,
        //QuerySrgbRead = 65536,
        //QueryFilter = 131072,
        //QuerySrgbWrite = 262144,
        //QueryPostPixelShaderBlending = 524288,
        //QueryVertexTexture = 1048576,
        //QueryWrapAndMip = 2097152,
    }
  
    /// <summary>
    /// Enum : Defines the memory class that holds buffers for a resource.
    /// </summary>
    public enum Pool
    {
        Default = 0,
        Managed = 1,
        SystemMemory = 2,
        //Scratch = 3,
    }
    public enum LightType
    {
        Point = 1,
        Spot = 2,
        Directional = 3,
    }

    #endregion

    [Flags]
    public enum BufferUsage : byte
    {
        /// <summary>
        /// </summary>
        Static = 1,

        /// <summary>
        ///		Indicates the application would like to modify this buffer with the CPU
        ///		sometimes. Absence of this flag means the application will never modify. 
        ///		Buffers created with this flag will typically end up in AGP memory rather 
        ///		than video memory.
        /// </summary>
        Dynamic = 2,

        /// <summary>
        ///		Indicates the application will never read the contents of the buffer back, 
        ///		it will only ever write data. Locking a buffer with this flag will ALWAYS 
        ///		return a pointer to new, blank memory rather than the memory associated 
        ///		with the contents of the buffer; this avoids DMA stalls because you can 
        ///		write to a new memory area while the previous one is being used
        /// </summary>
        WriteOnly = 4,

        /// <summary>
        ///     Indicates that the application will be refilling the contents
        ///     of the buffer regularly (not just updating, but generating the
        ///     contents from scratch), and therefore does not mind if the contents 
        ///     of the buffer are lost somehow and need to be recreated. This
        ///     allows and additional level of optimisation on the buffer.
        ///     This option only really makes sense when combined with 
        ///     DynamicWriteOnly.
        /// </summary>
        Discardable = 8,

        /// <summary>
        ///    Combination of Static and WriteOnly
        /// </summary>
        StaticWriteOnly = 5,

        /// <summary>
        ///    Combination of Dynamic and WriteOnly. If you use 
        ///    this, strongly consider using DynamicWriteOnlyDiscardable
        ///    instead if you update the entire contents of the buffer very 
        ///    regularly. 
        /// </summary>
        DynamicWriteOnly = 6,

        
    }
}
