﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Graphics
{
    /// <summary>
    /// A resources what can be restore after device reset
    /// </summary>
    public interface IRestore
    {
        void Restore();
    }
}
