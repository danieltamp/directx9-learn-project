﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Maths;


namespace Engine.Graphics
{
    /// <summary>
    /// Manage the buffer stream pointer get with buffer locking
    /// </summary>
    [DebuggerDisplay("{m_type.Name}, Count = {m_buffCount}")]
    public class BufferStream : IDisposable
    {
        public string name = "BufferStream";

        long m_offset_counter = 0; // use to increment offset when write singular value
        Type m_type;
        IntPtr m_bufferPtr;
        unsafe byte* p_buffer;
        Int64 m_buffSize; // long because value can be very big
        Int16 m_typeSize;  // usually a small value
        Int32 m_buffCount; // (m_buffSize / m_typeSize)

        bool fromarray;  // GCHandle.FromIntPtr issue

        /// <summary>
        /// The type of buffer's elements (uint, ushort , VERTEX, CVERTEX ....)
        /// </summary>
        public Type BufferType 
        { 
            get { return m_type; }
        }
        /// <summary>
        /// The number of buffer's elements as "BufferType"
        /// </summary>
        public int BufferLength 
        {
            get { return m_buffCount; }
        }

        /// <summary>
        /// If this class are delete, relase from garbace collection the pointer
        /// </summary>
        public void Dispose()
        {
            if (fromarray)
            {
                GCHandle pData = GCHandle.FromIntPtr(m_bufferPtr);
                pData.Free();
            }
            m_bufferPtr = IntPtr.Zero;
        }

        ~BufferStream()
        {
            Dispose();
        }

        /// <summary>
        /// Manage the stream from a managed array
        /// </summary>
        public BufferStream(Array array)
        {
            GCHandle pData = GCHandle.Alloc(array, GCHandleType.Pinned);
            this.m_bufferPtr = pData.AddrOfPinnedObject();
            this.m_buffSize = GetGCSize(pData);
            this.m_buffCount = array.GetUpperBound(0) + 1;
            this.m_type = array.GetType().GetElementType();
            this.m_typeSize = (Int16)Marshal.SizeOf(m_type);

            this.fromarray = true;

        }
        /// <summary>
        /// Manage the stream from pointer
        /// </summary>
        /// <param name="bufferPtr">buffer pointer</param>
        /// <param name="type">type of buffer elements</param>
        /// <param name="count">number of elements</param>
        public BufferStream(IntPtr bufferPtr, Type type, int count)
        {
            if (count < 1) throw new IndexOutOfRangeException("buffer size invalid, count must be > 0");

            unsafe
            {
                this.p_buffer = (byte*)bufferPtr.ToPointer();
            }
            this.m_bufferPtr = bufferPtr;
            this.m_type = type;
            this.m_typeSize = (Int16)Marshal.SizeOf(type);
            this.m_buffSize = this.m_typeSize * count;
            this.m_buffCount = count;
            this.fromarray = false;
        }
        /// <summary>
        /// Returns the number of bytes in a pinned object or 0 if not specified
        /// </summary>
        int GetGCSize(GCHandle data)
        {
            Object target = data.Target;

            // If obj was null, return 0 bytes
            if (target == null) return 0;

            // If obj is not an array, return the size of it
            if (!target.GetType().IsArray) return Marshal.SizeOf(target);

            // obj is an array, return the total size of the array
            Array a = (Array)target;
            return a.Length * Marshal.SizeOf(a.GetType().GetElementType());
        }


        /// <summary>
        /// Get or Set where need to start writeandadvance
        /// </summary>
        public long Offset
        {
            get { return m_offset_counter; }
            set { m_offset_counter = value; }
        }

        #region unsafe write value implementations

        public unsafe void Write(float f)
        {
            byte* pdest = (byte*)(p_buffer + m_offset_counter);
            *(float*)pdest = f;
            m_offset_counter += 4;
        }   
        public unsafe void Write(Vector3 v)
        {
            byte* pdest = (byte*)(p_buffer + m_offset_counter);
            *(Vector3*)pdest = v;
            m_offset_counter += 12;
        }
        public unsafe void Write(int i)
        {
            byte* pdest = (byte*)(p_buffer + m_offset_counter);
            *(int*)pdest = i;
            m_offset_counter += 4;
        }
        public unsafe void Write(CVERTEX v)
        {
            byte* pdest = (byte*)(p_buffer + m_offset_counter);
            *(CVERTEX*)pdest = v;
            m_offset_counter += sizeof(CVERTEX);
        }
        public unsafe void Write(NTVERTEX v)
        {
            byte* pdest = (byte*)(p_buffer + m_offset_counter);
            *(NTVERTEX*)pdest = v;
            m_offset_counter += sizeof(NTVERTEX);
        }
        #endregion


        #region unsafe write and read array implementations
        /// <summary>
        /// </summary>
        /// <param name="src">array to write</param>
        /// <param name="count">number of elements to write</param>
        /// <param name="offsetJump">size in byte between element[i] and element[i+1] to don't write in buffer</param>
        /// <param name="offset">position in byte where start writing in buffer</param>
        void Write(Array src, int count, int offsetJump, long offset)
        {
            Type elementType = src.GetType().GetElementType();
            Debug.Assert(elementType.IsValueType, "carefull, not a valid array element");
            int srcTypeSize = Marshal.SizeOf(elementType);

            // ensure we won't go past the end of the stream
            long writeSize = (long)(srcTypeSize + offsetJump) * count;

            if (offset + writeSize > m_buffSize)
                throw new IndexOutOfRangeException("pass the end of buffer");

            // pin the array so we can get a pointer to it
            GCHandle handle = GCHandle.Alloc(src, GCHandleType.Pinned);
            unsafe
            {
                // get byte pointers for the source and target
                byte* srcPtr = (byte*)handle.AddrOfPinnedObject().ToPointer();
                byte* trgPtr = (byte*)m_bufferPtr.ToPointer();

                long srcSize = (long)count * (long)srcTypeSize;

                // copy the data from the source to the target
                // is not use offsetJump avoid superfluos calculus 
                if (offsetJump > 0)
                {
                    for (long i = offset, j = 0; j < srcSize; i++, j++)
                    {
                        //Console.Write("[{0}] -> [{1}]\n", i, j);
                        trgPtr[i] = srcPtr[j];
                        if ((j + 1) % srcTypeSize == 0) i += offsetJump;
                    }
                }
                else
                {
                    for (long i = offset, j = 0; j < srcSize; i++, j++)
                    {
                        trgPtr[i] = srcPtr[j];
                    }
                }
            }
            handle.Free();
        }

        /// <summary>
        /// </summary>
        /// <param name="dst">array already initialized where data are filled</param>
        /// <param name="dstTypeSize">size in byte of elements to read, not relation with array's type</param>
        /// <param name="count">num of elements to read</param>
        /// <param name="offsetJump">size in byte between element[i] and element[i+1] to don't read from buffer</param>
        /// <param name="offset">position in byte where start reading from buffer</param>       
        void Read(ref Array dst, int dstTypeSize, int count, int offsetJump, long offset)
        {
            // ensure we won't go past the end of the stream
            long readSize = (long)(dstTypeSize + offsetJump) * count;
            if (offset + readSize > m_buffSize)
                throw new IndexOutOfRangeException("pass the end of buffer");


            // pin the array so we can get a pointer to it
            GCHandle destHandle = GCHandle.Alloc(dst, GCHandleType.Pinned);
            unsafe
            {
                // get byte pointers for the source and target
                byte* trgPtr = (byte*)destHandle.AddrOfPinnedObject().ToPointer();
                byte* srcPtr = (byte*)m_bufferPtr.ToPointer();

                long dstSize = (long)count * (long)dstTypeSize;

                // copy the data from the source to the target
                if (offsetJump > 0) // reduce the required operations
                    for (long i = offset, j = 0; j < dstSize; i++, j++)
                    {
                        trgPtr[j] = srcPtr[i];
                        if ((j + 1) % dstTypeSize == 0) i += offsetJump;
                    }
                else
                    for (long i = offset, j = 0; j < dstSize; i++, j++)
                    {
                        trgPtr[j] = srcPtr[i];
                    }
            }
            destHandle.Free();
        }
        #endregion

        /// <summary>
        /// Write an array of struct, size of array and buffer's elements must be the same
        /// </summary>
        /// <param name="src">Array</param>
        /// <param name="offsetIdx">buffer's element where start writing</param>
        /// <param name="count">num of array's element to write</param>
        public void Write(Array src, int offsetIdx)
        {
            int srcLength = src.GetUpperBound(0) + 1;
            Write(src, srcLength, 0, (long)offsetIdx * (long)m_typeSize);
        }

        /// <summary>
        /// Write an array of struct, size of array and buffer's elements must be the same
        /// </summary>
        /// <param name="src">Array</param>
        /// <param name="offsetIdx">buffer's element where start writing</param>
        /// <param name="count">num of array's element to write</param>
        public void Write(Array src, int offsetIdx, int count)
        {
            int srcLength = src.GetUpperBound(0) + 1;
            if (count < 0 || count > srcLength) count = srcLength;
            Write(src, count, 0, (long)offsetIdx * (long)m_typeSize);
        }

        /// <summary>
        /// Write an array of different struct, "srcElement" contain description for paste method 
        /// </summary>
        /// <remarks>
        /// srcElement.SizeInByte can be a multiple of src's elements size, example when src = float[3*2]
        /// you can use src like a Vector3[2], "count" must be 2 and not 3*2.
        /// </remarks>
        /// <param name="src">Array, no relation with array type and count</param>
        /// <param name="offsetIdx">buffer's element where start writing</param>
        /// <param name="srcElement">array's element descriptor</param>
        /// <param name="count">num of "srcElement" to write, not array's elements</param>
        public void Write(Array src, int offsetIdx, VertexElement srcElement, int count)
        {
            // get the original type size and type count of array
            int srcTypeSize = Marshal.SizeOf(src.GetType().GetElementType());
            int srcLength = src.GetUpperBound(0) + 1;
            int srcSize = srcLength * srcTypeSize;

            Debug.Assert(srcTypeSize <= m_typeSize, "Writing a array with elements greater than used in buffer have no sense");
            Debug.Assert(srcSize <= count * srcElement.SizeInByte, "Array not contain " + count + " elements with this elements descriptor");

            // need to sum also the srcElement.Offset value for a correct match
            long offset = (long)offsetIdx * (long)m_typeSize + srcElement.Offset;
            // calculate the byte to not read for each elements
            int offsetJump = m_typeSize - srcElement.SizeInByte;

            if (offsetJump < 0)
                throw new ArgumentException("srcElement.SizeInByte " + srcElement.SizeInByte + " is smaller than buffer's elements size and have no sense");


            Write(src, count, offsetJump, offset);
        }

        /// <summary>
        /// Read all buffer and return an array with same type used to create the buffer
        /// </summary>
        public Array Read(int offsetIdx, int count)
        {
            // array type == m_type
            // array type size == m_typeSize

            long offset = (long)offsetIdx * (long)m_typeSize;
            Array dest = Array.CreateInstance(m_type, count);

            Read(ref dest, m_typeSize, count, 0, offset);
            return dest;
        }
        /// <summary>
        /// Read from buffer a generic array using its type
        /// </summary>
        /// <param name="offsetIdx">num of buffer's elements where start reading</param>
        /// <param name="count">num of array's elements to read from buffer</param>
        public Array Read(Type type, int offsetIdx, int count)
        {
            Array dest = Array.CreateInstance(type, count);
            long offset = (long)offsetIdx * (long)m_typeSize;
            int destTypeSize = Marshal.SizeOf(type);

            Read(ref dest, destTypeSize, count, 0, offset);
            return dest;
        }

        /// <summary>
        /// Read an array with different type used for buffer, "dstElement" contain description to copy the buffers data
        /// </summary>
        /// <remarks>
        /// if Destination Element describe a Vector3 with count = 2 and returned array's type is "float", the reading
        /// method return a float[2*3] array.
        /// The returned array must contain continuos elements values, example can't return a vector3[] array
        /// with array[].x = NTVERTEX[].position.x ; array[].y = NTVERTEX[].normal.x ; array[].z = NTVERTEX[].uvw.x
        /// </remarks>
        /// <param name="type">type of result array's elements, can be different from vertex descriptor</param>
        /// <param name="offsetIdx">buffer's element where start reading</param>
        /// <param name="dstElement">array's element descriptor</param>
        /// <param name="count">num of "dstElement" to read, not array's elements, must be &lt;= this buffer count</param>
        public Array Read(Type type, int offsetIdx, int count, VertexElement dstElement)
        {
            if (count > this.m_buffCount)
                throw new ArgumentOutOfRangeException("num of dstElement must be <= buffer elements count");

            // the size of elements descriptor, 
            int dstTypeSize = dstElement.SizeInByte;

            // need to sum also the srcElement.Offset value to match start position with first array byte
            long offset = (long)offsetIdx * (long)m_typeSize + dstElement.Offset;

            // calculate the byte to not read when pass from element[i] to element[i+1]
            int offsetJump = m_typeSize - dstTypeSize;

            if (m_typeSize < dstTypeSize)
                throw new ArgumentException("dstElement.SizeInByte " + dstTypeSize + " is greater than buffer's elements size and have no sense because you extract a buffer elements");

            // the size of type used in array
            int typeSize = Marshal.SizeOf(type);

            // example reading float3 using vector3 is possible but don't have sense, but reading a vector3 using 3xfloat have sense 
            //Debug.Assert(dstTypeSize > typeSize , "dstElement.SizeInByte " + dstTypeSize + " is smaller than array type size and have no sense, must be equal or greater");

            // get the true length of returned array
            int newCount = (count * dstTypeSize) / typeSize;
            // add a new items but in this case will not be filled completly... some data will be zero
            if (dstTypeSize < typeSize) newCount++;

            if (newCount <= 0)
                throw new ArgumentException("");

            Array dest = Array.CreateInstance(type, newCount);

            Read(ref dest, dstTypeSize, count, offsetJump, offset);

            return dest;
        }
        /// <summary>
        /// Return in a matrix array the buffer's data split in elements
        /// </summary>
        public byte[,] StreamInMatrix()
        {
            byte[,] stream = new byte[m_buffCount, m_typeSize];
            unsafe
            {
                Debug.Assert(m_buffCount * m_typeSize != m_buffSize, "buffer size not correct");

                byte* bufferPtr = (byte*)m_bufferPtr.ToPointer();
                int i = 0;

                for (int n = 0; n < m_buffCount; n++)
                    for (int m = 0; m < m_typeSize; m++)
                        stream[n, m] = bufferPtr[i++];
            }
            return stream;
        }
        /// <summary>
        /// Return in a byte array the buffer's data
        /// </summary>
        public byte[] StreamInByte()
        {
            byte[] stream = new byte[m_buffSize];
            unsafe
            {
                byte* bufferPtr = (byte*)m_bufferPtr.ToPointer();
                for (int i = 0; i < m_buffSize; i++)
                    stream[i] = bufferPtr[i];
            }
            return stream;
        }

        public override string ToString()
        {
            return String.Format(name + "\nTotalSize {0}b\nTypeSize {1}b\nTypeCount {2}", m_buffSize, m_typeSize, m_buffCount);
        }
    }
}
