﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Graphics
{
    // Riepilogo:
    //     Defines the vertex data layout. Each vertex can contain one or more data
    //     types, and each data type is described by a vertex element.
    //
    // Note:
    //     Vertex data is defined using an array of SharpDX.Direct3D9.VertexElement
    //     structures. Use D3DDECL_END to declare the last element in the declaration.
    public struct VertexElement
    {
        public int SizeInByte;

        public DeclarationMethod Method;
        //
        // Riepilogo:
        //      Offset from the beginning of the vertex data to the data associated with
        //     the particular data type.
        public short Offset;
        //
        // Riepilogo:
        //      Stream number.
        public short Stream;
        //
        // Riepilogo:
        //      The data type, specified as a SharpDX.Direct3D9.DeclarationType. One of
        //     several predefined types that define the data size. Some methods have an
        //     implied type.
        public DeclarationType Type;
        //
        // Riepilogo:
        //      Defines what the data will be used for; that is, the interoperability between
        //     vertex data layouts and vertex shaders. Each usage acts to bind a vertex
        //     declaration to a vertex shader. In some cases, they have a special interpretation.
        //     For example, an element that specifies SharpDX.Direct3D9.DeclarationUsage.Normal
        //     or SharpDX.Direct3D9.DeclarationUsage.Position is used by the N-patch tessellator
        //     to set up tessellation. See SharpDX.Direct3D9.DeclarationUsage for a list
        //     of the available semantics. SharpDX.Direct3D9.DeclarationUsage.TextureCoordinate
        //     can be used for user-defined fields (which don't have an existing usage defined).
        public DeclarationUsage Usage;
        //
        // Riepilogo:
        //      Modifies the usage data to allow the user to specify multiple usage types.
        public byte UsageIndex;
        //
        // Riepilogo:
        //     Used for closing a VertexElement declaration.
        public static VertexElement VertexDeclarationEnd = new VertexElement(255, 0, DeclarationType.Unused, DeclarationMethod.Default, DeclarationUsage.Position, 0);

        public VertexElement(short stream, short offset, DeclarationType type, DeclarationMethod method, DeclarationUsage usage, byte usageIndex)
        {
            Method = method;
            Stream = stream;
            Offset = offset;
            Type = type;
            Usage = usage;
            UsageIndex = usageIndex;

            switch (type)
            {
                case DeclarationType.Color: SizeInByte = 4; break;
                case DeclarationType.Float1: SizeInByte = 4; break;
                case DeclarationType.Float2: SizeInByte = 8; break;
                case DeclarationType.Float3: SizeInByte = 12; break;
                case DeclarationType.Float4: SizeInByte = 16; break;
                case DeclarationType.HalfFour: SizeInByte = 8; break;
                case DeclarationType.HalfTwo: SizeInByte = 4; break;
                case DeclarationType.Ubyte4: SizeInByte = 4; break;
                case DeclarationType.Short2: SizeInByte = 4; break;
                case DeclarationType.Short4: SizeInByte = 8; break;
                default : SizeInByte = 0; break;
            }
        }
    }

    public struct CameraValues
    {
        public float nearZ;
        public float farZ;
        /// <summary>
        /// DEPRECATED : precalculated shader matrix
        /// </summary>
        public Matrix4 worldViewProj { get { return world * view * projection; } }
        public Matrix4 world { get; set; }
        public Matrix4 view { get; set; }
        /// <summary>
        /// Inverse of view, usefull for many calculation
        /// </summary>
        public Matrix4 inview { get; set; }
        public Matrix4 projection { get; set; }
        /// <summary>
        /// BackBufferSize
        /// </summary>
        public Viewport viewport { get; set; }
    }

    public struct Material
    {
        public Color Ambient { get; set; }
        public Color Diffuse { get; set; }
        public Color Emissive { get; set; }
        public Color Specular { get; set; }
        /// <summary>Power</summary>
        public float SpecularSharpness { get; set; }

        public static readonly Material Default = new Material
        {
            Ambient = Color.Black,
            Diffuse = Color.White,
            Emissive = Color.Black,
            Specular = Color.Black,
            SpecularSharpness = 0.0f,
        };

        public static bool operator ==(Material mat1, Material mat2)
        {
            return (mat1.Ambient == mat2.Ambient) &&
                   (mat1.Diffuse == mat2.Diffuse) &&
                   (mat1.Emissive == mat2.Emissive) &&
                   (mat1.Specular == mat2.Specular) &&
                   (mat1.SpecularSharpness == mat2.SpecularSharpness); 
        }
        public static bool operator !=(Material mat1, Material mat2)
        {
            return !(mat1 == mat2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
    public struct Light
    {
        public Color Ambient { get; set; }
        public Color Diffuse { get; set; }
        public Color Specular { get; set; }
        public Vector3 Direction { get; set; }
        public Vector3 Position { get; set; }
        public LightType Type { get; set; }
        public float Attenuation0 { get; set; }
        public float Attenuation1 { get; set; }
        public float Attenuation2 { get; set; }
        public float Falloff { get; set; }
        public float OuterConeAngle { get; set; }
        public float InnerConeAngle { get; set; }
        public float Range { get; set; }

        public static readonly Light Sun = new Light
        {
            Ambient = Color.Black,
            Diffuse = Color.White,
            Specular = Color.Black,
            Type = LightType.Directional,
            Direction = Vector3.GetNormal(new Vector3(-1, -1, -1)),
            Position = new Vector3(1000, 1000, 1000),
            Range = 1000.0f
        };
    }

    /// <summary>
    /// Defines the window dimensions of a render target surface onto which a 3D volume projects.
    /// </summary>
    public struct Viewport
    {
        //     +-----> x (Width)
        //     |
        //     |
        //     y (Height)

        /// <summary>
        /// </summary>
        /// <param name="width">dX length</param>
        /// <param name="height">dY length</param>
        /// <param name="x">horizontal min</param>
        /// <param name="y">vertical min</param>
        public Viewport(int width, int height, int x, int y)
        {
            Height = height;
            MaxDepth = 1;
            MinDepth = 0;
            Width = width;
            X = x;
            Y = y;
        }
        /// <summary>
        /// Retrieves or sets the height(vertical y) dimension of the viewport on the render target surface, in pixels.
        /// </summary>
        public int Height;
        /// <summary>
        ///  Retrieves or sets the maximum value of the clip volume.
        /// </summary>
        public float MaxDepth;
        /// <summary>
        /// Retrieves or sets the minimum value of the clip volume.
        /// </summary>
        public float MinDepth;
        /// <summary>
        /// Retrieves or sets the width(horizontal x) dimension of the viewport on the render target surface, in pixels.
        /// </summary>
        public int Width;
        /// <summary>
        /// Retrieves or sets the pixel coordinate of the upper-left corner of the viewport on the render target surface.
        /// </summary>
        public int X;
        /// <summary>
        /// Retrieves or sets the pixel coordinate of the upper-left corner of the viewport on the render target surface.
        /// </summary>
        public int Y;
    }
}
