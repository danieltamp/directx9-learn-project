﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Engine.Maths
{
    /// <summary>
    /// A 4 byte vector
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)] // pack 4 or 1... i don't know what is best
    public struct Byte4
    {
        public byte r, g, b, a;

        public Byte4(byte r, byte g, byte b, byte a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public Byte4(uint integer)
        {
            this.a = (byte)(integer >> 24);
            this.b = (byte)(integer >> 16);
            this.g = (byte)(integer >> 8);
            this.r = (byte)(integer);
        }

        public static readonly Byte4 Zero = new Byte4(0);

        public byte this[int index]
        {
            get
            {
                Debug.Assert(index >= 0 && index < 4, "Indexer boundaries overrun in Vector4.");
                // using pointer arithmetic here for less code.  Otherwise, we'd have a big switch statement.
#if !UNSAFE
                switch (index)
                {
                    case 0: return r;
                    case 1: return g;
                    case 2: return b;
                    case 3: return a;
                }
                return 0;
#else
				unsafe
				{
					fixed ( float* pX = &this.r )
					{
						return *( pX + index );
					}
				}
#endif
            }
            set
            {
                Debug.Assert(index >= 0 && index < 4, "Indexer boundaries overrun in Vector4.");

                // using pointer arithmetic here for less code.  Otherwise, we'd have a big switch statement.
#if !UNSAFE
                switch (index)
                {
                    case 0: r = value; break;
                    case 1: g = value; break;
                    case 2: b = value; break;
                    case 3: a = value; break;
                }
#else
				unsafe
				{
                    fixed (float* pX = &this.r)
					{
						*( pX + index ) = value;
					}
				}
#endif
            }
        }

        public static explicit operator uint(Byte4 bytes)
        {
            return (uint)(bytes.a << 24 + bytes.b << 16 + bytes.g << 8 + bytes.r);
        }
        public static explicit operator Color(Byte4 bytes)
        {
            return Color.FromArgb(bytes.a, bytes.r, bytes.g, bytes.b);
        }
        public override string ToString()
        {
            return String.Format("0x{0:H}{1:H}{2:H}{3:H}", r, g, b, a);
        }
    }
}
