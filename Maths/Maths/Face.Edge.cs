﻿using System;
using System.Text;
using System.Runtime.InteropServices;


namespace Engine.Maths
{
    public interface IIndexType
    {
        bool is32bit { get; }

    }

    /// <summary>
    /// 16 bit version
    /// The single DxNode use maximum ushort indices, a obj with 65535 vertices are big for my purpose, can be split in more DxNode
    /// Pack = 2 to minimize the struct sizeof
    /// </summary>
    [StructLayout(LayoutKind.Sequential , Pack = 2)]
    public struct Face : IIndexType
    {
        public ushort I;
        public ushort J;
        public ushort K;

        public Face(ushort i, ushort j, ushort k)
        {
            I = i;
            J = j;
            K = k;
        }
        public Face(int i, int j, int k) : this((ushort)i,(ushort)j , (ushort)k)
        {
            if (i < 0 || i > ushort.MaxValue || j < 0 || j > ushort.MaxValue || k < 0 || k > ushort.MaxValue)
                throw new ArgumentException("index must be in the range 0 : 65535");
        }
        public int Sizeof() { return sizeof(ushort) * 3; }

        public ushort this[int i]
        {
            get
            {
                switch (i)
                {
                    case 0: return I;
                    case 1: return J;
                    default: return K;
                }
            }
            set
            {
                switch (i)
                {
                    case 0: I = value; break;
                    case 1: J = value; break;
                    default: K = value; break;
                }
            }
        }
        
        ///<summary>
        /// Add a offset "i" to face
        /// </summary>
        public static Face operator +(Face f, int i)
        {
            return new Face(f.I + i, f.J + i, f.K + i);
        }
        /// <summary>
        /// 
        /// </summary>
        public static Face operator -(Face f, int i)
        {
            return new Face(f.I - i, f.J - i, f.K - i);
        }


        public bool is32bit { get { return false; } }

        public override string ToString()
        {
            return (String.Format("{0,3} {1,3} {2,3}", I, J, K));
        }

    }

    /// <summary>
    /// 32 bit version
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Face32 : IIndexType
    {
        public uint I;
        public uint J;
        public uint K;

        public Face32(uint i, uint j, uint k)
        {
            I = i;
            J = j;
            K = k;
        }
        public Face32(int i, int j, int k)
            : this((uint)i, (uint)j, (uint)k)
        {
            if (i < 0 || j < 0 || k < 0 )
                throw new ArgumentException("index must be not negative");
        }
        public int Sizeof() { return sizeof(uint) * 3; }

        public uint this[int i]
        {
            get
            {
                switch (i)
                {
                    case 0: return I;
                    case 1: return J;
                    default: return K;
                }
            }
            set
            {
                switch (i)
                {
                    case 0: I = value; break;
                    case 1: J = value; break;
                    default: K = value; break;
                }
            }
        }

        ///<summary>
        /// Add a offset "i" to face
        /// </summary>
        public static Face32 operator +(Face32 f, uint i)
        {
            return new Face32(f.I + i, f.J + i, f.K + i);
        }
        /// <summary>
        /// 
        /// </summary>
        public static Face32 operator -(Face32 f, uint i)
        {
            return new Face32(f.I - i, f.J - i, f.K - i);
        }


        public bool is32bit { get { return true; } }

        public override string ToString()
        {
            return (String.Format("{0,3} {1,3} {2,3}", I, J, K));
        }

    }

    /// <summary>
    /// 16 bit version
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct Edge : IIndexType
    {
        public ushort I;
        public ushort J;
        public Edge(ushort i, ushort j)
        {
            I = i;
            J = j;
        }
        public Edge(int i, int j) : this((ushort)i, (ushort)j)
        {
            if (i < 0 || i > ushort.MaxValue || j < 0 || j > ushort.MaxValue)
                throw new ArgumentException("index must be in the range 0 : 65535");
        }
        public int Sizeof() { return sizeof(ushort) * 2; }

        public ushort this[int i]
        {
            get { return i > 0 ? J : I; }
            set { if (i > 0) J = value; else I = value; }
        }

        ///<summary>
        /// Add a offset "i" to edge
        ///</summary>
        public static Edge operator +(Edge e, int i)
        {
            return new Edge(e.I + i, e.J + i);
        }
        /// <summary>
        /// 
        /// </summary>
        public static Edge operator -(Edge e, int i)
        {
            return new Edge(e.I - i, e.J - i);
        }

        public bool is32bit { get { return false; } }

        public override string ToString()
        {
            return (String.Format("{0,3} {1,3} ", I, J));
        }
    }

    /// <summary>
    /// 16 bit version
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Edge32 : IIndexType
    {
        public uint I;
        public uint J;
        public Edge32(uint i, uint j)
        {
            I = i;
            J = j;
        }
        public Edge32(int i, int j)
            : this((uint)i, (uint)j)
        {
            if (i < 0  || j < 0 )
                throw new ArgumentException("index must be not negative");
        }
        public int Sizeof() { return sizeof(uint) * 2; }

        public uint this[int i]
        {
            get { return i > 0 ? J : I; }
            set { if (i > 0) J = value; else I = value; }
        }

        ///<summary>
        /// Add a offset "i" to edge
        ///</summary>
        public static Edge32 operator +(Edge32 e, uint i)
        {
            return new Edge32(e.I + i, e.J + i);
        }
        /// <summary>
        /// </summary>
        public static Edge32 operator -(Edge32 e, uint i)
        {
            return new Edge32(e.I - i, e.J - i);
        }

        public bool is32bit { get { return true; } }

        public override string ToString()
        {
            return (String.Format("{0,3} {1,3} ", I, J));
        }
    }
}
