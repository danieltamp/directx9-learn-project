﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;


namespace Engine.Maths
{
    /// <summary>
    /// Half Vector4
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct Vector4x16
    {
        Half m_x;
        Half m_y;
        Half m_z;
        Half m_w;

        public float x { get { return (float)m_x; } set { m_x = new Half(value); } }
        public float y { get { return (float)m_y; } set { m_y = new Half(value); } }
        public float z { get { return (float)m_z; } set { m_z = new Half(value); } }
        public float w { get { return (float)m_w; } set { m_w = new Half(value); } }


        /// <summary>
        /// </summary>
        public Vector4x16(double X, double Y, double Z,double W)
        {
            this.m_x = new Half(X);
            this.m_y = new Half(Y);
            this.m_z = new Half(Z);
            this.m_w = new Half(W);
        }
        /// <summary>
        /// Passing double or float value need a normalization
        /// </summary>
        public Vector4x16(float X, float Y, float Z,float W)
        {
            this.m_x = new Half(X);
            this.m_y = new Half(Y);
            this.m_z = new Half(Z);
            this.m_w = new Half(W);
        }
        public float this[int i]
        {
#if UNSAFE
            get
            {
                unsafe
                {
                    fixed (short* pX = &this.x)
                    {
                        return *(pX + i);
                    }
                }
            }
             
            set
            {
                unsafe
                {
                    fixed (short* pX = &this.x)
                    {
                        *(pX + i) = value;
                    }
                }
            }
#else
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    case 3: return w;
                    default: throw new ArgumentException("i must be 0,1,2,3");
                }
            }
            set
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    case 3: w = value; break;
                    default: throw new ArgumentException("i must be 0,1,2,3");
                }
            }
#endif
        }

        public static readonly int sizeinbyte = sizeof(ushort) * 4;

        public override string ToString()
        {
            return (String.Format("<{0,4};{1,4};{2,4};{3,4}>", x, y, z,w));
        }

        public static readonly Vector4x16 Zero = new Vector4x16 { m_x = 0, m_y = 0, m_z = 0 ,m_w = 0};
        public static readonly Vector4x16 UnitX = new Vector4x16 { m_x = new Half(1.0f), m_y = 0, m_z = 0, m_w = new Half(1.0f) };
        public static readonly Vector4x16 UnitY = new Vector4x16 { m_x = 0, m_y = new Half(1.0f), m_z = 0, m_w = new Half(1.0f) };
        public static readonly Vector4x16 UnitZ = new Vector4x16 { m_x = 0, m_y = 0, m_z = new Half(1.0f), m_w = new Half(1.0f) };

        /// <summary>
        /// Transform the vector using matrix (= transform * vector), the product are from normal vector and rotation part of transform
        /// </summary>
        public static Vector4x16 TransformCoordinate(Vector4x16 point, Matrix4 transform)
        {
            Vector3 vector = (Vector3)point;
            float inverseW = 1.0f / (transform.m30 * vector.x + transform.m31 * vector.y + transform.m32 * vector.z + transform.m33);
            float xf = ((transform.m00 * vector.x) + (transform.m01 * vector.y) + (transform.m02 * vector.z) + transform.m03) * inverseW;
            float yf = ((transform.m10 * vector.x) + (transform.m11 * vector.y) + (transform.m12 * vector.z) + transform.m13) * inverseW;
            float zf = ((transform.m20 * vector.x) + (transform.m21 * vector.y) + (transform.m22 * vector.z) + transform.m23) * inverseW;

            return (Vector4x16)vector;

        }
        public static Vector4x16 TransformCoordinate(Vector4x16 point, Matrix3 rotation)
        {
            Vector3 vector = (Vector3)point;
            float xf = rotation.m00 * vector.x + rotation.m01 * vector.y + rotation.m02 * vector.z;
            float yf = rotation.m10 * vector.x + rotation.m11 * vector.y + rotation.m12 * vector.z;
            float zf = rotation.m20 * vector.x + rotation.m21 * vector.y + rotation.m22 * vector.z;

            return (Vector4x16)vector;
        }

        public static implicit operator Vector3(Vector4x16 vector)
        {
            return new Vector3(vector.x, vector.y, vector.z);
        }
        public static implicit operator Vector4(Vector4x16 vector)
        {
            return new Vector4(vector.x, vector.y, vector.z, vector.w);
        }
        public static implicit operator Vector4x16(Vector3 vector)
        {
            return new Vector4x16(vector.x, vector.y, vector.z, 1f);
        }
    }



    /// <summary>
    /// Half Vector2
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct Vector2x16
    {
        Half m_x;
        Half m_y;

        public float x { get { return (float)m_x; } set { m_x = new Half(value); } }
        public float y { get { return (float)m_y; } set { m_y = new Half(value); } }

        /// <summary>
        /// Passing double or float value need a normalization
        /// </summary>
        public Vector2x16(double x, double y)
        {
            this.m_x = new Half(x);
            this.m_y = new Half(y);
        }
        /// <summary>
        /// Passing double or float value need a normalization
        /// </summary>
        public Vector2x16(float x, float y)
        {
            this.m_x = new Half(x);
            this.m_y = new Half(y);
        }
        public float this[int i]
        {
#if UNSAFE
            get
            {
                unsafe
                {
                    fixed (short* pX = &this.x)
                    {
                        return *(pX + i);
                    }
                }
            }
             
            set
            {
                unsafe
                {
                    fixed (short* pX = &this.x)
                    {
                        *(pX + i) = value;
                    }
                }
            }
#else
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    default: throw new ArgumentException("i must be 0,1");
                }
            }
            set
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    default: throw new ArgumentException("i must be 0,1");
                }
            }
#endif
        }

        public static readonly int sizeinbyte = sizeof(ushort) * 3;

        public override string ToString()
        {
            return (String.Format("<{0,4};{1,4}>", x, y));
        }

        public static readonly Vector2x16 Zero = new Vector2x16 { m_x = 0, m_y = 0 };
        public static readonly Vector2x16 UnitX = new Vector2x16 { m_x = new Half(1.0f), m_y = 0 };
        public static readonly Vector2x16 UnitY = new Vector2x16 { m_x = 0, m_y = new Half(1.0f) };

        public static implicit operator Vector2(Vector2x16 vector)
        {
            return new Vector2(vector.x, vector.y);
        }
        public static implicit operator Vector2x16(Vector2 vector)
        {
            return new Vector2x16(vector.x, vector.y);
        }
    }
}
