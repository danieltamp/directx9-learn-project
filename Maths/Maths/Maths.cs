﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Maths
{
    public static class MathUtils
    {
        public static int[] powersOfTwo = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };

        /// <summary>
        /// </summary>
        /// <param name="Degree"></param>
        /// <returns>Radians</returns>
        public static float DegreeToRadian(float Degree)
        {
            return (float)(Math.PI * Degree/ 180.0);
        }
        /// <summary>
        /// </summary>
        /// <param name="Radian"></param>
        /// <returns>Degree</returns>
        public static float RadianToDegree(float Radian)
        {
            return (float)(180.0 * Radian / Math.PI );
        }
        /// <summary>
        /// Linearly interpolates between two values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="amount">amount = 0 return value1 , amount = 1 return value2</param>
        /// <returns></returns>
        public static float Lerp(float value1, float value2, float amount)
        {
            return value1 + (value2 - value1) * amount;
        }
        /// <summary>
        /// Is power or 2 ?
        /// </summary>
        public static bool IsPowOf2(int value)
        {
            if (value < 1) return false;
            return (value & (value - 1)) == 0;
        }
    }
}
