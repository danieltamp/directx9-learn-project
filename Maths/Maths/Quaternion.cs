﻿/*
Axiom Graphics Engine Library
Copyright � 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Engine.Maths
{
    [StructLayout(LayoutKind.Sequential , Pack = 4)]
    public struct Quaternion
    {
        public float w, x, y, z;
        
        const float EPSILON = 1e-03f; // not imfluence the Marshal.SizeOf(Quaternion) = 16

        public Quaternion(float x, float y, float z, float w)
        {
            this.w = w;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static readonly Quaternion Identity = new Quaternion(1, 0, 0, 0);
        public static readonly Quaternion Zero = new Quaternion(0, 0, 0, 0);
        private static readonly int[] next = new int[3] { 1, 2, 0 };

        /// <summary>
        /// Used to multiply 2 Quaternions together.
        /// </summary>
        /// <remarks>
        ///		Quaternion multiplication is not communative in most cases.
        ///		i.e. p*q != q*p
        /// </remarks>
        public static Quaternion operator *(Quaternion left, Quaternion right)
        {
            var q = new Quaternion();

            q.w = left.w * right.w - left.x * right.x - left.y * right.y - left.z * right.z;
            q.x = left.w * right.x + left.x * right.w + left.y * right.z - left.z * right.y;
            q.y = left.w * right.y + left.y * right.w + left.z * right.x - left.x * right.z;
            q.z = left.w * right.z + left.z * right.w + left.x * right.y - left.y * right.x;

            return q;
        }

        public static Vector3 operator *(Quaternion quat, Vector3 vector)
        {
            // nVidia SDK implementation
            Vector3 uv, uuv;
            var qvec = new Vector3(quat.x, quat.y, quat.z);

            uv = Vector3.Cross(qvec, vector);
            uuv = Vector3.Cross(qvec, uv);
            uv *= (2.0f * quat.w);
            uuv *= 2.0f;

            return vector + uv + uuv;

            // get the rotation matrix of the Quaternion and multiply it times the vector
            //return quat.ToRotationMatrix() * vector;
        }

        /// <summary>
        /// Used when a Real value is multiplied by a Quaternion.
        /// </summary>
        public static Quaternion operator *(float scalar, Quaternion right)
        {
            return new Quaternion(scalar * right.w, scalar * right.x, scalar * right.y, scalar * right.z);
        }
        /// <summary>
        /// Used when a Quaternion is multiplied by a Real value.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="scalar"></param>
        /// <returns></returns>
        public static Quaternion operator *(Quaternion left, float scalar)
        {
            return (scalar * left);
        }
        public static Quaternion operator +(Quaternion left, Quaternion right)
        {
            return new Quaternion(left.w + right.w, left.x + right.x, left.y + right.y, left.z + right.z);
        }

        public static Quaternion operator -(Quaternion left, Quaternion right)
        {
            return new Quaternion(left.w - right.w, left.x - right.x, left.y - right.y, left.z - right.z);
        }
        public static Quaternion operator -(Quaternion right)
        {
            return new Quaternion(-right.w, -right.x, -right.y, -right.z);
        }

        /// <summary>
        ///		Squared 'length' of this quaternion.
        /// </summary>
        public float LengthSq { get { return this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w; } }

        /// <summary>
        ///    Local X-axis portion of this rotation.
        /// </summary>
        public Vector3 XAxis
        {
            get
            {
                var fTx = 2.0f * this.x;
                var fTy = 2.0f * this.y;
                var fTz = 2.0f * this.z;
                var fTwy = fTy * this.w;
                var fTwz = fTz * this.w;
                var fTxy = fTy * this.x;
                var fTxz = fTz * this.x;
                var fTyy = fTy * this.y;
                var fTzz = fTz * this.z;

                return new Vector3(1.0f - (fTyy + fTzz), fTxy + fTwz, fTxz - fTwy);
            }
        }

        /// <summary>
        ///    Local Y-axis portion of this rotation.
        /// </summary>
        public Vector3 YAxis
        {
            get
            {
                var fTx = 2.0f * this.x;
                var fTy = 2.0f * this.y;
                var fTz = 2.0f * this.z;
                var fTwx = fTx * this.w;
                var fTwz = fTz * this.w;
                var fTxx = fTx * this.x;
                var fTxy = fTy * this.x;
                var fTyz = fTz * this.y;
                var fTzz = fTz * this.z;

                return new Vector3(fTxy - fTwz, 1.0f - (fTxx + fTzz), fTyz + fTwx);
            }
        }

        /// <summary>
        ///    Local Z-axis portion of this rotation.
        /// </summary>
        public Vector3 ZAxis
        {
            get
            {
                var fTx = 2.0f * this.x;
                var fTy = 2.0f * this.y;
                var fTz = 2.0f * this.z;
                var fTwx = fTx * this.w;
                var fTwy = fTy * this.w;
                var fTxx = fTx * this.x;
                var fTxz = fTz * this.x;
                var fTyy = fTy * this.y;
                var fTyz = fTz * this.y;

                return new Vector3(fTxz + fTwy, fTyz - fTwx, 1.0f - (fTxx + fTyy));
            }
        }
        /// <summary>
        /// Creates a Quaternion from a supplied angle and axis.
        /// </summary>
        /// <param name="angle">Value of an angle in radians.</param>
        /// <param name="axis">Arbitrary axis vector.</param>
        /// <returns></returns>
        public static Quaternion FromAngleAxis(float angle, Vector3 axis)
        {
            Quaternion quat = new Quaternion();

            float halfAngle = 0.5f * angle;
            float sin = (float)Math.Sin(halfAngle);

            quat.w = (float)Math.Cos(halfAngle);
            quat.x = sin * axis.x;
            quat.y = sin * axis.y;
            quat.z = sin * axis.z;

            return quat;
        }

        public Vector3 ToEulerAngles()
        {
            float pitch, yaw, roll;
            ToEulerAngles(out pitch, out yaw, out roll);
            return new Vector3(pitch, yaw, roll);
        }

        public void ToEulerAngles(out float pitch, out float yaw, out float roll)
        {
            float halfPi = (float)Math.PI / 2;
            float test = this.x * this.y + this.z * this.w;
            
            if (test > 0.499f)
            {
                // singularity at north pole
                yaw = 2 * (float)Math.Atan2(this.x, this.w);
                roll = halfPi;
                pitch = 0;
            }
            else if (test < -0.499f)
            {
                // singularity at south pole
                yaw = -2 * (float)Math.Atan2(this.x, this.w);
                roll = -halfPi;
                pitch = 0;
            }
            else
            {
                float sqx = this.x * this.x;
                float sqy = this.y * this.y;
                float sqz = this.z * this.z;
                yaw = (float)Math.Atan2(2 * this.y * this.w - 2 * this.x * this.z, 1 - 2 * sqy - 2 * sqz);
                roll = (float)Math.Asin(2 * test);
                pitch = (float)Math.Atan2(2 * this.x * this.w - 2 * this.y * this.z, 1 - 2 * sqx - 2 * sqz);
            }

            if (pitch <= float.Epsilon)
            {
                pitch = 0f;
            }
            if (yaw <= float.Epsilon)
            {
                yaw = 0f;
            }
            if (roll <= float.Epsilon)
            {
                roll = 0f;
            }
        }

        /// <summary>
        /// Combines the euler angles in the order yaw, pitch, roll to create a rotation quaternion
        /// </summary>
        /// <param name="pitch"></param>
        /// <param name="yaw"></param>
        /// <param name="roll"></param>
        /// <returns></returns>
        public static Quaternion FromEulerAngles(float pitch, float yaw, float roll)
        {
            return Quaternion.FromAngleAxis(yaw, Vector3.UnitY) *
                   Quaternion.FromAngleAxis(pitch, Vector3.UnitX) *
                   Quaternion.FromAngleAxis(roll, Vector3.UnitZ);

            /*TODO: Debug
            //Equation from http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm
            //heading
			
            Real c1 = (Real)Math.Cos(yaw/2);
            Real s1 = (Real)Math.Sin(yaw/2);
            //attitude
            Real c2 = (Real)Math.Cos(roll/2);
            Real s2 = (Real)Math.Sin(roll/2);
            //bank
            Real c3 = (Real)Math.Cos(pitch/2);
            Real s3 = (Real)Math.Sin(pitch/2);
            Real c1c2 = c1*c2;
            Real s1s2 = s1*s2;

            Real w =c1c2*c3 - s1s2*s3;
            Real x =c1c2*s3 + s1s2*c3;
            Real y =s1*c2*c3 + c1*s2*s3;
            Real z =c1*s2*c3 - s1*c2*s3;
            return new Quaternion(w,x,y,z);*/
        }
        /// <summary>
        /// Performs a Dot Product operation on 2 Quaternions.
        /// </summary>
        public float Dot(Quaternion quat)
        {
            return this.w * quat.w + this.x * quat.x + this.y * quat.y + this.z * quat.z;
        }
        /// <summary>
        ///		Normalizes elements of this quaterion to the range [0,1].
        /// </summary>
        public void Normalize()
        {
            float factor = 1.0f / (float)Math.Sqrt(LengthSq);

            this.w = this.w * factor;
            this.x = this.x * factor;
            this.y = this.y * factor;
            this.z = this.z * factor;
        }
        /// <summary>
        /// Gets a 3x3 rotation matrix from this Quaternion.
        /// </summary>
        /// <returns></returns>
        public Matrix3 ToRotationMatrix()
        {
            Matrix3 rotation = new Matrix3();

            var tx = 2.0f * this.x;
            var ty = 2.0f * this.y;
            var tz = 2.0f * this.z;
            var twx = tx * this.w;
            var twy = ty * this.w;
            var twz = tz * this.w;
            var txx = tx * this.x;
            var txy = ty * this.x;
            var txz = tz * this.x;
            var tyy = ty * this.y;
            var tyz = tz * this.y;
            var tzz = tz * this.z;

            rotation.m00 = 1.0f - (tyy + tzz);
            rotation.m01 = txy - twz;
            rotation.m02 = txz + twy;
            rotation.m10 = txy + twz;
            rotation.m11 = 1.0f - (txx + tzz);
            rotation.m12 = tyz - twx;
            rotation.m20 = txz - twy;
            rotation.m21 = tyz + twx;
            rotation.m22 = 1.0f - (txx + tyy);

            return rotation;
        }

        public static Quaternion FromRotationMatrix(Matrix3 matrix)
        {
            // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
            // article "Quaternion Calculus and Fast Animation".

            Quaternion result = Quaternion.Zero;

            float trace = matrix.m00 + matrix.m11 + matrix.m22;

            float root = 0.0f;

            if (trace > 0.0f)
            {
                // |this.w| > 1/2, may as well choose this.w > 1/2
                root = (float)Math.Sqrt(trace + 1.0f); // 2w
                result.w = 0.5f * root;

                root = 0.5f / root; // 1/(4w)

                result.x = (matrix.m21 - matrix.m12) * root;
                result.y = (matrix.m02 - matrix.m20) * root;
                result.z = (matrix.m10 - matrix.m01) * root;
            }
            else
            {
                // |result.w| <= 1/2

                int i = 0;
                if (matrix.m11 > matrix.m00)
                {
                    i = 1;
                }
                if (matrix.m22 > matrix[i, i])
                {
                    i = 2;
                }

                int j = next[i];
                int k = next[j];

                root = (float)Math.Sqrt(matrix[i, i] - matrix[j, j] - matrix[k, k] + 1.0f);

#if !UNSAFE
                float pi = 0.5f * root;
                root = 0.5f / root;                                                                                                       
                float pw = (matrix[k, j] - matrix[j, k]) * root;
                float pj = (matrix[j, i] + matrix[i, j]) * root;
                float pk = (matrix[k, i] + matrix[i, k]) * root;
			    result = i == 0
			                 ? new Quaternion(pw, pi, pj, pk)
			                 : i == 1
			                       ? new Quaternion(pw, pk, pi, pj)
			                       : new Quaternion(pw, pj, pk, pi);
#else
                unsafe
                {
                    float* apkQuat = &result.x;

                    apkQuat[i] = 0.5f * root;
                    root = 0.5f / root;

                    result.w = (matrix[k, j] - matrix[j, k]) * root;

                    apkQuat[j] = (matrix[j, i] + matrix[i, j]) * root;
                    apkQuat[k] = (matrix[k, i] + matrix[i, k]) * root;
                }
#endif
            }

            return result;
        }
        
        
        /// <summary>
        /// Computes the inverse of a Quaternion.
        /// </summary>
        /// <returns></returns>
        public Quaternion Inverse()
        {
            var norm = this.w * this.w + this.x * this.x + this.y * this.y + this.z * this.z;
            if (norm > 0.0f)
            {
                var inverseNorm = 1.0f / norm;
                return new Quaternion(this.w * inverseNorm, -this.x * inverseNorm, -this.y * inverseNorm, -this.z * inverseNorm);
            }
            else
            {
                // return an invalid result to flag the error
                return new Quaternion(0, 0, 0, 0);
            }
        }
    }
}