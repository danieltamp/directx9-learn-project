﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Graphics;

namespace Engine.Maths
{
    /// <summary>
    /// Equivalent to a Line in 3d
    /// </summary>
    public struct Ray
    {
        // invdir are a precalculated vector to reduce the math operations
        public Vector3 orig, dir, invdir;
        /// <summary>
        /// true for positive
        /// </summary>
        public bool signx, signy, signz;

        /// <summary>
        /// Direction will be normalized for safety
        /// </summary>
        public Ray(Vector3 Origin, Vector3 Direction)
        {
            orig = Origin;
            dir = Direction;
            float length = dir.Normalize();

            Debug.Assert(length > float.Epsilon, "invalid direction normal");

            invdir = new Vector3(1.0f / dir.x, 1.0f / dir.y, 1.0f / dir.z);
            signx = dir.x < 0;
            signy = dir.y < 0;
            signz = dir.z < 0;
        }
        /// <summary>
        /// TODO : Get ray from screen picking
        /// </summary>
        /// <param name="screen">the z value is the nearz plane</param>
        public Ray(Vector3 screen , Viewport viewport, Matrix4 proj, Matrix4 inv_view)
        {
            throw new NotImplementedException();

            /*
            Vector3 v = new Vector3(
                (screen.x * 2 / viewport.Width - 1) * proj.m00,
                -(screen.y * 2 / viewport.Height - 1) * proj.m01,
                1);

            dir = new Vector3(
                v.x * inv_view.m00 + v.y * inv_view.m01 + v.z * inv_view.m02,
                v.x * inv_view.m10 + v.y * inv_view.m11 + v.z * inv_view.m12,
                v.x * inv_view.m20 + v.y * inv_view.m21 + v.z * inv_view.m22);

            float length = dir.Normalize();

            Debug.Assert(length > float.Epsilon, "invalid direction normal");

            orig = new Vector3(inv_view.m03, inv_view.m13, inv_view.m23);

            orig += dir * screen.z;

            invdir = new Vector3(1.0f / dir.x, 1.0f / dir.y, 1.0f / dir.z);

            signx = dir.x < 0;
            signy = dir.y < 0;
            signz = dir.z < 0;
            */
        }
        /// <summary>
        /// If ray isn't initialized or empty the direction length is Zero and invdir can't be used (!DIV0)
        /// </summary>
        public bool IsEmpty
        {
            get { return (dir.isNaN || dir.LengthSq < float.Epsilon); }
        }
        /// <summary>
        /// Return a empty array not usable
        /// </summary>
        public static Ray Empty
        {
            get
            {
                Ray ray = new Ray();
                ray.orig = Vector3.NaN;
                ray.dir = Vector3.NaN;
                ray.invdir = Vector3.NaN;
                return ray;
            }

        }
        /// <summary>
        /// Get the function Ray(t)
        /// </summary>
        public Vector3 this[float t] { get { return orig + dir * t; } }
        /// <summary>
        /// Convert ray into different coordinate system
        /// </summary>
        public static Ray TransformCoordinate(Ray ray, Matrix4 coordsys)
        {
            // simply coordsys * orig
            Vector3 P0 = Vector3.TransformCoordinate(ray.orig, coordsys);
            // direction isn't a point but a vector
            Vector3 P1 = Vector3.TransformCoordinate(ray.orig + ray.dir, coordsys);
            return new Ray(P0, P1 - P0);
        }
        /// <summary>
        /// CounterClockWire triangle test , out Intersection can also be calculated with Ray(t)
        /// </summary>
        public bool IntersectTriangle(Vector3 P0, Vector3 P1, Vector3 P2, bool CullTest, out float t, out Vector3 intersection)
        {
            intersection = Vector3.Zero;
            t = 0;

            /* find vectors for two edges sharing vert */
            Vector3 e1 = P1 - P0;
            Vector3 e2 = P2 - P0;

            /* begin calculating determinant, also used to calculate U parameter */
            Vector3 pvec = Vector3.Cross(this.dir, e2);

            /* if determinant is near zero, ray lies in plane of triangle */
            float det = Vector3.Dot(e1, pvec);

            /* define TEST_CULL if culling is desired */
            if (det < float.Epsilon) // (det < Eps || det > -E) for cullmode.none
            {
                //not intersect
            }
            else
            {
                /* calculate distance from vert0 to ray origin */
                Vector3 tvec = this.orig - P0;

                /* calculate U parameter and test bounds */
                float u = Vector3.Dot(tvec, pvec);
                if (u < 0 || u > det)
                {
                    //not intersect
                }
                else
                {
                    /* prepare to test V parameter */
                    Vector3 qvec = Vector3.Cross(tvec, e1);

                    /* calculate V parameter and test bounds */
                    float v = Vector3.Dot(this.dir, qvec);
                    if (v < 0 || u + v > det)
                    {
                        //not intersect
                    }
                    else
                    {
                        /* calculate t, scale parameters, ray intersects triangle */
                        t = Vector3.Dot(e2, qvec);
                        t *= 1.0f / det;
                        u *= 1.0f / det;
                        v *= 1.0f / det;

                        intersection = (1 - u - v) * P0 + u * P1 + v * P2;
                        return true;
                    }
                }
            }
            return false;
        }
        
        #region Currently Not Used
        /// <summary>
        /// The max min values are for Axis Aligned Bounding Box (global coordinated system)
        /// </summary>
        public bool IntersectAABB(Vector3 max, Vector3 min, out float t)
        {
            return BoundaryAABB.BoundaryIntersectRay(max, min, this, out t);
        }
        /// <summary>
        /// The max min values are for Oriented Bounding Box (local coordinated system)
        /// </summary>
        public bool IntersectOBB(Vector3 max, Vector3 min, Matrix4 transform , out float t)
        {
            throw new NotImplementedException("Coming soon");
        }
        /// <summary>
        /// Fast and basic Ray-Sphere intersection
        /// </summary>
        public bool IntersectSphere(Vector3 center , float radius)
        {
            return BoundarySphere.BoundaryIntersectRay(center, radius, this );
        }
        /// <summary>
        /// Restur also the parametric values t of ray at the "first" intesection point
        /// </summary>
        public bool IntersectSphere(Vector3 center, float radius, out float t)
        {
            t = 0;
            if (radius < 0) return false;
            
            float t0, t1;
            bool test = BoundarySphere.BoundaryIntersectRay(center, radius, this, out t0, out t1);

            // make sure t0 is smaller than t1
            if (t0 > t1)
            {
                // if t0 is bigger than t1 swap them around
                float temp = t0;
                t0 = t1;
                t1 = temp;
            }

            // if t1 is less than zero, the object is in the ray's negative direction
            // and consequently the ray misses the sphere
            if (t1 < 0)
                return false;

            // if t0 is less than zero, the intersection point is at t1
            t = t0 < 0 ? t1 : t0;

            return true;
        }
        /// <summary>
        /// can't do a perfect selection with mouse, so use a EPSILON error
        /// </summary>
        public bool IntersectSegment(Vector3 P0, Vector3 P1, float EPSILON , out float t)
        {
            t = 0;
            Vector3 u = P0 - P1;
            u.Normalize();
            Vector3 cross = Vector3.Cross(u, dir);
            float mindist = Math.Abs(Vector3.Dot(cross, orig - P0) / cross.Length);
            return mindist < EPSILON;

            /*
            Vector3 u = this.dir;
            Vector3 v = P1 - P0;
            Vector3 w = this.orig - P0;
            float a = Vector3.Dot(u, u);         // always >= 0
            float b = Vector3.Dot(u, v);
            float c = Vector3.Dot(v, v);         // always >= 0
            float d = Vector3.Dot(u, w);
            float e = Vector3.Dot(v, w);
            float D = a * c - b * b;        // always >= 0
            float sc, tc;

            // compute the line parameters of the two closest points
            if (D < EPSILON)
            {
                // the lines are almost parallel
                sc = 0.0f;
                tc = (b > c ? d / b : e / c);    // use the largest denominator
            }
            else
            {
                sc = (b * e - c * d) / D;
                tc = (a * e - b * d) / D;
            }

            // get the difference of the two closest points
            Vector3 dP = w + (sc * u) - (tc * v);  // =  L1(sc) - L2(tc)
            float mindist = dP.LengthSq();// return the closest distance^2

            return mindist < EPSILON * EPSILON;
            */
        }
        #endregion

        public override string ToString()
        {
            return "{" + orig.x + ";" + orig.y + ";" + orig.z + "} {" + dir.x + ";" + dir.y + ";" + dir.z + "}";
        }
    }
}
