﻿#region LGPL License

/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#endregion

using System;
using System.Text;
using System.Runtime.InteropServices;



namespace Engine.Maths
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Vector2
    {
        public float x, y;

        public float u
        {
            get { return x; }
            set { x = value; }
        }
        public float v
        {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        /// In a texture notation : u = x , v = y 
        /// </summary>
        public Vector2(float u, float v)
        {
            this.x = u;
            this.y = v;
        }

        public float this[int i]
        {
#if UNSAFE
            get
            {
                unsafe
                {
                    fixed (float* pX = &this.u)
                    {
                        return *(pX + i);
                    }
                }
            }
             
            set
            {
                unsafe
                {
                    fixed (float* pX = &this.u)
                    {
                        *(pX + i) = value;
                    }
                }
            }
#else
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
            set 
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    default: throw new ArgumentException("i must be 0,1");
                }   
            }
#endif
        }
        
        public static readonly int sizeinbyte = sizeof(float) * 2;
        
        public override string ToString()
        {
            return (String.Format("{0,4} {1,4} ", x,y));
        }

        public static readonly Vector2 Zero = new Vector2(0, 0);

        #region operator overload
        public static Vector2 operator /(Vector2 left, float scalar)
        {
            if (scalar <= float.Epsilon) throw new ArgumentException("Cannot divide a Vector2 by zero");
            Vector2 vector;
            var inverse = 1.0f / scalar;
            vector.x = left.x * inverse;
            vector.y = left.y * inverse;
            return vector;
        }
        public static Vector2 operator +(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x + right.x, left.y + right.y);
        }
        public static Vector2 operator +(Vector2 v, float scalar)
        {
            return new Vector2(v.x + scalar, v.y + scalar);
        }
        public static Vector2 operator *(Vector2 left, float scalar)
        {
            Vector2 retVal;
            retVal.x = left.x * scalar;
            retVal.y = left.y * scalar;
            return retVal;
        }
        public static Vector2 operator *(float scalar, Vector2 right)
        {
            return right * scalar;
        }
        public static Vector2 operator -(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x - right.y, left.x - right.y);
        }
        public static Vector2 operator -(Vector2 left)
        {
            return new Vector2(-left.x, -left.y);
        }
        #endregion

        public float LengthSq
        {
            get { return x * x + y * y; }
        }
        public float Length
        {
            get { return (float)Math.Sqrt(LengthSq); }
        }

        /// <summary>
        /// Normalize the vector and get the calculated length. 
        /// </summary>
        public float Normalize()
        {
            float length = Length;

            if (length > float.Epsilon)
            {
                x /= length;
                y /= length;
            }
            return length;
        }

        public Vector2 Normal
        {
            get
            {

                Vector2 vect = new Vector2(x,y);
                vect.Normalize();
                return vect;
            }
        }
    }
}