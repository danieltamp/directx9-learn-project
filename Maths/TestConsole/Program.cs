﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Graphics;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix4 view = Matrix4.MakeViewLH(new Vector3(10, 10, 10), Vector3.Zero, Vector3.UnitY);
            Matrix4 proj = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), new Viewport(100, 100, 0, 0), 0.1f, 1000.0f);

            Frustum frustum = new Frustum(proj, view, Matrix4.Identity);

            bool test = frustum.isPointInside(Vector3.Zero);
            
            Console.Read();
        }
    }
}
