﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

namespace Engine.Tools
{
    [Flags]
    public enum eAxis : byte
    {
        None = 0x00,
        X = 0x01,
        Y = 0x02,
        Z = 0x04
    }

    public static class Tool
    {
        /// <summary>
        /// return n^2;
        /// </summary>
        public static int Pow2(int n)
        {
            return n << 1;
        }

        public static bool GetFlag(Byte value, Byte flag) { return (value & flag) != 0; }

        public static void SetFlag(ref Byte value, Byte flag, bool flagval)
        {
            if (flagval) value |= flag;
            else value &= (byte)~flag;
        }

        /// <summary>
        /// Swap to values, val1 and val2 can be from array
        /// </summary>
        public static void Swap<T>(ref T val1, ref T val2)
        {
            T tmp = val1;
            val1 = val2;
            val2 = tmp;
        }
        /// <summary>
        /// Swap to values for a list class
        /// </summary>
        public static void Swap<T>(List<T> list, int i, int j)
        {
            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
        
        public static object RawDeserialize(byte[] rawData, int position, Type anyType)
        {
            int rawsize = Marshal.SizeOf(anyType);
            if (rawsize > rawData.Length)
                return null;
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.Copy(rawData, position, buffer, rawsize);
            object retobj = Marshal.PtrToStructure(buffer, anyType);
            Marshal.FreeHGlobal(buffer);
            return retobj;
        }

        public static byte[] RawSerialize(object anything)
        {
            int rawSize = Marshal.SizeOf(anything);
            IntPtr buffer = Marshal.AllocHGlobal(rawSize);
            Marshal.StructureToPtr(anything, buffer, false);
            byte[] rawDatas = new byte[rawSize];
            Marshal.Copy(buffer, rawDatas, 0, rawSize);
            Marshal.FreeHGlobal(buffer);
            return rawDatas;
        }
        
        public static int[] CopyMemory(byte[] source)
        {
            // Initialize unmanged memory to hold the array. 
            int sizeinByteA = Marshal.SizeOf(typeof(byte));
            int sizeinByteB = Marshal.SizeOf(typeof(int));

            int meminbyteA = sizeinByteA * source.Length;
            int meminbyteB = (meminbyteA / sizeinByteB) * sizeinByteB;

            int numA = source.Length;
            int numB = meminbyteB / sizeinByteB;


            Debug.Assert(meminbyteB == meminbyteA, "size different, some values at the end of array will be incorrect");

            IntPtr pntA = Marshal.AllocHGlobal(meminbyteA);
            int[] result = null;
            try
            {
                // Copy the array to unmanaged memory.
                Marshal.Copy(source, 0, pntA, numA);
                // Copy the unmanaged array back to another managed array. 
                result = new int[numB];
                Marshal.Copy(pntA, result, 0, numB);

            }
            catch (Exception e)
            {
                Console.WriteLine("can't copy two unmanaged memory :\n" + e.ToString());
                result = null;
            }
            finally
            {
                // Free the unmanaged memory.
                Marshal.FreeHGlobal(pntA);
            }
            return result;
        }




        public unsafe static B[] CopyMemoryStruct<A,B>(A[] source)
            where A : struct
            where B : struct
        {
            // Initialize unmanged memory to hold the array. 
            int sizeinByteA = Marshal.SizeOf(typeof(A));
            int sizeinByteB = Marshal.SizeOf(typeof(B));

            int meminbyteA = sizeinByteA * source.Length;
            int meminbyteB = (meminbyteA / sizeinByteB) * sizeinByteB;

            int numA = source.Length;
            int numB = meminbyteB / sizeinByteB;

            return null;
        }
    }
}
