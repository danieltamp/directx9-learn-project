﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

using Lzo32;

namespace Engine.M2TW
{
    public class AniminstanceVertexEntry
    {
        internal static bool flag = true;

        public int  num = 1;
        public uint fileoffset = 0;
        public uint type = 0;
        public uint filesize = 0;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();// short0;
            num = file.ReadInt32(); // 1 first loop, 2 all other
            fileoffset = file.ReadUInt32();
            type = file.ReadUInt32(); // vertex type
            filesize = file.ReadUInt32();
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file)
        {
            if (flag) file.Write((short)0);
            file.Write(num);
            file.Write(fileoffset);
            file.Write(type);
            file.Write(filesize);
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = num.ToString() + " " + fileoffset.ToString() + " " + type.ToString() + " " + filesize.ToString() + "\n";
            return str;
        }
    }
    public class AniminstanceTransition
    {
        internal static LZO1XCompressor lzo = new LZO1XCompressor();
        internal static bool flag = true;

        public string name = "";
        public uint numindex = 0;
        public AniminstanceVertexEntry[] entry;
        public int numverts = 0;
        public byte[] chunk;
        public bool iscompressed = true;
        public int totalfilesize;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();// short0;
            name = Utils.ReadGameString(file);
            if (flag) file.ReadUInt32();// integer0;
            file.ReadUInt32();// integer1;
            if (flag) file.ReadUInt32();// integer0;
            numindex = file.ReadUInt32(); //always5
            if (numindex > 5) throw new ArgumentOutOfRangeException("numindex ", numindex, "numindex  > 5 , never tested, exit for safety");

            entry = new AniminstanceVertexEntry[numindex];
            int i = 0;
            while (i < numindex)
            {
                entry[i] = new AniminstanceVertexEntry();
                entry[i].ReadBin(file);
                i++;
            }
            numverts = file.ReadInt32();
            if (numverts > 256 * 256) throw new ArgumentOutOfRangeException("numverts ", numverts, "numverts  > 65536 this is not possible, exit for safety");
            int numbytes = file.ReadInt32();

            iscompressed = true;
            chunk = new byte[numbytes];
            if (file.Read(chunk, 0, numbytes) != numbytes)
                throw new InvalidDataException("fail read " + numbytes + " bytes... boh!");
                
            totalfilesize = file.ReadInt32();

            file.ReadInt32(); //integer1
            flag = false;
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (flag) file.Write((short)0);
            Utils.WriteGameString(name,file);
            if (flag) file.Write((uint)0);
            file.Write((uint)1);
            if (flag) file.Write((uint)0);
            file.Write(entry.Length);
            int i = 0;
            while (i < entry.Length)
            {
                if (entry[i] == null) throw new ArgumentException("entry[i] = null, exit for safety");
                entry[i].WriteBin(file);
                i++;
            }
            file.Write(numverts);
            file.Write(chunk.Length);
            file.Write(chunk);
            file.Write(totalfilesize);
            file.Write((uint)1);

            file.Flush();
            flag = false;
            return true;
        }
        public bool Write(StreamWriter file)
        {
            flag = false;
            return true;
        }

        public void Decompress()
        {
            Console.Write("Decompress " + chunk.Length);
            byte[] decompres = lzo.Decompress(chunk);
            chunk = decompres;
            Console.WriteLine(" > " + chunk.Length);
            iscompressed = false;
        }
        public void Compress()
        {
            byte[] compress = lzo.Compress(chunk);
            chunk = compress;
            iscompressed = true;
        }

        public override string ToString()
        {
            string str = "   " + name.ToString() + " numverts = " + numverts.ToString() + " numindex = " + numindex.ToString() + "\n";
            foreach (AniminstanceVertexEntry e in entry)
                str += "   " + e.ToString();
            str += "   Chunk size : " + chunk.Length.ToString() + "\n";
            return str;
        }
    }
    public class AniminstanceObjectSection
    {
        internal static bool flag = true;

        public uint numtransition = 0;
        public AniminstanceTransition[] transition;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();// short0;
            numtransition = file.ReadUInt32();
            if (numtransition > 100) throw new ArgumentOutOfRangeException("numtransition", numtransition, "Too many transition, exit for safety");
            transition = new AniminstanceTransition[numtransition];
            int i = 0;
            while (i < numtransition)
            {
                transition[i] = new AniminstanceTransition();
                transition[i].ReadBin(file);
                i++;
            }

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file)
        {
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "num transition = " + numtransition.ToString() + "\n";
            foreach (AniminstanceTransition t in transition)
                str += t.ToString();
            return str;
        }
    }

    /// <summary>
    ///  AniminstanceFormat class is a collection of method to read and manage the *.animinstance file.
    ///  the methods clearflags() was called in ReadBin and WriteBin for safety, this because
    ///  the static bool flag are like globals value, if you write or read a singular class
    ///  this value are set to false every time.
    /// </summary>
    public class AniminstanceFormat
    {
        public string debug = "all ok";
        internal static bool flag = true;
        
        public uint numobj = 0;
        public AniminstanceObjectSection[] objsection;
        
        /// <summary>
        ///  same of WorldFormat
        /// </summary>
        public static void clearAllflags(bool status)
        {
            flag = status;
            AniminstanceObjectSection.flag = status;
            AniminstanceTransition.flag = status;
            AniminstanceVertexEntry.flag = status;
        }
        /// <summary>
        ///  same of WorldFormat
        /// </summary>
        public Int64 OpenFile(string filepath)
        {
            Int64 filepos = -1;

            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    ReadBin(file);
                    filepos = -1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("!!!! ERROR !!!!\n" + debug);
                    filepos = file.BaseStream.Position;
                    debug = (string)ex.Message;
                }
                finally
                {
                    file.Close();
                }
            }
            return filepos;
        }
        /// <summary>
        ///  same of WorldFormat
        /// </summary>
        public Int64 SaveFile(string filepath)
        {
            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath)))
                if (!WriteBin(file)) return file.BaseStream.Position;
            return -1;
        }
        /// <summary>
        ///  same of WorldFormat
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            clearAllflags(true);
            file.BaseStream.Seek(0, SeekOrigin.Begin);
            // header of world file
            string headerStr = Utils.ReadGameString(file);// = "serialization::archive";
            if (headerStr == null) { return false; }
            byte[] headerBytes = new byte[9];// = { 3, 4, 4, 8, 1, 0, 0, 0};
            file.Read(headerBytes, 0, 9);

            
            if (flag) file.ReadUInt32();// integer0;
            numobj = file.ReadUInt32();
            if (flag) file.ReadUInt16();// short0;
            if (numobj > 1000000) throw new ArgumentOutOfRangeException("numobj", numobj, "Too many objects, exit for safety");
            objsection = new AniminstanceObjectSection[numobj];
            int i = 0;
            while (i < numobj)
            {
                objsection[i] = new AniminstanceObjectSection();
                objsection[i].ReadBin(file);
                i++;
            }
            flag = false;
            return true;
        }
        /// <summary>
        ///  same of WorldFormat
        /// </summary>
        public bool WriteBin(BinaryWriter file)
        {
            file.Flush();
            flag = false;
            return false;
        }
        
        /// <summary>
        ///  Before use it you must read animinstance file with "ReadBin". Extract each transition block into
        ///  pkg file in the folderpath directory , these compressed file AREN'T utilized to rebuild animinstance with
        ///  the 3dstudio's script because the name DON'T HAVE the animation string
        /// </summary>
        public bool WritePakage(string folderpath)
        {
            if (numobj <= 0) return false;
            
            // creo una cartella per salvare il risultato
            clearAllflags(false);
            string newfolderpath = Path.Combine(folderpath, "ExtractPkg");
            Directory.CreateDirectory(newfolderpath);

            for (int nobj = 0; nobj < numobj; nobj++)
            {
                AniminstanceObjectSection section = objsection[nobj];
                uint numtransition = section.numtransition;

                if (numtransition > 0) for (int t = 0; t < numtransition; t++)
                {
                    AniminstanceTransition transition = section.transition[t];

                    string filename = Path.Combine(newfolderpath, "obj" + nobj.ToString()+ "." + section.transition[t].name + ".pkg");
                    using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filename)))
                    {
                        transition.WriteBin(file);
                    }
                }
            }
            return true;
        }
        /// <summary>
        ///  overloaded function of WritePakage() , a world instance can write pkg file with corresponding
        ///  animation string , so the result isn't all animinstance block but only the unique block data 
        /// </summary>
        public bool WritePakage(string folderpath , WorldFormat world , bool unlzo)
        {
            if (numobj <= 0 | world.tabletwo.numobj<numobj) return false;

            // creo una cartella per salvare il risultato
            clearAllflags(false);
            string newfolderpath = Path.Combine(folderpath, "ExtractPkg");
            Directory.CreateDirectory(newfolderpath);

            // ricavo il numero di animazioni usate
            int numanim = (int)world.damageanim.numanim;
            if (numanim <= 0)
            {
                debug = "no animation found in world instance";
                return false;
            }
            BitArray animcheck = new BitArray(numanim,false);

            for (int nobj = 0; nobj < numobj; nobj++)
            {
                
                // verifico che l'animazione non sia già stata estratta
                int a = world.tabletwo.row[nobj].anim;
                if (a < 0) continue;
                if (animcheck[a]) continue;

                Console.WriteLine("> try extract anim for obj " + nobj);
                // verifico che l'animazione esista
                if (world.damageanim.anim.Length <= a)
                {
                    debug = "error , the world instance have a bad animation index";
                    return false;
                }
                
                // verifico che l'animazione del file world combacia con l'animinstance
                AniminstanceObjectSection section = objsection[nobj];
                uint numtransition = section.numtransition;
                if (world.damageanim.anim[a].numinfo != numtransition)
                {
                    debug = "error , the world and animinstance numtransition don't match";
                    return false;
                }
               
                // finalmente posso creare il nome appropriato
                for (int t = 0; t < numtransition; t++)
                {
                    AniminstanceTransition transition = section.transition[t];
                    string animname = Path.GetFileNameWithoutExtension(world.damageanim.anim[a].comment[t].infoB);
                    string transname = world.damageanim.anim[a].comment[t].infoA;
                    string filename = Path.Combine(newfolderpath, animname +"." + transname+ ".pkg");
                    using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filename))) { transition.WriteBin(file); }
                    
                    //provo a decomprimere
                    if (unlzo)
                    {
                        try
                        {
                            transition.Decompress();
                            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filename+".unlzo")))
                            {
                                transition.WriteBin(file);
                            }
                        }
                        catch (Exception ex) { Console.WriteLine("ERROR : " + ex.ToString()); return false; }
                    }
                }
                animcheck[a] = true;
            }
            return true;
        }




        public override string ToString()
        {
            string str = "NUMOBJ : " + numobj + "\n";
            int i = 0;
            foreach (AniminstanceObjectSection a in objsection)
            {
                if (a.numtransition > 0)
                    str += i.ToString() + "]\n" + a.ToString() + "\n";
                i++;
            }
            return str;
        }
    }
}