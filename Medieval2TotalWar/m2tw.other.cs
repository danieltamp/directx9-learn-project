using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Drawing;

namespace Engine.M2TW
{
    /// <summary>
    ///  Modeltraversablenetwork class is a collection of method to read and manage the 
    ///  *.modeltraversabelnetwork file. the methods clearflags() was called in ReadBin and WriteBin.
    ///  This file use a similar wall section class
    /// </summary>
    public class ModelNetworkFormat
    {
        internal static bool flag = true;

        public PathControl pathcontrol;

        /// <summary>
        ///  these string store the error message, in some case the code don't get error but a "memorandum"
        ///  was written when debugging
        /// </summary>
        public static string debug = "all ok";
        /// <summary>
        ///  this function initialize a new set of flag,
        ///  call it if you create a new instance of ModelNetworkFormat.
        /// </summary>
        public static void clearAllflags(bool status)
        {
            // flag used to work only with modeltraversablenetwork variant.
            Structure.flagmesh = false;
            PathControl.isnetwork = true;
            Globals.counterA = 0;
            flag = status;

            BorderOfWall.flag = status;
            SegmentOfWall.flag = status;
            SplineOfWall.flag = status;
            DamageOfWall.flag = status;
            DeploymentSoldierLevel.flag = status;
            WallSection.flag = status;

            LadderPoint.flag = status;
            Ladder.flag = status;
            DoorReference.flag = status;
            Door.flag = status;
            Bridge.flag = status;
            PathControl.flag = status;
        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public long OpenFile(string filepath)
        {
            Int64 filepos = -1;
            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    ReadBin(file);
                    filepos = -1;
                }
                catch (Exception ex)
                {
                    filepos = file.BaseStream.Position;
                    debug = (string)ex.Message;
                    debug += "\nStop at : " + filepos.ToString();
                    Console.WriteLine("!!!! ERROR !!!!\n" + debug);
                }
                finally
                {
                    file.Close();
                }
            }
            return filepos;
        }
        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public long SaveFile(string filepath)
        {
            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath)))
                if (!WriteBin(file)) return file.BaseStream.Position;
            return -1;
        }



        /// <summary>
        ///  thise function read a binary data, at the beginning use the methods "clearflags" and place file seek at
        ///  start.
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            clearAllflags(true);

            file.BaseStream.Seek(0, SeekOrigin.Begin);

            // header of world file
            string headerStr = Utils.ReadGameString(file);// = "serialization::archive";
            if (headerStr == null) { return false; }
            byte[] headerBytes = new byte[5];// = { 3, 4, 4, 4, 8};
            file.Read(headerBytes, 0, headerBytes.Length);

            int numpath = file.ReadInt32();
            pathcontrol = new PathControl();
            pathcontrol.ReadBin(file);

            return true;
        }
        public bool WriteBin(BinaryWriter file)
        {
            return true;
        }
    }
}