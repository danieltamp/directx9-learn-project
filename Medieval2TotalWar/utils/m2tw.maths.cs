using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.IO;

using Engine.Maths;

namespace Engine.M2TW
{
    public struct sAngle
    {
        double _radians;

        public double radians 
        { 
            get { return _radians; }
            set { _radians = value; }
        }
        public double degree 
        { 
            get { return (_radians * (180.0 / Math.PI)); }
            set { _radians = Math.PI * value / 180.0; }
        }
        public override string ToString()
        { 
            return (String.Format("{0:F3}d", degree));
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct sFace
    {
        public Face face;
        public ushort x { get { return face.I; } set{face.I = value;}}
        public ushort y { get { return face.J; } set{face.J = value;}}
        public ushort z { get { return face.K; } set{face.K = value;}}

        public sFace(ushort X, ushort Y, ushort Z)
        {
            face = new Face(X, Y, Z);
        }

        public static sFace[] FromFile(BinaryReader file, int count)
        {
            sFace[] face = new sFace[count];
            for (int i = 0; i < count; i++)
            {
                face[i].face.I = file.ReadUInt16();
                face[i].face.J = file.ReadUInt16();
                face[i].face.K = file.ReadUInt16();
            }
            return face;
        }

        public void ConvertTo3ds()
        {
            ushort X = this.x;
            this.x = this.y;
            this.y = X;
            this.x++;
            this.y++;
            this.z++;
        }
        public static explicit operator sPoint3D(sFace f)
        {
            return new sPoint3D((float)f.x, (float)f.y, (float)f.z);
        }
        public override string ToString()
        {
            return (String.Format("[{0:D},{1:D},{2:D} ]\n", x, y, z));
        }
    }

    /// <summary>
    ///  For game , the 2D space is XZ plane (in 3dstudio XY) so there aren't "height" value Y because
    ///  DirectX use left-handed coordinates. This Structure can also use to store UVW coordinate as u v value
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sPoint2D
    {
        public Vector2 vector;

        public float u { get { return vector.u; } set { vector.u = value; } }
        public float v { get { return vector.v; } set { vector.v = value; } }

        public sPoint2D(float U, float V)
        {
            vector = new Vector2(U, V);
        }
        public static sPoint2D FromFile(BinaryReader file)
        {
            sPoint2D point = new sPoint2D();
            point.vector.u = file.ReadSingle();
            point.vector.v = file.ReadSingle();
            return point;
        }
        public static sPoint2D[] FromFile(BinaryReader file , int count)
        {
            sPoint2D[] array = new sPoint2D[count];
            for (int i = 0; i < count; i++)
            {
                array[i].vector.u = file.ReadUInt16();
                array[i].vector.v = file.ReadUInt16();
            }
            return array;
        }
        
        
        /// <summary>
        ///  For game , the 2D space is XZ plane (in 3dstudio XY) so there aren't "height" value (Y axis)
        ///  the explicit conversion set y coordinates to 0.
        /// </summary>
        public static explicit operator sPoint3D(sPoint2D p)
        {
            return new sPoint3D(p.u, 0f, p.v);
        }

        public static explicit operator sPoint4B(sPoint2D p)
        {
            float xx = 64 * (p.u + 2);
            byte b2 = (byte)xx;
            byte b3 = (byte)((xx - b2) * 256f);

            float yy = 64 * (p.v + 2);
            byte b4 = (byte)yy;
            byte b1 = (byte)((yy - b4) * 256f);

            return new sPoint4B(b1, b2, b3, b4);
        }

        /// <summary>
        ///  Attention, this conversion are used for 3d coordinates (XZ version), not for texture (UVW version)
        /// </summary>
        public void ConvertTo3ds()
        {
            //sPoint3D p = (sPoint2D.x , 0 , sPoint2D.z) --to3ds--> (- sPoint2D.x , - sPoint2D.z , 0)  
            u = u * (-1);
            v = v * (-1);
        }
        public override string ToString()
        {
            return (String.Format("[{0:F2} {1:F2}]\n", u, v));
        }
    }
    
    /// <summary>
    ///  DirectX use left-handed coordinates ( Y = height , XZ the 2d plane)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sPoint3D
    {
        public Vector3 vector;

        public float x { get { return vector.x; } set { vector.x = value; } }
        public float y { get { return vector.y; } set { vector.y = value; } }
        public float z { get { return vector.z; } set { vector.z = value; } }

        public sPoint3D(float X, float Y, float Z)
        {
            vector = new Vector3(X, Y, Z);
        }

        public void ConvertTo3ds()
        {
            float X = this.x;
            float Y = this.y;
            float Z = this.z;
            this.x = -X;
            this.y = -Z;
            this.z = Y;
        }
        public static sPoint3D FromFile(BinaryReader file)
        {
            sPoint3D point = new sPoint3D();
            point.vector.x = file.ReadSingle();
            point.vector.y = file.ReadSingle();
            point.vector.z = file.ReadSingle();
            return point;
        }
        public static sPoint3D[] FromFile(BinaryReader file, int count)
        {
            sPoint3D[] array = new sPoint3D[count];
            for (int i = 0; i < count; i++)
            {
                array[i].vector.x = file.ReadSingle();
                array[i].vector.y = file.ReadSingle();
                array[i].vector.z = file.ReadSingle();
            }
            return array;
        }
        /// <summary>
        ///  For game , the 2D space is XZ plane (in 3dstudio XY) so there aren't "height" value (Y axis)
        ///  the explicit conversion lose the y coordinates.
        /// </summary>
        public static explicit operator sPoint2D(sPoint3D p)
        {
            return new sPoint2D(p.x, p.z);
        }
        public override string ToString()
        { 
            return (String.Format("[ {0:F2} {1:F2} {2:F2} ]\n", x, y, z));
        }
    }

    /// <summary>
    ///  This particular version are used to reduce the size of Point3D and Point2D with a custom method.
    ///  Encoding and decoding was made for Normalized vector where the range is (-1.0 , +1.0) not used for
    ///  Vertices case , there range was too big (-INF , +INF ). Point2D have more precision than Point3D
    /// </summary>
    [StructLayout(LayoutKind.Sequential , Pack = 4)]
    public struct sColor
    {
        public int color;
        public sColor(int i) { color = i; }
        public sColor(byte R, byte G, byte B, byte A) { color = Color.FromArgb(A, R, G, B).ToArgb(); }
        public byte r { get { return Color.FromArgb(color).R; } }
        public byte g { get { return Color.FromArgb(color).G; } }
        public byte b { get { return Color.FromArgb(color).B; } }
        public byte a { get { return Color.FromArgb(color).A; } }

        public static explicit operator sPoint4B(sColor c) { return new sPoint4B(c.r, c.g, c.b, c.a); }

        public static sColor[] FromFile(BinaryReader file, int count)
        {
            sColor[] array = new sColor[count];
            for (int i = 0; i < count; i++)
            {
                array[i].color = file.ReadInt32();
            }
            return array;
        }

    }

    /// <summary>
    /// A int32 package data, can be use to store Normal, Texture , Color , BonesID or Weigth.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sPoint4B
    {
        public Byte4 bytes;

        public sPoint4B(byte r, byte g, byte b, byte a)
        {
            bytes = new Byte4(r, g, b, a);
        }
        public sPoint4B(uint i)
        {
            bytes = new Byte4(i);
        }
        public static explicit operator sColor(sPoint4B q) 
        {
            return new sColor(
                q.bytes.r,
                q.bytes.g,
                q.bytes.b,
                q.bytes.a);
        }
        public static explicit operator sPoint3D(sPoint4B q)
        {
            return new sPoint3D
            {
                vector = new Vector3(
                        q.bytes.b / 127.5f - 1f,
                        q.bytes.g / 127.5f - 1f,
                        q.bytes.r / 127.5f - 1f)
            };
        }
        public static explicit operator sPoint2D(sPoint4B q)
        {
            return new sPoint2D
            {
                vector = new Vector2(
                    q.bytes.g / 64f + q.bytes.b / 16384f - 2f,
                    q.bytes.a / 64f + q.bytes.r / 16384f - 2f)
            };
        }
        public static explicit operator UInt32(sPoint4B q)
        {
            return (uint)q.bytes;
        }

        public static sPoint4B FromFile(BinaryReader file)
        {
            return new sPoint4B(file.ReadUInt32());
        }
        public static sPoint4B[] FromFile(BinaryReader file, int count)
        {
            sPoint4B[] array = new sPoint4B[count];
            for (int i = 0; i < count; i++)
                array[i] = sPoint4B.FromFile(file);
            return array;
        }

        public override string ToString()
        {
            return bytes.ToString();
        }

    }

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sQuaternion
    {
        public Quaternion quaternion;

        public float x { get { return quaternion.x; } set { quaternion.x = value; } }
        public float y { get { return quaternion.y; } set { quaternion.y = value; } }
        public float z { get { return quaternion.z; } set { quaternion.z = value; } }
        public float w { get { return quaternion.w; } set { quaternion.w = value; } }

        public sQuaternion(float X, float Y, float Z, float W)
        {
            quaternion = new Quaternion(X, Y, Z, W);
        }

        public override string ToString()
        { 
            return ((string)String.Format("[ {0:F2} {1:F2} {2:F2} {3:F2} ]\n", x, y, z, w)); 
        }

        public static sQuaternion FromFile(BinaryReader file)
        {
            sQuaternion quat = new sQuaternion();
            quat.quaternion.x = file.ReadSingle();
            quat.quaternion.y = file.ReadSingle();
            quat.quaternion.z = file.ReadSingle();
            quat.quaternion.w = file.ReadSingle();
            return quat;
        }
        public static sQuaternion[] FromFile(BinaryReader file, int count)
        {
            sQuaternion[] array = new sQuaternion[count];
            for (int i = 0; i < count; i++)
            {
                array[i].quaternion.x = file.ReadSingle();
                array[i].quaternion.y = file.ReadSingle();
                array[i].quaternion.z = file.ReadSingle();
                array[i].quaternion.w = file.ReadSingle();
            }
            return array;
        }
        
    }
    
    /// <summary>
    ///  For 3D rotation matrix the game use tipically a 4x4 float array in Directx notation. The matrix are trasposed
    ///  in a tipical Math notation.
    /// </summary>  
    /// <remarks>
    ///  1 0 0 X
    ///  0 1 0 Y
    ///  0 0 1 Z
    ///  0 0 0 1
    /// </remarks>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sMatrix16
    {
        /// <summary>
        /// matrix in math notation
        /// </summary>
        public Matrix4 matrix;
        
        /// <summary>
        /// Matrix in Directx notation (trasposed)
        /// </summary>
        public sMatrix16(float[] field) 
        {
            if (field.Length != 16) 
                throw new ArgumentOutOfRangeException("not 16 elements");
            matrix = new Matrix4();
            for (int i = 0; i < 16; i++) matrix[i] = field[i];
            matrix = Matrix4.Traspose(matrix);
        }

        public static sMatrix16 MaxCoordSys = new sMatrix16(new float[] { -1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1 });
        public static sMatrix16 IMaxCoordSys = MaxCoordSys.GetInverted();


        public void ConvertTo3ds()
        {
            sPoint3D pos = new sPoint3D();
            pos.vector = matrix.TranslationComponent;
            pos.ConvertTo3ds();
            this.matrix = MaxCoordSys.matrix * this.matrix * IMaxCoordSys.matrix;
            matrix.TranslationComponent = pos.vector;
        }

        public sMatrix16 GetInverted()
        {
            sMatrix16 inverse = new sMatrix16();
            inverse.matrix = Matrix4.Inverse(matrix);
            return inverse;
        }
        /// <summary>
        /// the explicit conversion into 2d space isn't easy
        ///  1 * 0 X     1 0 X
        ///  * * * Y --> 0 1 Z
        ///  0 * 1 Z
        /// </summary> 
        public static explicit operator sMatrix9(sMatrix16 M3)
        {
            sMatrix9 M2 = new sMatrix9();
            Matrix4 m3 = M3.matrix;
            Matrix4 m2 = M2.matrix;
            // X row
            m2.m00 = m3.m00;
            m2.m01 = m3.m02;
            m2.m02 = m3.m03;
            // Z row
            m2.m10 = m3.m20;
            m2.m11 = m3.m22;
            m2.m12 = m3.m23;

            return M2;
        }

        /// <summary>
        /// WARNING , this function can be wrong.
        /// </summary> 
        public void RotateZ(sAngle angle)
        {
            Matrix4 Rz = Matrix4.Identity;
            double radians = angle.radians;
            Rz[0, 0] = (float)Math.Cos(radians);
            Rz[0, 1] = (float)Math.Sin(radians);
            Rz[1, 0] = (float)Math.Sin(radians) * -1.0f;
            Rz[1, 1] = (float)Math.Cos(radians);
            this.matrix = Rz * this.matrix;
        }

        /// <summary>
        ///  this version read a point3 position and a point3 direction , the rotation was made
        ///  with Y fixed , X-->X' so Z' = X'*Y , matrix4x4 = [X'][Y][Z'][0,0,0]
        /// </summary>  
        public void ReadBin2(BinaryReader file)
        {
            matrix = Matrix4.Identity;

            sPoint3D pos = sPoint3D.FromFile(file);
            sPoint3D row1 = sPoint3D.FromFile(file);
            sPoint3D row2 = new sPoint3D(0,1,0);     
            sPoint3D row3 = new sPoint3D();
            row3.vector = Vector3.Cross(row1.vector, row2.vector);

            matrix.m00 = row1.x;
            matrix.m01 = row1.y;
            matrix.m02 = row1.z;

            matrix.m10 = row2.x;
            matrix.m11 = row2.y;
            matrix.m12 = row2.z;

            matrix.m20 = row3.x;
            matrix.m21 = row3.y;
            matrix.m22 = row3.z;

            matrix.TranslationComponent = pos.vector;
        }

        public void WriteBin(BinaryWriter file)
        {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    file.Write((float)matrix[i, j]);
        }


        public static sMatrix16[] FromFile(BinaryReader file, int count)
        {
            sMatrix16[] array = new sMatrix16[count];
            for (int i = 0; i < count; i++)
            {
                float[] field = new float[16];
                for (int j = 0; j < 16; j++)
                    field[j] = file.ReadSingle();
                array[i] = new sMatrix16(field);
            }
            return array;
        }
        public static sMatrix16 FromFile(BinaryReader file)
        {
            float[] field = new float[16];
            for (int j = 0; j < 16; j++)
                field[j] = file.ReadSingle();
            return new sMatrix16(field);
        }
        
        
        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < 3; i++) // last identity row aren't used
            {
                for (int j = 0; j < 4; j++)
                    str += String.Format("{0:00.00}  ", matrix[i, j]);
                str += "\n";
            }
            return str;
        }

    }

    /// <summary>
    ///  For 2D rotation matrix the game use tipically a 2x2 float array , unlike standard 3x3matrix there are a simple
    ///  point2d for traslation and a atan2 function for angle rotation. The internal "matrix" property is a stardard 3x3
    /// </summary>
    ///  <remarks>
    ///  1 0 X
    ///  0 1 Z
    ///  0 0 1
    /// </remarks>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sMatrix9
    {
        public Matrix3 matrix;

        public sMatrix9(float[] field)
        {
            matrix = Matrix3.Identity;

            if (field.Length != 9)
                throw new ArgumentOutOfRangeException("not 9 elements");

            for (int i = 0; i < 9; i++)
                matrix[i] = field[i];
        }

        /// <summary>
        /// the explicit conversion into 3d space isn't easy
        ///  1 0 X     1 * 0 X
        ///  0 1 Z --> * * * Y
        ///            0 * 1 Z
        /// </summary> 
        public static explicit operator sMatrix16(sMatrix9 M2)
        {
            sMatrix16 M3 = new sMatrix16();
            Matrix4 m3 = M3.matrix;
            Matrix4 m2 = M2.matrix;
            // X row
            m3.m00 = m2.m00;
            m3.m01 = 0;
            m3.m02 = m2.m01;
            m3.m03 = m2.m02;
            // Z row
            m3.m20 = m2.m10;
            m3.m21 = 0;
            m3.m22 = m2.m11;
            m3.m23 = m2.m12;
            return M3;
        }
        
        /// <summary>
        ///  this function is only for maxscript, 3dstudio don't have the matrix2 value.
        /// </summary> 
        public sMatrix16 GetAsMatrix4x4() { return (sMatrix16)this; }

        public void Rotate(sAngle angle)
        {
            Matrix4 Rz = Matrix4.Identity;
            double radians = angle.radians;
            Rz.m00 = (float)Math.Cos(radians);
            Rz.m01 = (float)Math.Sin(radians);
            Rz.m10 = (float)Math.Sin(radians) * -1.0f;
            Rz.m11 = (float)Math.Cos(radians);
            //this.matrix = Rz * this.matrix;
        }

        /// <summary>
        ///  this version read 4 float , the first two are position, last two the atan2 coordinated
        /// </summary>
        /// <remarks>
        /// X , Y , atan2x , atan2y 
        /// </remarks>
        public static sMatrix9 FromFile_atan2(BinaryReader file)
        {
            sMatrix9 mat = new sMatrix9();
            sPoint2D pos = sPoint2D.FromFile(file);
            sPoint2D vect = sPoint2D.FromFile(file);
            float angle = (float)Math.Atan2(pos.v, pos.u);
            mat.matrix = Matrix3.Rotation(angle);
            mat.matrix.TranslationComponent = vect.vector;
            return mat;

        }
        /// <summary>
        ///  this version read 4 float , is a standard 3x3 matrix with Y component omitted
        /// </summary>  
        /// <remarks>
        ///   a  0  b
        ///   0  1  0
        ///   c  0  d
        /// </remarks>
        public static sMatrix9 FromFile_float4(BinaryReader file)
        {
            sMatrix9 mat = new sMatrix9();
            mat.matrix.m00 = file.ReadSingle();
            mat.matrix.m01 = file.ReadSingle();
            mat.matrix.m10 = file.ReadSingle();
            mat.matrix.m21 = file.ReadSingle();
            return mat;
        }
        /// <summary>
        /// this version read 4 float , is a standard 3x3 matrix with Y component omitted
        /// </summary>
        /// <remarks>
        ///  -a  0 +b
        ///   0  1  0
        ///  +c  0 -d
        ///  a and d are inverted, i don't understand why , but in 3dstudio the matrix is correct.
        /// </remarks>
        public static sMatrix9 FromFile_float4inv(BinaryReader file)
        {
            sMatrix9 mat = sMatrix9.FromFile_float4(file);
            mat.matrix.m00 *= -1;
            mat.matrix.m21 *= -1;
            return mat;
        }

        public void ConvertTo3ds()
        {
            sPoint2D pos = new sPoint2D();
            pos.vector = matrix.TranslationComponent;
            pos.ConvertTo3ds();
            sMatrix16 M = (sMatrix16)this;
            M.ConvertTo3ds();
            this.matrix = ((sMatrix9)M).matrix;
            matrix.TranslationComponent = pos.vector;
        }

        
        public override string ToString()
        {
            return matrix.ToString();
        }
    }    
    
    /// <summary>
    /// m2tw use a standard Center + Radius format
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct sBoundingSphere
    {
        public BoundarySphere bsphere;
        public float radius
        {
            get { return bsphere.radius; }
            set { bsphere.radius = value; }
        }
        public sPoint3D position 
        {
            get { return new sPoint3D { vector = bsphere.center }; }
            set { bsphere.center = value.vector; }
        }

        public sBoundingSphere(sPoint3D pos, float r)
        {
            bsphere = new BoundarySphere { center = pos.vector, radius = r };
        }

        public static sBoundingSphere FromFile(BinaryReader file)
        {
            sPoint3D position = sPoint3D.FromFile(file);
            float radius = file.ReadSingle();

            return new sBoundingSphere
            {
                bsphere = new BoundarySphere
                {
                    center = position.vector,
                    radius = radius
                }
            };
        }
        public static sBoundingSphere[] FromFile(BinaryReader file,int count)
        {
            sBoundingSphere[] array = new sBoundingSphere[count];
            for (int i = 0; i < count; i++)
            {
                sPoint3D pos = sPoint3D.FromFile(file);
                float r = file.ReadSingle();
                array[i].bsphere.center = pos.vector;
                array[i].bsphere.radius = r;
            }
            return array;
        }

        public override string ToString()
        { 
            return bsphere.ToString();
        }
    }
}