﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

using Engine.Tools;
using System.IO;

namespace Engine.Graphics
{
    /// <summary>
    /// Microsoft DirectX texture implementations
    /// </summary>
    public class DxTexture : IDisposable , IRestore
    {
        public Pool MemoryPool { get { return memoryPool; } }

        DX9Device device = null;
        Bitmap m_buffer = null;
        string m_filename = "";
        Pool memoryPool;
        Usage usage;

        internal D3D.Texture m_texture;
        
        /// <summary>
        /// Null texture
        /// </summary>
        public DxTexture()
        {
            device = null;
            m_texture = null;
            m_filename = null;
        }
        /// <summary>
        /// From original texture
        /// </summary>
        internal DxTexture(DX9Device device, D3D.Texture texture)
        {
            this.device = device;
            this.m_texture = texture;
            this.m_filename = null;
            this.memoryPool = Pool.Managed;
            this.usage = Usage.None; //if pool.default the usage must be "writeonly"
        }
        /// <summary>
        /// From file name
        /// </summary>
        public DxTexture(DX9Device device, string filename)
        {
            this.device = device;
            this.m_filename = filename;
            this.memoryPool = Pool.Managed;
            this.usage = Usage.None; //if pool.default the usage must be "writeonly"

            Bitmap bmp = new Bitmap(filename);
            Initialize(device, bmp);
        }
        /// <summary>
        /// Texture from image
        /// </summary>
        public DxTexture(DX9Device device, Bitmap bitmap)
        {
            this.memoryPool = Pool.Managed;
            this.usage = Usage.None; //if pool.default the usage must be "writeonly"

            Initialize(device, bitmap);
        }

        void Initialize(DX9Device device, Bitmap bitmap)
        {
            if (!device.SupportTextureSize(bitmap.Size))
                throw new Exception("Device not support this image");

            m_texture = D3D.Texture.FromBitmap(device.m_device, bitmap, (D3D.Usage)usage, (D3D.Pool)memoryPool);
        }

        public void Dispose()
        {
            if (m_texture != null)
                m_texture.Dispose();
        }
        public void Restore()
        {
            // not restore if it's use managed memory
            if (memoryPool == Pool.Managed) return;

            if (m_buffer != null)
            {
                Initialize(device, m_buffer);
            }
            else if (!String.IsNullOrEmpty(m_filename))
            {
                Bitmap bmp = new Bitmap(Bitmap.FromFile(m_filename));
                Initialize(device, bmp);
            }
            throw new ArgumentNullException("not found a memorized image");
        }
    }
}