﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;


namespace Engine.Graphics
{
    /// <summary>
    /// SharpDX vertex declaration
    /// </summary>
    public class DX9VertexDeclaration
    {
        D3D.Device m_device;
        D3D.VertexDeclaration m_declaration;

        public DX9VertexDeclaration(DX9Device device, ICollection<VertexElement> element)
        {
            m_device = device.m_device;

            D3D.VertexElement[] m_elements = new D3D.VertexElement[element.Count];
            int i = 0;
            foreach(VertexElement e in element)
                m_elements[i++] = Helper.Convert(e);

            m_declaration = new D3D.VertexDeclaration(m_device, m_elements);
        }

        public void SetToDevice()
        {
            m_device.VertexDeclaration = m_declaration;
        }
    }
    
    /// <summary>
    /// SharpDX vertex buffer
    /// </summary>
    public class DX9VertexBuffer : IDisposable
    {
        D3D.Device m_device;
        D3D.VertexBuffer m_vbuffer;
     
        /// <summary>
        /// Sharp Dx9 vertex buffer
        /// </summary>
        /// <param name="numverts">num or T elements</param>
        /// <param name="readable">if false is only writable</param>
        /// <param name="managed">if false put in a Pool.default, but need a shadow copy</param>
        public DX9VertexBuffer(DX9Device device, VertexFormat fvf, int byteSize, Usage usage, Pool memory)
        {
            m_device = device.m_device;
            m_vbuffer = new D3D.VertexBuffer(m_device, byteSize, (D3D.Usage)usage, (D3D.VertexFormat)fvf, (D3D.Pool)memory);
            m_vbuffer.DebugName = "ShardDx9VertexBuffer";
        }

        /// <summary>
        /// Set data original implementation
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        /// <param name="boffset">num of vertice to jump</param>
        public void SetData<T>(T[] vertices, int offset, int bstride, LockFlags mode) where T:struct
        {
            DX.DataStream dataStream = m_vbuffer.Lock(bstride*offset, bstride*vertices.Length, (D3D.LockFlags)mode);
            dataStream.WriteRange<T>(vertices, 0, vertices.Length);
            m_vbuffer.Unlock();
        }

        /// <summary>
        /// Get data original implementation
        /// </summary>
        /// <param name="count">number of vertices</param>
        /// <param name="boffset">number of vertices to jump</param>
        /// <param name="bstride">size in byte of one vertex</param>
        public T[] GetData<T>(int count, int offset, int bstride) where T : struct
        {
            DX.DataStream dataStream = m_vbuffer.Lock(bstride*offset, bstride * count, D3D.LockFlags.ReadOnly);
            T[] data = dataStream.ReadRange<T>(count);
            m_vbuffer.Unlock();
            return data;
        }

        /// <summary>
        /// Return the managed pointers of gpu buffer
        /// </summary>
        /// <param name="boffset">offset in bytes</param>
        /// <param name="bsize">size to lock in bytes</param>
        public IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            return m_vbuffer.Lock(boffset, bsize, (D3D.LockFlags)mode).DataPointer;
        }
        /// <summary>
        /// Release the managed pointers of gpu buffer
        /// </summary>
        public void UnLock()
        {
            m_vbuffer.Unlock();
        }
        /// <summary>
        /// Apply buffer to device
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        public void SetStreamSource(int nstream, int bstride)
        {
            m_device.SetStreamSource(nstream, m_vbuffer, 0, bstride);
        }

        public void Dispose()
        {
            m_vbuffer.Dispose();
        }

        public override string ToString()
        {
            return m_vbuffer.DebugName.ToString() + "_" + m_vbuffer.Description.Type.ToString();
        }



    }

    /// <summary>
    /// SharpDX index buffer
    /// </summary>
    public class DX9IndexBuffer : IDisposable
    {
        D3D.Device m_device;
        D3D.IndexBuffer m_ibuffer;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxsize">max number of vertices what can be stored</param>
        /// <param name="usage">define if is a static or dynamic buffer</param>
        /// <param name="type">define the fvf of vertices</param>
        public DX9IndexBuffer(DX9Device device, bool is32bit, int byteSize,  Usage usage, Pool memory)
        {
            m_device = device.m_device;
            m_ibuffer = new D3D.IndexBuffer(m_device, byteSize, (D3D.Usage)usage, (D3D.Pool)memory,!is32bit);
            m_ibuffer.DebugName = "ShardDx9IndexBuffer";
        }
        /// <summary>
        /// Original set data implementation
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        /// <param name="boffset">num of vertice to jump</param>
        public void SetData<T>(T[] vertices, int offset, int bstride, LockFlags mode) where T:struct
        {
            DX.DataStream dataStream = m_ibuffer.Lock(bstride * offset, bstride * vertices.Length, (D3D.LockFlags)mode);
            dataStream.WriteRange<T>(vertices, 0, vertices.Length);
            m_ibuffer.Unlock();
        }

        /// <summary>
        /// </summary>
        /// <param name="count">number of vertices</param>
        /// <param name="boffset">number of vertices to jump</param>
        /// <param name="bstride">size in byte of one vertex</param>
        public T[] GetData<T>(int count, int offset, int bstride) where T : struct
        {
            DX.DataStream dataStream = m_ibuffer.Lock(bstride * offset, bstride * count, D3D.LockFlags.ReadOnly);
            T[] data = dataStream.ReadRange<T>(count);
            m_ibuffer.Unlock();
            return data;
        }

        /// <summary>
        /// Return the managed pointers of gpu buffer
        /// </summary>
        /// <param name="boffset">offset in bytes</param>
        /// <param name="bsize">size to lock in bytes</param>
        public IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            return m_ibuffer.Lock(boffset, bsize, (D3D.LockFlags)mode).DataPointer;
        }
        /// <summary>
        /// Release the managed pointers of gpu buffer
        /// </summary>
        public void UnLock()
        {
            m_ibuffer.Unlock();
        }

        /// <summary>
        /// Apply buffer to device
        /// </summary>
        public void SetToDevice()
        {
            m_device.Indices = m_ibuffer;
        }

        public void Dispose()
        {
            m_ibuffer.Dispose();
        }
        public override string ToString()
        {
            return m_ibuffer.DebugName.ToString() + "_" + m_ibuffer.Description.Type.ToString();
        }
    }

}
