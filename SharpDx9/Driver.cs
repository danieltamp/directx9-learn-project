﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;

namespace Engine.Graphics
{
    /// <summary>
    /// Graphic Device using SharpDx
    /// </summary>
    public class DX9Device : IDisposable
    {
        public const string graphicVersion = "SharpDx9";

        Control m_control;

        // i'm using "internal" to use these values only inside library
        internal D3D.Device m_device;
        internal D3D.SwapChain m_swapchain;
        internal D3D.Surface m_backbuffer;
        internal D3D.Surface m_zbuffer;
        internal DX.Viewport m_viewport;
        internal D3D.PresentParameters m_param;

        internal bool useZbuffer = false;
        internal bool useStencil = false;
        internal bool full_screen = false;
        internal bool softwareProcessing = false;
        
            /// <summary>
        /// Directx9 constant
        /// </summary>
        const int MaxLights = 8;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="useStencil">for a true 3d world</param>
        /// <param name="tryPureDevice">try the best perfomance, old gpu don't support puredevice</param>
        public DX9Device(Control control, bool useZbuffer, bool useStencil, bool tryPureDevice)
        {
            m_control = control;
            this.useZbuffer = useZbuffer;
            this.useStencil = useZbuffer && useStencil;

            //// initialise directX with presentParameters setting
            Trace.WriteLine("Initialize Device");
            // 8-bit and 24-bit back buffers are not supported in DX9.
            /*
                D3D.Format.R5G6B5,   // 16bit
                D3D.Format.A1R5G5B5, // 16bit with 1 alpha bit (only windowed)
                D3D.Format.X1R5G5B5, // 16bit with 1 ignored bit (only windowed)
                D3D.Format.X8R8G8B8, // 24bit (used 32bit because is more fast)
                D3D.Format.A8R8G8B8, // 32bit
                D3D.Format.A2R10G10B10, // never use (full-screen mode only)
                D3D.Format.Unknown   // (only windowed)

                D3D.Format.D16,    // 16bit z-buffer , 16 for depth
                D3D.Format.D24X8,  // 32bit z-buffer , 24 for depth
                D3D.Format.D24S8,  // 32bit z-buffer , 24 for depth , 8 for stencil
                D3D.Format.D24X4S4 // 32bit z-buffer , 24 for depth , 4 for stencil
            */
            D3D.Format backbufferFormat = D3D.Format.X8R8G8B8;
            D3D.Format depthstencilFormat = useStencil ? D3D.Format.D24S8 : D3D.Format.D24X8;


            D3D.Direct3D d3d = new D3D.Direct3D();
            // Store the default adapter
            int ordinal = d3d.Adapters[0].Adapter;
            int adapterOrdinal = 0;

            Debug.Assert(m_device.Direct3D.CheckDeviceType(adapterOrdinal, D3D.DeviceType.Hardware, backbufferFormat, backbufferFormat, true), backbufferFormat.ToString() + " not compatible");
            if (useZbuffer) Debug.Assert(m_device.Direct3D.CheckDepthStencilMatch(0, D3D.DeviceType.Hardware, backbufferFormat, backbufferFormat, depthstencilFormat), depthstencilFormat.ToString() + " not compatible");


            //// setting for a correct windows directX application
            m_param = new D3D.PresentParameters();
            m_param.Windowed = !full_screen;
            m_param.DeviceWindowHandle = control.Handle;
            m_param.BackBufferFormat = full_screen ? D3D.Format.Unknown : backbufferFormat;
            m_param.EnableAutoDepthStencil = useZbuffer;
            m_param.PresentationInterval = D3D.PresentInterval.Immediate;
            m_param.SwapEffect = D3D.SwapEffect.Discard;
            m_param.BackBufferCount = 1;
            m_param.BackBufferHeight = full_screen ? 0 : control.ClientSize.Height;
            m_param.BackBufferWidth = full_screen ? 0 : control.ClientSize.Width;
            if (useZbuffer) m_param.AutoDepthStencilFormat = depthstencilFormat;

            // Check to see if we can use a pure hardware device
            D3D.Capabilities caps = d3d.GetDeviceCaps(adapterOrdinal, D3D.DeviceType.Hardware);
            D3D.CreateFlags flags = D3D.CreateFlags.SoftwareVertexProcessing;
            softwareProcessing = true;

            if (tryPureDevice)
            {
                if ((caps.DeviceCaps & D3D.DeviceCaps.HWTransformAndLight) != 0)  //SupportsHardwareTransformAndLight)
                {
                    flags = D3D.CreateFlags.HardwareVertexProcessing;
                    softwareProcessing = false;
                }
                if ((caps.DeviceCaps & D3D.DeviceCaps.PureDevice) != 0)// SupportsPureDevice)
                {
                    flags |= D3D.CreateFlags.PureDevice;
                    softwareProcessing = false;
                }
            }
            Trace.WriteLine("> Device Caps : " + flags.ToString());
            Trace.WriteLine("> Device pixel shader version : " + caps.PixelShaderVersion.ToString());
            Trace.WriteLine("> Device pixel shader version : " + caps.VertexShaderVersion.ToString());

            int pxShader = caps.PixelShaderVersion.Major;
            int vxShader = caps.VertexShaderVersion.Major;

            m_device = new D3D.Device(d3d,adapterOrdinal, D3D.DeviceType.Hardware, control.Handle, flags, m_param);
            m_device.Direct3D.Disposing += new EventHandler<EventArgs>(OnDeviceReset);
            m_device.Direct3D.Disposed += new EventHandler<EventArgs>(OnDeviceLost);

            SetRendersStates();

            m_viewport = m_device.Viewport;
            m_swapchain = m_device.GetSwapChain(0);
            m_backbuffer = m_device.GetBackBuffer(0, 0);
            m_zbuffer = useZbuffer ? m_device.DepthStencilSurface : null;

            // clean the viewport
            this.Clear(control.BackColor);
        }

        #region Properties
        /// <summary>
        /// Set the render targhet size where scene are rendered
        /// </summary>
        public Viewport viewport
        {
            get { return Helper.Convert(m_viewport); }
            set { m_viewport = Helper.Convert(value); }
        }
        /// <summary> Need reset device to apply </summary>
        public int BackBufferWidth
        {
            get { return m_param.BackBufferWidth; }
            set { m_param.BackBufferWidth = value; }
        }
        /// <summary> Need reset device to apply </summary>
        public int BackBufferHeight
        {
            get { return m_param.BackBufferHeight; }
            set { m_param.BackBufferHeight = value; ; }
        }
        
        public int MaxVertexIndex
        {
            get { return m_device.Capabilities.MaxVertexIndex; } 
        }
        public int MaxPrimitiveCount 
        { 
            get { return m_device.Capabilities.MaxPrimitiveCount; } 
        }
        public int MaxActiveLights
        {
            get
            {
                // if sofware vertex processing there aren't limitation and return -1
                int val = m_device.Capabilities.MaxActiveLights;
                return val <= 0 ? int.MaxValue : val;
            }
        }
        public int MaxActiveTexture 
        { 
            get { return m_device.Capabilities.MaxSimultaneousTextures; } 
        }

        public bool SupportTextureSize(Size size)
        {
            if (m_device.Capabilities.MaxTextureWidth < size.Width || m_device.Capabilities.MaxTextureHeight < size.Height) return false;


            if ((m_device.Capabilities.TextureCaps & D3D.TextureCaps.SquareOnly)!=0)
            {
                if (size.Height != size.Width) return false;
            }

            if ((m_device.Capabilities.TextureCaps & D3D.TextureCaps.Pow2) !=0)
            {
                List<int> pow2 = new List<int> { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };
                if (!pow2.Contains(size.Width) || !pow2.Contains(size.Height)) return false;
            }
            return true;
        }

        #endregion

        #region events

        void OnDeviceReset(object sender, EventArgs e)
        {
            Console.WriteLine("Device Reseting");
        }
        void OnDeviceLost(object sender, EventArgs e)
        {
            Console.WriteLine("Device Lost");
        }
#endregion

        #region RendersStates

        /// <summary>
        /// Set default render states, used example after device reseting
        /// </summary>
        public void SetRendersStates()
        {
            ///////// RENDERS
            m_device.SetRenderState(D3D.RenderState.NormalizeNormals, true);
            m_device.SetRenderState(D3D.RenderState.StencilEnable, useStencil);
            m_device.SetRenderState(D3D.RenderState.ZEnable, useZbuffer);
                  
            ///////// TEXTURES
            m_device.SetSamplerState(0, D3D.SamplerState.MinFilter, D3D.TextureFilter.None);
            m_device.SetSamplerState(0, D3D.SamplerState.MagFilter, D3D.TextureFilter.None);
            //m_device.SetSamplerState(0, D3D.SamplerState.MaxAnisotropy, 1);
            //m_device.SetSamplerState(0, D3D.SamplerState.AddressU, D3D.TextureAddress.Wrap);
            //m_device.SetSamplerState(0, D3D.SamplerState.AddressV, D3D.TextureAddress.Wrap);
            //m_device.SetSamplerState(0, D3D.SamplerState.AddressW, D3D.TextureAddress.Wrap);
            m_device.SetTextureStageState(0, D3D.TextureStage.ColorOperation, D3D.TextureOperation.Modulate);
            m_device.SetTextureStageState(0, D3D.TextureStage.ColorArg1, D3D.TextureArgument.Texture);
            m_device.SetTextureStageState(0, D3D.TextureStage.ColorArg2, D3D.TextureArgument.Diffuse);
            m_device.SetTextureStageState(0, D3D.TextureStage.AlphaOperation, D3D.TextureOperation.Disable);
        }

        /// <summary>
        /// Set of Get the testure, can be null
        /// </summary>
        public DxTexture Texture
        {
            get { return new DxTexture(this, m_device.GetTexture(0)); }
            set { m_device.SetTexture(0, value != null ? value.m_texture : null); }
        }
        public Matrix4 World
        {
            get { return Helper.Convert(m_device.GetTransform(D3D.TransformState.World)); }
            set { m_device.SetTransform(D3D.TransformState.World , Helper.Convert(value)); }
        }
        public Matrix4 Projection
        {
            get { return Helper.Convert(m_device.GetTransform(D3D.TransformState.Projection)); }
            set { m_device.SetTransform(D3D.TransformState.Projection, Helper.Convert(value)); }
        }
        public Matrix4 View
        {
            get { return Helper.Convert(m_device.GetTransform(D3D.TransformState.View)); }
            set { m_device.SetTransform(D3D.TransformState.View, Helper.Convert(value)); }
        }
        public Cull CullMode
        {
            get { return (Cull)m_device.GetRenderState(D3D.RenderState.CullMode); }
            set { m_device.SetRenderState(D3D.RenderState.CullMode , (D3D.Cull)value); }
        }
        public FillMode FillMode
        {
            get { return (FillMode)m_device.GetRenderState(D3D.RenderState.FillMode); }
            set { m_device.SetRenderState(D3D.RenderState.FillMode , (D3D.FillMode)value); }
        }
        public float DepthBias
        {
            get { return m_device.GetRenderState(D3D.RenderState.DepthBias); }
            set { m_device.SetRenderState(D3D.RenderState.DepthBias , value); }
        }
        public bool LightEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.Lighting) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.Lighting, value); }
        }
        public Material Material
        {
            get { return Helper.Convert(m_device.Material); }
            set { m_device.Material = Helper.Convert(value); }
        }
        public Color Ambient
        {
            get { DX.Color color = DX.Color.FromRgba(m_device.GetRenderState(D3D.RenderState.Ambient)); return Color.FromArgb(color.A, color.R, color.G, color.B); }
            set { m_device.SetRenderState(D3D.RenderState.Ambient, (new DX.Color(value.R, value.G, value.B, value.A)).ToRgba()); }
        }
        
        public void SetLight(Light light, int index)
        {
            if (index >= MaxActiveLights)
                throw new ArgumentOutOfRangeException("index of light out of range");
           
            D3D.Light d3dlight = Helper.Convert(light);
            m_device.SetLight(index, ref d3dlight);
            m_device.EnableLight(index, true);
        }
        /// <summary>
        /// Return the light at index, TODO : get a light in Software Device Type generate a exception
        /// </summary>
        public Light GetLight(int index)
        {
            return new Light();
            /*
            if (index >= MaxActiveLights)
                throw new ArgumentOutOfRangeException("index of light out of range");
            return Helper.Convert(m_device.GetLight(index));
            */
        }
        #endregion
        
        #region Methods
        public void DrawIndexedPrimitives(PrimitiveType type, int baseVertex, int minVertexIndex, int numVertices, int startIndex, int numPrimitives)
        {
            if (numPrimitives > 0 && numVertices > 0)
                m_device.DrawIndexedPrimitive((D3D.PrimitiveType)type, baseVertex, minVertexIndex, numVertices, startIndex, numPrimitives);
        }
        public void DrawPrimitives(PrimitiveType type, int startVertex, int numPrimitives)
        {
            if (numPrimitives > 0)
                m_device.DrawPrimitives((D3D.PrimitiveType)type, startVertex, numPrimitives);
        }
        public void DrawUserPrimitives<T>(PrimitiveType type,int numPrimitives,T[] array) where T : struct
        {
            m_device.DrawUserPrimitives<T>((D3D.PrimitiveType)type, numPrimitives, array);
        }
        /// <summary>
        /// reset the device
        /// </summary>
        public void Reset()
        {
            Trace.WriteLine("Device Reset Manualy");

            if (m_backbuffer != null) m_backbuffer.Dispose();
            if (m_zbuffer != null) m_zbuffer.Dispose();
            if (m_swapchain != null) m_swapchain.Dispose();

            m_device.Indices = null;
            m_device.SetTexture(0, null);
            m_device.SetStreamSource(0, null, 0, 0);


            m_device.Reset(m_param);

            m_swapchain = m_device.GetSwapChain(0);
            m_backbuffer = m_device.GetBackBuffer(0, 0);
            m_zbuffer = useZbuffer ? m_device.DepthStencilSurface : null;
            m_device.Viewport = m_viewport;
            
            SetRendersStates();
        }
        /// <summary>
        /// </summary>
        public void Clear(Color color)
        {
            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            if (useZbuffer) flags |= D3D.ClearFlags.ZBuffer;
            if (useStencil) flags |= D3D.ClearFlags.Stencil;

            m_device.Clear(flags, DX.ColorBGRA.FromBgra(color.ToArgb()), 1.0f, 0);
        }
        /// <summary>
        /// begin scene , return a device state, if isn't DeviceState.OK can't render 
        /// </summary>
        public DeviceState Begin()
        {
            // don't understand if sharpdx use these values...
            const int DeviceLost = -2005530520;
            const int DeviceNotReset = -2005530519;

            DX.Result result = m_device.TestCooperativeLevel();

            //Okay to render
            if (result.Success) //result.Code == 0
            {
                try
                {
                    m_device.Viewport = m_viewport;
                    m_device.BeginScene();
                }
                catch
                {
                    return DeviceState.InvalidCall;
                }
                return DeviceState.OK;
            }
            switch (result.Code)
            {
                case 0:
                    return DeviceState.OK;
                case DeviceLost:
                    Debug.WriteLine("DeviceIsLost");
                    return DeviceState.DeviceLost;

                case DeviceNotReset:
                    Debug.WriteLine("DeviceIsNotReset");
                    return DeviceState.DeviceNotReset;

                default:
                    Debug.WriteLine("UnknowDeviceState");
                    return DeviceState.InvalidCall;
            }

        }
        /// <summary>
        /// end scene
        /// </summary>
        public void End()
        {
            m_device.SetStreamSource(0, null, 0, 0);
            m_device.Indices = null;

            m_device.EndScene();
        }
        /// <summary>
        /// present on screen
        /// </summary>  
        public void Present()
        {
            m_device.Present();
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetRenderTarghet()
        {
            m_device.SetRenderTarget(0, m_backbuffer);
            if (m_zbuffer!=null) m_device.DepthStencilSurface = m_zbuffer;

            SetRendersStates();
        }
        /// <summary>
        /// reseting
        /// </summary>
        public void ResizeViewport()
        {
            DX.Viewport viewport = new DX.Viewport();
            viewport.Width = m_control.ClientSize.Width;
            viewport.Height = m_control.ClientSize.Height;
            viewport.X = 0;
            viewport.Y = 0;
            viewport.MaxDepth = 1;
            viewport.MinDepth = 0;
            this.m_viewport = viewport;
        }

        #endregion

        /// <summary>
        /// dispose the device
        /// </summary>
        ~DX9Device()
        {
            this.Dispose();
        }

        /// <summary>
        /// dispose
        /// </summary>
        public void Dispose()
        {
            if (m_swapchain != null) m_swapchain.Dispose();
            if (m_device != null) m_device.Dispose();
            if (m_zbuffer != null) m_zbuffer.Dispose();
            if (m_backbuffer != null) m_backbuffer.Dispose();
        }
        /// <summary>
        /// return video card description
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            //stampa la descrizione della scheda video
            //return video card description
            string info = "\n";

            //foreach (D3D.AdapterInformation details in m_device.Direct3D.Adapters)
           /// {
           //     info += details.Details.ToString() + "\n";
           /// }
            info += "use depthstencil : " + useStencil.ToString()+"\n";
            info += "use hardware     : " + (!softwareProcessing).ToString()+"\n";
            return info;
        }

    }
}