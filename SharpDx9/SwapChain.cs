/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;

namespace Engine.Graphics
{
    public class DX9SwapChain : IDisposable
    {
        internal bool useStencil = false;
        internal bool useZbuffer = false;

        DX9Device device;
        Control m_control;

        D3D.PresentParameters m_param;
        D3D.SwapChain m_swapchain;
        D3D.Device m_device;
        D3D.Surface m_backbuffer;
        D3D.Surface m_zbuffer;

        public int BackBufferHeight
        {
            get { return m_param.BackBufferHeight; }
            set { m_param.BackBufferHeight = value > 10 ? value : 10; }
        }
        public int BackBufferWidth
        {
            get { return m_param.BackBufferWidth; }
            set { m_param.BackBufferWidth = value > 10 ? value : 10; }
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="devicedx">device</param>
        /// <param name="handle">user control</param>
        public DX9SwapChain(DX9Device device, Control control, bool useZbuffer, bool useStencil)
        {
            this.m_control = control;
            this.device = device;
            this.m_device = device.m_device;
            this.useZbuffer = useZbuffer;
            this.useStencil = useZbuffer && useStencil;

            //copy same parameters
            m_param = device.m_param;
            m_param.EnableAutoDepthStencil = useZbuffer;
            
            D3D.Format backbufferFormat = device.m_param.BackBufferFormat;

            if (useZbuffer)
            {
                D3D.Format depthstencilFormat = useStencil ? D3D.Format.D24S8 : D3D.Format.D24X8;
                Debug.Assert(m_device.Direct3D.CheckDepthStencilMatch(0, D3D.DeviceType.Hardware, backbufferFormat, backbufferFormat, depthstencilFormat), depthstencilFormat.ToString() + " not compatible");
                m_param.AutoDepthStencilFormat = depthstencilFormat;
            }


            int width = m_control.ClientSize.Width;
            int height = m_control.ClientSize.Height;

            if (width < 10) width = 10;
            if (height < 10) height = 10;

            m_param.BackBufferHeight = height;
            m_param.BackBufferWidth = width;

            Init(m_device);
        }

        void Init(D3D.Device d3dDevice)
        {
            m_param.DeviceWindowHandle = this.m_control.Handle;
            m_param.Windowed = true;

            m_swapchain = new D3D.SwapChain(d3dDevice, m_param);
            m_backbuffer = m_swapchain.GetBackBuffer(0);
            m_zbuffer = useZbuffer ? D3D.Surface.CreateDepthStencil(d3dDevice, BackBufferWidth, BackBufferHeight, m_param.AutoDepthStencilFormat, D3D.MultisampleType.None, 0, true) : null;

            //d_backbuffer = d3dDevice.GetBackBuffer(0, 0, D3D.BackBufferType.Mono);
            //d_zbuffer = d3dDevice.DepthStencilSurface;

            this.disposed = false;
        }
        /// <summary>
        /// Reinitialize the swapchain
        /// </summary>
        public void Resize(Size backbuffersize)
        {
            this.Dispose();

            int width = backbuffersize.Width;
            int height = backbuffersize.Height;

            if (width < 10) width = 10;
            if (height < 10) height = 10;

            m_param.BackBufferHeight = height;
            m_param.BackBufferWidth = width;

            Init(m_device);
        }
        /// <summary>
        /// prepare the device's backbuffer 
        /// </summary>
        public void SetRenderTarget()
        {
            m_device.SetRenderTarget(0, m_backbuffer);
            if (useZbuffer) m_device.DepthStencilSurface = m_zbuffer;

            m_device.SetRenderState(D3D.RenderState.StencilEnable, useStencil);
            m_device.SetRenderState(D3D.RenderState.ZEnable, useZbuffer);
        }
        /// <summary>
        /// Clear the current backbuffer
        /// </summary>
        /// <param name="background"></param>
        public void Clear(Color background)
        {
            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            if (useZbuffer) flags |= D3D.ClearFlags.ZBuffer;
            if (useStencil) flags |= D3D.ClearFlags.Stencil;

            m_device.Clear(flags, DX.ColorBGRA.FromBgra(background.ToArgb()), 1.0f, 0);
        }
        /// <summary>
        /// Draw result to windows handle
        /// </summary>
        public void Present()
        {
            m_swapchain.Present(D3D.Present.None);
        }

        #region Disposing
        protected bool disposed;
        ~DX9SwapChain()
        {
            Dispose(false);
        }
        /// <summary>
        /// release unmanaged resource
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (m_backbuffer != null) m_backbuffer.Dispose();
                if (m_zbuffer != null) m_zbuffer.Dispose();
                if (m_swapchain != null) m_swapchain.Dispose();
            }
            disposed = true;
        }
        #endregion
    }
}