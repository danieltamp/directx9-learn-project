﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.IO;
using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// SharpDX texture implementations
    /// </summary>
    public class DxTexture : IDisposable, IRestore
    {
        public Pool MemoryPool { get { return memoryPool; } }

        DX9Device device = null;
        byte[] m_buffer;
        string m_filename = "";
        Pool memoryPool;
        Usage usage;

        internal D3D.BaseTexture m_texture;
        

        /// <summary>
        /// Null texture
        /// </summary>
        public DxTexture()
        {
            device = null;
            m_texture = null;
            m_filename = null;
        }
        /// <summary>
        /// From original texture
        /// </summary>
        internal DxTexture(DX9Device device, D3D.BaseTexture texture)
        {
            this.device = device;
            m_texture = texture;
            m_filename = null;
            memoryPool = Pool.Managed;

        }
        /// <summary>
        /// From file name
        /// </summary>
        public DxTexture(DX9Device device, string filename)
        {
            this.device = device;
            this.m_filename = filename;
            Bitmap bmp = new Bitmap(filename);
            Initialize(device, bmp, Pool.Managed);
        }
        /// <summary>
        /// Texture from image
        /// </summary>
        public DxTexture(DX9Device device, Bitmap bitmap)
        {
            if (!device.SupportTextureSize(bitmap.Size))
                throw new Exception("Device not support this image");

            Initialize(device, bitmap, Pool.Managed);
        }

        void Initialize(DX9Device device, Bitmap bitmap, Pool pool)
        {
            ImageConverter converter = new ImageConverter();
            byte[] buffer = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
            Initialize(device, buffer, pool);
        }
        void Initialize(DX9Device device, MemoryStream stream, Pool pool)
        {
            this.device = device;
            this.memoryPool = pool;
            usage = pool == Pool.Managed ? Usage.None : Usage.WriteOnly;
            m_texture = D3D.Texture.FromStream(device.m_device, (Stream)stream, (D3D.Usage)usage, (D3D.Pool)memoryPool);
        }
        void Initialize(DX9Device device, byte[] buffer,Pool pool)
        {
            this.device = device;
            this.memoryPool = pool;
            if (pool!= Pool.Managed)
                this.m_buffer = buffer;
            usage = pool == Pool.Managed ? Usage.None : Usage.WriteOnly;

            m_texture = D3D.Texture.FromMemory(device.m_device, buffer, (D3D.Usage)usage, (D3D.Pool)memoryPool);
        }
        void Initialize(DX9Device device, string filename, Pool pool)
        {
            usage = pool == Pool.Managed ? Usage.None : Usage.WriteOnly;
            m_texture = D3D.Texture.FromFile(device.m_device, m_filename, (D3D.Usage)usage, (D3D.Pool)memoryPool);
        }

        public void Dispose()
        {
            if (m_texture != null)
                m_texture.Dispose();
        }
        public void Restore()
        {
            // not restore if it's use managed memory
            if (memoryPool == Pool.Managed) return;

            if (m_buffer != null)
            {
                Initialize(device, m_buffer, memoryPool);
            }
            else if (!String.IsNullOrEmpty(m_filename))
            {
                Initialize(device, m_filename, memoryPool);
            }
            throw new ArgumentNullException("not found a memorized image");
        }
    }
}