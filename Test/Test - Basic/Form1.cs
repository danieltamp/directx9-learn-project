﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;

using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        GraphicDevice graphicDevice;
        Font font;

        bool draw = true;
        VertexDeclaration cvertex_decl;
        VertexBuffer vertexbuffer;
        IndexBuffer facebuffer;
        CVERTEX[] vertices;
        Face[] faces;
        CameraValues camera;
        Vector3 eye, look;
        VertexBuffer axis;
        VertexBuffer frustumV;
        IndexBuffer frustumE;

        float yangle = 0;
        float l;
        Point start = new Point(0, 0);
        Point end = new Point(0, 0);

        // cyòinder
        float radius=141;
        float height = 0f;
        Vector3 center = Vector3.Zero;



        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 form = new Form1();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();

            Console.WriteLine(Marshal.SizeOf(typeof(Quaternion)));
        }

        void InitializeGraphics()
        {
            graphicDevice = new GraphicDevice(this);
            font = new Font(graphicDevice, FontName.Arial, 8);

            /////////////////////////// Geosphere
            string message;

            Tetrahedron thetra = new Tetrahedron(100.0f);
            thetra.IncreaseTesselation();
            thetra.IncreaseTesselation();

            thetra.CoerenceTest(out message);
            Console.WriteLine(message);


            List<Vector3> vertex = new List<Vector3>();
            vertex.Add(new Vector3(0, 0, 0));
            vertex.Add(new Vector3(100, 0, 0));
            vertex.Add(new Vector3(0, 100, 0));

            List<Face> tri = new List<Face>();
            tri.Add(new Face(0, 1, 2));
            tri.Add(new Face(0, 2, 1));

            HMesh mesh = new HMesh(tri, vertex, null);
            mesh.TriangleTessellate1<VERTEX>();

            mesh = (HMesh)thetra;
            //TriMesh cylinder = TriMesh.Cylinder(radius, height, 12);


            cvertex_decl = VertexDeclaration.GetDeclaration<CVERTEX>(graphicDevice);

            vertices = new CVERTEX[mesh.NumVerts];
            for (int i = 0; i < mesh.NumVerts; i++)
                vertices[i] = new CVERTEX(mesh.m_verts[i].data.position, Color.Blue);

            vertexbuffer = new VertexBuffer(graphicDevice, cvertex_decl, mesh.NumVerts, true, false);
            vertexbuffer.Open();
            vertexbuffer.Write(vertices);
            vertexbuffer.Close();
            vertexbuffer.Count = mesh.NumVerts;

            faces = new Face[mesh.NumFaces];
            for (int i = 0; i < mesh.NumFaces; i++)
                faces[i] = new Face(mesh.m_faces[i].v[0].ID, mesh.m_faces[i].v[1].ID, mesh.m_faces[i].v[2].ID);


            facebuffer = new IndexBuffer(graphicDevice, typeof(Face), mesh.NumFaces, true, false);
            facebuffer.Open();
            facebuffer.Write(faces);
            facebuffer.Close();
            facebuffer.Count = mesh.NumFaces;

            ///////////////////// axis
            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(200,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,200,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,200, Color.Blue)
            };
            axis = new VertexBuffer(graphicDevice, cvertex_decl, verts.Length, false, true);
            axis.Open();
            axis.Write(verts);
            axis.Close();
            axis.Count = verts.Length;


            camera = new CameraValues();
            eye = new Vector3(-180, 200, -180);
            l = eye.Length;
            look = new Vector3(0, 0, 0);
           
            UpdateCamera();
        }
        
        
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            graphicDevice.Resize(this.ClientSize);
            this.Invalidate();
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            start.X = e.X;
            start.Y = e.Y;
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            end.X = e.X;
            end.Y = e.Y;
            DoFrustumArea(start, end);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;

            UpdateCamera();

            graphicDevice.renderstates.world = camera.world;
            graphicDevice.renderstates.projection = camera.projection;
            graphicDevice.renderstates.view = camera.view;
            graphicDevice.renderstates.fillMode = FillMode.Solid;

            bool state = graphicDevice.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            {
                graphicDevice.SetRenderTarghet();
                graphicDevice.Clear(Color.CornflowerBlue);

                cvertex_decl.SetToDevice();
                graphicDevice.renderstates.lightEnable = false;

                ////////////////////// 3d axis //////////////////////
                axis.SetToDevice();
                graphicDevice.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);

                ////////////////////// frustum //////////////////////
                if (frustumV != null && frustumV.Count > 0)
                {
                    cvertex_decl.SetToDevice();
                    frustumV.SetToDevice();
                    frustumE.SetToDevice();
                    graphicDevice.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, frustumV.Count, 0, frustumE.Count);
                }


                ////////////////////// cylinder //////////////////////
                
                vertexbuffer.Open();
                vertexbuffer.Write(vertices);
                vertexbuffer.Close();
                vertexbuffer.Count = vertices.Length;

                facebuffer.Open();
                facebuffer.Write(faces);
                facebuffer.Close();
                facebuffer.Count = faces.Length;

                vertexbuffer.SetToDevice();
                facebuffer.SetToDevice();

                graphicDevice.renderstates.fillMode = FillMode.Solid;
                graphicDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);
                graphicDevice.renderstates.lightEnable = true;
                graphicDevice.renderstates.fillMode = FillMode.WireFrame;
                graphicDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);

                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3 s = Vector3.Project(vertices[i].position,camera.viewport,camera.projection,camera.view,camera.world);
                    font.Draw(i.ToString(), (int)s.x, (int)s.y, Color.Black);        
                }

                graphicDevice.EndDraw();
                graphicDevice.PresentDraw();
            }
        }


        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        void UpdateCamera()
        {
            Vector3 neweye = eye;
            neweye = new Vector3(Math.Sin(yangle) * l, eye.y, Math.Cos(yangle) * l);
            yangle += 0.002f;

            camera.nearZ = 20f;
            camera.farZ = 500f;
            camera.viewport = new Viewport(ClientSize.Width, ClientSize.Height, 0, 0);
            camera.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), camera.viewport, 100f, 1000f);
            camera.view = Matrix4.MakeViewLH(neweye, look, Vector3.UnitY);
            camera.inview = Matrix4.Inverse(camera.view);
            camera.world = Matrix4.Identity;
        }
        void DoFrustumArea(Point ps, Point pe)
        {
            int xmin = ps.X;
            int ymin = ps.Y;
            int xmax = pe.X;
            int ymax = pe.Y;

            if (xmin > xmax) swap(ref xmin, ref xmax);
            if (ymin > ymax) swap(ref ymin, ref ymax);

            Viewport area = new Viewport(xmax - xmin, ymax - ymin, xmin, ymin);
            Frustum main = new Frustum(camera.projection, camera.view, camera.world);
            Frustum child = main.GetDerivedArea(camera.viewport, area, camera.projection, camera.view, camera.world);

            Frustum.ePlane planes = Frustum.ePlane.ALL;
            bool test = child.GetCylindrerSide(center, Vector3.UnitY, height/2, radius, ref planes);

            Console.WriteLine("cilinder visible " + test + " intersect plane " + planes.ToString());

            RenderFrustum(child);
        }
        void RenderFrustum(Frustum frustum)
        {
            Edge[] edges;
            CVERTEX[] verts;
            frustum.Render(out verts, out edges);

            frustumV = new VertexBuffer(graphicDevice, cvertex_decl, verts.Length, false, true);
            frustumV.Open();
            frustumV.Write(verts);
            frustumV.Count = verts.Length;

            frustumE = new IndexBuffer(graphicDevice, typeof(Edge), edges.Length, false, true);
            frustumE.Open();
            frustumE.Write(edges);
            frustumE.Count = edges.Length;
        }



        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle )
            {
                System.Threading.Thread.Sleep(10);
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
