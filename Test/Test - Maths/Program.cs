﻿using System;
using System.Drawing;
using System.Collections.Generic;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;

using Vector3 = Engine.Maths.Vector3;
using Viewport = Engine.Graphics.Viewport;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;


namespace Test___Maths
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Viewport viewport = new Viewport(200, 100, 0, 0);
            Viewport area = new Viewport(100, 50, 50, 25);
            Vector3 eye = new Vector3(10, 0, 0);

            Matrix4 view = Matrix4.MakeViewLH(eye, Vector3.Zero, Vector3.UnitY);
            Matrix4 proj = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), viewport, 1f, 10f);
            Matrix4 world = Matrix4.Identity;

            Frustum frustum = new Frustum(proj, view, world);
            Frustum frustum2 = frustum.GetDerivedArea(viewport, area, proj, view, world);


            Frustum.ePlane flag = Frustum.ePlane.ALL;

            Plane[] p = new Plane[6];

            Plane[] c = new Plane[6];


            c[0] = new Plane { A = 0.789471f, B = -0.1023788f, C = -0.6051894f, D = 53.64645f };
            c[1] = new Plane { A = -0.6106144f, B = -0.09739058f, C = 0.7859167f, D = 51.03253f };
            c[2] = new Plane { A = -0.4575474f, B = -0.7624308f, C = -0.4575475f, D = -12.23095f };
            c[3] = new Plane { A = 0.5508105f, B = 0.6270688f, C = 0.5508109f, D = 72.87811f };
            c[4] = new Plane { A = 0.5560216f, B = -0.6178024f, C = 0.5560218f, D = 223.7282f };
            c[5] = new Plane { A = -0.5560218f, B = 0.617802f, C = -0.556022f, D = 176.2717f };

            Vector3 bottom = new Vector3(0, 0, 0);
            Vector3 top = new Vector3(0, 16, 0);

            for (int i = 0; i < 6; i++)
            {
                Plane.eSide b = c[i].GetCylindrerSide(bottom, top, 141.42f);
                Console.WriteLine(b);
            }

            //HMesh mesh = new HMesh(thetra.faces, thetra.vertices);
            */
            string message;


            List<Vector3> verts = new List<Vector3>();
            verts.Add(new Vector3(0, 0, 0));
            verts.Add(new Vector3(2, 0, 0));
            verts.Add(new Vector3(0, 2, 0));

            List<Face> faces = new List<Face>();
            faces.Add(new Face(0, 1, 2));
            faces.Add(new Face(0, 2, 1));

            HMesh mesh = new HMesh(faces, verts, null);
            
            mesh.TriangleTessellate1<VERTEX>();
           


            mesh.CoerenceTest(out message);
            Console.WriteLine(message);

            Console.Read();
        }
    }
}
