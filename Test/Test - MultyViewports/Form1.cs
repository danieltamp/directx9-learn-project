﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

namespace Test___MultyViewports
{
    public partial class Form1 : Form
    {
        Random Rnd = new Random();
        GraphicDevice graphicDevice;
        EngineCore renderer;
        Texture img;
        ConvexHull3D algo;
        TriMesh hull;

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            renderer.Dispose();
            graphicDevice.Dispose();
        }

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 myForm = new Form1();
            Application.Idle+=new EventHandler(myForm.Application_Idle);
            Application.Run(myForm);
        }
        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);
            this.Text = "Direct3D (DX9/C#) - Setting Multiple View Ports";
            this.BackColor = Color.Black;
            InitializeComponent();
            InitializeGraphics();
        }
        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (algo.result == ConvexHull3D.Result.OK)
            {
                while(algo.MoveNext());
                //algo.MoveNext();
                algo.Extract(out hull.vertices, out hull.faces);
                renderer.Update(hull,GeometryUpdateFlags.Force);
            }
        }
        void InitializeGraphics()
        {
            graphicDevice = new GraphicDevice(this);
            renderer = new EngineCore(graphicDevice);
            img = new Texture(graphicDevice, null);
            Light sun = new Light
            {
                Ambient = Color.White,
                Diffuse = Color.White,
                Specular = Color.Black,
                Direction = Vector3.GetNormal(new Vector3(-1, -1, -1)),
                Position = new Vector3(1000, 1000, 1000),
                Type = LightType.Directional,

            };

            Light ambient = new Light
            {
                Ambient = Color.Black,
                Diffuse = Color.FromArgb(100, 100, 100),
                Specular = Color.Black,
                Direction = Vector3.GetNormal(new Vector3(1, 1, 1)),
                Position = new Vector3(-1000, -1000, -1000),
                Type = LightType.Directional,
            };

            graphicDevice.renderstates.ambient = Color.Black;
            graphicDevice.renderstates.cull = Cull.None;
            graphicDevice.renderstates.fillMode = FillMode.Solid;

            graphicDevice.lights.Add(sun);
            graphicDevice.lights.Add(ambient);

            renderer.Add(img);


            List<Vector3> allvertices = new List<Vector3>();

            for (int i = 0; i < 10; i++)
            {
                TriMesh obj = TriMesh.Sphere(10, 10, 1);
                obj.isAnimable = false;
                obj.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;
                obj.textureKey = img.Key;
                obj.transform = Matrix4.Translating(Rnd.Next(-20, 20), Rnd.Next(-20, 20), Rnd.Next(-20, 20));

                renderer.Add(obj);

                for (int j = 0; j < obj.numVertices; j++)
                {
                    allvertices.Add(Vector3.TransformCoordinate(obj.vertices[j], obj.transform));
                }
            }

            algo = new ConvexHull3D(allvertices,false);
            hull = new TriMesh();

            if (algo.result == ConvexHull3D.Result.OK)
            {
                algo.Extract(out hull.vertices, out hull.faces);
                hull.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Diffuse;
                hull.isAnimable = false;
                renderer.Add(hull);
            }
        }


        void IncreaseObjects()
        {

            for (int i = 0; i < 10; i++)
            {
                float PI2 = (float)(Math.PI * 2);


                TriMesh obj = TriMesh.Sphere(10, 10, 1);
                //TriMesh obj = new Resources.ObjCubeOpen();

                obj.isAnimable = false;

                if (i != 0)
                {
                    obj.transform = Matrix4.RotationYawPitchRoll((float)Rnd.NextDouble() * PI2, (float)Rnd.NextDouble() * PI2, (float)Rnd.NextDouble() * PI2);
                    obj.transform *= Matrix4.Translating(Rnd.Next(-40, 40), Rnd.Next(-40, 40), Rnd.Next(-40, 40));
                    obj.transform *= Matrix4.Scaling(new Vector3(4, 5, 5));
                }
                else
                {
                    //obj.transform = Matrix4.RotationY(MathUtils.DegreeToRadian(45));
                    obj.transform = Matrix4.Scaling(new Vector3(5, 5, 5));
                }
                obj.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;
                obj.isAnimable = false;
                obj.textureKey = img.Key;

                renderer.Add(obj);

                //// Build lines of normals to see their orientation
                Line lines = new Line();
                lines.isAnimable = false;
                lines.primitiveType = PrimitiveType.LineList;
                lines.vertices = new Vector3[obj.numVertices * 2];
                lines.colors = new Color[obj.numVertices * 2];

                Matrix4 transform = obj.transform;
                Matrix3 rotation = obj.transform.ExtractRotation();

                for (int j = 0; j < obj.numVertices; j++)
                {
                    Vector3 vertex = transform * obj.vertices[j];
                    Vector3 normal = rotation * obj.normals[j];
                    lines.vertices[j * 2 + 0] = vertex;
                    lines.vertices[j * 2 + 1] = vertex + normal * 5.0f;
                }
                for (int j = 0; j < obj.numVertices; j++)
                {
                    lines.colors[j * 2 + 0] = Color.Black;
                    lines.colors[j * 2 + 1] = Color.White;
                }
                lines.vertexFormat = VertexFormat.Position | VertexFormat.Diffuse;

                renderer.Add(lines);

            }
        }


        public void Render()
        {
            for (int i = 0; i < graphicDevice.lights.Count; i++)
            {
                Light light = graphicDevice.lights[i];
                Matrix4 rot = Matrix4.Identity;
                switch (Rnd.Next(3))
                {
                    case 0: rot = Matrix4.RotationX(0.02f); break;
                    case 1: rot = Matrix4.RotationY(0.01f); break;
                    case 2: rot = Matrix4.RotationZ(0.015f); break;
                }
                light.Position = Vector3.TransformCoordinate(light.Position, rot);
                light.Direction = -light.Position.Normal;
                graphicDevice.lights[i] = light;
            }

            graphicDevice.renderstates.world = Matrix4.Identity;
            graphicDevice.renderstates.view = Matrix4.MakeViewLH(new Vector3(50, 50, 50), Vector3.Zero, Vector3.UnitY);


            //Vieport left
            Viewport viewport1 = new Viewport();
            viewport1.Height = this.ClientSize.Height;
            viewport1.Width = this.ClientSize.Width / 2-10;
            viewport1.X = 0;
            viewport1.Y = 0;
            viewport1.MinDepth = 0;
            viewport1.MaxDepth = 1;

            //Vieport right
            Viewport viewport2 = new Viewport();
            viewport2.Height = this.ClientSize.Height;
            viewport2.Width = this.ClientSize.Width / 2-10;
            viewport2.X = this.ClientSize.Width / 2 +10;
            viewport2.Y = 0;
            viewport2.MinDepth = 0;
            viewport2.MaxDepth = 1;


            graphicDevice.SetRenderTarghet();
            
            bool state = graphicDevice.BeginDraw();
            Debug.Assert(state, "Render1 Fail");
            if (state)
            {
                graphicDevice.Clear(Color.CornflowerBlue);
                graphicDevice.viewport = viewport1;
                graphicDevice.renderstates.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45),viewport1,0.1f,1000.0f);

                renderer.Draw();
                graphicDevice.EndDraw();
            }
            state = graphicDevice.BeginDraw();
            Debug.Assert(state, "Render2 Fail");
            if (state)
            {
                graphicDevice.Clear(Color.CornflowerBlue);
                graphicDevice.viewport = viewport2;
                graphicDevice.renderstates.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), viewport2, 0.1f, 1000.0f);
                
                renderer.Draw();
                graphicDevice.EndDraw();
            }
            graphicDevice.PresentDraw();
        }
        
        
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            graphicDevice.Resize(this.ClientSize);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Render();
        }

        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle)
            {
                System.Threading.Thread.Sleep(10);
                this.Invalidate();
            }
        }
        bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }

        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
