﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

using Engine.M2TW;
using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        Random Rnd = new Random();
        Viewport panelviewport;
        GraphicDevice graphicDevice; 
        EngineCore renderer;
        Font font;
        Vector3 eye = new Vector3(100, 100, 100);
        List<uint> nodes = new List<uint>();
        Texture img;
        bool draw = true;
        bool increase = true;
        bool startdemo = false;

        int increasenum = 1;
        int maxnum = 5;
        int wait = 0;
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out)); 

            Form1 myForm = new Form1();
            Application.Idle += new EventHandler(myForm.Application_Idle);
            Application.Run(myForm);
        }
        #region form
        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (renderer != null) renderer.Dispose();
            graphicDevice.Dispose();
        }
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            panelviewport = new Viewport 
            {
                Width = this.splitContainer.Panel1.ClientSize.Width,
                Height = this.splitContainer.Panel1.ClientSize.Height
            };
            if (graphicDevice!=null) graphicDevice.Resize(this.splitContainer.Panel1.ClientSize);
            this.Invalidate();
        }
        private void splitContainer_Panel1_Resize(object sender, EventArgs e)
        {
            draw = true;
            panelviewport = new Viewport
            {
                Width = this.splitContainer.Panel1.ClientSize.Width,
                Height = this.splitContainer.Panel1.ClientSize.Height
            };
            if (graphicDevice != null) graphicDevice.Resize(this.splitContainer.Panel1.ClientSize);
            this.Invalidate();
        }  
        #endregion

        void InitializeGraphics()
        {
            panelviewport = new Viewport
            { 
                Width = this.splitContainer.Panel1.ClientSize.Width,
                Height = this.splitContainer.Panel1.ClientSize.Height
            };
            graphicDevice = new GraphicDevice(this.splitContainer.Panel1);
            graphicDevice.Clear(Color.CornflowerBlue);

            renderer = new EngineCore(graphicDevice);

            img = new Texture(graphicDevice, null);

            Light sun = new Light
            {
                Ambient = Color.White,
                Diffuse = Color.White,
                Specular = Color.Black,
                Direction = Vector3.GetNormal( new Vector3(-1, -1, -1)),
                Position = new Vector3(1000,1000,1000),
                Type = LightType.Directional,
                
            };

            Light ambient = new Light
            {
                Ambient = Color.Black,
                Diffuse = Color.FromArgb(100, 100, 100),
                Specular = Color.Black,
                Direction = Vector3.GetNormal(new Vector3(1, 1, 1)),
                Position = new Vector3(-1000, -1000, -1000),
                Type = LightType.Directional,
            };

            graphicDevice.renderstates.ambient = Color.Black;
            graphicDevice.renderstates.cull = Cull.CounterClockwise;
            graphicDevice.renderstates.fillMode = FillMode.Solid;

            graphicDevice.lights.Add(sun);
            graphicDevice.lights.Add(ambient);

            renderer.Add(img);

            Node axis = Line.Axis3D();
            axis.transform *= Matrix4.Scaling(100, 100, 100);
            renderer.Add(axis);

            font = new Font(graphicDevice, FontName.Calibri, 10);
        }
        TriMesh AddCube()
        {
            TriMesh obj = TriMesh.CubeOpen();
            obj.isAnimable = false;
            obj.transform = Matrix4.Scaling(5, 5, 5) * Matrix4.Translating(Rnd.Next(0, 10), Rnd.Next(0, 10), Rnd.Next(0, 10));
            obj.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;
            obj.textureKey = img.Key;
            obj.name = "TriMesh";
            renderer.Add(obj);
            nodes.Add(obj.Key);
            return obj;
        }
        Line AddLine(TriMesh obj)
        {
            //// Build lines of normals to see their orientation
            Line lines = new Line();
            lines.isAnimable = false;
            lines.primitive = PrimitiveType.LineList;
            lines.vertices = new Vector3[obj.numVertices * 2];
            lines.colors = new Color[obj.numVertices * 2];
            lines.name = "Line";

            for (int j = 0; j < obj.numVertices; j++)
            {
                Vector3 vertex = Vector3.TransformCoordinate(obj.vertices[j], obj.transform);
                Vector3 normal = Vector3.TransformNormal(obj.normals[j], obj.transform);
                lines.vertices[j * 2 + 0] = vertex;
                lines.vertices[j * 2 + 1] = vertex + normal * 2.0f;
            }
            for (int j = 0; j < obj.numVertices; j++)
            {
                lines.colors[j * 2 + 0] = Color.Black;
                lines.colors[j * 2 + 1] = Color.White;
            }
            lines.vertexFormat = VertexFormat.Position | VertexFormat.Diffuse;
            lines.boundSphere.center = obj.transform.TranslationComponent + new Vector3(0.1f,0.1f,0.1f);
            lines.boundSphere.radius = obj.boundSphere.radius;
            
            
            renderer.Add(lines);
            nodes.Add(lines.Key);

            return lines;
        }
        void IncreaseObjects()
        {
            if (increasenum > maxnum) increasenum = maxnum - nodes.Count;
            for (int i = 0; i < increasenum; i++)
            {
                TriMesh cube = AddCube();
                Line line = AddLine(cube);
            }
        }
        void DecreaseObjects()
        {
            for (int i = 0; i < increasenum; i++)
            {
                if (nodes.Count == 0) break;
                uint key = nodes[nodes.Count - 1];
                Node obj = renderer.Nodes[key];
                renderer.Remove(obj);
                nodes.RemoveAt(nodes.Count - 1);
                Console.WriteLine("Del " + obj.name.ToString());
            }
        }
        void AnimateObjects()
        {
            foreach (uint handle in nodes)
            {
                Node mesh = renderer.Nodes[handle];
                mesh.transform *= Matrix4.Translating((float)(Rnd.NextDouble() / 10 - 0.05), (float)(Rnd.NextDouble() / 10 - 0.05), (float)(Rnd.NextDouble() / 10 - 0.05));
                //mesh.transform *= Matrix4.Scaling((float)(Rnd.NextDouble() * 0.2 + 0.9), (float)(Rnd.NextDouble() * 0.2 + 0.9), (float)(Rnd.NextDouble() * 0.2 + 0.9));
                mesh.transform *= Matrix4.RotationYawPitchRoll((float)Rnd.NextDouble() * 0.01f, (float)Rnd.NextDouble() * 0.01f, (float)Rnd.NextDouble() * 0.01f);
               // mesh.transform = Matrix4.Orthogonalize(mesh.transform);
                renderer.Update(mesh, GeometryUpdateFlags.OverwriteTransform);
            }
        }    
        void Render()
        {
            //foreach (ICache cache in renderer.staticMeshGroup.CachesList)
            //{
            //    Array vertices = cache.CacheVertices();
            //    Array indices = cache.CacheIndices();
            //}

            eye = Vector3.TransformCoordinate(eye, Matrix4.RotationYawPitchRoll(0.002f, 0.001f,0));

            for (int i = 0; i < graphicDevice.lights.Count; i++)
            {
                Light light = graphicDevice.lights[i];
                Matrix4 rot = Matrix4.Identity;
                switch (Rnd.Next(3))
                {
                    case 0: rot = Matrix4.RotationX(0.02f); break;
                    case 1: rot = Matrix4.RotationY(0.02f); break;
                    case 2: rot = Matrix4.RotationZ(0.02f); break;
                }
                light.Position =  Vector3.TransformCoordinate(light.Position, rot);
                light.Direction = -light.Position.Normal;
                graphicDevice.lights[i] = light;
            }

            Matrix4 world = Matrix4.Identity;
            Matrix4 view = Matrix4.MakeViewLH(eye, Vector3.Zero, Vector3.UnitY);
            Matrix4 proj = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), panelviewport, 0.1f, 1000.0f);

            graphicDevice.renderstates.world = world;
            graphicDevice.renderstates.view = view;
            graphicDevice.renderstates.projection = proj;

            graphicDevice.SetRenderTarghet();

            bool state = graphicDevice.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            { 
                graphicDevice.Clear(Color.CornflowerBlue);
                graphicDevice.SetRenderTarghet();
                
                renderer.Draw();

                foreach (Node node in renderer.Nodes)
                {
                    Vector3 center = Vector3.TransformCoordinate(node.boundSphere.center, node.transform);
                    Vector3 pos = Vector3.Project(center, graphicDevice.viewport, proj, view, world);
                    font.Draw(node.name, (int)pos.x, (int)pos.y, Color.Black);
                }

                graphicDevice.EndDraw();
                graphicDevice.PresentDraw();
            }

        }


        #region GAME LOOP
        int LastTickCount = 0;

        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {       
                if (renderer != null)
                {
                    if (startdemo)
                    {
                        System.Threading.Thread.Sleep(2);
                        if (increase && nodes.Count <= maxnum)
                        {
                            if (wait++ > 100)
                            {
                                wait = 0;
                                IncreaseObjects();
                                if (nodes.Count > maxnum) increase = false;
                            }
                        }
                        else if (!increase && nodes.Count > 0)
                        {
                            if (wait++ > 100)
                            {
                                wait = 100;
                                //DecreaseObjects();
                                AnimateObjects();
                                //if (nodes.Count == 0) increase = true;
                            }
                        }
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(20);   
                    }

                    // each 1s do a render
                    if (Math.Abs(Environment.TickCount - LastTickCount) > 500)
                    {
                        Render();
                        LastTickCount = Environment.TickCount;
                    }
                }
            }
        }
        public bool AppStillIdle { get { PeekMsg msg; return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0); } }
        
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion

        
        
        void UpdateProgress(float percent)
        {
            this.progressBar.Value = (int)(percent * 100f);
        }

        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startdemo = true;
            renderer.Clear();
            nodes.Clear();
            increase = true;

        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startdemo = false;
            renderer.Clear();
            nodes.Clear();

            img = new Texture(graphicDevice, null);
            renderer.Add(img);

            string filepath = @"roman_awesome_temple.world";
            WorldFormat world = new WorldFormat();

            world.PositionProgress += new FileProgressEventHandler(UpdateProgress);
            this.toolStripStatusLabel.Text = "reading";

            long result = world.OpenFile(filepath);

            if (result > 0)
            {
                Console.WriteLine(WorldFormat.debug);
                return;
            }
            else
            {
                Console.WriteLine("Succes reading");
            }
            this.progressBar.Value = 0;
            this.toolStripStatusLabel.Text = "extracting meshes";

            ushort[][] address = world.GetObjectsAddress();
            M2TWWorldNode[] objects = world.GetAllMeshNode(address);
            int numObj = address.GetUpperBound(0)+1;


            float numtot = (float)world.meshdata.nummesh;
            float n = 0;

            for (int nobj = 0; nobj < numObj; nobj++)
            {
                int numMesh = address[nobj].GetUpperBound(0) + 1;

                for (int ndam = 0; ndam < numMesh; ndam++)
                {
                    UpdateProgress(n / numtot);
                    n += 1f;

                    // find there is the first damage mesh of nobj-th elements 
                    int m = address[nobj][ndam];

                    M2TWWorldNode gameMesh = objects[m];

                    if (!gameMesh.ExtractTriMesh(world)) continue;

                    gameMesh.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;
                    gameMesh.textureKey = img.Key;
                    gameMesh.name = objects[m].worldNameFormat;
                    gameMesh.transform = Matrix4.Identity;
                    gameMesh.isAnimable = false;

                    //mesh.SetDefaultNormal();
                    for (int i = 0; i < gameMesh.numVertices; i++)
                        gameMesh.normals[i].Normalize();

                    gameMesh.boundSphere = BoundarySphere.GetBoundingSphereFast(objects[m].vertices);

                    Matrix4 newTransform = Matrix4.Translating(gameMesh.boundSphere.center);
                    gameMesh.changeTransform(newTransform);

                    renderer.Add(gameMesh);
                }
            }
            this.progressBar.Value = 0;
        }


        TriMesh currMesh;
        TriMesh prevMesh;
        private void buttonCreate_Click(object sender, EventArgs e)
        {
            currMesh = AddCube();
            if (prevMesh != null)
            {
                renderer.Update(prevMesh, GeometryUpdateFlags.OverwriteVertices);
            }
            prevMesh = currMesh;
        }
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Update");
            if (currMesh != null)
            {
                for (int i = 0; i < currMesh.numVertices; i++)
                {
                    currMesh.vertices[i] += new Vector3(
                        (float)(Rnd.NextDouble() / 10 - 0.05),
                        (float)(Rnd.NextDouble() / 10 - 0.05),
                        (float)(Rnd.NextDouble() / 10 - 0.05));

                }
                renderer.Update(currMesh, GeometryUpdateFlags.OverwriteVertices);
            }
        }
    }

}
