﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;
using Engine.Scene;

using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        Random Rnd = new Random();
        GraphicDevice graphicDevice;
        SceneManager scene;
        SceneMaterial material;

        CameraValues camera = new CameraValues();
        Pen pen;
        Vector3 eye = new Vector3(10, 5, 10);
        Vector3 look;

        bool draw = true;

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out)); 

            Form1 myForm = new Form1();
            Application.Idle += new EventHandler(myForm.Application_Idle);
            Application.Run(myForm);
        }
        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            pen = new Pen(Brushes.Gray, 2);
            pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;

        }

        #region Form override
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);     
            graphicDevice = new GraphicDevice(this);
            graphicDevice.lights.Add(Light.Sun);
            
            scene = new SceneManager(graphicDevice);

            material = SceneMaterial.Default(scene);

            UpdateCamera();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            graphicDevice.Dispose();
        }
        Size prevsize;
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            prevsize = this.ClientSize;
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            if (prevsize != this.ClientSize)
                graphicDevice.Resize(this.ClientSize);
            UpdateCamera();
            this.Invalidate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);

            if (graphicDevice != null && draw)
            {
                Render();
            }
            else
            {
                e.Graphics.Clear(Color.White);
            }

            if (mousing && endX > -1)
            {
                float x = Math.Min(startX, endX);
                float y = Math.Min(startY, endY);
                float width = Math.Abs(startX - endX);
                float height = Math.Abs(startY - endY);
                e.Graphics.DrawRectangle(pen, x, y, width, height);
            }
        }
        #endregion

        SplineShape frustumObj;
        
        void BuildFrustum(Frustum frustum)
        {
            // custom change of coordinate system
            Matrix4 translation = Matrix4.Translating(1, 0, 2);
            Matrix4 translation_inv = Matrix4.Inverse(translation);

            frustum = Frustum.TransformCoordinate(frustum, translation_inv);

            if (frustumObj == null || frustumObj.m_key == 0)
            {
                frustumObj = new SplineShape();
                frustumObj.name = "frustum";
                frustum.Geometry(out frustumObj.vertices, out frustumObj.colors, out frustumObj.edges);
                frustumObj.transform = translation;
                scene.renderer.Add(frustumObj);
            }
            else
            {
                frustum.Geometry(out frustumObj.vertices, out frustumObj.colors, out frustumObj.edges);
                frustumObj.transform = translation;
                frustumObj.engineProp.updateflags = UpdateFlags.Vertices;
                scene.renderer.Update(frustumObj);
            }

            return;
            
            for (int i = 0; i < 1; i++)
            {
                // Test the change of coordinate system, the math is correct but when building the planes
                // in the renderer the plane.origin are in local coordsystem and not math with the smaller distance
                // of plane to World origin.
                // to do this is necessary calculate the projection of origin to plane as global coordsystem
                Vector3 origin = frustum.m_plane[i].GetProject(Vector3.TransformCoordinate(Vector3.Zero, Matrix4.Inverse(translation)));

                Line line = new Line
                {
                    vertices = new Vector3[]
                    {
                        origin,
                        origin + frustum.m_plane[i].Normal * 5f
                    },
                    colors = new Color[]
                    { 
                        Color.Red,
                        Color.CornflowerBlue
                    },
                    vertexFormat = VertexFormat.Diffuse | VertexFormat.Position
                };
                line.transform = translation;
                line.name = "plane_normal_" + i.ToString();
                scene.renderer.Add(line);
            }

            return;
            scene.renderer.Add(TriMesh.Plane(frustum.m_plane[Frustum.LEFT]));
            scene.renderer.Add(TriMesh.Plane(frustum.m_plane[Frustum.RIGHT]));
            scene.renderer.Add(TriMesh.Plane(frustum.m_plane[Frustum.TOP]));
            scene.renderer.Add(TriMesh.Plane(frustum.m_plane[Frustum.BOTTOM]));
            scene.renderer.Add(TriMesh.Plane(frustum.m_plane[Frustum.NEAR]));
            scene.renderer.Add(TriMesh.Plane(frustum.m_plane[Frustum.FAR]));
        }
        
        void UpdateCamera()
        {
            camera.nearZ = 0f;
            camera.farZ = 1000f;
            camera.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), new Viewport { Width = this.ClientSize.Width, Height = this.ClientSize.Height }, 0.1f, 1000.0f);
            camera.view = Matrix4.MakeViewLH(eye, Vector3.Zero, Vector3.UnitY);
            camera.inview = Matrix4.Inverse(camera.view);
            camera.world = Matrix4.Identity;
            camera.viewport = new Viewport(ClientSize.Width, ClientSize.Height, 0, 0);

            //eye = Vector3.TransformCoordinate(eye, Matrix4.RotationYawPitchRoll(0, 0.005f, 0));
            look = Vector3.GetNormal(Vector3.Zero - eye);

            //Frustum frustum = new Frustum(camera.projection, camera.view, camera.world);
            //bool test = frustum.isPointInside(Vector3.Zero);
            //Console.WriteLine(test);
        }
        void Render()
        {
            UpdateCamera();

            // move lights
            for (int i = 0; i < graphicDevice.lights.Count; i++)
            {
                Light light = graphicDevice.lights[i];
                Matrix4 rot = Matrix4.Identity;
                switch (Rnd.Next(3))
                {
                    case 0: rot = Matrix4.RotationX(0.02f); break;
                    case 1: rot = Matrix4.RotationY(0.02f); break;
                    case 2: rot = Matrix4.RotationZ(0.02f); break;
                }
                light.Position =  Vector3.TransformCoordinate(light.Position, rot);
                light.Direction = -light.Position.Normal;
                graphicDevice.lights[i] = light;
            }

            graphicDevice.renderstates.world = camera.world;
            graphicDevice.renderstates.view = camera.view;
            graphicDevice.renderstates.projection = camera.projection;

            bool state = graphicDevice.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            { 
                graphicDevice.Clear(Color.CornflowerBlue);
                graphicDevice.SetRenderTarghet();

                scene.Draw();

                graphicDevice.EndDraw();
                graphicDevice.PresentDraw();
            }

        }
        int operation = 0;
        float x = 1;
        float y = 0;
        float z = 2;
        bool demo = false;        
        
        bool Increase()
        {
            if (scene.renderer.nTotNodeCount >= 10000) return true;
            
            int t0 = Environment.TickCount;
            for (int i = 0; i < 100; i++)
            {
                SceneMesh mesh = SceneMesh.Sphere(scene, 10, 10, 0.5f);
                x = (float)Rnd.NextDouble() - 0.5f;
                y = (float)Rnd.NextDouble() - 0.5f;
                z = (float)Rnd.NextDouble() - 0.5f;
                mesh.TransformMatrix = Matrix4.Translating(x * 8, y * 8, z * 8);
            }

            int t1 = Environment.TickCount;

            //Console.WriteLine("add 100 node in " + (t1 - t0));

            return false;
        }
        bool Decrease()
        {
            List<SceneNode> list = new List<SceneNode>(scene.SceneNodes);
            int count = list.Count;
            if (count == 0)
            {
                return true;
            }
            
            for (int i = 0; i < 10; i++)
            {
                if (count == 0) return false;

                SceneNode node = list[count - 1];
                // ensure to remove it from renderer NOW
                node.Remove();
                count--;
            }
            return false;
        }
        // add 100 remove 10
        bool Mixing()
        {
            if (scene.renderer.nTotNodeCount >= 10000) return true;
            for (int i = 0; i < 100; i++)
            {
                SceneMesh mesh = SceneMesh.Sphere(scene, 10, 10, 0.5f);
                x = (float)Rnd.NextDouble() - 0.5f;
                y = (float)Rnd.NextDouble() - 0.5f;
                z = (float)Rnd.NextDouble() - 0.5f;
                mesh.TransformMatrix = Matrix4.Translating(x * 8, y * 8, z * 8);
                //mesh.material = material;
            }

            List<SceneNode> list = new List<SceneNode>(scene.SceneNodes);
            int count = list.Count;
            for (int i = 0; i < 10; i++)
            {
                SceneNode node = list[count - 1];
                // ensure to remove it from renderer NOW
                node.Remove();
                count--;
            }
            return false;
        }

        #region GAME LOOP
        int LastTickCount = 0;
        int demostart = 0;
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                // calculate the time what application need to wait to ensure minimum 100ms foreach frames
                // inactivitytime = 0 mean that renderer engine need exactly 100ms foreach frames
                // inactivitytime > 90 mean that renderer engine is very faster
                int inactivitytime = 100 - (Environment.TickCount - LastTickCount);
                LastTickCount = Environment.TickCount;

                if (inactivitytime > 0)
                {
                    //System.Threading.Thread.Sleep(inactivitytime);
                    //this.performance.Value = (int)(inactivitytime * 100f / 100f);
                }

                System.Threading.Thread.Sleep(100);
                
                if (demo)
                {
                    switch (operation)
                    {
                        //case 0: if (Increase()) operation++; break;
                        //case 1: if (Decrease()) operation++; break;
                        case 0: if (Mixing()) operation++; break;
                        default: demo = false; GC.Collect(); break;
                    }
                    if (!demo)
                    {
                        Console.WriteLine("Demo complete in " + (Environment.TickCount - demostart));
                    }
                }

                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        } 
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion

        #region Events
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            //SceneMesh cube = SceneMesh.Box(scene, 0.5f, 0.5f, 0.5f);
            SceneMesh obj = SceneMesh.Sphere(scene, 6, 6, 1f);
            obj.TransformMatrix = Matrix4.Translating(x, y, z);
            
            x = (float)Rnd.NextDouble() - 0.5f;
            //y = (float)Rnd.NextDouble()- 0.5f;
            z = (float)Rnd.NextDouble() - 0.5f;

            x *= 8;
            z *= 8;
        }
        private void buttonRestart_Click(object sender, EventArgs e)
        {
            scene.graphicDevice = null;
            scene.graphicDevice = graphicDevice;
            x = 0;
        }
        private void buttonDemo_Click(object sender, EventArgs e)
        {
            demo = !demo;
            if (demo)
            {
                operation = 0;
                demostart = Environment.TickCount;
            }
        }

        void OnSelecting()
        {
            // ray picking
            if (startX == endX && startY == endY)
            {
                Vector3 p0 = Vector3.Unproject(startX, startY, 0, camera.viewport, camera.projection, camera.view, camera.world);
                Vector3 p1 = Vector3.Unproject(startX, startY, 1, camera.viewport, camera.projection, camera.view, camera.world);
                Ray ray0 = new Ray(p0, p1 - p0);
                
                scene.UnselectAll();
                
                SceneNode node = scene.PickSceneNode(ray0);

                if (node != null)
                {
                    node.Selected = true;
                    Console.WriteLine("Pick -> " + node.ToString());
                }
                else
                {
                    Console.WriteLine("Pick -> Nothing ");
                }
            }
            // area picking
            else
            {
                // main camera frustum
                Frustum cam = new Frustum(camera.projection, camera.view, camera.world);
                
                // get child frustum from screen rectangle
                int minx = Math.Min(startX, endX);
                int miny = Math.Min(startY, endY);
                int maxx = Math.Max(startX, endX);
                int maxy = Math.Max(startY, endY);
                Viewport rect = new Viewport(maxx - minx, maxy - miny, minx, miny);
                Frustum area = cam.GetDerivedArea(camera.viewport, rect, camera.projection, camera.view, camera.world);

                //BuildFrustum(area);

                scene.UnselectAll();     
                List<SceneNode> selection = scene.RectangleSelectionSceneNode(area);

                Console.Write("Select : " + selection.Count + " nodes");
                foreach(SceneNode node in selection)
                {
                    node.Selected = true;
                }
                Console.WriteLine("");
            }
        }
        
        int startX = -1;
        int startY = -1;
        int endX = -1;
        int endY = -1;
        bool mousing = false;
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            mousing = true;
            startX = e.X;
            startY = e.Y;
            endX = -1;
            endY = -1;
        }
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            endX = e.X;
            endY = e.Y;
            OnSelecting();
            mousing = false;
        }
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mousing)
            {
                endX = e.X;
                endY = e.Y;
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            scene.Stamp("SceneDebug.txt");
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            scene.Destroy();
        }
    }

}

