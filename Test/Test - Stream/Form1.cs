﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Renderer;
using Engine.Tools;
using Engine.Maths;
using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    struct Color32bit
    {
        byte A, R, B, G;

        public Color32bit(int argb)
        {
            Color color = Color.FromArgb(argb);
            A = color.A;
            R = color.R;
            B = color.B;
            G = color.G;
        }
        public override string ToString()
        {
            return String.Format("A {0,3} R {1,3} G {2,3} B {3,3}", A, R, G, B);
        }
    }


    public partial class Form1 : Form
    {
        Random rnd = new Random();
        GraphicDevice graphicDevice;
        Font font;
        
        DxVertexBuffer vertexbuffer = null;
        DxIndexBuffer indexBuffer = null;
        int wait = 0;
        int LastTickCount = 1; // ms
        int Frames = 0;
        float LastFrameRate = 0;
        bool draw = true;
        List<TriMesh> meshes = new List<TriMesh>();

        #region form
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();
            Console.WriteLine(Marshal.SizeOf(typeof(Quaternion)));
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            graphicDevice.Dispose();
            base.OnClosing(e);
        }
        #endregion



        void RESTORE()
        {
            vertexbuffer.Initialize(graphicDevice.capabilities.MaxVertexIndex);
            indexBuffer.Initialize(graphicDevice.capabilities.MaxVertexIndex * 6);

            vertexbuffer.Count = 0;
            indexBuffer.Count = 0;

            int n = 0;
            foreach (TriMesh mesh in meshes)
            {
                APPEND(mesh, n++);
            }
        }

        void APPEND(TriMesh obj, int n)
        {

            obj.isAnimable = false;
            obj.vertexFormat = VertexFormat.Diffuse | VertexFormat.Position;


            Matrix4 transform = Matrix4.Translating(n * 2.1f, n / 2 * 2.1f, n / 4 * 2.1f);


            int nverts = obj.numVertices;
            int nindis = obj.numIndices;
            int voffset = vertexbuffer.Count;
            int ioffset = indexBuffer.Count;

            CVERTEX[] vertices = new CVERTEX[nverts];
            ushort[] indices = new ushort[nindis];

            for (int i = 0; i < nverts; i++)
            {
                vertices[i] = new CVERTEX();
                vertices[i].position = Vector3.TransformCoordinate(obj.vertices[i], transform);
                vertices[i].color = obj.colors[i].ToArgb();
            }

            for (int i = 0; i < obj.numTriangles; i++)
            {
                indices[i * 3 + 0] = (ushort)(obj.faces[i].I + voffset);
                indices[i * 3 + 1] = (ushort)(obj.faces[i].J + voffset);
                indices[i * 3 + 2] = (ushort)(obj.faces[i].K + voffset); 
            }

            BufferStream gstream = vertexbuffer.Open(voffset, nverts);
            BufferStream istream = indexBuffer.Open(ioffset, nindis);

            gstream.Write(vertices, 0);
            istream.Write(indices, 0);

            vertexbuffer.Close();
            indexBuffer.Close();

            vertexbuffer.Count += nverts;
            indexBuffer.Count += nindis;
        }


        void InitializeGraphics()
        {
            graphicDevice = new GraphicDevice(this);

            font = new Font(graphicDevice, FontName.Arial, 8);
            vertexbuffer = graphicDevice.CreateVertexBuffer(CVERTEX.Declaration, graphicDevice.capabilities.MaxVertexIndex, BufferUsage.StaticWriteOnly, false);
            indexBuffer = graphicDevice.CreateIndexBuffer(false, graphicDevice.capabilities.MaxVertexIndex * 6, BufferUsage.StaticWriteOnly, false);
            RESTORE();
        }
        
        
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            graphicDevice.Resize(this.ClientSize);

            RESTORE();
            this.Invalidate();
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;
            if (wait++ > 10 && meshes.Count < 3)
            {
                Console.WriteLine("Add");
                wait = 0;
                TriMesh obj = TriMesh.CubeOpen();
                APPEND(obj,meshes.Count);
                meshes.Add(obj);
            }


            Frames++;
            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                LastFrameRate = (float)Frames * 1000 / Math.Abs(Environment.TickCount - LastTickCount);
                LastTickCount = Environment.TickCount;
                Frames = 0;
            }

            font.Draw("FPS : " + LastFrameRate.ToString(), 10, 10, Color.Black);

            graphicDevice.renderstates.world = Matrix4.Identity;
            graphicDevice.renderstates.view = Matrix4.MakeViewLH(new Vector3(20, 20, 20), new Vector3(5,0,0), Vector3.UnitY);
            graphicDevice.renderstates.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), graphicDevice.viewport, 0.1f, 1000.0f);
            graphicDevice.renderstates.cull = Cull.None;
            graphicDevice.renderstates.fillMode = FillMode.Solid;
   

            bool state = graphicDevice.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            {
                graphicDevice.SetRenderTarghet();
                graphicDevice.Clear(Color.CornflowerBlue);

                if (vertexbuffer.Count > 0)
                {
                    graphicDevice.SetBuffer(indexBuffer);
                    graphicDevice.SetBuffer(vertexbuffer);
                    graphicDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, indexBuffer.Count / 3);
                }
                graphicDevice.EndDraw();
                graphicDevice.PresentDraw();
            }

            this.Invalidate();

            System.Threading.Thread.Sleep(100);
        }
    }
}
