﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        static Random Rnd = new Random();  
        GraphicDevice graphicDevice;

        List<Node> nodes = new List<Node>();
        EngineCore renderer;
        NodeMaterial img;
        bool draw = true;
        bool increase = true;
        int increasenum = 10;
        int maxnum = 100;
        int wait = 0;

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 myForm = new Form1();
            Application.Idle += new EventHandler(myForm.Application_Idle);
            Application.Run(myForm);
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            InitializeComponent();
            InitializeGraphics();
            InitializeSwapChains();
        }

        void InitializeSwapChains()
        {
            viewport1.InitializeSwapChain(graphicDevice);
            viewport1.camera.eye = new Vector3(100, 100, 100);
            viewport1.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);

            viewport2.InitializeSwapChain(graphicDevice);
            viewport2.camera.eye = new Vector3(-100, 100, 100);
            viewport2.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);
            
            viewport3.InitializeSwapChain(graphicDevice);
            viewport3.camera.eye = new Vector3(50, 50, -50);
            viewport3.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);  
         
            viewport4.InitializeSwapChain(graphicDevice);
            viewport4.camera.eye = new Vector3(-50, 50, -50);
            viewport4.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            graphicDevice.Dispose();
            base.OnClosing(e);
        }


        void InitializeGraphics()
        {
            graphicDevice = new GraphicDevice(this);
            //graphicDevice = viewport1.InitializeDevice();

            renderer = new EngineCore(graphicDevice);
            img = NodeMaterial.TextureFromFile(graphicDevice, string.Empty);

            
            Light sun = new Light
            {
                Ambient = Color.White,
                Diffuse = Color.White,
                Specular = Color.Black,
                Direction = Vector3.GetNormal( new Vector3(-1, -1, -1)),
                Position = new Vector3(1000,1000,1000),
                Type = LightType.Directional,
                
            };

            Light ambient = new Light
            {
                Ambient = Color.Black,
                Diffuse = Color.FromArgb(100, 100, 100),
                Specular = Color.Black,
                Direction = Vector3.GetNormal(new Vector3(1, 1, 1)),
                Position = new Vector3(-1000, -1000, -1000),
                Type = LightType.Directional,
            };

            graphicDevice.renderstates.ambient = Color.Black;
            graphicDevice.renderstates.cull = Cull.None;
            graphicDevice.renderstates.fillMode = FillMode.Solid;

            graphicDevice.lights.Add(sun);
            graphicDevice.lights.Add(ambient);

            renderer.Add(img);
        }

        void IncreaseObjects()
        {
            if (increasenum > maxnum) increasenum = maxnum - nodes.Count;

            for (int i = 0; i < increasenum; i++)
            {
                float PI2 = (float)(Math.PI * 2);

                TriMesh obj = (i % 2 > 0) ? TriMesh.Sphere(10, 10, 1) : TriMesh.CubeOpen();

                obj.engineProp.isAnimable = false;

                if (i != 0)
                {
                    obj.transform = Matrix4.RotationYawPitchRoll((float)Rnd.NextDouble() * PI2, (float)Rnd.NextDouble() * PI2, (float)Rnd.NextDouble() * PI2);
                    obj.transform *= Matrix4.Translating(Rnd.Next(-40, 40), Rnd.Next(-40, 40), Rnd.Next(-40, 40));
                    obj.transform *= Matrix4.Scaling(4, 5, 5);
                }
                else
                {
                    //obj.transform = Matrix4.RotationY(MathUtils.DegreeToRadian(45));
                    obj.transform = Matrix4.Scaling(5, 5, 5);
                }
                obj.vertexFormat = VertexFormat.Position | VertexFormat.Normal | VertexFormat.Texture1;
                obj.engineProp.isAnimable = false;
                obj.m_material = img;

                renderer.Add(obj);
                nodes.Add(obj);

                //// Build lines of normals to see their orientation
                Line lines = new Line();
                lines.primitive = PrimitiveType.LineList;
                lines.engineProp.isAnimable = false;
                lines.vertices = new Vector3[obj.numVertices * 2];
                lines.colors = new Color[obj.numVertices * 2];

                Matrix4 transform = obj.transform;
                Matrix3 rotation = obj.transform.ExtractRotation();

                for (int j = 0; j < obj.numVertices; j++)
                {
                    Vector3 vertex = transform * obj.vertices[j];
                    Vector3 normal = rotation * obj.normals[j];
                    lines.vertices[j * 2 + 0] = vertex;
                    lines.vertices[j * 2 + 1] = vertex + normal * 5.0f;
                }
                for (int j = 0; j < obj.numVertices; j++)
                {
                    lines.colors[j * 2 + 0] = Color.Black;
                    lines.colors[j * 2 + 1] = Color.White;
                }
                lines.vertexFormat = VertexFormat.Position | VertexFormat.Diffuse;
                nodes.Add(lines);
                renderer.Add(lines);
            }
        }
        void DecreaseObjects()
        {
            for (int i = 0; i < increasenum; i++)
            {
                if (nodes.Count == 0) break;

                Node node = nodes[nodes.Count - 1];
                nodes.RemoveAt(nodes.Count - 1);
                renderer.Remove(node);
            }
        }
        
        
        void Render(GraphicPanel panel, ViewportOption options , CameraValues matrices)
        {
            if (!draw) return;

            for (int i = 0; i < graphicDevice.lights.Count; i++)
            {
                Light light = graphicDevice.lights[i];
                Matrix4 rot = Matrix4.Identity;
                switch (Rnd.Next(3))
                {
                    case 0: rot = Matrix4.RotationX(0.02f); break;
                    case 1: rot = Matrix4.RotationY(0.01f); break;
                    case 2: rot = Matrix4.RotationZ(0.03f); break;
                }
                light.Position = Vector3.TransformCoordinate(light.Position, rot);
                light.Direction = -light.Position.Normal;
                graphicDevice.lights[i] = light;
            }

            graphicDevice.renderstates.world = matrices.world;
            graphicDevice.renderstates.projection = matrices.projection;
            graphicDevice.renderstates.view = matrices.view;
            renderer.Draw();
        }

        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {            
                System.Threading.Thread.Sleep(10);

                if (increase && nodes.Count < maxnum)
                {
                    wait = 0;
                    IncreaseObjects();
                    if (nodes.Count >= maxnum) increase = false;
                }
                else if (!increase && nodes.Count > 0)
                {
                    if (wait++ > 1000)
                    {
                        //DecreaseObjects();
                        //if (nodes.Count == 0) increase = true;
                    }
                }
                this.viewport1.Invalidate();
                this.viewport2.Invalidate();
                this.viewport3.Invalidate();
                this.viewport4.Invalidate();

            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
        }
        
    }

}
